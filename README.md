# sis8300llrf

Low Level RF Module for struck sis8300.

This EPICS modules implement the core functionalities of Low Level RF for one struck board sis8300.
This module is developed using the nds 2 as base to organize the different classes.

## Repository organization

The repository organization is quite simple:
* db: database files
* doc: documentation
* iocsh: snippets
* src: C++ and C code
* test: tests written in python (pytest)

## Dependencies modules 

* asyn
* loki
* nds
* scaling
* sis8300drv
* sis8300
* sis8300llrfdrv

## Other relevant repositories

* https://gitlab.esss.lu.se/opis/dev-rf/gui-llrf : OPIs
* https://gitlab.esss.lu.se/icshwi/llrf-deployment-tools : Tools for deployment support
* https://gitlab.esss.lu.se/icshwi/llrf-maintenance-scripts : Tools for maintenance

## General overview of LLRF organization

The LLRF organization is mainly based on NDS module concepts so it is being 
implemented using 3 main concepts: Channnel, Channel Group and Device. All 
the LLRF entities are grouped in these 3 concepts. The analog channel inputs 
for example are mapped as a Channel and there is a channel group for all
the 10 analog channels that include functionalities for the group of channels

On the src folder all the files cpp and h files started with sis8300llrf 
implements a Channel, Channel Group or Device for LLRF. The functionalities
implemented by the c++ code are mapped into the epics records on files
started with sis8300llrf present on db folder.

A large group of LLRF settings are write/read from the firmware in a common
way so to facilitate the implementation of these features we have the class
sis8300llrfChannel that provides the methods to implement this common 
approach.

## Main features

LLRF implements a big set of features so we try to describe the main ones here:

### Acquisition of Analog Signals

One of the main features is to acquire analog signals. These feature is 
implemented mainly by the classes sis8300llrfAIChannel and 
sis8300llrfDownSampledChannel. The sis8300llrfAIChannel  
implements the acquisition of the Raw Channels (without any kind of 
downsampling), and it is based on the sis8300AIChannel from sis8300 module.
The sis8300llrfDownSampledChannel implements the acquisition of the
same channel but down sampled. This class is implemented as a sub-class
of sis8300llrfAuxChannel.
The 2 sets of signals are acquired by firmware and the module reads them
from different places on the memory.

The signals read from these analog inputs are arrays that could have
big length this is the reason to have the possibility to acquire them 
in a downsampled.

It is important to mention that the Raw channels arrive as one array and
the down sampled channel comes as 2 array (one for each component). The 
components can be selected by the user (for example I/Q or Magnitude and
Angle).

### PI Controller

Another main functionality from LLRF is the PI Controller. The PI controller
can be handled by Set Point and/or Feed Forward values. The classes responsible
for the PI Controller are the following:

* sis8300llrfPIChannel
* sis8300llrfPIMagAngChannel
* sis8300llrfControlTableChannel
* sis8300llrfControlTableChannelGroup
* sis8300llrfControllerChannelGroup

The values for the Set Point and Feed Forward can be set with a constant value
or a table (with 2 components). 
The main functionality of PI Controller is implemented on LLRF firmware, the 
IOC is mainly responsibly for configure it.

### Calibration

The analog input values read from the firmware and the values set for
the PI Controller are available in ADC counts, so the IOC needs to provide
a calibration so the value can be read and set on desired format (KW for 
example).
Currently this feature is implemented mainly on the following classes:

* sis8300llrfAIChannel
* sis8300llrfPIMagAngChannel

This feature is implemented with the support of the scaling module.

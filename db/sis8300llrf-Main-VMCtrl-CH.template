#### Records for Vector Modulator (output) specific setup ####

# VM output magnitude limiter status and value and enable setup and readback

record(bi, "$(P)$(R)OutMagLimStat") {
    field(DESC, "Is magnitude limiter active")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(ASYN_PORT).ctrl, $(ASYN_ADDR)) MagnitudeLimitStatus")
    field(ZNAM, "None")
    field(ONAM, "Active")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(SCAN, "I/O Intr")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "Is magnitude limiter active")
    info(SYSTEM,	"LLRF")
}

record(bo, "$(P)$(R)OutMagLimEn") {
    field(DESC, "Enable magnitude limit for VM output")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) MagnitudeLimitEnable")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")

    info(DESCRIPTION, "Enable magnitude limit for VM output")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)OutMagLim-UpLim") {
    field(DESC, "Upper limit for magnitude limit")
    field(OUT, "$(P)$(R)OutMagLim.DRVH PP")
}

record(ao, "$(P)$(R)OutMagLim") {
    field(DESC, "Set magnitude limit value")
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) MagnitudeLimitVal")
    field(DRVL, "$(MAGLIM_DRVL)")
    field(PREC, "12")

    info(DESCRIPTION, "Set magnitude limit value")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(bi, "$(P)$(R)OutMagLimEn-RB") {
    field(DESC, "Mag lim enable RBV from hw")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) MagnitudeLimitEnable")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(SCAN, "I/O Intr")

    info(DESCRIPTION, "Mag lim enable RBV from hw")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

record(ai, "$(P)$(R)OutMagLim-RB") {
    field(DESC, "Magnitude limit RBV from hw")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) MagnitudeLimitVal")
    field(SCAN, "I/O Intr")
    field(PREC, "12")

    info(DESCRIPTION, "Magnitude limit RBV from hw")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

# invert I part of output enable and readback from hw

record(bo, "$(P)$(R)OutInvIEn") {
    field(DESC, "Enable  invert I")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) InvertOutputI")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(VAL,  "$(INVERT_I)")
    field(PINI, "YES")

    info(DESCRIPTION, "Enable  invert I")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
    info(SAVRES_THIS, "")
}

record(bi, "$(P)$(R)OutInvIEn-RB") {
    field(DESC, "Enable invert I RBV from hw")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) InvertOutputI")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(SCAN, "I/O Intr")

    info(DESCRIPTION, "Enable invert I RBV from hw")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

# invert Q part of output eanble and readback from hw

record(bo, "$(P)$(R)OutInvQEn") {
    field(DESC, "Enable  invert Q")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) InvertOutputQ")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(VAL,  "$(INVERT_Q)")
    field(PINI, "YES")

    info(DESCRIPTION, "Enable  invert Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
    info(SAVRES_THIS, "")
}

record(bi, "$(P)$(R)OutInvQEn-RB") {
    field(DESC, "Enable invert Q RBV from hw")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) InvertOutputQ")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(SCAN, "I/O Intr")

    info(DESCRIPTION, "Enable invert Q RBV from hw")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

# SWAP IQ part of output enable and readback from hw

record(bo, "$(P)$(R)OutSwapIQEn") {
    field(DESC, "Enable swap IQ")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) SwapIQEn")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(VAL,  "$(INVERT_Q)")
    field(PINI, "YES")

    info(DESCRIPTION, "Enable swap IQ")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(bi, "$(P)$(R)OutSwapIQEn-RB") {
    field(DESC, "Enable swap IW Q RBV from hw")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) SwapIQEn")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(SCAN, "I/O Intr")

    info(DESCRIPTION, "Enable swap IW Q RBV from hw")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

###### Out phase offset

record(bo, "$(P)$(R)OutAngOffsetEn") {
    field(DESC, "Enable angle offset")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) AngOffsetEnable")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(VAL,  "0")

    info(DESCRIPTION, "Enable angle offset")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(bi, "$(P)$(R)OutAngOffsetEn-RB") {
    field(DESC, "Enable linearization LUT RB")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) AngOffsetEnable")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(SCAN, "I/O Intr")

    info(DESCRIPTION, "Enable angle offset RB")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)OutAngOffset") {
    field(DESC, "Set output block angle offset")
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) AngleOffset")
    field(DRVH, "3.141592653589")
    field(DRVL, "-3.141592653589")
    field(VAL,  "0.0")
    field(PREC, "12")

    info(DESCRIPTION, "Set output block angle offset")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ai, "$(P)$(R)OutAngOffset-RB") {
    field(DESC, "Output block angle offset RBV")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) AngleOffset")
    field(SCAN, "I/O Intr")
    field(PREC, "12")

    info(DESCRIPTION, "Set output block angle offset RB")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

########
# Pre-distort input to VM settings and readback from hw

record(bo, "$(P)$(R)VMPredistEn") {
    field(DESC, "VM out predistort enbl")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) PreDistEn")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(VAL,  "$(PREDISTORT_VM_OUT_EN)")
    field(PINI, "YES")

    info(DESCRIPTION, "VM out predistort enbl")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(bi, "$(P)$(R)VMPredistEn-RB") {
    field(DESC, "VM out predistort enbl RBV")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) PreDistEn")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(SCAN, "I/O Intr")

    info(DESCRIPTION, "VM out predistort enbl RBV")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)VMPredistRC00") {
    field(DESC, "VM preditort mtrx 00")
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) PreDistRC00")
    field(DRVH, "$(PREDISTRC_DRVH)")
    field(DRVL, "$(PREDISTRC_DRVL)")
    field(PREC, "$(PREDISTRC_PREC=12)")

    info(DESCRIPTION, "VM preditort mtrx 00")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ai, "$(P)$(R)VMPredistRC00-RB") {
    field(DESC, "VM preditort mtrx 00 RBV")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) PreDistRC00")
    field(SCAN, "I/O Intr")
    field(PREC, "$(PREDISTRC_PREC=12)")

    info(DESCRIPTION, "VM preditort mtrx 00 RBV")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)VMPredistRC01") {
    field(DESC, "VM preditort mtrx 01")
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) PreDistRC01")
    field(DRVH, "$(PREDISTRC_DRVH)")
    field(DRVL, "$(PREDISTRC_DRVL)")
    field(PREC, "$(PREDISTRC_PREC=12)")

    info(DESCRIPTION, "VM preditort mtrx 01")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ai, "$(P)$(R)VMPredistRC01-RB") {
    field(DESC, "VM preditort mtrx 01 RBV")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) PreDistRC01")
    field(SCAN, "I/O Intr")
    field(PREC, "$(PREDISTRC_PREC=12)")

    info(DESCRIPTION, "VM preditort mtrx 01 RBV")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)VMPredistRC10") {
    field(DESC, "VM preditort mtrx 10")
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) PreDistRC10")
    field(DRVH, "$(PREDISTRC_DRVH)")
    field(DRVL, "$(PREDISTRC_DRVL)")
    field(PREC, "$(PREDISTRC_PREC=12)")

    info(DESCRIPTION, "VM preditort mtrx 10")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ai, "$(P)$(R)VMPredistRC10-RB") {
    field(DESC, "VM preditort mtrx 10 RBV")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) PreDistRC10")
    field(SCAN, "I/O Intr")
    field(PREC, "$(PREDISTRC_PREC=12)")

    info(DESCRIPTION, "VM preditort mtrx 10 RBV")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)VMPredistRC11") {
    field(DESC, "VM preditort mtrx 11")
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) PreDistRC11")
    field(DRVH, "$(PREDISTRC_DRVH)")
    field(DRVL, "$(PREDISTRC_DRVL)")
    field(PREC, "$(PREDISTRC_PREC=12)")

    info(DESCRIPTION, "VM preditort mtrx 11")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ai, "$(P)$(R)VMPredistRC11-RB") {
    field(DESC, "VM preditort mtrx 11 RBV")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) PreDistRC11")
    field(SCAN, "I/O Intr")
    field(PREC, "$(PREDISTRC_PREC=12)")

    info(DESCRIPTION, "VM preditort mtrx 11 RBV")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)VMPredistDCOff-I") {
    field(DESC, "VM preditort DC ofst I")
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) PreDistDCOI")
    field(DRVH, "$(PREDISTDC_DRVH)")
    field(DRVL, "$(PREDISTDC_DRVL)")
    field(PREC, "$(PREDISTDC_PREC=12)")

    info(DESCRIPTION, "VM preditort DC ofst I")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ai, "$(P)$(R)VMPredistDCOff-I-RB") {
    field(DESC, "VM preditort DC ofst I RBV")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) PreDistDCOI")
    field(SCAN, "I/O Intr")
    field(PREC, "$(PREDISTDC_PREC=12)")

    info(DESCRIPTION, "VM preditort DC ofst I RBV")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)VMPredistDCOff-Q") {
    field(DESC, "VM preditort DC ofst Q")
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) PreDistDCOQ")
    field(DRVH, "$(PREDISTDC_DRVH)")
    field(DRVL, "$(PREDISTDC_DRVL)")
    field(PREC, "$(PREDISTDC_PREC=12)")

    info(DESCRIPTION, "VM preditort DC ofst Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ai, "$(P)$(R)VMPredistDCOff-Q-RB") {
    field(DESC, "VM preditort DC ofst Q RBV")
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) PreDistDCOQ")
    field(SCAN, "I/O Intr")
    field(PREC, "$(PREDISTDC_PREC=12)")

    info(DESCRIPTION, "VM preditort DC ofst Q RBV")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

#### Records to manage the 2 possible calibration tables for Set Point ####
# And SqrtFit

# Different orders for the 2 possible tables, and the 2 ways (EGU to Raw and
# Raw 2 EGU)

record(longout, "$(P)$(R)SPCalOrdE2RCav") {
    field(DESC, "Calibration order for SP w/ cav E2R")
    field(VAL,  "2")
    field(DRVH, "$(MAX_COEF=20)")
    field(DRVL, "1")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")

    info(DESCRIPTION, "Calibration order EGU to Raw when in closed cloop on cavity")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(longout, "$(P)$(R)SPCalOrdE2RKly") {
    field(DESC, "Calibration order for SP w/ Kly E2R")
    field(VAL,  "2")
    field(DRVH, "$(MAX_COEF=20)")
    field(DRVL, "1")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")

    info(DESCRIPTION, "Calibration order EGU to Raw when in closed cloop on Klystron")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(seq, "$(P)$(R)#SPCalSetOrd") {
    field(DESC, "Set SP cal. ord.")
    field(SELM, "Mask")
    field(SHFT, "0")
    # Set coefficient from EGU to Raw (using cav values)
    field(DOL0, "$(P)$(R)SPCalOrdE2RCav")
    field(LNK0, "$(P)$(R)RFCtrlSP-CalOrdE2R PP")
    # Set coefficient from Raw to EGU (using cav values)
    field(DOL1, "$(P)$(R)AI$(CAVITYCH)-CalOrd")
    field(LNK1, "$(P)$(R)RFCtrlSP-CalOrdR2E PP")
    # Set coefficient from EGU to Raw (using kly values)
    field(DOL2, "$(P)$(R)SPCalOrdE2RKly")
    field(LNK2, "$(P)$(R)RFCtrlSP-CalOrdE2R PP")
    # Set coefficient from Raw to EGU (using kly values)
    field(DOL3, "$(P)$(R)AI$(PWRFWDCH)-CalOrd")
    field(LNK3, "$(P)$(R)RFCtrlSP-CalOrdR2E PP")

    info(DESCRIPTION, "Set SP calibration order")
}

record(calcout, "$(P)$(R)#SPCalChkOrd"){
    field(INPA, "$(P)$(R)CloseLoopCav CP")
    field(INPB, "$(P)$(R)AI$(PWRFWDCH)-CalOrd CP")  # everytime it changes, re-process
    field(INPC, "$(P)$(R)AI$(CAVITYCH)-CalOrd CP")  # everytime it changes, re-process
    field(CALC, "A=1?3:12")
    field(OUT,  "$(P)$(R)#SPCalSetOrd.SELN PP")
}

# Store the order for EGU to Raw

record(seq, "$(P)$(R)#SPCalSaveOrd") {
    field(DESC, "Save SP cal. order")
    field(SELM, "Specified")
    field(SHFT, "0")
    field(DOL0, "$(P)$(R)RFCtrlSP-CalOrdE2R")
    field(LNK0, "$(P)$(R)SPCalOrdE2RCav PP")
    field(DOL1, "$(P)$(R)RFCtrlSP-CalOrdE2R")
    field(LNK1, "$(P)$(R)SPCalOrdE2RKly PP")

    info(DESCRIPTION, "Save SP calibration order for EGU to Raw  ")
}

record(calcout, "$(P)$(R)#SPCalChkOrdSav"){
    field(INPA, "$(P)$(R)CloseLoopCav")
    field(INPB, "$(P)$(R)RFCtrlSP-CalOrdE2R CP")
    field(CALC, "A=1?0:1")
    field(OUT,  "$(P)$(R)#SPCalSaveOrd.SELN PP")
}

# PV to enable Sqrt Fit
record(bo, "$(P)$(R)SPCalSqrtFitEn") {
    field(DESC, "Enable fit using sqrt")
    field(VAL,  "0")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn( $(ASYN_PORT).ctrl, $(ASYN_ADDR)) CalibSqrtFitEnable")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")

    info(DESCRIPTION, "Enable fit using sqrt")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

# PVs to store SqrtFitEn for Cav or Kly
record(bo, "$(P)$(R)SPCalSqrtFitEnCav") {
    field(DESC, "Save sqrt fit en when close in cav")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")

    info(DESCRIPTION, "Store Sqrt fit enable when close loop in cavity")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(bo, "$(P)$(R)SPCalSqrtFitEnKly") {
    field(DESC, "Save sqrt fit en when close in kly")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")

    info(DESCRIPTION, "Store Sqrt fit enable when close loop in klystron")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(seq, "$(P)$(R)#SPCalSaveSqrtEn") {
    field(DESC, "Save SP cal. sqrt en")
    field(SELM, "Specified")
    field(SELL, "$(P)$(R)CloseLoopCav")
    field(SHFT, "0")
    field(DOL0, "$(P)$(R)SPCalSqrtFitEn CP")
    field(LNK0, "$(P)$(R)SPCalSqrtFitEnKly PP")
    field(DOL1, "$(P)$(R)SPCalSqrtFitEn")
    field(LNK1, "$(P)$(R)SPCalSqrtFitEnCav PP")

    info(DESCRIPTION, "Save SP sqrt fit en when changed ")
}

record(seq, "$(P)$(R)#SPCalLoadSqrtEn") {
    field(DESC, "Load SP cal. sqrt en")
    field(SELM, "Specified")
    field(SELL, "$(P)$(R)CloseLoopCav CP")
    field(SHFT, "0")
    field(DLY0, "0.5")
    field(DOL0, "$(P)$(R)SPCalSqrtFitEnKly")
    field(LNK0, "$(P)$(R)SPCalSqrtFitEn PP")
    field(DLY1, "0.5")
    field(DOL1, "$(P)$(R)SPCalSqrtFitEnCav")
    field(LNK1, "$(P)$(R)SPCalSqrtFitEn PP")

    info(DESCRIPTION, "Load SP sqrt fit en when change where the loop is closed")
}

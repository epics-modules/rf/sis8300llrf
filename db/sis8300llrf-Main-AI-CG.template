#### Records for Channel Group of Analog Inputs ####
#### Record configurations overridden from sis8300AIChannelGroup.template ####

# since default val for clk src is 0, the divider must be at least 4,
# otherwise the clk is too fast
record(ao, "$(P)$(R)ChGrp$(CHANNEL_ID)-ClkFreq") {
    field(VAL,  "62500000")
    field(DRVH, "62500000")

    info(SYSTEM,	"LLRF")
}

record(longout, "$(P)$(R)ChGrp$(CHANNEL_ID)-SmpNm") {
    field(DRVH, "$(AI_NELM)")
    field(DRVL, "0")
    field(VAL,  "$(SMNM_VAL)")

    info(SYSTEM,	"LLRF")
}

##########
#override from parent because we don't support these settings

record(seq, "$(P)$(R)#ChGrp$(CHANNEL_ID)-EnSeq") {
    field(SELM, "Mask")
    field(SELN, "0x3fc")

    info(SYSTEM,	"LLRF")
}

record(waveform, "$(P)$(R)ChGrp$(CHANNEL_ID)-MsgS") {
    field(DTYP, "")
    field(INP,  "")
    field(SCAN, "Passive")

    info(SYSTEM,	"LLRF")
}

record(waveform, "$(P)$(R)ChGrp$(CHANNEL_ID)-MsgR") {
    field(DTYP, "")
    field(INP,  "")
    field(SCAN, "Passive")

    info(SYSTEM,	"LLRF")
}

### New record for LLRF AI Channels
record(ao, "$(P)$(R)CtrlInSel") {
    field(DESC, "Select controller input")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn( $(ASYN_PORT), 1) CtrlInputSel")
    field(DRVL, "0")
    field(DRVH, "7")
    field(VAL,  "0")
    field(PINI, "YES")
    field(PREC, "0")

    info(DESCRIPTION, "Select controller input")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ai, "$(P)$(R)CtrlInSel-RB") {
    field(DESC, "Select controller input readback")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn( $(ASYN_PORT), 1) CtrlInputSel")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(SCAN, "I/O Intr")

    info(DESCRIPTION, "Select controller input readback")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

# Acquisition Len

record(ao, "$(P)$(R)ChGrp$(CHANNEL_ID)-AcqLen") {
    field(DESC, "Acquisition Len")
    field(EGU,  "ms")
    field(DRVL, "0.0001")
    field(DRVH, "10")
    field(PREC, "4")
    field(FLNK, "$(P)$(R)#ChGrp$(CHANNEL_ID)-SetSmpNm")

    info(autosaveFields, "VAL")
}

record(calcout, "$(P)$(R)#ChGrp$(CHANNEL_ID)-SetSmpNm") {
    field(DESC, "Number of acquired samples")
    field(INPA, "$(P)$(R)ChGrp$(CHANNEL_ID)-AcqLen")
    field(INPB, "$(FREQSAMPLING)")
    field(CALC, "A*B*1000")
    field(OUT,  "$(P)$(R)ChGrp$(CHANNEL_ID)-SmpNm PP")

    info(DESCRIPTION, "set smpnm")
}

record(calc, "$(P)$(R)ChGrp$(CHANNEL_ID)-AcqLen-RB") {
    field(DESC, "Acquisition Len RB")
    field(INPA, "$(P)$(R)ChGrp$(CHANNEL_ID)-SmpNm-RB CP")
    field(INPB, "$(FREQSAMPLING)")
    field(CALC, "A/(B*1000)")
    field(EGU,  "ms")
    field(PREC, "4")
}

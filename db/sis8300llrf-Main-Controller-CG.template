#### Records for Controller Channel Group ####

########
# PIERR_SMNM_MAX shoul be equal to maximum allowed by hw
#######

record(longin, "$(P)$(R)IntChRFCErrSmpNmTot") {
    field(DESC, "Num PI err smpls total.")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(ASYN_PORT), $(ASYN_ADDR)) SamplesCntPITotal")
    field(EGU,  "samples")
    field(SCAN, "I/O Intr")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "Num PI err smpls total.")
    info(SYSTEM,	"LLRF")
}

######
# DAQ Cycles Count
######

record(longin, "$(P)$(R)PosAfterRFStartCyc") {
    field(DESC, "Cycles after RF Start")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(ASYN_PORT), $(ASYN_ADDR)) CyclesAfterRFStart")
    field(EGU,  "cycles")
    field(SCAN, "I/O Intr")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "Number of cycles since the last RF Start")
    info(ARCHIVE_THIS, "")
}

record(longin, "$(P)$(R)PosBeamStartCyc") {
    field(DESC, "Cycles pos Beam Start")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(ASYN_PORT), $(ASYN_ADDR)) CyclesBeamStart")
    field(EGU,  "cycles")
    field(SCAN, "I/O Intr")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "Cycles pos Beam Start")
    info(ARCHIVE_THIS, "")
}

record(longin, "$(P)$(R)PosBeamEndCyc") {
    field(DESC, "Cycles pos Beam End")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(ASYN_PORT), $(ASYN_ADDR)) CyclesBeamEnd")
    field(EGU,  "cycles")
    field(SCAN, "I/O Intr")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "Cycles pos Beam End")
    info(ARCHIVE_THIS, "")
}

record(longin, "$(P)$(R)PosRFEndCyc") {
    field(DESC, "Cycles pos RF End")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(ASYN_PORT), $(ASYN_ADDR)) CyclesRFEnd")
    field(EGU,  "cycles")
    field(SCAN, "I/O Intr")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "Cycles pos RF End")
    info(ARCHIVE_THIS, "")
}

record(longin, "$(P)$(R)PosILockCyc") {
    field(DESC, "Cycles pos ILock")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(ASYN_PORT), $(ASYN_ADDR)) CyclesILock")
    field(EGU,  "cycles")
    field(SCAN, "I/O Intr")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "Cycles pos Ilock")
    info(ARCHIVE_THIS, "")
}

######
# PVs to identify if Cycles Count were triggered
######

record(bi, "$(P)$(R)TrigAfterRFStart") {
    field(DESC, "Pos RFStart was triggered?")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(ASYN_PORT), $(ASYN_ADDR)) TrigAfterRFStart")
    field(ZNAM, "NOT TRIGGERED")
    field(ONAM, "TRIGGERED")
    field(SCAN, "I/O Intr")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "Pos RFStart was triggered?")
    info(ARCHIVE_THIS, "")
}

record(bi, "$(P)$(R)TrigBeamStart") {
    field(DESC, "Pos Beam start was triggered?")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(ASYN_PORT), $(ASYN_ADDR)) TrigBeamStart")
    field(ZNAM, "NOT TRIGGERED")
    field(ONAM, "TRIGGERED")
    field(SCAN, "I/O Intr")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "Pos Beam Start was triggered?")
    info(ARCHIVE_THIS, "")
}

record(bi, "$(P)$(R)TrigBeamEnd") {
    field(DESC, "Pos Beam end was triggered?")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(ASYN_PORT), $(ASYN_ADDR)) TrigBeamEnd")
    field(ZNAM, "NOT TRIGGERED")
    field(ONAM, "TRIGGERED")
    field(SCAN, "I/O Intr")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "Pos Beam end was triggered?")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

record(bi, "$(P)$(R)TrigRFEnd") {
    field(DESC, "Pos RF end was triggered?")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(ASYN_PORT), $(ASYN_ADDR)) TrigRFEnd")
    field(ZNAM, "NOT TRIGGERED")
    field(ONAM, "TRIGGERED")
    field(SCAN, "I/O Intr")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "Pos RF end was triggered?")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

record(bi, "$(P)$(R)TrigILock") {
    field(DESC, "Pos ILock was triggered?")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(ASYN_PORT), $(ASYN_ADDR)) TrigILock")
    field(ZNAM, "NOT TRIGGERED")
    field(ONAM, "TRIGGERED")
    field(SCAN, "I/O Intr")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "Pos ILock was triggered?")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

####
# DAQ values in ms
###

record(calc, "$(P)$(R)PosAfterRFStart") {
    field(DESC, "Pos after RF Start")
    field(INPA, "$(P)$(R)PosAfterRFStartCyc CPP")
    field(INPB, "$(FREQSAMPLING)")  # in MhZ
    field(CALC, "A<0?-1:(A/B)/1000")
    field(PREC, "12")
    field(EGU,  "ms")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "position in ms since the last RF Start")
    info(ARCHIVE_THIS, "")
}

record(calc, "$(P)$(R)PosBeamStart") {
    field(DESC, "Pos beam Start")
    field(INPA, "$(P)$(R)PosBeamStartCyc CPP")
    field(INPB, "$(FREQSAMPLING)")
    field(CALC, "A<0?-1:(A/B)/1000")
    field(PREC, "12")
    field(EGU,  "ms")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "position in ms for beam start")
    info(ARCHIVE_THIS, "")
}

record(calc, "$(P)$(R)PosBeamEnd") {
    field(DESC, "Pos beam End")
    field(INPA, "$(P)$(R)PosBeamEndCyc CPP")
    field(INPB, "$(FREQSAMPLING)")
    field(CALC, "A<0?-1:(A/B)/1000")
    field(PREC, "12")
    field(EGU,  "ms")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "position in ms for beam End")
    info(ARCHIVE_THIS, "")
}

record(calc, "$(P)$(R)PosRFEnd") {
    field(DESC, "Pos RF End")
    field(INPA, "$(P)$(R)PosRFEndCyc CPP")
    field(INPB, "$(FREQSAMPLING)")
    field(CALC, "A<0?-1:(A/B)/1000")
    field(PREC, "12")
    field(EGU,  "ms")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "position in ms for RF End")
    info(ARCHIVE_THIS, "")
}

record(calc, "$(P)$(R)PosILock") {
    field(DESC, "Pos Ilock")
    field(INPA, "$(P)$(R)PosILockCyc CPP")
    field(INPB, "$(FREQSAMPLING)")
    field(CALC, "A<0?-1:(A/B)/1000")
    field(PREC, "12")
    field(EGU,  "ms")
    field(TSEL, "$(TSELPV)")

    info(DESCRIPTION, "position in ms for Ilock")
    info(ARCHIVE_THIS, "")
}

####
# DAQ values in samples
###

record(calc, "$(P)$(R)PosBeamStartSmp") {
    field(DESC, "Pos beam Start smp")

    info(DESCRIPTION, "position in samples for beam start")
    field(INPA, "$(P)$(R)PosBeamStartCyc CPP")
    field(INPB, "$(P)$(R)IQSmpNearIQ-N-RB")
    field(CALC, "A<0?0:FLOOR(A/B)")
    field(EGU,  "samples")
    field(TSEL, "$(TSELPV)")
}

record(calc, "$(P)$(R)PosBeamEndSmp") {
    field(DESC, "Pos beam End smp")

    info(DESCRIPTION, "position in smp for beam End")
    field(INPA, "$(P)$(R)PosBeamEndCyc CPP")
    field(INPB, "$(P)$(R)IQSmpNearIQ-N-RB")
    field(CALC, "A<0?0:CEIL(A/B)")
    field(EGU,  "samples")
    field(TSEL, "$(TSELPV)")
}

record(calc, "$(P)$(R)PosRFEndSmp") {
    field(DESC, "Pos RF End smp")

    info(DESCRIPTION, "position in samples for RF End")
    field(INPA, "$(P)$(R)PosRFEndCyc CPP")
    field(INPB, "$(P)$(R)IQSmpNearIQ-N-RB")
    field(CALC, "A<0?0:CEIL(A/B)")
    field(EGU,  "samples")
    field(TSEL, "$(TSELPV)")
}

record(calc, "$(P)$(R)PosILockSmp") {
    field(DESC, "Pos Ilock smp")

    info(DESCRIPTION, "position in samples for Ilock")
    field(INPA, "$(P)$(R)PosILockCyc CPP")
    field(INPB, "$(P)$(R)IQSmpNearIQ-N-RB")
    field(CALC, "A<0?0:FLOOR(A/B)")
    field(EGU,  "samples")
    field(TSEL, "$(TSELPV)")
}

#####
# PI Controller - Combined Settings
#####
record(bo, "$(P)$(R)RFCtrl-CommitPIParams") {
    field(DESC, "Commit PI params at once")
    field(FLNK, "$(P)$(R)#RFCtrl-ChkCommitPI")
}

record(calcout, "$(P)$(R)#RFCtrl-ChkCommitPI") {
    field(DESC, "Check if commit PI Params")
    field(CALC, "A?15:0")
    field(INPA, "$(P)$(R)RFCtrl-CommitPIParams CP")
    field(FLNK, "$(P)$(R)#RFCtrl-SetPIParams")
    field(OUT,  "$(P)$(R)#RFCtrl-SetPIParams.SELN")
}

record(seq, "$(P)$(R)#RFCtrl-SetPIParams") {
    field(DESC, "Set PI Params at once")
    field(SELM, "Mask")
    field(DOL1, "$(P)$(R)RFCtrlKp-UserInp")
    field(LNK1, "$(P)$(R)RFCtrlKp PP")
    field(DOL2, "$(P)$(R)RFCtrlKi-UserInp")
    field(LNK2, "$(P)$(R)RFCtrlKi PP")
    field(DOL3, "$(P)$(R)RFCtrlKd-UserInp PP")
    field(LNK3, "$(P)$(R)RFCtrlKd PP")
    field(DOL4, "$(P)$(R)RFCtrlTf-UserInp PP")
    field(LNK4, "$(P)$(R)RFCtrlTf PP")
}

record(ao, "$(P)$(R)RFCtrlKp-UserInp") {
    field(DESC, "Set K gain, on I and Q - user input")
    field(DRVH, "$(GAIN_DRVH)")
    field(DRVL, "$(GAIN_DRVL)")
    field(PREC, "$(GAIN_PREC)")

    info(DESCRIPTION, "Set K gain, on I and Q - user input")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
    info(SAVRES_THIS, "")
}

record(ao, "$(P)$(R)RFCtrlKp") {
    field(DESC, "Set K gain, on I and Q")
    field(FLNK, "$(P)$(R)#RFCtrlSetKp")
    field(DRVH, "$(GAIN_DRVH)")
    field(DRVL, "$(GAIN_DRVL)")
    field(PREC, "$(GAIN_PREC)")
    field(PINI, "RUNNING")

    info(DESCRIPTION, "Set K gain, on I and Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
    info(SAVRES_THIS, "")
}

record(seq, "$(P)$(R)#RFCtrlSetKp") {
    field(SELM, "All")
    field(LNK1, "$(P)$(R)RFCtrlKp-I PP")
    field(LNK2, "$(P)$(R)RFCtrlKp-Q PP")
    field(DOL1, "$(P)$(R)RFCtrlKp")
    field(DOL2, "$(P)$(R)RFCtrlKp")

    info(SYSTEM,	"LLRF")
}

record(ao, "$(P)$(R)RFCtrlKi-UserInp") {
    field(DESC, "Set Ts/Ti gain, on I and Q - user input")
    field(DRVH, "$(GAIN_DRVH)")
    field(DRVL, "$(GAIN_DRVL)")
    field(PREC, "$(GAIN_PREC)")

    info(DESCRIPTION, "Set Ts/Ti gain, on I and Q - user input")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
    info(SAVRES_THIS, "")
}

record(ao, "$(P)$(R)RFCtrlKi") {
    field(DESC, "Set Ts/Ti gain, on I and Q")
    field(FLNK, "$(P)$(R)#RFCtrlSetKi")
    field(DRVH, "$(GAIN_DRVH)")
    field(DRVL, "$(GAIN_DRVL)")
    field(PREC, "$(GAIN_PREC)")
    field(PINI, "RUNNING")

    info(DESCRIPTION, "Set Ts/Ti gain, on I and Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
    info(SAVRES_THIS, "")
}

record(seq, "$(P)$(R)#RFCtrlSetKi") {
    field(SELM, "All")
    field(LNK1, "$(P)$(R)RFCtrlKi-I PP")
    field(LNK2, "$(P)$(R)RFCtrlKi-Q PP")
    field(DOL1, "$(P)$(R)RFCtrlKi")
    field(DOL2, "$(P)$(R)RFCtrlKi")

    info(SYSTEM,	"LLRF")
}

record(ao, "$(P)$(R)RFCtrlDa2") {
    field(DESC, "Set D coef. a2, on I and Q")
    field(FLNK, "$(P)$(R)#RFCtrlSetDa2")
    field(DRVH, "$(GAIN_DRVH)")
    field(DRVL, "$(GAIN_DRVL)")
    field(PREC, "$(GAIN_PREC)")

    info(DESCRIPTION, "Set D part coefficent a2, on I and Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
}

record(seq, "$(P)$(R)#RFCtrlSetDa2") {
    field(SELM, "All")
    field(DOL1, "$(P)$(R)RFCtrlDa2")
    field(LNK1, "$(P)$(R)RFCtrlDa2-I PP")
    field(DOL2, "$(P)$(R)RFCtrlDa2")
    field(LNK2, "$(P)$(R)RFCtrlDa2-Q PP")

    info(SYSTEM,	"LLRF")
}

record(ao, "$(P)$(R)RFCtrlDb0") {
    field(DESC, "Set D coef. b0, on I and Q")
    field(FLNK, "$(P)$(R)#RFCtrlSetDb0")
    field(DRVH, "$(GAIN_DRVH)")
    field(DRVL, "$(GAIN_DRVL)")
    field(PREC, "$(GAIN_PREC)")

    info(DESCRIPTION, "Set D part coefficent b0, on I and Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
}

record(seq, "$(P)$(R)#RFCtrlSetDb0") {
    field(SELM, "All")
    field(DOL1, "$(P)$(R)RFCtrlDb0")
    field(LNK1, "$(P)$(R)RFCtrlDb0-I PP")
    field(DOL2, "$(P)$(R)RFCtrlDb0")
    field(LNK2, "$(P)$(R)RFCtrlDb0-Q PP")

    info(SYSTEM,	"LLRF")
}

record(ao, "$(P)$(R)RFCtrlDb1") {
    field(DESC, "Set D coef. b1, on I and Q")
    field(FLNK, "$(P)$(R)#RFCtrlSetDb1")
    field(DRVH, "$(GAIN_DRVH)")
    field(DRVL, "$(GAIN_DRVL)")
    field(PREC, "$(GAIN_PREC)")

    info(DESCRIPTION, "Set D part coefficent b1, on I and Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
}

record(seq, "$(P)$(R)#RFCtrlSetDb1") {
    field(SELM, "All")
    field(DOL1, "$(P)$(R)RFCtrlDb1")
    field(LNK1, "$(P)$(R)RFCtrlDb1-I PP")
    field(DOL2, "$(P)$(R)RFCtrlDb1")
    field(LNK2, "$(P)$(R)RFCtrlDb1-Q PP")

    info(SYSTEM,	"LLRF")
}

record(ao, "$(P)$(R)RFCtrlDb2") {
    field(DESC, "Set D coef. b2, on I and Q")
    field(FLNK, "$(P)$(R)#RFCtrlSetDb2")
    field(DRVH, "$(GAIN_DRVH)")
    field(DRVL, "$(GAIN_DRVL)")
    field(PREC, "$(GAIN_PREC)")

    info(DESCRIPTION, "Set D part coefficent b2, on I and Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
}

record(seq, "$(P)$(R)#RFCtrlSetDb2") {
    field(SELM, "All")
    field(DOL1, "$(P)$(R)RFCtrlDb2")
    field(LNK1, "$(P)$(R)RFCtrlDb2-I PP")
    field(DOL2, "$(P)$(R)RFCtrlDb2")
    field(LNK2, "$(P)$(R)RFCtrlDb2-Q PP")

    info(SYSTEM,	"LLRF")
}

## Real D inputs from the user perspective

record(ao, "$(P)$(R)RFCtrlKd-UserInp") {
    field(DESC, "Set Kd - user input")
    field(VAL,  "2e-07")
    field(PREC, "$(GAIN_PREC)")

    info(autosaveFields_pass1, "VAL")
    info(DESCRIPTION, "Set Kd - user input")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)RFCtrlKd") {
    field(DESC, "Set Kd")
    field(FLNK, "$(P)$(R)#ChkDisD")
    field(VAL,  "2e-07")
    field(PREC, "$(GAIN_PREC)")
    field(PINI, "RUNNING")

    info(autosaveFields_pass1, "VAL")
    info(DESCRIPTION, "Set Kd")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)RFCtrlTf-UserInp") {
    field(DESC, "Set Tf - user input")
    field(VAL,  "1e-07")
    field(PREC, "$(GAIN_PREC)")

    info(DESCRIPTION, "Set Tf - user input")
    info(autosaveFields_pass1, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)RFCtrlTf") {
    field(DESC, "Set Tf")
    field(FLNK, "$(P)$(R)#ChkDisD")
    field(VAL,  "1e-07")
    field(PREC, "$(GAIN_PREC)")
    field(PINI, "RUNNING")

    info(DESCRIPTION, "Set Tf")
    info(autosaveFields_pass1, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

# check if TF or Nd are 0
record(calcout, "$(P)$(R)#ChkDisD"){
    field(DESC, "Check if deriv. should be disabled")
    field(INPA, "$(P)$(R)RFCtrlKd")
    field(INPB, "$(P)$(R)RFCtrlTf")
    field(CALC, "A#0&&B#0?1:30")
    field(OUT,  "$(P)$(R)#ProcDDisD.SELN")
    field(FLNK, "$(P)$(R)#ProcDDisD")
}

record(seq, "$(P)$(R)#ProcDDisD"){
    field(DESC, "Proc. deriv. or dis. deriv.")
    field(SELM, "Mask")
    field(SHFT, "0")
    # continue to calculate Ds and Dt
    field(DO0,  "1")
    field(LNK0, "$(P)$(R)#RFCtrlDs.PROC")
    # disable derivative part
    field(DO1,  "0")
    field(LNK1, "$(P)$(R)RFCtrlDa2 PP")
    field(DO2,  "0")
    field(LNK2, "$(P)$(R)RFCtrlDb0 PP")
    field(DO3,  "0")
    field(LNK3, "$(P)$(R)RFCtrlDb1 PP")
    field(DO4,  "0")
    field(LNK4, "$(P)$(R)RFCtrlDb2 PP")
}

# acalcout pointing to the MASK of a seq
# the seq should set a, b0, b1, b2 to 0 or process the RFCtrlDs

# Calculate s and t

record(calc, "$(P)$(R)#RFCtrlDs") {
    field(INPA, "$(P)$(R)RFCtrlKd")
    field(INPB, "$(P)$(R)RFCtrlTf")
    field(CALC, "A*(1/B)")
    field(FLNK, "$(P)$(R)#RFCtrlDt")
}

record(calc, "$(P)$(R)#RFCtrlDt") {
    field(INPA, "$(P)$(R)RFCtrlKd")
    field(INPB, "$(P)$(R)RFCtrlTf")
    field(INPC, "$(P)$(R)IQSmpNearIQ-N-RB")  # NearIqN
    field(INPD, "$(FREQSAMPLING)")  #in Mhz
    field(CALC, "1+(C/(D*1000000))*(1/B)")
    field(FLNK, "$(P)$(R)#CalcDa2")
}

# calculate the D coefficients

record(calcout, "$(P)$(R)#CalcDa2") {
    field(INPA, "$(P)$(R)#RFCtrlDt")
    field(CALC, "ISNAN(1/(A**2))?0:1/(A**2)")
    field(OUT,  "$(P)$(R)RFCtrlDa2 PP")
    field(FLNK, "$(P)$(R)#CalcDb0")
}

record(calcout, "$(P)$(R)#CalcDb0") {
    field(INPA, "$(P)$(R)#RFCtrlDs")
    field(INPB, "$(P)$(R)#RFCtrlDt")
    field(CALC, "ISNAN(-(A/B))?0:-(A/B)")
    field(OUT,  "$(P)$(R)RFCtrlDb0 PP")
    field(FLNK, "$(P)$(R)#CalcDb1")
}

record(calcout, "$(P)$(R)#CalcDb1") {
    field(INPA, "$(P)$(R)#RFCtrlDs")
    field(INPB, "$(P)$(R)#RFCtrlDt")
    field(CALC, "ISNAN((A*(B-1))/(B**2))?0:(A*(B-1))/(B**2)")
    field(OUT,  "$(P)$(R)RFCtrlDb1 PP")
    field(FLNK, "$(P)$(R)#CalcDb2")
}

record(calcout, "$(P)$(R)#CalcDb2") {
    field(INPA, "$(P)$(R)#RFCtrlDs")
    field(INPB, "$(P)$(R)#RFCtrlDt")
    field(CALC, "ISNAN(A/(B**2))?0:A/(B**2)")
    field(OUT,  "$(P)$(R)RFCtrlDb2 PP")
}

## Readback for Kd and Nd
#s = +/- sqrt(1/a_2) * b_0
#
#t = - b_0/b_2
#
#Tf = T_s / (t-1)
#
#Kd = (s*T_s)/(t-1)

record(calc, "$(P)$(R)#CalcDsRB") {
    field(INPA, "$(P)$(R)RFCtrlDa2-RB CP")
    field(INPB, "$(P)$(R)RFCtrlDb0-RB CP")
    field(CALC, "-SQR(1/A)*B")
    field(FLNK, "$(P)$(R)RFCtrlKd-RB")
}

record(calc, "$(P)$(R)#CalcDtRB") {
    field(INPA, "$(P)$(R)RFCtrlDb0-RB CP")
    field(INPB, "$(P)$(R)RFCtrlDb2-RB CP")
    field(CALC, "-A/B")
    field(FLNK, "$(P)$(R)RFCtrlKd-RB")
}

record(calc, "$(P)$(R)RFCtrlTf-RB") {
    field(INPA, "$(P)$(R)#CalcDtRB")
    field(INPB, "$(P)$(R)IQSmpNearIQ-N-RB")  # NearIqN
    field(INPC, "$(FREQSAMPLING)")  #in Mhz
    field(INPD, "$(P)$(R)RFCtrlDa2-RB")
    field(INPE, "$(P)$(R)RFCtrlDb0-RB")
    field(INPF, "$(P)$(R)RFCtrlDb1-RB")
    field(INPG, "$(P)$(R)RFCtrlDb2-RB")
    field(CALC, "D=0&&E=0&&F=0&&G=0?0:(B/(C*1000000))/(A-1)")
    field(PREC, "$(GAIN_PREC)")

    info(ARCHIVE_THIS, "")
}

record(calc, "$(P)$(R)RFCtrlKd-RB") {
    field(INPA, "$(P)$(R)#CalcDtRB")
    field(INPB, "$(P)$(R)#CalcDsRB")
    field(INPC, "$(P)$(R)IQSmpNearIQ-N-RB")  # NearIqN
    field(INPD, "$(FREQSAMPLING)")  #in Mhz
    field(INPE, "$(P)$(R)RFCtrlDa2-RB")
    field(INPF, "$(P)$(R)RFCtrlDb0-RB")
    field(INPG, "$(P)$(R)RFCtrlDb1-RB")
    field(INPH, "$(P)$(R)RFCtrlDb2-RB")
    field(CALC, "E=0&&F=0&&G=0&&H=0?0:(B*(C/(D*1000000)))/(A-1)")
    field(FLNK, "$(P)$(R)RFCtrlTf-RB")
    field(PREC, "$(GAIN_PREC)")

    info(ARCHIVE_THIS, "")
}

# Sat Max

record(ao, "$(P)$(R)RFCtrlSatMax") {
    field(DESC, "Set maximum saturation value, on I and Q")
    field(FLNK, "$(P)$(R)#RFCtrlSetSatMax")
    field(DRVH, "$(SAT_DRVH)")
    field(DRVL, "$(SAT_DRVL)")
    field(PREC, "12")
    field(PINI, "RUNNING")

    info(DESCRIPTION, "Set maximum saturation value, on I and Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(seq, "$(P)$(R)#RFCtrlSetSatMax") {
    field(SELM, "All")
    field(LNK1, "$(P)$(R)RFCtrlSatMax-I PP")
    field(LNK2, "$(P)$(R)RFCtrlSatMax-Q PP")
    field(DOL1, "$(P)$(R)RFCtrlSatMax")
    field(DOL2, "$(P)$(R)RFCtrlSatMax")

    info(SYSTEM,	"LLRF")
}

record(ao, "$(P)$(R)RFCtrlSatMin") {
    field(DESC, "Set minimum saturation value, on I and Q")
    field(FLNK, "$(P)$(R)#RFCtrlSetSatMin")
    field(DRVH, "$(SAT_DRVH)")
    field(DRVL, "$(SAT_DRVL)")
    field(PREC, "12")
    field(PINI, "RUNNING")

    info(DESCRIPTION, "Set minimum saturation value, on I and Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(seq, "$(P)$(R)#RFCtrlSetSatMin") {
    field(SELM, "All")
    field(LNK1, "$(P)$(R)RFCtrlSatMin-I PP")
    field(LNK2, "$(P)$(R)RFCtrlSatMin-Q PP")
    field(DOL1, "$(P)$(R)RFCtrlSatMin")
    field(DOL2, "$(P)$(R)RFCtrlSatMin")

    info(SYSTEM,	"LLRF")
}

record(mbbo, "$(P)$(R)RFCtrlOutputSel") {
    field(DESC, "Set Ctrl Ouput Sel, on I and Q")
    field(ZRVL, "0")
    field(ZRST, "PI Ctrl. (normal op.)")
    field(ONVL, "1")
    field(ONST, "Input to PI ctrl.")
    field(TWVL, "2")
    field(TWST, "Set Point Value")
    field(THVL, "4")
    field(THST, "Feed Forward Value")
    field(FRVL, "8")
    field(FRST, "PI Error")
    field(FLNK, "$(P)$(R)#RFCtrlSetOutSel")

    info(DESCRIPTION, "Set RF Control Ouput Sel, on I and Q")
    info(autosaveFields, "VAL")
    info(ARCHIVE_THIS, "")
    info(SAVRES_THIS, "")
}

record(seq, "$(P)$(R)#RFCtrlSetOutSel") {
    field(SELM, "All")
    field(LNK1, "$(P)$(R)RFCtrlOutputSel-I PP")
    field(LNK2, "$(P)$(R)RFCtrlOutputSel-Q PP")
    field(DOL1, "$(P)$(R)RFCtrlOutputSel")
    field(DOL2, "$(P)$(R)RFCtrlOutputSel")
}

# GainK RBV

record(calcout,"$(P)$(R)#RFCtrlGetKp") {
    field(INPA, "$(P)$(R)RFCtrlKp-I-RB CP")
    field(INPB, "$(P)$(R)RFCtrlKp-Q-RB CP")
    field(CALC, "A=B?A:VAL")
    field(OUT,  "$(P)$(R)RFCtrlKp-RB PP")

    info(SYSTEM,	"LLRF")
}

record(ai, "$(P)$(R)RFCtrlKp-RB") {
    field(DESC, "K gain readback, if I=Q")
    field(PREC, "$(GAIN_PREC)")
    field(FLNK, "$(P)$(R)#RFCtrlCheckKp")

    info(DESCRIPTION, "K gain readback, if I=Q")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

record(calcout,"$(P)$(R)#RFCtrlCheckKp") {
    field(INPA, "$(P)$(R)RFCtrlKp-I-RB")
    field(INPB, "$(P)$(R)RFCtrlKp-Q-RB")
    field(CALC, "A=B?0:1")
    field(OUT,  "$(P)$(R)RFCtrlK-Stat PP")

    info(SYSTEM,	"LLRF")
}

record(bi, "$(P)$(R)RFCtrlKp-Stat") {
    field(DESC, "PI-GainK State. Invalid indicate I!=Q")
    field(ZNAM, "VALID")
    field(ONAM, "INVALID")
    field(OSV,  "MINOR")

    info(DESCRIPTION, "PI-GainK State. Invalid indicate I!=Q")
    info(SYSTEM,	"LLRF")
}

# GainKi RBV

record(calcout,"$(P)$(R)#RFCtrlGetKi") {
    field(INPA, "$(P)$(R)RFCtrlKi-I-RB CP")
    field(INPB, "$(P)$(R)RFCtrlKi-Q-RB CP")
    field(INPC, "$(P)$(R)#RFCtrlGetKi")
    field(CALC, "A=B?A:C")
    field(OUT,  "$(P)$(R)RFCtrlKi-RB PP")

    info(SYSTEM,	"LLRF")
}

record(ai, "$(P)$(R)RFCtrlKi-RB") {
    field(DESC, "Ts/Ti gain readback, if I= Q")
    field(PREC, "$(GAIN_PREC)")
    field(FLNK, "$(P)$(R)#RFCtrlCheckKi")

    info(DESCRIPTION, "Ts/Ti gain readback, if I= Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

record(calcout,"$(P)$(R)#RFCtrlCheckKi") {
    field(INPA, "$(P)$(R)RFCtrlKi-I-RB")
    field(INPB, "$(P)$(R)RFCtrlKi-Q-RB")
    field(CALC, "A=B?0:1")
    field(OUT,  "$(P)$(R)RFCtrlKi-Stat PP")

    info(SYSTEM,	"LLRF")
}

record(bi, "$(P)$(R)RFCtrlKi-Stat") {
    field(DESC, "GainTSDivTI State.Inv. when I!=Q")
    field(ZNAM, "VALID")
    field(ONAM, "INVALID")
    field(OSV,  "MINOR")

    info(DESCRIPTION, "GainTSDivTI State.Inv. when I!=Q")
    info(SYSTEM,	"LLRF")
}

# D a2 RBV

record(calcout,"$(P)$(R)#RFCtrlGetDa2") {
    field(INPA, "$(P)$(R)RFCtrlDa2-I-RB CP")
    field(INPB, "$(P)$(R)RFCtrlDa2-Q-RB CP")
    field(INPC, "$(P)$(R)#RFCtrlGetDa2")
    field(CALC, "A=B?A:C")
    field(OUT,  "$(P)$(R)RFCtrlDa2-RB PP")

    info(SYSTEM,	"LLRF")
}

record(ai, "$(P)$(R)RFCtrlDa2-RB") {
    field(DESC, "D a2 coef. readback, if I= Q")
    field(PREC, "$(GAIN_PREC)")
    field(FLNK, "$(P)$(R)#RFCtrlCheckDa2")

    info(DESCRIPTION, "D a2 coef. readback, if I= Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
}

record(calcout,"$(P)$(R)#RFCtrlCheckDa2") {
    field(INPA, "$(P)$(R)RFCtrlDa2-I-RB")
    field(INPB, "$(P)$(R)RFCtrlDa2-Q-RB")
    field(CALC, "A=B?0:1")
    field(OUT,  "$(P)$(R)RFCtrlDa2-Stat PP")

    info(SYSTEM,	"LLRF")
}

record(bi, "$(P)$(R)RFCtrlDa2-Stat") {
    field(DESC, "D a2 coef. State.Inv. when I!=Q")
    field(ZNAM, "VALID")
    field(ONAM, "INVALID")
    field(OSV,  "MINOR")

    info(DESCRIPTION, "D a2 coef. State.Inv. when I!=Q")
    info(SYSTEM,	"LLRF")
}

# D b0 RBV

record(calcout,"$(P)$(R)#RFCtrlGetDb0") {
    field(INPA, "$(P)$(R)RFCtrlDb0-I-RB CP")
    field(INPB, "$(P)$(R)RFCtrlDb0-Q-RB CP")
    field(INPC, "$(P)$(R)#RFCtrlGetDb0")
    field(CALC, "A=B?A:C")
    field(OUT,  "$(P)$(R)RFCtrlDb0-RB PP")

    info(SYSTEM,	"LLRF")
}

record(ai, "$(P)$(R)RFCtrlDb0-RB") {
    field(DESC, "D b0 coef. readback, if I= Q")
    field(PREC, "$(GAIN_PREC)")
    field(FLNK, "$(P)$(R)#RFCtrlCheckDb0")

    info(DESCRIPTION, "D b0 coef. readback, if I= Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
}

record(calcout,"$(P)$(R)#RFCtrlCheckDb0") {
    field(INPA, "$(P)$(R)RFCtrlDb0-I-RB")
    field(INPB, "$(P)$(R)RFCtrlDb0-Q-RB")
    field(CALC, "A=B?0:1")
    field(OUT,  "$(P)$(R)RFCtrlDb0-Stat PP")

    info(SYSTEM,	"LLRF")
}

record(bi, "$(P)$(R)RFCtrlDb0-Stat") {
    field(DESC, "D b0 coef. State.Inv. when I!=Q")
    field(ZNAM, "VALID")
    field(ONAM, "INVALID")
    field(OSV,  "MINOR")

    info(DESCRIPTION, "D b0 coef. State.Inv. when I!=Q")
    info(SYSTEM,	"LLRF")
}

# D b1 RBV

record(calcout,"$(P)$(R)#RFCtrlGetDb1") {
    field(INPA, "$(P)$(R)RFCtrlDb1-I-RB CP")
    field(INPB, "$(P)$(R)RFCtrlDb1-Q-RB CP")
    field(INPC, "$(P)$(R)#RFCtrlGetDb1")
    field(CALC, "A=B?A:C")
    field(OUT,  "$(P)$(R)RFCtrlDb1-RB PP")

    info(SYSTEM,	"LLRF")
}

record(ai, "$(P)$(R)RFCtrlDb1-RB") {
    field(DESC, "D b1 coef. readback, if I= Q")
    field(PREC, "$(GAIN_PREC)")
    field(FLNK, "$(P)$(R)#RFCtrlCheckDb1")

    info(DESCRIPTION, "D b1 coef. readback, if I= Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
}

record(calcout,"$(P)$(R)#RFCtrlCheckDb1") {
    field(INPA, "$(P)$(R)RFCtrlDb1-I-RB")
    field(INPB, "$(P)$(R)RFCtrlDb1-Q-RB")
    field(CALC, "A=B?0:1")
    field(OUT,  "$(P)$(R)RFCtrlDb1-Stat PP")

    info(SYSTEM,	"LLRF")
}

record(bi, "$(P)$(R)RFCtrlDb1-Stat") {
    field(DESC, "D b1 coef. State.Inv. when I!=Q")
    field(ZNAM, "VALID")
    field(ONAM, "INVALID")
    field(OSV,  "MINOR")

    info(DESCRIPTION, "D b1 coef. State.Inv. when I!=Q")
    info(SYSTEM,	"LLRF")
}

# D b2 RBV

record(calcout,"$(P)$(R)#RFCtrlGetDb2") {
    field(INPA, "$(P)$(R)RFCtrlDb2-I-RB CP")
    field(INPB, "$(P)$(R)RFCtrlDb2-Q-RB CP")
    field(INPC, "$(P)$(R)#RFCtrlGetDb2")
    field(CALC, "A=B?A:C")
    field(OUT,  "$(P)$(R)RFCtrlDb2-RB PP")

    info(SYSTEM,	"LLRF")
}

record(ai, "$(P)$(R)RFCtrlDb2-RB") {
    field(DESC, "D b2 coef. readback, if I= Q")
    field(PREC, "$(GAIN_PREC)")
    field(FLNK, "$(P)$(R)#RFCtrlCheckDb2")

    info(DESCRIPTION, "D b2 coef. readback, if I= Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
}

record(calcout,"$(P)$(R)#RFCtrlCheckDb2") {
    field(INPA, "$(P)$(R)RFCtrlDb2-I-RB")
    field(INPB, "$(P)$(R)RFCtrlDb2-Q-RB")
    field(CALC, "A=B?0:1")
    field(OUT,  "$(P)$(R)RFCtrlDb2-Stat PP")

    info(SYSTEM,	"LLRF")
}

record(bi, "$(P)$(R)RFCtrlDb2-Stat") {
    field(DESC, "D b2 coef. State.Inv. when I!=Q")
    field(ZNAM, "VALID")
    field(ONAM, "INVALID")
    field(OSV,  "MINOR")

    info(DESCRIPTION, "D b2 coef. State.Inv. when I!=Q")
    info(SYSTEM,	"LLRF")
}

# SatMax RBV

record(calcout,"$(P)$(R)#RFCtrlGetSatMax") {
    field(INPA, "$(P)$(R)RFCtrlSatMax-I-RB CP")
    field(INPB, "$(P)$(R)RFCtrlSatMax-Q-RB CP")
    field(INPC, "$(P)$(R)#RFCtrlGetSatMax")
    field(CALC, "A=B?A:C")
    field(OUT,  "$(P)$(R)RFCtrlSatMax-RB PP")

    info(SYSTEM,	"LLRF")
}

record(ai, "$(P)$(R)RFCtrlSatMax-RB") {
    field(DESC, "Maximum saturation rbk, if I= Q")
    field(PREC, "12")
    field(FLNK, "$(P)$(R)#RFCtrlCheckSatMax")

    info(DESCRIPTION, "Maximum saturation rbk, if I= Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

record(calcout,"$(P)$(R)#RFCtrlCheckSatMax") {
    field(INPA, "$(P)$(R)RFCtrlSatMax-I-RB")
    field(INPB, "$(P)$(R)RFCtrlSatMax-Q-RB")
    field(CALC, "A=B?0:1")
    field(OUT,  "$(P)$(R)RFCtrlSatMax-Stat PP")

    info(SYSTEM,	"LLRF")
}

record(bi, "$(P)$(R)RFCtrlSatMax-Stat") {
    field(DESC, "PI-SATMAX State. Invalid indicate I!=Q")
    field(ZNAM, "VALID")
    field(ONAM, "INVALID")
    field(OSV,  "MINOR")

    info(DESCRIPTION, "PI-SATMAX State. Invalid indicate I!=Q")
    info(SYSTEM,	"LLRF")
}

# SatMin RBV

record(calcout,"$(P)$(R)#RFCtrlGetSatMin") {
    field(INPA, "$(P)$(R)RFCtrlSatMin-I-RB CP")
    field(INPB, "$(P)$(R)RFCtrlSatMin-Q-RB CP")
    field(INPC, "$(P)$(R)#RFCtrlGetSatMin")
    field(CALC, "A=B?A:C")
    field(OUT,  "$(P)$(R)RFCtrlSatMin-RB PP")

    info(SYSTEM,	"LLRF")
}

record(ai, "$(P)$(R)RFCtrlSatMin-RB") {
    field(DESC, "Minimum saturation rbk, if I= Q")
    field(PREC, "12")
    field(FLNK, "$(P)$(R)#RFCtrlCheckSatMin")

    info(DESCRIPTION, "Minimum saturation rbk, if I= Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

record(calcout,"$(P)$(R)#RFCtrlCheckSatMin") {
    field(INPA, "$(P)$(R)RFCtrlSatMin-I-RBV")
    field(INPB, "$(P)$(R)RFCtrlSatMin-Q-RB")
    field(CALC, "A=B?0:1")
    field(OUT,  "$(P)$(R)RFCtrlSatMin-Stat PP")

    info(SYSTEM,	"LLRF")
}

record(bi, "$(P)$(R)RFCtrlSatMin-Stat") {
    field(DESC, "PI-SATMIN State. Invalid indicate I!=Q")
    field(ZNAM, "VALID")
    field(ONAM, "INVALID")
    field(OSV,  "MINOR")

    info(DESCRIPTION, "PI-SATMIN State. Invalid indicate I!=Q")
    info(SYSTEM,	"LLRF")
}

# RFCtrlOutputSel

record(calcout,"$(P)$(R)#RFCtrlGetOutSel") {
    field(INPA, "$(P)$(R)RFCtrlOutputSel-I-RB CP")
    field(INPB, "$(P)$(R)RFCtrlOutputSel-Q-RB CP")
    field(INPC, "$(P)$(R)#RFCtrlGetOutSel")
    field(CALC, "A=B?A:C")
    field(OUT,  "$(P)$(R)RFCtrlOutputSel-RB PP")

    info(SYSTEM,	"LLRF")
}

record(mbbi, "$(P)$(R)RFCtrlOutputSel-RB") {
    field(DESC, "RF Ctrl Output Sel rbk, if I= Q")
    field(ZRVL, "0")
    field(ZRST, "PI Ctrl. (normal op.)")
    field(ONVL, "1")
    field(ONST, "Input to PI ctrl.")
    field(TWVL, "2")
    field(TWST, "Set Point Value")
    field(THVL, "4")
    field(THST, "Feed Forward Value")
    field(FRVL, "8")
    field(FRST, "PI Error")
    field(FLNK, "$(P)$(R)#RFCtrlCheckOutSel")

    info(DESCRIPTION, "Select RF Ctrl Output - Readback, if I= Q")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

record(calcout,"$(P)$(R)#RFCtrlCheckOutSel") {
    field(INPA, "$(P)$(R)RFCtrlOutSel-I-RBV")
    field(INPB, "$(P)$(R)RFCtrlOutSel-Q-RB")
    field(CALC, "A=B?0:1")
    field(OUT,  "$(P)$(R)RFCtrlOutSel-Stat PP")

    info(SYSTEM,	"LLRF")
}

record(bi, "$(P)$(R)RFCtrlOutSel-Stat") {
    field(DESC, "Ctrl Out Sel State. Invalid  I!=Q")
    field(ZNAM, "VALID")
    field(ONAM, "INVALID")
    field(OSV,  "MINOR")

    info(DESCRIPTION, "Ctrl Out Sel State. Invalid indicate I!=Q")
    info(SYSTEM,	"LLRF")
}

## PID Delay RF Start
record(ao, "$(P)$(R)RFCtrlDelay") {
    field(DESC, "PID Delay RF start")
    field(VAL,  "0")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(ASYN_PORT), $(ASYN_ADDR)) PIDDelayRFStart")
    field(EGU,  "ms")
    field(DRVL, "0")
    field(DRVH, "10")
    field(PREC, "4")
    field(PINI, "RUNNING")

    info(DESCRIPTION, "Delay after RF_START until PID effect is applied ")
    info(autosaveFields_pass1, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ai, "$(P)$(R)RFCtrlDelay-RB") {
    field(DESC, "PID Delay RF start")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(ASYN_PORT), $(ASYN_ADDR)) PIDDelayRFStart")
    field(SCAN, "I/O Intr")
    field(EGU,  "ms")
    field(PREC, "4")

    info(DESCRIPTION, "Delay after RF_START until PID effect is applied ")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

# PVs to set correctly ASLO
record(calc, "$(P)$(R)#FacConvRFDelay"){
    field(INPA, "$(FREQSAMPLING)")
    field(INPB, "$(NEARIQN)")
    field(CALC, "B/(A*1000)")
    field(PINI, "YES")
    field(FLNK, "$(P)$(R)#SetASLORFDelay")
}

record(dfanout, "$(P)$(R)#SetASLORFDelay"){
    field(DOL,  "$(P)$(R)#FacConvRFDelay")
    field(OMSL, "closed_loop")
    field(OUTA, "$(P)$(R)RFCtrlDelay.ASLO PP")
    field(OUTB, "$(P)$(R)RFCtrlDelay-RB.ASLO PP")
}

########
# Channel Data Ready
########
record(bi, "$(P)$(R)ChDataRdy") {
    field(DESC, "Channel Data ready trigger")
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(ASYN_PORT), $(ASYN_ADDR)) ChannelDataReady")
    field(SCAN, "I/O Intr")
    field(FLNK, "$(CHDATARDY_FLNK=)")

    info(DESCRIPTION, "Channel Data ready trigger")
    info(SYSTEM,	"LLRF")
}

########
# Open loop/Close loop selector
#######
record(bo, "$(P)$(R)OpenLoop") {
    field(DESC, "Open Loop")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(VAL,  "0")
    field(PINI, "YES")

    info(DESCRIPTION, "Open Loop")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
    info(SAVRES_THIS, "")
}

record(calcout, "$(P)$(R)#SetOpenLoop"){
    field(INPA, "$(P)$(R)OpenLoop CP")
    field(CALC, "A=0?0:3")
    field(OUT,  "$(P)$(R)RFCtrlOutputSel PP")
}

record(bi, "$(P)$(R)OpenLoop-RB") {
    field(DESC, "Open Loop RBV")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")

    info(DESCRIPTION, "Open Loop RBV")
    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

record(calcout, "$(P)$(R)#ChkOpenLoop"){
    field(INPA, "$(P)$(R)RFCtrlOutputSel-RB CP")
    field(INPB, "$(P)$(R)RFCtrlOutSel-Stat CP")
    field(CALC, "A=3&&B=0?1:0")
    field(OUT,  "$(P)$(R)OpenLoop-RB PP")
}

####
# Block Clock Synchronous when in Open Loop
####
record(calc, "$(P)$(R)#OpenLoopInv") {
    field(INPA, "$(P)$(R)OpenLoop-RB CP")
    field(CALC, "A=0?1:0")
    field(FLNK, "$(P)$(R)#SetSpdMode")
}

record(seq, "$(P)$(R)#SetSpdMode"){
    field(DOL0, "0")
    field(LNK0, "$(P)$(R)SPTbl-SpdMode PP")
    field(DOL1, "0")
    field(LNK1, "$(P)$(R)FFTbl-SpdMode PP")
    field(DOL2, "$(P)$(R)#OpenLoopInv")
    field(LNK2, "$(P)$(R)SPTbl-SpdMode.DISP PP")
    field(DOL3, "$(P)$(R)#OpenLoopInv")
    field(LNK3, "$(P)$(R)FFTbl-SpdMode.DISP PP")
}

########
# Use Cavity or Klystron for Closed Loop selector
#######
record(bo, "$(P)$(R)CloseLoopCav") {
    field(DESC, "Use cavity for closed loop")
    field(ZNAM, "Klystron")
    field(ONAM, "Cavity")
    field(VAL,  "0")

    info(DESCRIPTION, "Use cavity for closed loop")
    info(autosaveFields, "VAL")
    info(SYSTEM,	"LLRF")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

## Record to set the channel to the Controller Input

record(calcout, "$(P)$(R)#ChkCtrlInp"){
    field(INPA, "$(P)$(R)CloseLoopCav CP")
    field(CALC, "A=1?$(CAVITYCH):$(PWRFWDCH)")
    field(OUT,  "$(P)$(R)CtrlInSel PP")
}

## Record to block ClosedLoopCav if is not in INIT state

record(calcout, "$(P)$(R)#BlkClosedLoopCav"){
    field(INPA, "$(P)$(R)FSM CP")
    field(CALC, "A=3?0:1")
    field(OUT,  "$(P)$(R)CloseLoopCav.DISP PP")
}

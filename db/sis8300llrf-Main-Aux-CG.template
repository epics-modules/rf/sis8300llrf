###########
### Commmon records for Auxiliary Group
###########

###############################################################################
# Time needed for auxiliary channel group and channels to come out of processing,
# meaning readout and process acquired data.

record(ai, "$(P)$(R)$(CHANNEL_ID)Perf") {
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(ASYN_PORT), $(ASYN_ADDR)) AuxPerfTiming")
    field(SCAN, "I/O Intr")
    field(EGU,  "ms")
    field(PREC, "0")

    info(SYSTEM,	"LLRF")
    info(ARCHIVE_THIS, "")
}

################################################################################
#
# Helper records that allow settings to be applied to all Aux channels at once.
#
# For DownSampled Channels
################################################################################

record(bo, "$(P)$(R)ChGrpDwnEn") {
    field(DESC, "Enable to all down sampled channels.")

    info(DESCRIPTION, "Enable to all down sampled channels.")
    field(FLNK, "$(P)$(R)#ChGrpDwnEnSeq")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(VAL,  "0")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")

    info(SYSTEM,	"LLRF")
}

record(seq, "$(P)$(R)#ChGrpDwnEnSeq") {
    field(SELM, "All")
    field(LNK1, "$(P)$(R)$(CHANNEL_DWN0)-En PP")
    field(LNK2, "$(P)$(R)$(CHANNEL_DWN1)-En PP")
    field(LNK3, "$(P)$(R)$(CHANNEL_DWN2)-En PP")
    field(LNK4, "$(P)$(R)$(CHANNEL_DWN3)-En PP")
    field(LNK5, "$(P)$(R)$(CHANNEL_DWN4)-En PP")
    field(LNK6, "$(P)$(R)$(CHANNEL_DWN5)-En PP")
    field(LNK7, "$(P)$(R)$(CHANNEL_DWN6)-En PP")
    field(LNK8, "$(P)$(R)$(CHANNEL_DWN7)-En PP")
    field(LNK9, "$(P)$(R)$(CHANNEL_DWN8)-En PP")
    field(LNKA, "$(P)$(R)$(CHANNEL_DWN9)-En PP")
    field(DOL1, "$(P)$(R)ChGrpDwnEn")
    field(DOL2, "$(P)$(R)ChGrpDwnEn")
    field(DOL3, "$(P)$(R)ChGrpDwnEn")
    field(DOL4, "$(P)$(R)ChGrpDwnEn")
    field(DOL5, "$(P)$(R)ChGrpDwnEn")
    field(DOL6, "$(P)$(R)ChGrpDwnEn")
    field(DOL7, "$(P)$(R)ChGrpDwnEn")
    field(DOL8, "$(P)$(R)ChGrpDwnEn")
    field(DOL9, "$(P)$(R)ChGrpDwnEn")
    field(DOLA, "$(P)$(R)ChGrpDwnEn")

    info(SYSTEM,	"LLRF")
}

record(bo, "$(P)$(R)ChGrpDwnEnTransf") {
    field(DESC, "Enable transf to all down channels")

    info(DESCRIPTION, "Enable transf to all down channels")
    field(FLNK, "$(P)$(R)#ChGrpDwnEnTransfSeq")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(VAL,  "0")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")

    info(SYSTEM,	"LLRF")
}

record(seq, "$(P)$(R)#ChGrpDwnEnTransfSeq") {
    field(SELM, "All")
    field(LNK1, "$(P)$(R)$(CHANNEL_DWN0)-EnTransf PP")
    field(LNK2, "$(P)$(R)$(CHANNEL_DWN1)-EnTransf PP")
    field(LNK3, "$(P)$(R)$(CHANNEL_DWN2)-EnTransf PP")
    field(LNK4, "$(P)$(R)$(CHANNEL_DWN3)-EnTransf PP")
    field(LNK5, "$(P)$(R)$(CHANNEL_DWN4)-EnTransf PP")
    field(LNK6, "$(P)$(R)$(CHANNEL_DWN5)-EnTransf PP")
    field(LNK7, "$(P)$(R)$(CHANNEL_DWN6)-EnTransf PP")
    field(LNK8, "$(P)$(R)$(CHANNEL_DWN7)-EnTransf PP")
    field(LNK9, "$(P)$(R)$(CHANNEL_DWN8)-EnTransf PP")
    field(LNKA, "$(P)$(R)$(CHANNEL_DWN9)-EnTransf PP")
    field(DOL1, "$(P)$(R)ChGrpDwnEnTransf")
    field(DOL2, "$(P)$(R)ChGrpDwnEnTransf")
    field(DOL3, "$(P)$(R)ChGrpDwnEnTransf")
    field(DOL4, "$(P)$(R)ChGrpDwnEnTransf")
    field(DOL5, "$(P)$(R)ChGrpDwnEnTransf")
    field(DOL6, "$(P)$(R)ChGrpDwnEnTransf")
    field(DOL7, "$(P)$(R)ChGrpDwnEnTransf")
    field(DOL8, "$(P)$(R)ChGrpDwnEnTransf")
    field(DOL9, "$(P)$(R)ChGrpDwnEnTransf")
    field(DOLA, "$(P)$(R)ChGrpDwnEnTransf")

    info(SYSTEM,	"LLRF")
}

record(bo, "$(P)$(R)ChGrpDwnDecEn") {
    field(DESC, "Enable decimation to all down channels")

    info(DESCRIPTION, "Enable decimation to all down channels")
    field(FLNK, "$(P)$(R)#ChGrpDwnDecEnSeq")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(VAL,  "0")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")

    info(SYSTEM,	"LLRF")
}

record(seq, "$(P)$(R)#ChGrpDwnDecEnSeq") {
    field(SELM, "All")
    field(LNK1, "$(P)$(R)$(CHANNEL_DWN0)-DecEn PP")
    field(LNK2, "$(P)$(R)$(CHANNEL_DWN1)-DecEn PP")
    field(LNK3, "$(P)$(R)$(CHANNEL_DWN2)-DecEn PP")
    field(LNK4, "$(P)$(R)$(CHANNEL_DWN3)-DecEn PP")
    field(LNK5, "$(P)$(R)$(CHANNEL_DWN4)-DecEn PP")
    field(LNK6, "$(P)$(R)$(CHANNEL_DWN5)-DecEn PP")
    field(LNK7, "$(P)$(R)$(CHANNEL_DWN6)-DecEn PP")
    field(LNK8, "$(P)$(R)$(CHANNEL_DWN7)-DecEn PP")
    field(LNK9, "$(P)$(R)$(CHANNEL_DWN8)-DecEn PP")
    field(LNKA, "$(P)$(R)$(CHANNEL_DWN9)-DecEn PP")
    field(DOL1, "$(P)$(R)ChGrpDwnDecEn")
    field(DOL2, "$(P)$(R)ChGrpDwnDecEn")
    field(DOL3, "$(P)$(R)ChGrpDwnDecEn")
    field(DOL4, "$(P)$(R)ChGrpDwnDecEn")
    field(DOL5, "$(P)$(R)ChGrpDwnDecEn")
    field(DOL6, "$(P)$(R)ChGrpDwnDecEn")
    field(DOL7, "$(P)$(R)ChGrpDwnDecEn")
    field(DOL8, "$(P)$(R)ChGrpDwnDecEn")
    field(DOL9, "$(P)$(R)ChGrpDwnDecEn")
    field(DOLA, "$(P)$(R)ChGrpDwnDecEn")

    info(SYSTEM,	"LLRF")
}

record(ao, "$(P)$(R)ChGrpDwnAcqLen") {
    field(DESC, "Set AcqLen to all down channels")
    field(FLNK, "$(P)$(R)#ChGrpDwnAcqLenSeq")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(DRVL, "0.001")
    field(DRVH, "10")
    field(EGU,  "ms")
    field(PREC, "4")
    field(VAL,  "$(LEN_AUX="4.2")")

    info(DESCRIPTION, "Set acquisition length to all down channels")
    info(SYSTEM,	"LLRF")
}

record(seq, "$(P)$(R)#ChGrpDwnAcqLenSeq") {
    field(SELM, "All")
    field(LNK1, "$(P)$(R)$(CHANNEL_DWN0)-AcqLen PP")
    field(LNK2, "$(P)$(R)$(CHANNEL_DWN1)-AcqLen PP")
    field(LNK3, "$(P)$(R)$(CHANNEL_DWN2)-AcqLen PP")
    field(LNK4, "$(P)$(R)$(CHANNEL_DWN3)-AcqLen PP")
    field(LNK5, "$(P)$(R)$(CHANNEL_DWN4)-AcqLen PP")
    field(LNK6, "$(P)$(R)$(CHANNEL_DWN5)-AcqLen PP")
    field(LNK7, "$(P)$(R)$(CHANNEL_DWN6)-AcqLen PP")
    field(LNK8, "$(P)$(R)$(CHANNEL_DWN7)-AcqLen PP")
    field(LNK9, "$(P)$(R)$(CHANNEL_DWN8)-AcqLen PP")
    field(LNKA, "$(P)$(R)$(CHANNEL_DWN9)-AcqLen PP")
    field(DOL1, "$(P)$(R)ChGrpDwnAcqLen")
    field(DOL2, "$(P)$(R)ChGrpDwnAcqLen")
    field(DOL3, "$(P)$(R)ChGrpDwnAcqLen")
    field(DOL4, "$(P)$(R)ChGrpDwnAcqLen")
    field(DOL5, "$(P)$(R)ChGrpDwnAcqLen")
    field(DOL6, "$(P)$(R)ChGrpDwnAcqLen")
    field(DOL7, "$(P)$(R)ChGrpDwnAcqLen")
    field(DOL8, "$(P)$(R)ChGrpDwnAcqLen")
    field(DOL9, "$(P)$(R)ChGrpDwnAcqLen")
    field(DOLA, "$(P)$(R)ChGrpDwnAcqLen")

    info(SYSTEM,	"LLRF")
}

record(ao, "$(P)$(R)ChGrpDwnDecF") {
    field(DESC, "Set dec fac to all down channels")

    info(DESCRIPTION, "Set dec fac to all down channels")
    field(FLNK, "$(P)$(R)#ChGrpDwnDecFSeq")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(VAL,  "$(DECFAC_AUX="1")")

    info(SYSTEM,	"LLRF")
}

record(seq, "$(P)$(R)#ChGrpDwnDecFSeq") {
    field(SELM, "All")
    field(LNK1, "$(P)$(R)$(CHANNEL_DWN0)-DecF PP")
    field(LNK2, "$(P)$(R)$(CHANNEL_DWN1)-DecF PP")
    field(LNK3, "$(P)$(R)$(CHANNEL_DWN2)-DecF PP")
    field(LNK4, "$(P)$(R)$(CHANNEL_DWN3)-DecF PP")
    field(LNK5, "$(P)$(R)$(CHANNEL_DWN4)-DecF PP")
    field(LNK6, "$(P)$(R)$(CHANNEL_DWN5)-DecF PP")
    field(LNK7, "$(P)$(R)$(CHANNEL_DWN6)-DecF PP")
    field(LNK8, "$(P)$(R)$(CHANNEL_DWN7)-DecF PP")
    field(LNK9, "$(P)$(R)$(CHANNEL_DWN8)-DecF PP")
    field(LNKA, "$(P)$(R)$(CHANNEL_DWN9)-DecF PP")
    field(DOL1, "$(P)$(R)ChGrpDwnDecF")
    field(DOL2, "$(P)$(R)ChGrpDwnDecF")
    field(DOL3, "$(P)$(R)ChGrpDwnDecF")
    field(DOL4, "$(P)$(R)ChGrpDwnDecF")
    field(DOL5, "$(P)$(R)ChGrpDwnDecF")
    field(DOL6, "$(P)$(R)ChGrpDwnDecF")
    field(DOL7, "$(P)$(R)ChGrpDwnDecF")
    field(DOL8, "$(P)$(R)ChGrpDwnDecF")
    field(DOL9, "$(P)$(R)ChGrpDwnDecF")
    field(DOLA, "$(P)$(R)ChGrpDwnDecF")

    info(SYSTEM,	"LLRF")
}

record(mbbo, "$(P)$(R)ChGrpDwnFmt") {
    field(DESC, "Set DAQ Format for all down channels")

    info(DESCRIPTION, "Set DAQ Format for all down channels")
    field(ZRVL, "0")
    field(ZRST, "MAG/ANG")
    field(ONVL, "1")
    field(ONST, "I/Q")
    field(TWVL, "2")
    field(TWST, "DC")
    field(VAL,  "0")
    field(FLNK, "$(P)$(R)#ChGrpDwnFmtSeq")

    info(SYSTEM,	"LLRF")
}

record(seq, "$(P)$(R)#ChGrpDwnFmtSeq") {
    field(SELM, "All")
    field(LNK1, "$(P)$(R)$(CHANNEL_DWN0)-Fmt PP")
    field(LNK2, "$(P)$(R)$(CHANNEL_DWN1)-Fmt PP")
    field(LNK3, "$(P)$(R)$(CHANNEL_DWN2)-Fmt PP")
    field(LNK4, "$(P)$(R)$(CHANNEL_DWN3)-Fmt PP")
    field(LNK5, "$(P)$(R)$(CHANNEL_DWN4)-Fmt PP")
    field(LNK6, "$(P)$(R)$(CHANNEL_DWN5)-Fmt PP")
    field(LNK7, "$(P)$(R)$(CHANNEL_DWN6)-Fmt PP")
    field(LNK8, "$(P)$(R)$(CHANNEL_DWN7)-Fmt PP")
    field(LNK9, "$(P)$(R)$(CHANNEL_DWN8)-Fmt PP")
    field(LNKA, "$(P)$(R)$(CHANNEL_DWN9)-Fmt PP")
    field(DOL1, "$(P)$(R)ChGrpDwnFmt")
    field(DOL2, "$(P)$(R)ChGrpDwnFmt")
    field(DOL3, "$(P)$(R)ChGrpDwnFmt")
    field(DOL4, "$(P)$(R)ChGrpDwnFmt")
    field(DOL5, "$(P)$(R)ChGrpDwnFmt")
    field(DOL6, "$(P)$(R)ChGrpDwnFmt")
    field(DOL7, "$(P)$(R)ChGrpDwnFmt")
    field(DOL8, "$(P)$(R)ChGrpDwnFmt")
    field(DOL9, "$(P)$(R)ChGrpDwnFmt")
    field(DOLA, "$(P)$(R)ChGrpDwnFmt")

    info(SYSTEM,	"LLRF")
}

################################################################################
#
# Helper records that allow settings to be applied to all Aux channels at once.
#
# For Internal Channels
################################################################################

record(bo, "$(P)$(R)ChGrpIntEn") {
    field(DESC, "Enable to all intern channels.")

    info(DESCRIPTION, "Enable to all intern channels.")
    field(FLNK, "$(P)$(R)#ChGrpIntEnSeq")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(VAL,  "0")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")

    info(SYSTEM,	"LLRF")
}

record(seq, "$(P)$(R)#ChGrpIntEnSeq") {
    field(SELM, "All")
    field(LNK1, "$(P)$(R)IntCh$(CHANNEL_INTERN0)En PP")
    field(LNK2, "$(P)$(R)IntCh$(CHANNEL_INTERN1)En PP")
    field(LNK3, "$(P)$(R)IntCh$(CHANNEL_INTERN2)En PP")
    field(LNK4, "$(P)$(R)IntCh$(CHANNEL_INTERN3)En PP")
    field(LNK5, "$(P)$(R)IntCh$(CHANNEL_INTERN4)En PP")
    field(LNK6, "$(P)$(R)IntCh$(CHANNEL_INTERN5)En PP")
    field(DOL1, "$(P)$(R)ChGrpIntEn")
    field(DOL2, "$(P)$(R)ChGrpIntEn")
    field(DOL3, "$(P)$(R)ChGrpIntEn")
    field(DOL4, "$(P)$(R)ChGrpIntEn")
    field(DOL5, "$(P)$(R)ChGrpIntEn")
    field(DOL6, "$(P)$(R)ChGrpIntEn")

    info(SYSTEM,	"LLRF")
}

record(bo, "$(P)$(R)ChGrpIntEnTransf") {
    field(DESC, "Enable transf to all intern channels")

    info(DESCRIPTION, "Enable transf to all intern channels")
    field(FLNK, "$(P)$(R)#ChGrpIntEnTransfSeq")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(VAL,  "0")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")

    info(SYSTEM,	"LLRF")
}

record(seq, "$(P)$(R)#ChGrpIntEnTransfSeq") {
    field(SELM, "All")
    field(LNK1, "$(P)$(R)IntCh$(CHANNEL_INTERN0)EnTransf PP")
    field(LNK2, "$(P)$(R)IntCh$(CHANNEL_INTERN1)EnTransf PP")
    field(LNK3, "$(P)$(R)IntCh$(CHANNEL_INTERN2)EnTransf PP")
    field(LNK4, "$(P)$(R)IntCh$(CHANNEL_INTERN3)EnTransf PP")
    field(LNK5, "$(P)$(R)IntCh$(CHANNEL_INTERN4)EnTransf PP")
    field(LNK6, "$(P)$(R)IntCh$(CHANNEL_INTERN5)EnTransf PP")
    field(DOL1, "$(P)$(R)ChGrpIntEnTransf")
    field(DOL2, "$(P)$(R)ChGrpIntEnTransf")
    field(DOL3, "$(P)$(R)ChGrpIntEnTransf")
    field(DOL4, "$(P)$(R)ChGrpIntEnTransf")
    field(DOL5, "$(P)$(R)ChGrpIntEnTransf")
    field(DOL6, "$(P)$(R)ChGrpIntEnTransf")

    info(SYSTEM,	"LLRF")
}

record(bo, "$(P)$(R)ChGrpIntDecEn") {
    field(DESC, "Enable decimation to all intern channels")

    info(DESCRIPTION, "Enable decimation to all intern channels")
    field(FLNK, "$(P)$(R)#ChGrpIntDecEnSeq")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(VAL,  "0")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")

    info(SYSTEM,	"LLRF")
}

record(seq, "$(P)$(R)#ChGrpIntDecEnSeq") {
    field(SELM, "All")
    field(LNK1, "$(P)$(R)IntCh$(CHANNEL_INTERN0)DecEn PP")
    field(LNK2, "$(P)$(R)IntCh$(CHANNEL_INTERN1)DecEn PP")
    field(LNK3, "$(P)$(R)IntCh$(CHANNEL_INTERN2)DecEn PP")
    field(LNK4, "$(P)$(R)IntCh$(CHANNEL_INTERN3)DecEn PP")
    field(LNK5, "$(P)$(R)IntCh$(CHANNEL_INTERN4)DecEn PP")
    field(DOL1, "$(P)$(R)ChGrpIntDecEn")
    field(DOL2, "$(P)$(R)ChGrpIntDecEn")
    field(DOL3, "$(P)$(R)ChGrpIntDecEn")
    field(DOL4, "$(P)$(R)ChGrpIntDecEn")
    field(DOL5, "$(P)$(R)ChGrpIntDecEn")

    info(SYSTEM,	"LLRF")
}

record(ao, "$(P)$(R)ChGrpIntAcqLen") {
    field(DESC, "Set SMNM to all intern channels")

    info(DESCRIPTION, "Set SMNM to all intern channels")
    field(FLNK, "$(P)$(R)#ChGrpIntAcqLenSeq")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(VAL,  "$(LEN_AUX="4.2")")
    field(PREC, "4")
    field(EGU,  "ms")

    info(SYSTEM,	"LLRF")
}

record(seq, "$(P)$(R)#ChGrpIntAcqLenSeq") {
    field(SELM, "All")
    field(LNK1, "$(P)$(R)IntCh$(CHANNEL_INTERN0)AcqLen PP")
    field(LNK2, "$(P)$(R)IntCh$(CHANNEL_INTERN1)AcqLen PP")
    field(LNK3, "$(P)$(R)IntCh$(CHANNEL_INTERN2)AcqLen PP")
    field(LNK4, "$(P)$(R)IntCh$(CHANNEL_INTERN3)AcqLen PP")
    field(LNK5, "$(P)$(R)IntCh$(CHANNEL_INTERN4)AcqLen PP")
    field(LNK6, "$(P)$(R)IntCh$(CHANNEL_INTERN5)AcqLen PP")
    field(DOL1, "$(P)$(R)ChGrpIntAcqLen")
    field(DOL2, "$(P)$(R)ChGrpIntAcqLen")
    field(DOL3, "$(P)$(R)ChGrpIntAcqLen")
    field(DOL4, "$(P)$(R)ChGrpIntAcqLen")
    field(DOL5, "$(P)$(R)ChGrpIntAcqLen")
    field(DOL6, "$(P)$(R)ChGrpIntAcqLen")

    info(SYSTEM,	"LLRF")
}

record(ao, "$(P)$(R)ChGrpIntDecF") {
    field(DESC, "Set dec fac to all intern channels")

    info(DESCRIPTION, "Set dec fac to all intern channels")
    field(FLNK, "$(P)$(R)#ChGrpIntDecFSeq")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(VAL,  "$(DECFAC_AUX="1")")

    info(SYSTEM,	"LLRF")
}

record(seq, "$(P)$(R)#ChGrpIntDecFSeq") {
    field(SELM, "All")
    field(LNK1, "$(P)$(R)IntCh$(CHANNEL_INTERN0)DecF PP")
    field(LNK2, "$(P)$(R)IntCh$(CHANNEL_INTERN1)DecF PP")
    field(LNK3, "$(P)$(R)IntCh$(CHANNEL_INTERN2)DecF PP")
    field(LNK4, "$(P)$(R)IntCh$(CHANNEL_INTERN3)DecF PP")
    field(LNK5, "$(P)$(R)IntCh$(CHANNEL_INTERN4)DecF PP")
    field(DOL1, "$(P)$(R)ChGrpIntDecF")
    field(DOL2, "$(P)$(R)ChGrpIntDecF")
    field(DOL3, "$(P)$(R)ChGrpIntDecF")
    field(DOL4, "$(P)$(R)ChGrpIntDecF")
    field(DOL5, "$(P)$(R)ChGrpIntDecF")

    info(SYSTEM,	"LLRF")
}

########### Interlock timestamp for Post Mortem Records

record(stringin, "$(P)$(R)ChGrpPMTime") {
    field(DESC, "Timestamp of last interlock")
    field(DTYP, "Soft Timestamp")
    field(INP,  "@%Y-%m-%d %H:%M:%S.%03f")
    field(TSEL, "$(P)$(R)Dwn0-PMCmp0.TIME CP")
}

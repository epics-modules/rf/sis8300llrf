/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfControlTableChannelGroup.cpp
 * @brief Implementation of the LLRF control table (FF and SP) CG class
 * @author urojec
 * @date 26.5.2014
 * 
 * This Channel group will contain as many channels as there are pulse 
 * types defined
 * when the defice structure is created 
 * @see #sis8300llrfDevice::crateStructure.
 * 
 * The last channel in the group always has the ID of the highest pulse 
 * type plus one and is used for special operating modes.
 * 
 * The class also defines the concept of active table, which is the 
 * table that corresponds to the currently selected pulse type.
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"
#include "sis8300llrfdrv_types.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfControlTableChannelGroup.h"
#include "sis8300llrfControlTableChannel.h"


std::string sis8300llrfControlTableChannelGroup::PV_REASON_MAX_SAMPLESCNT = 
                                                    "MaxNelm";

/**
 * @brief Control Table ChannelGroup constructor.
 * 
 * @param [in] name Channel Group name.
 * 
 * @return The function will fail if the table type selected is not 
 * either FF or SP.
 *
 */
sis8300llrfControlTableChannelGroup::sis8300llrfControlTableChannelGroup(
            const std::string& name, sis8300drv_usr *newDeviceUser, 
            sis8300llrfdrv_ctrl_table ctrlTableType) :
        sis8300llrfChannelGroup(name, newDeviceUser) {

    switch (ctrlTableType) {
        case ctrl_table_sp:
            break;
        case ctrl_table_ff:
            break;
        default:
            NDS_CRT("Invalid option for control table! Must be FF or SP!");
    }

    _CtrlTableType = ctrlTableType;
}

/**
 * @brief ControlTAbleChannelGroup destructor
 */
sis8300llrfControlTableChannelGroup::~sis8300llrfControlTableChannelGroup() {}

/**
 * @brief Initialize the channel group
 * 
 * @return ndsError     Parent initialization failed or could not read 
 *                      from hw
 * @return ndsSucces    initialization successfull
 * 
 * This function will query the hardware to get the maximum allowed 
 * number of elements in the table.
 */
ndsStatus sis8300llrfControlTableChannelGroup::initialize() {
    ndsStatus statusNds;
    int       statusLib;
    unsigned  uRegVal;

    statusNds = sis8300llrfChannelGroup::initialize();

    /* Get max samples and allocate the buffers for the tables
     * Max samples is the largest they can be */
    statusLib = sis8300llrfdrv_get_ctrl_table_max_nelm(
                    _DeviceUser, _CtrlTableType, &uRegVal);
    SIS8300NDS_STATUS_ASSERT(
        "sis8300llrfdrv_get_ctrl_table_max_nelm", statusLib);
    _MaxSamplesCount = (epicsInt32) uRegVal;

    doCallbacksInt32(_MaxSamplesCount, _interruptIdMaxNsamples);

    return statusNds;
}

/**
 * @see #sis8300llrfChannelGroup::writeToHardware
 */
ndsStatus sis8300llrfControlTableChannelGroup::writeToHardware() {
    NDS_TRC("%s", __func__);

    int status;

    if (_SamplesCountChanged) {
        _SamplesCountChanged = 0;

        NDS_DBG("Writing new samples count %s", getName().c_str());
        NDS_DBG("New samples count %d %ul", _SamplesCount, _SamplesCount);
        status = sis8300llrfdrv_set_ctrl_table_nelm(
                    _DeviceUser, _CtrlTableType, _SamplesCount);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_set_ctrl_table_nelm", status);

        _UpdateReason |= SIS8300LLRFDRV_UPDATE_REASON_NEW_PARAMS;

        doCallbacksInt32(_SamplesCount, _interruptIdSamplesCount);
    }

    return sis8300llrfChannelGroup::writeToHardware();
}

/**
 * @see #sis8300llrfChannelGroup::readParamters
 */
ndsStatus sis8300llrfControlTableChannelGroup::readParameters() {
    NDS_TRC("%s", __func__);

    int status;
    unsigned uRegVal;

    status = sis8300llrfdrv_get_ctrl_table_nelm(
                _DeviceUser, _CtrlTableType, &uRegVal);
    SIS8300NDS_STATUS_ASSERT(
        "sis8300llrfdrv_get_ctrl_table_nelm", status);

    doCallbacksInt32((epicsInt32) uRegVal, _interruptIdSamplesCount);

    return ndsSuccess;
}

/**
 * @see #sis8300llrfChannelGroup::markAllParamtersAsChanged
 */
ndsStatus sis8300llrfControlTableChannelGroup::markAllParametersChanged() {
    NDS_TRC("%s", __func__);

    _SamplesCountChanged = 1;

    return sis8300llrfChannelGroup::markAllParametersChanged();
}

/**
 * @brief Override parent beacuse the operation is not allowed
 *
 * @return ndsError always
 *
 * The nsamples of the channel group is equal to maximum number of
 * samples for control table type allowed by the board. It is read 
 * directly from the device and is not allowed to be changed at runtime
 */
ndsStatus sis8300llrfControlTableChannelGroup::setSamplesCount(
                asynUser *pasynUser, epicsInt32 value) {

    if (pasynUser) {
        NDS_ERR("Setting of nsamples directly is not allowed for "
            "Control table channel group");
        return ndsError;
    }

    _SamplesCount = value;
    _SamplesCountChanged = 1;

    return commitParameters();
}

/**
 * @brief Check if tables on a specific channels were written down.
 *        This is how we determine if a specific pulse type (=channel)
 *        is set up.
 *
 * @param [in] pulse_type   Pulse type to check
 *
 * @return ndsError         Tables are not set up, or chan number is out 
 *                          of range
 * @return ndsSucess        Tables re set up, the pulse type is ok and 
 *                          can be selected
 *
 *
 * This is noramlly called from @see @sis8300llrfDevice::setPulseType to 
 * check if the requested pulse type can be selected or not.
 */
ndsStatus sis8300llrfControlTableChannelGroup::checkNewPulseType(
                int pulse_type) {
    NDS_TRC("%s", __func__);

    sis8300llrfControlTableChannel *chan;

    if (getChannel(pulse_type, &chan) != ndsSuccess) {
        NDS_ERR("getChannelFailed");
        return ndsError;
    }

    return ndsSuccess;
}

/**
 * @brief Get ctrl table type for this channel group. The type
 *        can be either FF or SP.
 *
 * @return Control table type
 *
 * Control table type is the same for all the channels in the group.
 */
sis8300llrfdrv_ctrl_table 
        sis8300llrfControlTableChannelGroup::getCtrlTableType() {
    return _CtrlTableType;
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfControlTableChannelGroup::registerHandlers(
                nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_INT32(
            sis8300llrfControlTableChannelGroup::PV_REASON_MAX_SAMPLESCNT,
            &sis8300llrfControlTableChannelGroup::setInt32,
            &sis8300llrfControlTableChannelGroup::getMaxNelm,
            &_interruptIdMaxNsamples);

    return sis8300llrfChannelGroup::registerHandlers(pvContainers);
}

/**
 * @brief Get the maximum number of elements that the table can contatin
 *        (limited from hardware)
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hoild the maximum allowed samples count 
 *                          on success
 * 
 * @return ndsError     To prevent faulty values when in IOC INIT state
 * @return ndsSuccess   All calls not made in IOC INIT state
 */
ndsStatus sis8300llrfControlTableChannelGroup::getMaxNelm(
                asynUser *pasynUser, epicsInt32 *value) {
    if (getCurrentState() != nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = _MaxSamplesCount;
    return ndsSuccess;
}

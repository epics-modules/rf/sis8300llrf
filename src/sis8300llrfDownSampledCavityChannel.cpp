/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfDownSampledCavityChannel.cpp
 * @brief File defining the LLRF DownSampled channel class that handles
 * downsampled values and their settings
 * @author gabriel.fedel@ess.eu
 * @date 31.3.2020
 *
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"
#include "sis8300llrfdrv_types.h"
#include "sis8300llrfDownSampledCavityChannel.h"
#include "sis8300llrfDevice.h"


#define ROUNDUP_TWOHEX(val) ((unsigned) (val + 0x1F) &~0x1F)

/**
 * @brief Read all the downsampled data from memory and push to PV
 *
 * @return ndsSuccess   Data read successfully
 * @return ndsError     Read failed, chan goes to ERR state or
 *                      couldn't allocate memory.
 *
 * This will read all the downsampled data, convert to float and push
 * to the PV.
 *
 * */
ndsStatus sis8300llrfDownSampledCavityChannel::onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    int status;
    unsigned nsamples;
    bool pm;

    if (sis8300llrfAuxChannel::onLeaveProcessing(from, to) == ndsError)
        return ndsError;

    if (_SamplesAcquired == 0) {
        return ndsSuccess;
    }

    pm = checkPM();
    // If there it is not enable the transfer and there is no interlock
    // We shouldn't get anything from firmware
    if (!pm && !_EnableTransf){
        if (_XAxis != NULL)
            delete [] _XAxis;
        _XAxis = NULL;
        _SamplesAcquired = 0;
        doCallbacksInt32(_SamplesAcquired, _interruptIdAUXNSamplesAcq);
        doCallbacksFloat64Array(_XAxis, _SamplesAcquired, _interruptIdAUXXAxis);
        doCallbacksFloat64Array(NULL, (size_t)_SamplesAcquired, _interruptIdAUXIMag);
        doCallbacksFloat32Array(NULL, (size_t)_SamplesAcquired, _interruptIdAUXIMagW);
        doCallbacksFloat64Array(NULL, (size_t)_SamplesAcquired, _interruptIdAUXQAng);
        return ndsSuccess;
    }

    /* we need to read in 512 bit blocks - firmware imposed limitation */
    NDS_DBG("Rounding _SamplesAcquired from %d", _SamplesAcquired);
    nsamples = ROUNDUP_TWOHEX(_SamplesAcquired);
    NDS_DBG("_SamplesAcquired rounded to %u", nsamples);

    if (_rawDataI16 != NULL)
        delete [] _rawDataI16;

    _rawDataI16 = new (std::nothrow) epicsInt16[nsamples * 2]; //*2 because is 32bits per sample

    if (!_rawDataI16)
        return ndsError;

    status = sis8300llrfdrv_read_aux_channel(
                _DeviceUser, getChannelNumber(), _DownOrIntern, _rawDataI16,
                nsamples);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_read_aux_channel", status);

    int iterSrc, iterDest;
    epicsFloat64 ConvFactIMag, ConvFactQAng, IVal, QVal;
    epicsInt32 daq_fmt;
    double param;

    NDS_DBG("Entered Thread");

    if (_IMagData != NULL)
        delete [] _IMagData;
    if (_IMagDataW != NULL)
        delete [] _IMagDataW;

    if (_QAngData != NULL)
        delete [] _QAngData;

    _IMagData = new (std::nothrow) epicsFloat64[_SamplesAcquired];
    _IMagDataW = new (std::nothrow) epicsFloat32[_SamplesAcquired];

    _QAngData = new (std::nothrow) epicsFloat64[_SamplesAcquired];

    if (!_IMagData || !_IMagDataW || !_QAngData) {
        NDS_ERR("Error allocating memory for  downsampled chanel %d ", getChannelNumber());
        doCallbacksFloat64Array(NULL, 0, _interruptIdAUXQAng);
        doCallbacksFloat64Array(NULL, 0, _interruptIdAUXIMag);
        doCallbacksFloat32Array(NULL, 0, _interruptIdAUXIMagW);
        return ndsError;
    }

    //get dat format - 0: MA / 1: IQ / 2: DC
    readParameter(aux_param_daq_fmt, &param);
    daq_fmt = (epicsInt32) param;

    switch (daq_fmt) {
        case 1: //IQ - no calibration
            ConvFactIMag = ConvFactQAng = 1.0 / (epicsFloat64) (1 << sis8300llrfdrv_Qmn_down_IQ_sample.frac_bits_n);

            for (iterSrc = 0, iterDest = 0; iterDest < _SamplesAcquired; iterSrc += 2, iterDest ++) {
                IVal = (epicsFloat64)(((epicsInt16)_rawDataI16[iterSrc]) * ConvFactIMag);
                _IMagData[iterDest] = IVal;
                _IMagDataW[iterDest] = (epicsFloat32)IVal;
                _QAngData[iterDest] = (epicsFloat64)(((epicsInt16)_rawDataI16[iterSrc + 1]) * ConvFactIMag);
            }
            doCallbacksFloat64Array(_QAngData, (size_t)_SamplesAcquired, _interruptIdAUXQAng);
        break;
        case 2: //DC - just component I has data, and could be calibrated
            ConvFactIMag = 1.0 / (epicsFloat64) (1 << sis8300llrfdrv_Qmn_down_raw_sample.frac_bits_n);
            if (m_CavChan->isCalibrated())//with polynomial calibration (DC doesn't have gradient calibration)
                for (iterSrc = 0, iterDest = 0; iterDest < _SamplesAcquired; iterSrc += 2, iterDest ++)
                {
                    IVal = (epicsFloat64)(((epicsInt16)_rawDataI16[iterSrc]) * ConvFactIMag);
                    IVal = m_CavChan->calibrateSanityCheck(IVal);
                    _IMagData[iterDest] = IVal;
                    _IMagDataW[iterDest] = (epicsFloat32)IVal;
                }
            else { // no calibration
                for (iterSrc = 0, iterDest = 0; iterDest < _SamplesAcquired; iterSrc += 2, iterDest ++){
                    IVal = (epicsFloat64)(((epicsInt16)_rawDataI16[iterSrc]) * ConvFactIMag);
                    _IMagData[iterDest] =  IVal;
                    _IMagDataW[iterDest] = (epicsFloat32)IVal;
                }
            }
            doCallbacksFloat64Array(NULL, 0, _interruptIdAUXQAng);
        break;
        case 0:
        default: //MA - just magnitude could be calibrated
            ConvFactQAng = 1.0 / (epicsFloat64) (1 << sis8300llrfdrv_Qmn_down_ang_sample.frac_bits_n);
            if(m_CavChan->isCalibrated()) {
                if(m_CavChan->isGradCalibrated()) { //with gradient calibration
                    for (iterSrc = 0, iterDest = 0; iterDest < _SamplesAcquired; iterSrc += 2, iterDest ++) {
                        sis8300llrfdrv_Qmn_2_double((uint32_t)(uint16_t)_rawDataI16[iterSrc],
                                sis8300llrfdrv_Qmn_down_mag_sample, &IVal);
                        // convert to degrees
                        QVal = ((epicsFloat64)(((epicsInt16)_rawDataI16[iterSrc + 1]) * ConvFactQAng)) * 180/M_PI;
                        IVal = m_CavChan->calibratePolyAndGrad(IVal);
                        _IMagData[iterDest] = IVal;
                        _QAngData[iterDest] = QVal;
                    }
                }
                else { //with polynomial calibration
                    for (iterSrc = 0, iterDest = 0; iterDest < _SamplesAcquired; iterSrc += 2, iterDest ++) {
                        sis8300llrfdrv_Qmn_2_double((uint32_t)(uint16_t)_rawDataI16[iterSrc],
                                sis8300llrfdrv_Qmn_down_mag_sample, &IVal);
                        QVal = ((epicsFloat64)(((epicsInt16)_rawDataI16[iterSrc + 1]) * ConvFactQAng)) * 180/M_PI;
                        IVal = m_CavChan->calibrateSanityCheck(IVal);
                        _IMagData[iterDest] = IVal;
                        _QAngData[iterDest] = QVal;
                    }
                }
                // Gradient or not we calculate cavity PU as long as calibration(polyfit) is enabled.
                IVal = 0.0;
                for (iterSrc = 0, iterDest = 0; iterDest < _SamplesAcquired; iterSrc += 2, iterDest ++) {
                    sis8300llrfdrv_Qmn_2_double((uint32_t)(uint16_t)_rawDataI16[iterSrc],
                            sis8300llrfdrv_Qmn_down_mag_sample, &IVal);
                     IVal = m_CavChan->calibrateSanityCheck(IVal),
                    _IMagDataW[iterDest] = (epicsFloat32)m_CavChan->calibrateCavityPu(IVal);
                }
            }
            else {// no calibration
                for (iterSrc = 0, iterDest = 0; iterDest < _SamplesAcquired; iterSrc += 2, iterDest ++) {
                    sis8300llrfdrv_Qmn_2_double((uint32_t)(uint16_t)_rawDataI16[iterSrc], 
                            sis8300llrfdrv_Qmn_down_mag_sample, &IVal);
                    _IMagData[iterDest] = IVal;
                    _IMagDataW[iterDest] = IVal;
                    _QAngData[iterDest] = ((epicsFloat64)(((epicsInt16)_rawDataI16[iterSrc + 1]) * ConvFactQAng)) * 180/M_PI;
                }
            }
            doCallbacksFloat64Array(_QAngData, (size_t)_SamplesAcquired, _interruptIdAUXQAng);

        break;
    }
    doCallbacksFloat64Array(_IMagData, (size_t)_SamplesAcquired, _interruptIdAUXIMag);
    // cavity PU data
    doCallbacksFloat32Array(_IMagDataW, (size_t)_SamplesAcquired, _interruptIdAUXIMagW);
    if (pm){
        //Push data for PM PVs
        doCallbacksFloat64Array(_IMagData, (size_t)_SamplesAcquired, _interruptIdAUXPMCmp0);
        doCallbacksFloat64Array(_QAngData, (size_t)_SamplesAcquired, _interruptIdAUXPMCmp1);
        doCallbacksFloat64Array(_XAxis, _SamplesAcquired, _interruptIdAUXPMXAxis);
    }

    return ndsSuccess;
}

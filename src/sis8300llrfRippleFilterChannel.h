/*
 * m-epics-sis8300llrf
 * Copyright (C) 2022 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfRippleFilterChannel.h
 * @brief Header file defining the LLRF Modulator ripple filter class
 * @author andreas.persson@ess.eu
 * @date 22.8.2022
 */

#ifndef _sis8300llrfRippleFilterChannel_h
#define _sis8300llrfRippleFilterChannel_h

#include "sis8300llrfChannel.h"

/**
 * @brief Ripple filter implementation of
 *        @see #sis8300llrfChannel Class
 */
class sis8300llrfRippleFilterChannel: public sis8300llrfChannel {
public:
    sis8300llrfRippleFilterChannel(epicsFloat64 FSampling, unsigned NearIQN);
    virtual ~sis8300llrfRippleFilterChannel();

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    virtual ndsStatus getConstantA3(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus getConstantA6(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus getConstantB1(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus getConstantB2(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus getConstantB3(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus getConstantB4(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus getConstantB5(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus getConstantB6(
                        asynUser *pasynUser, epicsFloat64 *value);
     virtual ndsStatus setZeroFrequency(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getZeroFrequency(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setZeroDamping(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getZeroDamping(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setPoleFrequency(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getPoleFrequency(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setPoleDamping(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getPoleDamping(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setEnable(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getEnable(
                        asynUser *pasynUser, epicsInt32 *value);

    virtual ndsStatus commitParameters();
    virtual ndsStatus updateFilter();

protected:

    /* for asynReasons */
    static std::string PV_REASON_CONST_A3;
    static std::string PV_REASON_CONST_A6;
    static std::string PV_REASON_CONST_B1;
    static std::string PV_REASON_CONST_B2;
    static std::string PV_REASON_CONST_B3;
    static std::string PV_REASON_CONST_B4;
    static std::string PV_REASON_CONST_B5;
    static std::string PV_REASON_CONST_B6;
    static std::string PV_REASON_FREQ_ZERO;
    static std::string PV_REASON_DAMP_ZERO;
    static std::string PV_REASON_FREQ_POLE;
    static std::string PV_REASON_DAMP_POLE;
    static std::string PV_REASON_EN;

    int _interruptIdFreqZero;
    int _interruptIdDampZero;
    int _interruptIdFreqPole;
    int _interruptIdDampPole;

    epicsFloat64 _ZeroFrequency;
    epicsFloat64 _ZeroDamping;
    epicsFloat64 _PoleFrequency;
    epicsFloat64 _PoleDamping;

    epicsInt32 _ZeroFrequencyChanged;
    epicsInt32 _ZeroDampingChanged;
    epicsInt32 _PoleFrequencyChanged;
    epicsInt32 _PoleDampingChanged;

    epicsFloat64 _FSampling;
    unsigned     _NearIQN;

    double checkRange(double v);

    /* parameter read/write */
    virtual int readParameter(int paramIdx, double *paramVal);
    virtual int writeParameter(int paramIdx, double *paramErr);
};

#endif /* _sis8300llrfRippleFilterChannel_h */

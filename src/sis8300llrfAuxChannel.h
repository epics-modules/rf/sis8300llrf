/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfAuxChannel.h
 * @brief Header file defining the LLRF Auxiliary channel class that handles
 * values for auxiliary channels (Downsampled or Internal) and their settings
 * @author gabriel.fedel@ess.eu
 * @date 8.4.2020
 */

#ifndef _sis8300llrfAuxChannel_h
#define _sis8300llrfAuxChannel_h

#include "sis8300llrfChannel.h"
#include "sis8300AIChannel.h"

//Maximum interval is 65 ms, if the combination of number of samples and
//decimation factor is > 65ms, the firmware will spend more time to acquire
//then expected and the system will have a inconsistent behavior
#define MAXINTERVAL 65
/**
 * @brief sis8300 LLRF specific nds::ADIOChannel class that supports
 *  Aux channel
 */
class sis8300llrfAuxChannel: public sis8300llrfChannel {
public:
    sis8300llrfAuxChannel(epicsFloat64 FSampling,epicsInt32 nearIqN, int downOrIntern);
    virtual ~sis8300llrfAuxChannel();

    virtual ndsStatus checkStatuses();

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    virtual ndsStatus setAUXEnable(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getAUXEnable(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus getAUXNSamplesAcq(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setAUXSamplesCount(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getAUXSamplesCount(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setAUXDecEnable(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getAUXDecEnable(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setAUXDecFactor(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getAUXDecFactor(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus getAUXXAxis(asynUser* pasynUser, epicsFloat64 *outArray, size_t nelem,  size_t *nIn);
    virtual ndsStatus setAUXEnableTransf(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getAUXEnableTransf(
                        asynUser *pasynUser, epicsInt32 *value);

    virtual ndsStatus commitParameters();

protected:
    int                 _DownOrIntern; /**< 0 for down-sampled and 1 for
                                           internal*/
    epicsInt32          _SamplesAcquired; /**< number of acquired samples*/
    epicsFloat64        _FSampling;
    epicsFloat64        *_XAxis;
    epicsInt32          _EnableTransf;
    epicsInt32          _NearIqN; /*< Near IQ N, used to calculate XAxis*/

    epicsInt32          _XAxisChanged;
    epicsFloat64        _LimitSamplesDec; /*< Value to limit number of samples and decimation factor*/

    unsigned _Channel;

    /* for asynReasons */
    static std::string PV_REASON_AUX_IMAG;
    static std::string PV_REASON_AUX_IMAGW;
    static std::string PV_REASON_AUX_QANG;
    static std::string PV_REASON_AUX_ENABLE;
    static std::string PV_REASON_AUX_NSAMPLES_ACQ;
    static std::string PV_REASON_AUX_SAMPLES_COUNT;
    static std::string PV_REASON_AUX_DEC_ENABLE;
    static std::string PV_REASON_AUX_DEC_FACTOR;
    static std::string PV_REASON_AUX_XAX;
    static std::string PV_REASON_AUX_ENABLE_TRANSF;
    static std::string PV_REASON_AUX_PM_CMP0;
    static std::string PV_REASON_AUX_PM_CMP1;
    static std::string PV_REASON_AUX_PM_XAXIS;

    int _interruptIdAUXIMag;
    int _interruptIdAUXIMagW;
    int _interruptIdAUXQAng;
    int _interruptIdAUXNSamplesAcq;
    int _interruptIdAUXXAxis;
    int _interruptIdAUXEnableTransf;
    int _interruptIdAUXPMCmp0;
    int _interruptIdAUXPMCmp1;
    int _interruptIdAUXPMXAxis;

    /* read/write parameter */
    virtual int readParameter(int paramIdx, double *paramVal);
    virtual int writeParameter(int paramIdx, double *paramErr);

    virtual bool checkPM();

    /* state transitions */
    virtual ndsStatus onEnterReset();
    virtual ndsStatus onLeaveProcessing(
                        nds::ChannelStates from, nds::ChannelStates to);
};

#endif /* _sis8300llrfAuxChannel_h */

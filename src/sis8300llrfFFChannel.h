/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfFFChannel.h
 * @brief Header file defining the LLRF FF channel class
 * @author mateusz.nabywaniec@ess.eu
 * @date 13.7.2020
 */

#ifndef _sis8300llrfFFWithCavityChannel_h
#define _sis8300llrfFFWithCavityChannel_h

#include "scalingSqrt.h"
#include "sis8300llrfControlTableChannel.h"
#include "sis8300llrfPIMagAngChannel.h"
/**
 * @brief sis8300 LLRF specific nds::ADIOChannel class from which
 *           all llrf specific channels are derived
 */
class sis8300llrfFFChannel: public sis8300llrfPIMagAngChannel {
public:
    sis8300llrfFFChannel(sis8300llrfPIChannel * PIChannels[2], sis8300llrfControlTableChannel * CtrlTableCh, sis8300llrfCalibrationProvider * calibrationProvider);

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    virtual ndsStatus setSqrtFitEnable(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setCalibEGU(asynUser *pasynUser, epicsFloat64 *value, size_t nelem);

    virtual double calibrateEGU2Raw(double val);
protected:
    scalingSqrt * calibratorSqrt; // used for output
    bool _useSqrtFit;
    static std::string PV_REASON_CALIB_SQRT_FIT_EN;
    int _interruptIdCalibSqrtFitEnable;

    virtual void reloadCalibPVs();
};

#endif /* _sis8300llrfFFChannel_h */

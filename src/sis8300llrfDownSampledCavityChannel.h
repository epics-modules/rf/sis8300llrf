/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfDownSampledCavityChannel.h
 * @brief Header file defining the LLRF DownSampled channel class that handles
 * downsampled values and their settings
 * @author gabriel.fedel@ess.eu
 * @date 31.3.2020
 */

#ifndef _sis8300llrfDownSampledCavityChannel_h
#define _sis8300llrfDownSampledCavityChannel_h

#include "sis8300llrfCavityChannel.h"
#include "sis8300llrfDownSampledChannel.h"
#include "sis8300llrfAuxChannel.h"
#include "sis8300llrfAIChannel.h"
#include "sis8300drv.h"

/**
 * @brief sis8300 LLRF specific nds::ADIOChannel class that supports 
 *  DownSampled channel
 */
class sis8300llrfDownSampledCavityChannel: public sis8300llrfDownSampledChannel {
public:
    sis8300llrfDownSampledCavityChannel(epicsFloat64 FSampling, epicsInt32 nearIqN, sis8300llrfCavityChannel * AIChan) :
        sis8300llrfDownSampledChannel(FSampling, nearIqN, AIChan) {
            m_CavChan = AIChan;
        }   ;
protected:
    virtual ndsStatus onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to);
    sis8300llrfCavityChannel * m_CavChan;
};

#endif /* _sis8300llrfDownSampledCavityChannel_h */

/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfPIErrorChannel.h
 * @brief Header file defining the LLRF PIError channel class that handles
 * internal values and their settings
 * @author gabriel.fedel@ess.eu
 * @date 9.4.2020
 */

#ifndef _sis8300llrfPIErrorChannel_h
#define _sis8300llrfPIErrorChannel_h

#include "sis8300llrfAuxChannel.h"
#include "sis8300AIChannel.h"
#include "sis8300llrfPIMagAngChannel.h"

/**
 * @brief sis8300 LLRF specific sis8300llrfAuxChannel class that supports 
 *  PIError channel
 */
class sis8300llrfPIErrorChannel: public sis8300llrfAuxChannel {
public:
    sis8300llrfPIErrorChannel(epicsFloat64 FSampling, epicsInt32 nearIqN, sis8300llrfPIMagAngChannel * SPChannel);

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    virtual ndsStatus setDAQFormat(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getDAQFormat(
                        asynUser *pasynUser, epicsInt32 *value);

protected:
    sis8300llrfPIMagAngChannel * _SPChannel;

    /*For asyn reasons*/
    static std::string PV_REASON_PIERROR_DAQ_FMT;

    /* state transitions */
    virtual ndsStatus onEnterReset();
    virtual ndsStatus onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to);
};

#endif /* _sis8300llrfPIErrorChannel_h */

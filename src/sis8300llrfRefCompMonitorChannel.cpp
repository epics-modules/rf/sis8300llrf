/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfRefCompMonitorChannelChannel.cpp
 * @brief Implementation of Monitoring for Reference Compensation
 * @author gabrielfedel gabriel.fedel@ess.eu
 * @date 20.05.2020
 * 
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfChannelGroup.h"
#include "sis8300llrfRefCompMonitorChannel.h"


std::string sis8300llrfRefCompMonitorChannel::
                    PV_REASON_CTRLIN_I_AVG        = "CtrlInIAvg";
std::string sis8300llrfRefCompMonitorChannel::
                    PV_REASON_CTRLIN_Q_AVG        = "CtrlInQAvg";
std::string sis8300llrfRefCompMonitorChannel::
                    PV_REASON_REF_I_AVG           = "RefIAvg";
std::string sis8300llrfRefCompMonitorChannel::
                    PV_REASON_REF_Q_AVG           = "RefQAvg";
std::string sis8300llrfRefCompMonitorChannel::
                    PV_REASON_CTRLIN_MAG_AVG      = "CtrlInMagAvg";
std::string sis8300llrfRefCompMonitorChannel::
                    PV_REASON_CTRLIN_ANG_AVG      = "CtrlInAngAvg";
std::string sis8300llrfRefCompMonitorChannel::
                    PV_REASON_REF_MAG_AVG         = "RefMagAvg";
std::string sis8300llrfRefCompMonitorChannel::
                    PV_REASON_REF_ANG_AVG         = "RefAngAvg";
std::string sis8300llrfRefCompMonitorChannel::
                    PV_REASON_AVG_POS             = "AveragePos";
std::string sis8300llrfRefCompMonitorChannel::
                    PV_REASON_AVG_WIDTH           = "AverageWidth";


/**
 * @brief sis8300llrfRefCompMonitorChannel constructor
 */
sis8300llrfRefCompMonitorChannel::sis8300llrfRefCompMonitorChannel() :
        sis8300llrfChannel(
            SIS8300LLRFDRV_REFCOMP_PARAM_NUM, 
            SIS8300LLRFDRV_REFCOMP_PARAM_INT_FIRST) {

    sprintf(_ChanStringIdentifier, "Reference Comp. Monit.");


    registerOnLeaveStateHandler(nds::CHANNEL_STATE_PROCESSING,
        boost::bind(&sis8300llrfRefCompMonitorChannel::onLeaveProcessing, 
        this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_RESETTING,
        boost::bind(&sis8300llrfRefCompMonitorChannel::onEnterReset, 
        this));
}
/**
 * @brief sis8300llrfRefCompMonitorChannel destructor
 */
sis8300llrfRefCompMonitorChannel::~sis8300llrfRefCompMonitorChannel() {}

/**
 * @brief Read out data that has to be updated after every pulse
 *
 * @param [in]  from    From channel state
 * @param [in]  to      To Channels state
 *
 * @return ndsSuccess   Always
 */
ndsStatus sis8300llrfRefCompMonitorChannel::onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    
    return checkStatuses();
}

/**
 * @see #sis8300llrfChannel::onEnterReset
 */
ndsStatus sis8300llrfRefCompMonitorChannel::onEnterReset() {
    
    if (sis8300llrfChannel::onEnterReset() != ndsSuccess) {
        return ndsError;
    }
    
    return checkStatuses();
}

/**
 * @see #sis8300llrfChannel::readParameter
 */
inline int sis8300llrfRefCompMonitorChannel::readParameter(
                int paramIdx, double *paramVal) {
    if ((sis8300llrfdrv_refcomp_param)paramIdx != refcomp_win_width)
        return sis8300llrfdrv_get_refcomp_param(
                _DeviceUser, (sis8300llrfdrv_refcomp_param) paramIdx, 
                paramVal);

    //if is Window width, there is a transformation
    int status;

    status = sis8300llrfdrv_get_refcomp_param(
                _DeviceUser, (sis8300llrfdrv_refcomp_param) paramIdx, 
                paramVal);
    if (!status)
        *paramVal = pow(2, *paramVal);

    return status;
}

/**
 * @see #sis8300llrfChannel::writeParameter
 */
inline int sis8300llrfRefCompMonitorChannel::writeParameter(
                int paramIdx, double *paramErr) {
    if ((sis8300llrfdrv_refcomp_param)paramIdx != refcomp_win_width)
        return sis8300llrfdrv_set_refcomp_param(
                    _DeviceUser, (sis8300llrfdrv_refcomp_param) paramIdx, 
                    _ParamVals[paramIdx], paramErr);

    //if is Window width, there is a transformation
    int status;
    unsigned val;

    val = (unsigned) ceil(log(_ParamVals[refcomp_win_width])/log(2));
    status = sis8300llrfdrv_set_refcomp_param(
                    _DeviceUser, (sis8300llrfdrv_refcomp_param) paramIdx, 
                    val, paramErr);
    
    return status;
}

/**
 * @brief All the statuses that need to be checked on leave processing
 *        and enter reset
 */
inline ndsStatus sis8300llrfRefCompMonitorChannel::checkStatuses() {
    NDS_TRC("%s", __func__);

    //Read I and Q at the same time
    if (updateCtrlInIQAvg() != ndsSuccess) {
        return ndsError;
    }

    //Read I and Q at the same time
    if (updateRefIQAvg() != ndsSuccess) {
        return ndsError;
    }
    
    return ndsSuccess;
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfRefCompMonitorChannel::registerHandlers(
                nds::PVContainers* pvContainers) {
    
    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRefCompMonitorChannel::PV_REASON_CTRLIN_I_AVG,
            &sis8300llrfRefCompMonitorChannel::setFloat64,
            &sis8300llrfRefCompMonitorChannel::getCtrlInIAvg,
            &_interruptIdCtrlInIAvg);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRefCompMonitorChannel::PV_REASON_CTRLIN_Q_AVG,
            &sis8300llrfRefCompMonitorChannel::setFloat64,
            &sis8300llrfRefCompMonitorChannel::getCtrlInQAvg,
            &_interruptIdCtrlInQAvg);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRefCompMonitorChannel::PV_REASON_REF_I_AVG,
            &sis8300llrfRefCompMonitorChannel::setFloat64,
            &sis8300llrfRefCompMonitorChannel::getRefIAvg,
            &_interruptIdRefIAvg);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRefCompMonitorChannel::PV_REASON_REF_Q_AVG,
            &sis8300llrfRefCompMonitorChannel::setFloat64,
            &sis8300llrfRefCompMonitorChannel::getRefQAvg,
            &_interruptIdRefQAvg);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRefCompMonitorChannel::PV_REASON_CTRLIN_MAG_AVG,
            &sis8300llrfRefCompMonitorChannel::setFloat64,
            &sis8300llrfRefCompMonitorChannel::getCtrlInMagAvg,
            &_interruptIdCtrlInMagAvg);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRefCompMonitorChannel::PV_REASON_CTRLIN_ANG_AVG,
            &sis8300llrfRefCompMonitorChannel::setFloat64,
            &sis8300llrfRefCompMonitorChannel::getCtrlInAngAvg,
            &_interruptIdCtrlInAngAvg);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRefCompMonitorChannel::PV_REASON_REF_MAG_AVG,
            &sis8300llrfRefCompMonitorChannel::setFloat64,
            &sis8300llrfRefCompMonitorChannel::getRefMagAvg,
            &_interruptIdRefMagAvg);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRefCompMonitorChannel::PV_REASON_REF_ANG_AVG,
            &sis8300llrfRefCompMonitorChannel::setFloat64,
            &sis8300llrfRefCompMonitorChannel::getRefAngAvg,
            &_interruptIdRefAngAvg);

    NDS_PV_REGISTER_INT32(
            sis8300llrfRefCompMonitorChannel::PV_REASON_AVG_POS,
            &sis8300llrfRefCompMonitorChannel::setAveragePos,
            &sis8300llrfRefCompMonitorChannel::getAveragePos,
            &_interruptIds[refcomp_win_pos]);

    NDS_PV_REGISTER_INT32(
            sis8300llrfRefCompMonitorChannel::PV_REASON_AVG_WIDTH,
            &sis8300llrfRefCompMonitorChannel::setAverageWidth,
            &sis8300llrfRefCompMonitorChannel::getAverageWidth,
            &_interruptIds[refcomp_win_width]);

    return sis8300llrfChannel::registerHandlers(pvContainers);
}


/**
 * @brief Update the average I and Q value for Controller Input
 * 
 * @param [out] i_val     Will hold the average i value on 
 *                          success
 * @param [out] q_val     Will hold the average q value on 
 *                          success
 * 
 * @return  ndsSuccess      Data retrieved successfully
 * @return  ndsError        If we are in 
 *                          IOC INIT STATE or if data could not be read 
 *                          from device.
 *
 * Read average i and q for Controller Input at the same time and update both 
 * PVs
 */
ndsStatus sis8300llrfRefCompMonitorChannel::updateCtrlInIQAvg() {
    NDS_TRC("%s", __func__);
    double i, q;
    int status;

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    status = sis8300llrfdrv_get_refcomp_iq_avg(
                _DeviceUser, 0, &i, &q);
    SIS8300NDS_STATUS_CHECK(
        "sis8300llrfdrv_get_refcomp_iq_avg", status);
  
    _CtrlInI = (epicsFloat64) i;
    _CtrlInQ = (epicsFloat64) q;

    //calculate mag/ang
    _CtrlInMag = sqrt(pow(_CtrlInI, 2) + pow(_CtrlInQ, 2));
    _CtrlInAng = atan2(_CtrlInQ, _CtrlInI);

    //update IQ values
    doCallbacksFloat64(_CtrlInI, _interruptIdCtrlInIAvg);
    doCallbacksFloat64(_CtrlInQ, _interruptIdCtrlInQAvg);

    //update mag/ang values
    doCallbacksFloat64(_CtrlInMag, _interruptIdCtrlInMagAvg);
    doCallbacksFloat64(_CtrlInAng, _interruptIdCtrlInAngAvg);

    return ndsSuccess;
}

/**
 * @brief Update the average I and Q value for Reference Line 
 * 
 * @param [out] i_val     Will hold the average i value on 
 *                          success
 * @param [out] q_val     Will hold the average q value on 
 *                          success
 * 
 * @return  ndsSuccess      Data retrieved successfully
 * @return  ndsError        If we are in 
 *                          IOC INIT STATE or if data could not be read 
 *                          from device.
 *
 * Update average i and q for Reference Line at the same time and update both
 * PVs
 */
ndsStatus sis8300llrfRefCompMonitorChannel::updateRefIQAvg() {
    NDS_TRC("%s", __func__);
    double i, q;
    int status;

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    status = sis8300llrfdrv_get_refcomp_iq_avg(
                _DeviceUser, 1, &i, &q);
    SIS8300NDS_STATUS_CHECK(
        "sis8300llrfdrv_get_refcomp_iq_avg", status);
  
    _RefI = (epicsFloat64) i;
    _RefQ = (epicsFloat64) q;

    //calculate mag/ang
    _RefMag = sqrt(pow(_RefI, 2) + pow(_RefQ, 2));
    _RefAng = atan2(_RefQ, _RefI);

    //update raw values
    doCallbacksFloat64(_RefI, _interruptIdRefIAvg);
    doCallbacksFloat64(_RefQ, _interruptIdRefQAvg);

    //update mag/ang values
    doCallbacksFloat64(_RefMag, _interruptIdRefMagAvg);
    doCallbacksFloat64(_RefAng, _interruptIdRefAngAvg);

    return ndsSuccess;
}


/**
 * @brief Get the average I for Controller Input
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the current magnitude value on 
 *                          success
 * 
 * @return  ndsSuccess      Data retrieved successfully
 * @return  ndsError        If we are in 
 *                          IOC INIT STATE or if data could not be read 
 *                          from device.
 */
ndsStatus sis8300llrfRefCompMonitorChannel::getCtrlInIAvg(
                asynUser *pasynUser,  epicsFloat64 *value) {
    NDS_TRC("%s", __func__);
    if (updateCtrlInIQAvg() != ndsSuccess)
        return ndsError;

    *value = _CtrlInI;

    return ndsSuccess;
}

/**
 * @brief Get the average Q for Controller Input
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the current magnitude value on 
 *                          success
 * 
 * @return  ndsSuccess      Data retrieved successfully
 * @return  ndsError        If we are in 
 *                          IOC INIT STATE or if data could not be read 
 *                          from device.
 */
ndsStatus sis8300llrfRefCompMonitorChannel::getCtrlInQAvg(
                asynUser *pasynUser,  epicsFloat64 *value) {
    NDS_TRC("%s", __func__);
    if (updateCtrlInIQAvg() != ndsSuccess)
        return ndsError;

    *value = _CtrlInQ;

    return ndsSuccess;
}

/**
 * @brief Get the average I for Reference Line
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the current magnitude value on 
 *                          success
 * 
 * @return  ndsSuccess      Data retrieved successfully
 * @return  ndsError        If we are in 
 *                          IOC INIT STATE or if data could not be read 
 *                          from device.
 */
ndsStatus sis8300llrfRefCompMonitorChannel::getRefIAvg(
                asynUser *pasynUser,  epicsFloat64 *value) {
    NDS_TRC("%s", __func__);
    if (updateRefIQAvg() != ndsSuccess)
        return ndsError;

    *value = _RefI;

    return ndsSuccess;
}

/**
 * @brief Get the average Q for Reference Line
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the current magnitude value on 
 *                          success
 * 
 * @return  ndsSuccess      Data retrieved successfully
 * @return  ndsError        If we are in 
 *                          IOC INIT STATE or if data could not be read 
 *                          from device.
 */
ndsStatus sis8300llrfRefCompMonitorChannel::getRefQAvg(
                asynUser *pasynUser,  epicsFloat64 *value) {
    NDS_TRC("%s", __func__);
    if (updateRefIQAvg() != ndsSuccess)
        return ndsError;

    *value = _RefQ;

    return ndsSuccess;
}


/**
 * @brief Get the average Magnitude for Controller Input
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the current magnitude value on 
 *                          success
 * 
 * @return  ndsSuccess      Data retrieved successfully
 * @return  ndsError        If we are in 
 *                          IOC INIT STATE or if data could not be read 
 *                          from device.
 */
ndsStatus sis8300llrfRefCompMonitorChannel::getCtrlInMagAvg(
                asynUser *pasynUser,  epicsFloat64 *value) {
    NDS_TRC("%s", __func__);
    if (updateCtrlInIQAvg() != ndsSuccess)
        return ndsError;

    *value = _CtrlInMag;

    return ndsSuccess;
}

/**
 * @brief Get the average Angle for Controller Input
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the current magnitude value on 
 *                          success
 * 
 * @return  ndsSuccess      Data retrieved successfully
 * @return  ndsError        If we are in 
 *                          IOC INIT STATE or if data could not be read 
 *                          from device.
 */
ndsStatus sis8300llrfRefCompMonitorChannel::getCtrlInAngAvg(
                asynUser *pasynUser,  epicsFloat64 *value) {
    NDS_TRC("%s", __func__);
    if (updateCtrlInIQAvg() != ndsSuccess)
        return ndsError;

    *value = _CtrlInAng;

    return ndsSuccess;
}

/**
 * @brief Get the average Magnitude for Reference Line
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the current magnitude value on 
 *                          success
 * 
 * @return  ndsSuccess      Data retrieved successfully
 * @return  ndsError        If we are in 
 *                          IOC INIT STATE or if data could not be read 
 *                          from device.
 */
ndsStatus sis8300llrfRefCompMonitorChannel::getRefMagAvg(
                asynUser *pasynUser,  epicsFloat64 *value) {
    NDS_TRC("%s", __func__);
    if (updateRefIQAvg() != ndsSuccess)
        return ndsError;

    *value = _RefMag;

    return ndsSuccess;
}

/**
 * @brief Get the average Angle for Reference Line
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the current magnitude value on 
 *                          success
 * 
 * @return  ndsSuccess      Data retrieved successfully
 * @return  ndsError        If we are in 
 *                          IOC INIT STATE or if data could not be read 
 *                          from device.
 */
ndsStatus sis8300llrfRefCompMonitorChannel::getRefAngAvg(
                asynUser *pasynUser,  epicsFloat64 *value) {
    NDS_TRC("%s", __func__);
    if (updateRefIQAvg() != ndsSuccess)
        return ndsError;

    *value = _RefAng;

    return ndsSuccess;
}



/* === GETTERS AND SETTERS FOR CONTROLLER SETTINGS === */
/*
 * The getters are meant to be used by records that will keep track of 
 * setup used in the pulse that just passed. These records should be 
 * set to process at IO interrupt, because all the callbacks will get 
 * called when receiving the PULSE_DONE interrupt.
 * */

/**
 * @brief Set position of the average window
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the position of the average window to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfRefCompMonitorChannel::setAveragePos(
                asynUser *pasynUser, epicsInt32 value) {
    if (value < 0) {
        NDS_ERR("Invalid value for Reference Compensation Average Position");
        return ndsError;
    }

    _ParamVals[refcomp_win_pos] = (unsigned) value;
    _ParamChanges[refcomp_win_pos] = 1;

    return commitParameters();
}
/**
 * @brief Get position of the average window
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the position of the average window on 
 *                          success
 * 
 * @return ndsSuccess  Success
 * @return ndsError    If a call is made during IOC INIT to prevent 
 *                     wrong settings 
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfRefCompMonitorChannel::getAveragePos(
                asynUser *pasynUser,  epicsInt32 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[refcomp_win_pos];
    return ndsSuccess;
}

/**
 * @brief Set width of the average window
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the width of the average window to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfRefCompMonitorChannel::setAverageWidth(
                asynUser *pasynUser, epicsInt32 value) {
    if (value < 1 || value > SIS8300LLRFDRV_MON_AVGWIDTH_MAX) {
        NDS_ERR("Invalid value for Reference Compensantion Average Width");
        return ndsError;
    }
    
    _ParamVals[refcomp_win_width] = (unsigned) value;
    _ParamChanges[refcomp_win_width] = 1;

    return commitParameters();
}
/**
 * @brief Get width of the average window
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the position of the average window on 
 *                          success
 * 
 * @return ndsSuccess  Success
 * @return ndsError    If a call is made during IOC INIT to prevent 
 *                     wrong settings 
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfRefCompMonitorChannel::getAverageWidth(
                asynUser *pasynUser,  epicsInt32 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[refcomp_win_width];
    return ndsSuccess;
}

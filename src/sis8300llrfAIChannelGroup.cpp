/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfAIChannelGroup.cpp
 * @brief Implementation of LLRF analog input channel group in NDS.
 * @author urojec
 * @date 30.5.2014
 */

#include <math.h>

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfAIChannel.h"
#include "sis8300llrfAIChannelGroup.h"


std::string sis8300llrfAIChannelGroup::PV_REASON_CTRL_INPUT_SEL  = "CtrlInputSel";

/**
 * @brief AI ChannelGroup constructor.
 * @param [in] name Channel Group name.
 *
 * Register state transition handlers and message handlers. For details
 * refer to NDS documentation.
 */
sis8300llrfAIChannelGroup::sis8300llrfAIChannelGroup(
    const std::string& name) : sis8300AIChannelGroup(name) {

    registerOnEnterStateHandler(nds::CHANNEL_STATE_PROCESSING,
        boost::bind(&sis8300llrfAIChannelGroup::onEnterProcessing, 
        this, _1, _2));


    /* Trigger delay is not supported. Also, by setting this to zero we 
     * don't need to override the Samples count function
     */
    _TriggerDelay = 0;
    _TriggerRepeat = 0;
    _isEnabled = 1;
    _ClockSource = (epicsInt32) clk_src_internal;
    _ClockFrequency = (epicsFloat64) SIS8300LLRFNDS_MAX_CLK_FREQUENCY;

    _TriggerConditionChanged = 0;
    _TriggerDelayChanged = 0;
    _TriggerRepeatChanged = 0;

    _CtrlInputSel = 0;
    _CtrlInputSelChanged = 1;

    _UpdateReason = 0x0;
}

sis8300llrfAIChannelGroup::~sis8300llrfAIChannelGroup() {}

/**
 * @brief Override parent, because the CG is not responsible for
 * arming the board and
 */
ndsStatus sis8300llrfAIChannelGroup::onSwitchProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);
    _DaqFinished = 0;
    return ndsSuccess;
}

ndsStatus sis8300llrfAIChannelGroup::onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);
    _DaqFinished = 1;

    _PerfTiming = sis8300AIChannelGroup::timemsec();

    return ndsSuccess;
}

/**
 * @brief State handler for transition to ERROR.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Override parent because it puts the device in ERROR
 * and we need to keep control of state transitions of
 * the device from the device class.
 */
ndsStatus sis8300llrfAIChannelGroup::onEnterError(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);
    
    return ndsSuccess;
}

/**
 * @brief State handler for entering PROCESSING state
 *
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Reset the changes paramter, and start tracking them from scratch
 */
ndsStatus sis8300llrfAIChannelGroup::onEnterProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    _UpdateReason = 0x0;

    return ndsSuccess;
}


/**
 * @brief see sis8300AIChannelGroup
 */
ndsStatus sis8300llrfAIChannelGroup::markAllParametersChanged() {

    ndsStatus status = sis8300AIChannelGroup::markAllParametersChanged();
    NDS_TRC("%s", __func__);
    _TriggerConditionChanged = 0;
    _TriggerDelayChanged = 0;
    _TriggerRepeatChanged = 0;

    _CtrlInputSelChanged = 1;

    return status;
}

/**
 * @brief Check if paramters for any of the channels in the cg have 
 *        changed
 *
 * @return The current update reason 
 * 
 * Update reason is used to inform the controller if it needs to
 * load new values from shadow registers or relaod the control tables.
 * Every channel group holds the update reason for the channels it
 * contains. Channels report their changes to the channel group and
 * than the device ( @see #sis8300llrfDevice ) queries the channel group 
 * for the update reason.
 */
unsigned sis8300llrfAIChannelGroup::getUpdateReason() {
    return _UpdateReason;
}

/**
 * @brief Set a specific update reason
 */
void sis8300llrfAIChannelGroup::setUpdateReason(unsigned reason) {
    _UpdateReason |= reason;
}


ndsStatus sis8300llrfAIChannelGroup::commitParameters(){
    NDS_TRC("%s", __func__);
    if ( getCurrentState() != nds::CHANNEL_STATE_DISABLED  ||
         (_device->getCurrentState() != nds::DEVICE_STATE_INIT &&
          _device->getCurrentState() != nds::DEVICE_STATE_ON)) {
        
        NDS_DBG("Sis8300llrfAiChannelGRoup not committing parameters.");
        return ndsSuccess;
    }

    if (_CtrlInputSelChanged) {
        double param_err, param_val;
        int status;
        
        status = sis8300llrfdrv_set_ctrl_input_sel(_DeviceUser, (double) _CtrlInputSel, &param_err);
        SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_set_ctrl_input_sel", status);

        status = sis8300llrfdrv_get_ctrl_input_sel(_DeviceUser, &param_val);
        SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_ctrl_input_sel", status);
    
        if (_CtrlInputSel == (int) param_val){
            doCallbacksInt32(_CtrlInputSel, _interruptIdCtrlInputSel);
            _CtrlInputSelChanged = 0;
            setUpdateReason(SIS8300LLRFDRV_UPDATE_REASON_NEW_PARAMS);
        }
        else
            NDS_ERR("Error writing CtrlInputSel. Read value != Written value");
    }
    return sis8300AIChannelGroup::commitParameters();
}

/* ==================== GETTERS, SETTERS, REGISTER HANDLERS ==================== */
/**
 * @brief Registers handlers for interfacing with records. For more information,
 * refer to NDS documentation.
 */
ndsStatus sis8300llrfAIChannelGroup::registerHandlers(nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_INT32(
            sis8300llrfAIChannelGroup::PV_REASON_CTRL_INPUT_SEL,
            &sis8300llrfAIChannelGroup::setCtrlInputSel,
            &sis8300llrfAIChannelGroup::getInt32,
            &_interruptIdCtrlInputSel);

    return sis8300AIChannelGroup::registerHandlers(pvContainers);
}



/**
 * @brief Set clock source
 *
 * @param [in]  pasynUser Asyn user struct.
 * @param [in]  value     Clock source
 *
 * @return ndsSuccess set successful
 * @return ndsError   set ailed
 *
 * Overrides parent to prevent changing the clock source while the 
 * device is in on state.
 */
ndsStatus sis8300llrfAIChannelGroup::setClockSource(
                asynUser* pasynUser, epicsInt32 value) {

    NDS_TRC("%s", __func__);
    if (_device->getCurrentState() == nds::DEVICE_STATE_ON) {
        NDS_ERR("Clock source cannot be changed while"
                "device is in on state");
        return ndsError;
    }

    return sis8300AIChannelGroup::setClockSource(pasynUser, value);
}

/**
 * @brief Set clock frequency
 *
 * @param [in]  pasynUser Asyn user struct.
 * @param [in]  value     Frequency
 *
 * @return ndsSuccess set successful
 * @return ndsError   set failed
 *
 * Overrides parent to prevent changing the frequency while the device 
 * is in on state.
 */
ndsStatus sis8300llrfAIChannelGroup::setClockFrequency(
                asynUser* pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);
    if (_device->getCurrentState() == nds::DEVICE_STATE_ON) {
        NDS_ERR("Clock frequency cannot be changed while device"
                "is in on state");
        return ndsError;
    }

    return sis8300AIChannelGroup::setClockFrequency(pasynUser, value);
}

/**
 * @brief Set clock divisor
 *
 * @param [in]  pasynUser Asyn user struct.
 * @param [in]  value     Clock divisor
 *
 * @return ndsSuccess set successful
 * @return ndsError   set failed
 *
 * Overrides parent to prevent changing the clock divisor while the 
 * device is in on state.
 */
ndsStatus sis8300llrfAIChannelGroup::setClockDivisor(
                asynUser* pasynUser, epicsInt32 value) {

    NDS_TRC("%s", __func__);
    if (_device->getCurrentState() == nds::DEVICE_STATE_ON) {
        NDS_ERR("Clock divisor cannot be changed while device "
                "is in on state");
        return ndsError;
    }

    return sis8300AIChannelGroup::setClockDivisor(pasynUser, value);
}

/**
 * @brief Overriden from parent (generic sis8300 module), 
 * setting is not supported
 */
ndsStatus sis8300llrfAIChannelGroup::setTrigger(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);
    return ndsError;
}
/**
 * @brief Overriden from parent (generic sis8300 module), 
 * setting is not supported
 */
ndsStatus sis8300llrfAIChannelGroup::setTriggerCondition(
                asynUser *pasynUser, const char *data, 
                size_t numchars, size_t *nbytesTransferead) {
    NDS_TRC("%s", __func__);
    return ndsError;
}
/**
 * @brief Overriden from parent (generic sis8300 module), 
 * setting is not supported
 */
ndsStatus sis8300llrfAIChannelGroup::onTriggerConditionParsed(
                asynUser *pasynUser, const nds::Trigger& trigger) {
    NDS_TRC("%s", __func__);
    return ndsError;
}
/**
 * @brief Overriden from parent (generic sis8300 module), 
 * setting is not supported
 */
ndsStatus sis8300llrfAIChannelGroup::setTriggerDelay(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);
    return ndsError;
}
/**
 * @brief Overriden from parent (generic sis8300 module), 
 * setting is not supported
 */
ndsStatus sis8300llrfAIChannelGroup::setTriggerRepeat(
                asynUser* pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);
    return ndsError;
}
/**
 * @brief Overriden from parent (generic sis8300 module), 
 * setting is not supported
 */
ndsStatus sis8300llrfAIChannelGroup::setEnabled(
                asynUser* pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);
    return ndsError;
}
/**
 * @brief Overriden from parent (generic sis8300 module), 
 * setting is not supported
 */
ndsStatus sis8300llrfAIChannelGroup::handleStartMsg(
                asynUser *pasynUser, const nds::Message& value) {
    NDS_TRC("%s", __func__);
    return ndsError;
}
/**
 * @brief Overriden from parent (generic sis8300 module), 
 * setting is not supported
 */
ndsStatus sis8300llrfAIChannelGroup::handleStopMsg(
                asynUser *pasynUser, const nds::Message& value) {
    NDS_TRC("%s", __func__);
    return ndsError;
}


/**
 * @brief Set the number of samples to acquire
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Number of samples to acquire
 * 
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfAIChannelGroup::setCtrlInputSel(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _CtrlInputSel = value;
    _CtrlInputSelChanged = 1;

    return commitParameters();
}



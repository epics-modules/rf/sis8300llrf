/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfChannel.cpp
 * @brief Implementation of LLRF specific channel in NDS.
 * @author urojec
 * @date 5.6.2014
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfChannel.h"
#include "sis8300llrfChannelGroup.h"
#include "sis8300llrfDevice.h"


/**
 * @brief llrf channel constructor.
 *
 * @param [in] paramNum         Number of parameters/settings that this 
 *                              channel supports
 * @param [in] intParamFirst    Index of the first intiger param
 *
 * This is tightly linked to sis8300llrfdrv library, where settings are 
 * grouped together (for example PI controller settings, VM settings). 
 * There is an enumerator available for each group and integer paramters 
 * are always stored at the end of the list.
 */
sis8300llrfChannel::sis8300llrfChannel(int paramNum, int intParamFirst)
    : nds::ADIOChannel() {
    NDS_TRC("%s", __func__);
    int i;

    _ParamNum = paramNum;
    _IntParamFirst = intParamFirst;

    if (_ParamNum == 0) {
        _ParamVals = NULL;
        _ParamChanges = NULL;
        _interruptIds = NULL;
    }
    else {
        _ParamVals    = (double *) malloc(sizeof(double) * (size_t) _ParamNum);
        _ParamChanges = (int *)    malloc(sizeof(int)    * (size_t) _ParamNum);
        _interruptIds = (int *)    malloc(sizeof(int)    * (size_t) _ParamNum);
    }

    for (i = 0; i < _ParamNum; i++) {
        _ParamChanges[i] = 0;
        _ParamVals[i]    = 0.0;
    }

    registerOnEnterStateHandler(nds::CHANNEL_STATE_DISABLED,
        boost::bind(&sis8300llrfChannel::onEnterDisabled, this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_RESETTING,
        boost::bind(&sis8300llrfChannel::onEnterReset, this));
            
    registerOnEnterStateHandler(nds::CHANNEL_STATE_ERROR,
        boost::bind(&sis8300llrfChannel::onEnterError, this, _1, _2));

    _isEnabled = 1;
    _DeviceUser = NULL;
    _Initialized = 0;
}

/**
 * @brief LLRF channel destructor.
 *
 * Free channel data buffer.
 */
sis8300llrfChannel::~sis8300llrfChannel() {
    freeParamBuffers();
}

void sis8300llrfChannel::freeParamBuffers() {

    NDS_TRC("%s", __func__);
    if (_ParamVals) {
        free(_ParamVals);
    }

    if (_ParamChanges) {
        free(_ParamChanges);
    }

    if (_interruptIds) {
        free(_interruptIds);
    }
}

/**
 * @brief Remember device context for the channel when it is registered.
 *
 */
void sis8300llrfChannel::onRegistered() {
    NDS_TRC("%s", __func__);
    _DeviceUser = 
        dynamic_cast<sis8300llrfChannelGroup *>
        (getChannelGroup())->getDeviceUser();
}

/**
 * @brief initialize the channel
 *
 * @return ndsSuccess always
 *
 * This is used when channel needs initial information from the
 * hw, so it cannot be done at IOC_INIT, because the device is not
 * yet open.
 *
 * This function is meant to be overridden by derived classes.
 */
ndsStatus sis8300llrfChannel::initialize() {
    NDS_TRC("%s", __func__);

    _Initialized = 1;

    markAllParametersChanged();

    return  commitParameters();
}

/**
 * @brief State handler for transition to DISABLED.
 *
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Write all new values to the controller
 */
ndsStatus sis8300llrfChannel::onEnterDisabled(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    if (from == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsSuccess;
    }

    return commitParameters();
}

/**
 * @brief State handler for transition to RESET.
 *
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Put the channel group to RESET state and back to DISABLED
 * if _AutoReset is enabled
 */
ndsStatus sis8300llrfChannel::onEnterReset() {
    NDS_TRC("%s", __func__);

    if (readParameters() != ndsSuccess) {
        return ndsError;
    }

    if (markAllParametersChanged() != ndsSuccess) {
        return ndsError;
    }
    
    return ndsSuccess;
}

/**
 * @brief State handler for transition to ERROR.
 *
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Put the channel group to ERROR state.
 */
ndsStatus sis8300llrfChannel::onEnterError(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s %i", __func__, getChannelNumber());

    nds::ChannelGroup   *cg;

    cg = getChannelGroup();


    if (cg->getCurrentState() != nds::CHANNEL_STATE_ERROR) {
        NDS_DBG("Putting %s channel group into ERROR state.", 
            cg->getName().c_str());
        cg->error();
    }

    return ndsSuccess;
}

/** 
 * @brief Check if the CG and if device is in INIT or ON 
 *        before writing to device 
 * 
 * this function is used in normal operating mode - it is called
 * whenever a parameter changes or when the CH transitions to 
 * disabled state.
 */
ndsStatus sis8300llrfChannel::commitParameters() {
    
    NDS_TRC("%s", __func__);
    sis8300llrfChannelGroup *cg = 
            dynamic_cast<sis8300llrfChannelGroup*> (getChannelGroup());
    
    if ( !_Initialized ||
         cg->getCurrentState() != nds::CHANNEL_STATE_DISABLED  ||
         (_device->getCurrentState() != nds::DEVICE_STATE_INIT &&
          _device->getCurrentState() != nds::DEVICE_STATE_ON)) {
        
        NDS_DBG("%s not committing parameters.", _ChanStringIdentifier);
        return ndsSuccess;
    }

    return writeToHardware();
}
/** 
 * @brief Check if device is in INIT or ON 
 *        before writing to device.
 * 
 * This function is used during special operating modes to force a 
 * write of new values to hardware even if the channel is not disabled.
 * This is necessary for operating in CW mode.
 * 
 * @warning The function should not be used in normal operating modes
 */
ndsStatus sis8300llrfChannel::forceCommitParameters() {
    
    NDS_TRC("%s", __func__);
    if ( !_Initialized ||
         (_device->getCurrentState() != nds::DEVICE_STATE_INIT &&
          _device->getCurrentState() != nds::DEVICE_STATE_ON)) {
        
        NDS_DBG("%s not committing parameters.", _ChanStringIdentifier);
        return ndsSuccess;
    }
    
    return writeToHardware();
}

/**
 * @brief Write new values to hardware
 *
 * @return ndsSuccess always
 *
 * Write new values to hardware
 * This is usually done after a new paramter value is set (trough a 
 * setter) or when the CH enters the DISABLED state. The function checks 
 * is any of the parmetrs have changed at all before writing to hw and 
 * calling callbacks. It can be used in combination with 
 * @see #markAllPArmatersChanged to force a rewrite of all the values to
 * the hw.
 * 
 * @warning This function should not be called directly. It is only
 * called from #commitParameters or #forceCommitParameters
 */
ndsStatus sis8300llrfChannel::writeToHardware() {
    int status, i, paramUpdate;
    double err, paramRbv;
    
    NDS_TRC("%s", __func__);
    sis8300llrfChannelGroup *cg = 
            dynamic_cast<sis8300llrfChannelGroup*> (getChannelGroup());
    
    ndsStatus statusCombined = ndsSuccess;

    paramUpdate = 0;
    for (i = 0; i < (int)_ParamNum; i++) {

        if (_ParamChanges[i]) {
            status = writeParameter(i, &err);
            // We don't want to go to error whent the argument is out of range
            // Instead notify with a warn message
            if (status == status_argument_range)
                NDS_WRN("%s %i: Param %i, val write %f - err %f - argument out\
                        of range", _ChanStringIdentifier, getChannelNumber(), 
                        i, _ParamVals[i], err);
            else
                SIS8300NDS_STATUS_ASSERT("writeParameter", status);

            //_ParamVals[i] -= err;
            status = readParameter(i, &paramRbv);
            SIS8300NDS_STATUS_ASSERT("readParameter", status);

            if (i < _IntParamFirst) {
                doCallbacksFloat64(
                    (epicsFloat64) paramRbv, _interruptIds[i]);
            }
            else {
                doCallbacksInt32(
                    (epicsInt32) paramRbv, _interruptIds[i]);
            }
            
            if (paramRbv != _ParamVals[i] - err) {
                SIS8300NDS_MSGERR("Val write != val read");
		//Changed from NDS_ERR to NDS_DGB on readback error as SIS8300 KU firmware does not provide readback of all values.
                NDS_DBG("%s %i: Param %i, val write %f - err %f != "
                    " val read %f", _ChanStringIdentifier, 
                    getChannelNumber(), i, _ParamVals[i], err, paramRbv);
                
                continue;
            }

			/* only if the write was successfull */
	        _ParamChanges[i] = 0;
	        paramUpdate = 1;
			
            NDS_DBG("Updating %s param %i: val=%f, err: %f", 
                _ChanStringIdentifier, i, _ParamVals[i], err);
        }
    }

    if (paramUpdate) {
        //cg->lock();
        cg->setUpdateReason(SIS8300LLRFDRV_UPDATE_REASON_NEW_PARAMS);
        //cg->unlock();
    }

    return statusCombined;
}

/**
 * @brief Read all the paramters from the controller
 *
 * This is typically done when the CG leaves processing after
 * every PULSE_DONE interrupt from the cotroller. This will
 * read all the wwaveforms and values taht change on
 * pulse-2-pulse basis. It will not read settings such as paramters,
 * since they are user input
 *
 * Deriving classes are meant to override this to read parameters
 * that they are responsible for
 */
ndsStatus sis8300llrfChannel::readParameters() {
    NDS_TRC("%s", __func__);
    int status, i;
    double doubleRegVal;

    for (i = 0; i < (int)_ParamNum; i++) {

        status = readParameter(i, &doubleRegVal);
        SIS8300NDS_STATUS_ASSERT("readParameter", status);

        if (i < _IntParamFirst) {
            doCallbacksFloat64(
                (epicsFloat64) doubleRegVal, _interruptIds[i]);
        }
        else {
            doCallbacksInt32(
                (epicsInt32) doubleRegVal, _interruptIds[i]);
        }
    }

    return ndsSuccess;
}

/**
 * @brief Mark all paramters as changed, so that they will get written
 *        on the next call to commitParamters (usually when enetering 
 *        DISABLED state)
 *
 * @return ndsSuccess always
 */
ndsStatus sis8300llrfChannel::markAllParametersChanged() {
    int i;

    for (i = 0; i < _ParamNum; i++) {
        _ParamChanges[i] = 1;
    }

    return ndsSuccess;
}

/**
 * @brief Function should be overrdien by every child class to call
 *        the correct getter function from the library
 *        ( @see #sis8300llrfdrv )
 * 
 * @param [in]  paramIdx    Index of the parameter to get
 * @param [in]  paramVal    Will hold the parameter value on success
 * 
 * @return 0    Always
 * 
 * This is basically a wrapper function to unify the calls to get 
 * parameter functions on the device. There are several groups of 
 * parameter settings, each group is represented by a channel.
 */
inline int sis8300llrfChannel::readParameter(
                int paramIdx, double *paramVal) {return 0;}
/**
 * @brief Function should be overrdien by every child class to call
 *        the correct setter function from the library
 *        ( @see #sis8300llrfdrv )
 * 
 * @param [in]  paramIdx    Index of the parameter to write
 * @param [in]  paramVal    Will hold the conversion error when 
 *                          parameter is converted from double to fixed 
 *                          point representation accepted by the device
 * 
 * @return 0    Always
 * 
 * This is basically a wrapper function to unify the calls to set 
 * parameter functions on the device. There are several groups of 
 * parameter settings, each group is represented by a channel.
 */
inline int sis8300llrfChannel::writeParameter(
                int paramIdx, double *paramErr)  {return 0;}

/**
 * @brief Override from parent, because setting is unsupported,
 *        LLRF channels represent controller settings and are 
 *        always enabled.
 */
ndsStatus sis8300llrfChannel::setEnabled(
                asynUser* pasynUser, epicsInt32 value) {
    NDS_ERR("Set enabled is not allowed for sis8300llrfChannel."
        "Always enabled.");
    return ndsError;
}
/**
 * @brief Override from parent, because action is unsupported
 * 
 * State Transitions of the LLRF specific channels are handled 
 * by the device ( @see #sis8300llrfDevice ) and can not be
 * manyally requested by user.
 */
ndsStatus sis8300llrfChannel::handleOnMsg(
                asynUser *pasynUser, const nds::Message& value) {
    NDS_ERR("LLRF channel cannot be started manually.");
    return ndsError;
}
/**
 * @brief Override from parent, because action is unsupported
 * 
 * State Transitions of the LLRF specific channels are handled 
 * by the device ( @see #sis8300llrfDevice ) and can not be
 * manyally requested by user.
 */
ndsStatus sis8300llrfChannel::handleOffMsg(
                asynUser *pasynUser, const nds::Message& value) {
    NDS_ERR("LLRF channel cannot be stopped manually.");
    return ndsError;
}
/**
 * @brief Override from parent, because action is unsupported
 * 
 * State Transitions of the LLRF specific channels are handled 
 * by the device ( @see #sis8300llrfDevice ) and can not be
 * manyally requested by user.
 */
ndsStatus sis8300llrfChannel::handleResetMsg(
                asynUser *pasynUser, const nds::Message& value) {
    NDS_ERR("LLRF channel cannot be reseted manually.");
    return ndsError;
}


/**
 * @brief Method to update values on filter channels
 * 
 * This method will update the filter constants, on filter channels. 
 * It is used when F_Sampling (from sis8300llrfControllerChannelGroup)
 * or NEAR_IQ_N (from sis8300llrfIQSamplingChannel) changes and it is necessary
 * to update the filteres.
 * For channels that is not filter it always return success.
 */
ndsStatus sis8300llrfChannel::updateFilter() {
    return ndsSuccess;
}




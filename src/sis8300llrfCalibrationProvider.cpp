/*
 * sis8300llrf
 * Copyright (C) 2023 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfCalibrationProvider.cpp
 * @brief Implementation of LLRF Calibration Provider class which provides
 * calibration from EGU to Raw and from Raw to EGU.
 * @author mateusz.nabywaniec@ess.eu
 * @date 27.06.2023
 * 
 */

#include "sis8300llrfCalibrationProvider.h"

sis8300llrfCalibrationProvider::sis8300llrfCalibrationProvider(){
    calibratorEGU2Raw = new scalingPoly(maxCalibCoef);
    calibratorRaw2EGU = new scalingPoly(maxCalibCoef);
};

sis8300llrfCalibrationProvider::~sis8300llrfCalibrationProvider(){
    delete calibratorEGU2Raw;
    delete calibratorRaw2EGU;
};


void sis8300llrfCalibrationProvider::setOrderEGU2Raw(int order){
    calibratorEGU2Raw -> setOrder(order);
};


void sis8300llrfCalibrationProvider::setOrderRaw2EGU(int order){
    calibratorRaw2EGU -> setOrder(order);
};


bool sis8300llrfCalibrationProvider::isCalibratorEGU2RawEnabled(){
    return calibratorEGU2Raw->isEnabled();
};


bool sis8300llrfCalibrationProvider::isCalibratorRaw2EGUEnabled(){
    return calibratorRaw2EGU->isEnabled();
};


void sis8300llrfCalibrationProvider::setCalibrationEGU2RawEnable(bool value){
   return calibratorEGU2Raw->setEnable(value);
};


void sis8300llrfCalibrationProvider::setCalibrationRaw2EGUEnable(bool value){
    return calibratorRaw2EGU->setEnable(value);
};


bool sis8300llrfCalibrationProvider::isCalibratorEGU2RawValid(){
    return calibratorEGU2Raw->isValid();
};


bool sis8300llrfCalibrationProvider::isCalibratorRaw2EGUValid(){
    return calibratorRaw2EGU->isValid();
};


scalingPoly* sis8300llrfCalibrationProvider::getCalibratorEGU2Raw() {
    return calibratorEGU2Raw;
};


scalingPoly* sis8300llrfCalibrationProvider::getCalibratorRaw2EGU() {
    return calibratorRaw2EGU;
};

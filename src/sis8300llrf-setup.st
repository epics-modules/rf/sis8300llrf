program sis8300llrf_setup

option +r;
option +w;

int DEVICE_STATE_INIT      = 3;
int DEVICE_STATE_ERROR     = 5;
int DEVICE_STATE_RESETTING = 7;

int setupActive, deviceState, signalActive, setupStart;
assign setupActive  to "{PREFIX}:SETUP-ACT";
assign deviceState  to "{PREFIX}";
assign signalActive to "{PREFIX}:SIGNALACT";
assign setupStart   to "{PREFIX}:SETUP-START";
monitor deviceState; monitor setupStart; monitor signalActive; 

string opmode, messages, forcetrigg;
assign messages   to "{PREFIX}:SMSGS";
assign opmode     to "{PREFIX}:OPMODE";
assign forcetrigg to "{PREFIX}:FORCETRIGG";

/* initialization of the calibration process */
int cardInitParams[7] = {0, 0, 0, 1, 1, 1, 1};
int cardInitParamsNum = 7;
int i;
assign cardInitParams to {
    "{PREFIX}:VM-INVIEN", 
    "{PREFIX}:VM-INVQEN", 
    "{PREFIX}:VM-SWAPIQEN",
    "{PREFIX}:PI-I-FIXEDSPEN", 
    "{PREFIX}:PI-Q-FIXEDSPEN", 
    "{PREFIX}:VM-MAGLIMEN", 
    "{PREFIX}:IQSMPL-ANGOFFSETEN"};

    
ss calibration {

    state WAITING {
        when (setupStart) {} state RESET_CARD
    }
    
    state RESET_CARD {
        entry {       
            strcpy(messages, "RESET");
            pvPut(messages);
        }
        
        when (deviceState == DEVICE_STATE_RESETTING) {} state INIT_CARD
    }
    
    state INIT_CARD {
        entry {
            strcpy(messages, "INIT");
            pvPut(messages);
        }
        
        /* user just started the calibration */
        when (deviceState == DEVICE_STATE_INIT && setupStart) {
        
            strcpy(opmode, "NORMAL");
            pvPut(opmode);  
          
            for (i = 0; i < cardInitParamsNum; i++) {
                pvPut(cardInitParams[i]);
            }  
            
            setupActive = 1;
            pvPut(setupActive);
            
        } state CALIBRATION_ACTIVE
        
        /* user just finished with calibration */
        when (deviceState == DEVICE_STATE_INIT && !setupStart) {
            
            strcpy(opmode, "NORMAL");
            pvPut(opmode);  
            
            setupActive = 0;
            pvPut(setupActive);
            
        } state WAITING
        
    }
    
    /* only wait for user to finish and than reset the card
       make sure that unplanned transition from INIT state breaks the process 
       make sure to not auto reset if there is an ERROR */
    state CALIBRATION_ACTIVE {
        
        when ( (!setupStart || deviceState != 3) && deviceState != DEVICE_STATE_ERROR) {} state RESET_CARD
        
        when ( deviceState == DEVICE_STATE_ERROR) {
        
            /* do what we can to avoid issues after device reset from the user */
        
            setupActive = 0;
            pvPut(setupActive);
            
            strcpy(opmode, "NORMAL");
            pvPut(opmode);  
        
        } state WAITING
    }
}

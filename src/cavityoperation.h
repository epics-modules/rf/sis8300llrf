#ifndef cavityoperation_H
#define cavityoperation_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include<unistd.h>

#include <mkl.h>
#include <mkl_vml_functions.h>
#include <mkl_vml_types.h>

#include <aSubRecord.h>

#include <epicsExport.h>
#include <registryFunction.h>


#define FIX_LD(x)   (((x + 255) & ~255) + 16)

#endif

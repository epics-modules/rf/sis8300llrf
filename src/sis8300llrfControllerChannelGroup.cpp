/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfPIChannelGroup.cpp
 * @brief Implementation of PI channel group in NDS.
 * @author urojec
 * @date 26.5.2014
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfChannel.h"

#include <math.h>

std::string sis8300llrfControllerChannelGroup::
                PV_REASON_SAMPLES_CNT_PI_TOTAL   = "SamplesCntPITotal";

std::string sis8300llrfControllerChannelGroup::
                PV_REASON_CYCLES_AFTER_RFSTART  = "CyclesAfterRFStart";
std::string sis8300llrfControllerChannelGroup::
                PV_REASON_CYCLES_BSTART  = "CyclesBeamStart";
std::string sis8300llrfControllerChannelGroup::
                PV_REASON_CYCLES_BEND  = "CyclesBeamEnd";
std::string sis8300llrfControllerChannelGroup::
                PV_REASON_CYCLES_RFEND  = "CyclesRFEnd";
std::string sis8300llrfControllerChannelGroup::
                PV_REASON_CYCLES_ILOCK  = "CyclesILock";

std::string sis8300llrfControllerChannelGroup::
                PV_REASON_TRIG_AFTER_RFSTART  = "TrigAfterRFStart";
std::string sis8300llrfControllerChannelGroup::
                PV_REASON_TRIG_BSTART  = "TrigBeamStart";
std::string sis8300llrfControllerChannelGroup::
                PV_REASON_TRIG_BEND  = "TrigBeamEnd";
std::string sis8300llrfControllerChannelGroup::
                PV_REASON_TRIG_RFEND  = "TrigRFEnd";
std::string sis8300llrfControllerChannelGroup::
                PV_REASON_TRIG_ILOCK  = "TrigILock";
std::string sis8300llrfControllerChannelGroup::
                PV_REASON_PID_DELAY_RFSTART  = "PIDDelayRFStart";

/**
 * @brief Controller ChannelGroup constructor.
 * @param [in] name Channel Group name.
 *
 * Register state transition handlers and message handlers. For details
 * refer to NDS documentation and @see #sis8300llrfChannelGroup
 */
sis8300llrfControllerChannelGroup::sis8300llrfControllerChannelGroup(
        const std::string& name, sis8300drv_usr *newDeviceUser) :
        sis8300llrfChannelGroup(name, newDeviceUser) {


    registerOnLeaveStateHandler(nds::CHANNEL_STATE_PROCESSING,
        boost::bind(&sis8300llrfControllerChannelGroup::onLeaveProcessing, 
        this, _1, _2));

    _TriggerType = (epicsInt32) mlvds_012;
    _TriggerTypeChanged = 1;
}

sis8300llrfControllerChannelGroup::~sis8300llrfControllerChannelGroup() {}

double sis8300llrfControllerChannelGroup::_Rad2Deg = 
                                            180.0 / (epicsFloat64) M_PI;

/**
 * @see #sis8300llrfChannelGroup::writeToHardware
 */
ndsStatus sis8300llrfControllerChannelGroup::writeToHardware() {
    int status;
    double err;

    if (_TriggerTypeChanged) {
        status = sis8300llrfdrv_set_trigger_setup(
                    _DeviceUser, 
                    (sis8300llrfdrv_trg_setup) _TriggerType);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_set_trigger_setup", status);

        doCallbacksInt32(_TriggerType, _interruptIdTriggerType);

        _TriggerTypeChanged = 0;

        _UpdateReason |= SIS8300LLRFDRV_UPDATE_REASON_NEW_PARAMS;
    }

    if (_PIDDelayRFStartChanged) {
        status = sis8300llrfdrv_set_pid_delay_rfstart(
                    _DeviceUser, 
                    _PIDDelayRFStart, &err);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_set_pid_delay_rfstart", status);

        doCallbacksInt32(_PIDDelayRFStart, _interruptIdPIDDelayRFStart);

        _PIDDelayRFStartChanged = 0;

        _UpdateReason |= SIS8300LLRFDRV_UPDATE_REASON_NEW_PARAMS;
    }

    return sis8300llrfChannelGroup::writeToHardware();
}

/**
 * @see #sis8300llrfChannelGroup::readParamters
 */
ndsStatus sis8300llrfControllerChannelGroup::readParameters() {
    int status;

    sis8300llrfdrv_trg_setup triggerType;
    double delay;

    status = sis8300llrfdrv_get_trigger_setup(_DeviceUser, &triggerType);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_trigger_setup", status);

    doCallbacksInt32((epicsInt32)_TriggerType, _interruptIdTriggerType);

    status = sis8300llrfdrv_get_pid_delay_rfstart(_DeviceUser, &delay);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_pid_delay_rfstart", status);

    _PIDDelayRFStart = (epicsInt32) delay;
    doCallbacksInt32((epicsInt32)_PIDDelayRFStart, _interruptIdPIDDelayRFStart);

    return ndsSuccess;
}

/**
 * @see #sis8300llrfChannelGroup::markAllParamtersAsChanged
 */
ndsStatus sis8300llrfControllerChannelGroup::markAllParametersChanged() {
    _TriggerTypeChanged = 1;
    _PIDDelayRFStartChanged = 1;

    return sis8300llrfChannelGroup::markAllParametersChanged();
}

/**
 * @brief State handler for leaving PROCESSING state
 *
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Update all the values from the previous pulse
 */
ndsStatus sis8300llrfControllerChannelGroup::onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);
    
    int status, i;
    unsigned uRegVal;

    //TODO take a look, to get only get acquired nsamples
    /* Read the acquired sample counts from the controller */
    for (i = 0; i < SIS8300LLRFDRV_CYCLES_SAMPLES_CNT_NUM; i++) {
        status = sis8300llrfdrv_get_cycles_samples_cnt(
                    _DeviceUser, (sis8300llrfdrv_cycles_samples_cnt)i, &uRegVal);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_get_cycles_samples_cnt", status);
        //Check if the position was triggered for cycles_pos values
        if (i != samples_cnt_pi_total)  {
            if (uRegVal == 0xFFFFFFFF) //trigger didn't hapen
                doCallbacksInt32(0, _interruptIds[i+SIS8300LLRFDRV_CYCLES_SAMPLES_CNT_NUM]);
            else {
                NDS_DBG("Value of Ilock %d \n",uRegVal);
                doCallbacksInt32(1, _interruptIds[i+SIS8300LLRFDRV_CYCLES_SAMPLES_CNT_NUM]);
                doCallbacksInt32((epicsInt32) uRegVal, _interruptIds[i]);
            }
        }
        else {
            doCallbacksInt32((epicsInt32) uRegVal, _interruptIds[i]);
        }
    }

    return ndsSuccess;
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfControllerChannelGroup::registerHandlers(
                nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_SAMPLES_CNT_PI_TOTAL,
        &sis8300llrfControllerChannelGroup::setInt32,
        &sis8300llrfControllerChannelGroup::getInt32,
        &_interruptIds[samples_cnt_pi_total]);

    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_CYCLES_AFTER_RFSTART,
        &sis8300llrfControllerChannelGroup::setInt32,
        &sis8300llrfControllerChannelGroup::getInt32,
        &_interruptIds[cycles_pos_after_rfstart]);

    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_CYCLES_BSTART,
        &sis8300llrfControllerChannelGroup::setInt32,
        &sis8300llrfControllerChannelGroup::getInt32,
        &_interruptIds[cycles_pos_bstart]);

    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_CYCLES_BEND,
        &sis8300llrfControllerChannelGroup::setInt32,
        &sis8300llrfControllerChannelGroup::getInt32,
        &_interruptIds[cycles_pos_bend]);

    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_CYCLES_RFEND,
        &sis8300llrfControllerChannelGroup::setInt32,
        &sis8300llrfControllerChannelGroup::getInt32,
        &_interruptIds[cycles_pos_rfend]);

    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_CYCLES_ILOCK,
        &sis8300llrfControllerChannelGroup::setInt32,
        &sis8300llrfControllerChannelGroup::getInt32,
        &_interruptIds[cycles_pos_ilock]);


    // Register PVs to indicate if trigger happening
    // The interruptId will be SIS8300LLRFDRV_CYCLES_SAMPLES_CNT_NUM + 
    // the related cycle_pos identifier)
    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_TRIG_AFTER_RFSTART,
        &sis8300llrfControllerChannelGroup::setInt32,
        &sis8300llrfControllerChannelGroup::getInt32,
        &_interruptIds[SIS8300LLRFDRV_CYCLES_SAMPLES_CNT_NUM + cycles_pos_after_rfstart]);

    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_TRIG_BSTART,
        &sis8300llrfControllerChannelGroup::setInt32,
        &sis8300llrfControllerChannelGroup::getInt32,
        &_interruptIds[SIS8300LLRFDRV_CYCLES_SAMPLES_CNT_NUM + cycles_pos_bstart]);

    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_TRIG_BEND,
        &sis8300llrfControllerChannelGroup::setInt32,
        &sis8300llrfControllerChannelGroup::getInt32,
        &_interruptIds[SIS8300LLRFDRV_CYCLES_SAMPLES_CNT_NUM + cycles_pos_bend]);

    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_TRIG_RFEND,
        &sis8300llrfControllerChannelGroup::setInt32,
        &sis8300llrfControllerChannelGroup::getInt32,
        &_interruptIds[SIS8300LLRFDRV_CYCLES_SAMPLES_CNT_NUM + cycles_pos_rfend]);

    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_TRIG_ILOCK,
        &sis8300llrfControllerChannelGroup::setInt32,
        &sis8300llrfControllerChannelGroup::getInt32,
        &_interruptIds[SIS8300LLRFDRV_CYCLES_SAMPLES_CNT_NUM + cycles_pos_ilock]);

    NDS_PV_REGISTER_INT32(
        sis8300llrfControllerChannelGroup::PV_REASON_PID_DELAY_RFSTART,
        &sis8300llrfControllerChannelGroup::setPIDDelayRFStart,
        &sis8300llrfControllerChannelGroup::getPIDDelayRFStart,
        &_interruptIdPIDDelayRFStart);

    return sis8300llrfChannelGroup::registerHandlers(pvContainers);
}

/* ======= GETTERS AND SETTERS ======= */

/**
 * @brief set trigger type, see in sis8300llrfdrv.h
 *
 * @param pasynUser asynUser
 * @param value     trigger value
 *
 * @return ndsError   if device is in ON - the control loop is running
 *                    or @see #commitParamters failed
 * @return ndsSuccess Successful set
 */
ndsStatus sis8300llrfControllerChannelGroup::setTriggerType(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (_device->getCurrentState() == nds::DEVICE_STATE_ON) {
        NDS_ERR("Trigger setup cannot be changed when device is in on"
            "or ioc init");
        return ndsError;
    }

    _TriggerType = value;
    _TriggerTypeChanged = 1;

    return commitParameters();
}

/**
 * @brief set trigger type, see in sis8300llrfdrv.h
 *
 * @param pasynUser asynUser
 * @param value     trigger value
 *
 * @return ndsError   if device is in ON - the control loop is running
 *                    or @see #commitParamters failed
 * @return ndsSuccess Successful set
 */
ndsStatus sis8300llrfControllerChannelGroup::setPIDDelayRFStart(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _PIDDelayRFStart = value;
    _PIDDelayRFStartChanged = 1;

    return commitParameters();
}


ndsStatus sis8300llrfControllerChannelGroup::getPIDDelayRFStart(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = _PIDDelayRFStart;

    return ndsSuccess;
}

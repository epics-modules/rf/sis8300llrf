#include "cavityoperation.h"

static const MKL_INT incx = 1;
static	MKL_INT n;
static double *magnitude, *tmp1, *tmp2;

static char llrf_algoMsg[120];

/*
 *
 * @brief: calculate simple linear regression y(x)=k*x+b
 *
 * double dt,
   	double* data, size_t Nsamples,
   	double *intercept, double *slope
 * @param [in]  dt                  time between samples
 * @param [in]  data                input data
 * @param [in]  Nsample             size of data array
 * @param [out] intercept           intercept b
 * @param [out] slope               slope k
 *
 *
 * Fits a linear function y(x)=k*x+b to the input data,
 * sampled with dt spacing, using standard linear regression
 *
 */
void linear_regression_llrf(
        double dt,
        double *data, size_t Nsamples,
        double *intercept, double *slope)
{
    double sum=0, sumi=0;

    for(unsigned int i=0; i<Nsamples; i++) {
        sum  += data[i];
        sumi += data[i]*i;
    }
    // average time: half of the sampling window
    double t_avg = 0.5*(Nsamples-1)*dt;
    // average phase in window
    double data_avg = sum/Nsamples;

    double norm	= dt*dt*Nsamples*(Nsamples*Nsamples-1)/12;
    double num	= (dt*sumi-Nsamples*data_avg*t_avg);

    *slope	= num/norm;
    *intercept	= data_avg-(*slope)*t_avg;
}

static int llrf_calibration_init(aSubRecord *precord)
{
  mkl_set_num_threads(1);
  vmlSetMode(VML_LA | VML_ERRMODE_DEFAULT);

  epicsInt32 INPSIZE = *(epicsInt32*)precord->o;

  epicsInt32 INPALIGN = (epicsInt32)FIX_LD(INPSIZE);
  n = (MKL_INT)INPALIGN;

  magnitude =  (double *)mkl_malloc(INPALIGN*sizeof( double ), 64 );
  tmp1 =  (double *)mkl_malloc(INPALIGN*sizeof( double ), 64 );
  tmp2 =  (double *)mkl_malloc(INPALIGN*sizeof( double ), 64 );
  if (!magnitude || !tmp1 || !tmp2 )
  {
    return -1;
  }
  sleep(1);
  return 0;
}

/*
 * cavity gradient calibration by llrf method
 *
 * **********************************************
 * Parameters:
 *
 *  INA: cavity PU
 *  INB: cavity forward
 *  INC: decimation enable from cav ch
 *  IND: decimation factor from cav ch
 *  INE: decimation enable from fwd ch
 *  INF: decimation factor from fwd ch
 *
 *  ING: cavity gradient enable
 *  INH: cold cable att
 *  INI: cavity kt
 *
 *  INJ: main digitizer state
 *  INK: VPD motor position
 *  INL: llrf pulse generator state
 *  INM: llrf pulse generator fill ration
 *  INN: RF pulse width [ms]
 *  INO: cavity reference Qt [llrf]
 *  INP: cavity gradient efficient length [m]
 *  INQ: cavity geometry R/Q
 *
 * **********************************************
 * Output:
 *
 *  VALA: @param [out] [scalar] tau
 *  VALB: @param [out] [scalar] quality factor
 *
 *  VALC: @param [out] [scalar] kt (Pf)
 *  VALD: @param [out] [scalar] kt% (Pf)
 *  VALE: @param [out] [scalar] Qt (Pf)
 *  VALF: @param [out] [scalar] Qt% (Pf)
 *
 *  VALG: @param [out] [waveform] Eacc (reference kt)
 *  VALH: @param [out] [waveform] Eacc (Pf)
 *
 */
static int llrf_calibration_algo(aSubRecord *precord)
{

  double s_initial = dsecnd();

  // Get cavity string
  const char *cav;
  cav = (char*)precord->n;

  /*
    *  requirements for calibration procedure:
    * 	 llrf PG disabled or (llrf PG enabled and fill ratio=1) [*PG: pulse generator]
   */
  const short calib_req_llrf	= *(short*)precord->h;

  if ( calib_req_llrf == 1) {
    snprintf(llrf_algoMsg, 100, "%s Invalid calibration requirements", cav);
    return -1;
  }

  // number of data points in INA
  const int Nin = precord->nea;
  // INB need to have the same number of points
  if (precord->neb!=Nin) {
    snprintf(llrf_algoMsg, 100, "%s Invalid input array", cav);
    return -1;
  }

  // input waveform
  double *cav_pu = (double*)precord->a;
  double *cavity_fwd = (double*)precord->b;

  // decimation for cavity field AI0
  const int i_rfend_cav	= *(int*)precord->c;
  // decimation for cavity  cavity forward AI5 channel
  const int i_rfend_fwd	= *(int*)precord->d;

  if (i_rfend_cav>Nin || i_rfend_fwd>Nin || i_rfend_cav!=i_rfend_fwd) {
    snprintf(llrf_algoMsg, 100, "%s Index Error", cav);
    return -1;
  }

  // cavity kt
  const double kt		= *(double*)precord->e;
  //system frequency [MHz]
  const double F0  				= *(double*)precord->f;
  // time between samples MHZ is tickTime in KHz.
  const double dt  				= *(double*)precord->g * 1000;

  // reference cavity Qt
  const double qt_ref			= *(double*)precord->j;
  // cavity effective length [m]
  const double leff				= *(double*)precord->k;
  // cavity R over Q
  const double R_over_Q		= *(double*)precord->l;
  // end of the RF pulse
  const double t_endpulse	= *(double*)precord->m;

  /*
   *  cavity gradient lower than 5MV/m (Pf<10kw) to avoid LFD
  */
  const double avgEaccMax       = *(double*)precord->p;

  const int length=16;
  double sumEacc=0;
  for (unsigned int m=0; m<length; m++) {
    sumEacc += kt*sqrt(cav_pu[abs(m+i_rfend_cav-length+1)]);
  }

  double avgEacc = sumEacc/length;

  if (isnan(avgEacc) || avgEacc < 1 || avgEacc > avgEaccMax) {
    snprintf(llrf_algoMsg, 100, "%s Gradient > %fMV/m (Pf<10kw)", cav, avgEaccMax);
    return -1;
  }

  // find the maximum value in data waveform after RF Pulse end

  double pmax=cav_pu[i_rfend_cav];
  for (unsigned int i=i_rfend_cav; i<Nin; i++) {
    if (cav_pu[i]>pmax) {
      pmax=cav_pu[i];
    }
  }

  int i_start=0, i_end=0;
  const double dmin=0.5, dmax=0.9;

  for (unsigned int i=i_rfend_cav; i<Nin; i++) {
    if (cav_pu[i]>=pmax*dmax && cav_pu[i+1]<pmax*dmax) {i_start=i;}
    else if (cav_pu[i]>=pmax*dmin && cav_pu[i+1]<pmax*dmin) {i_end=i;}
  }
  // check output has enough space
  int Nsamples = (i_end-i_start+1);

  if (Nsamples<=0) {
    snprintf(llrf_algoMsg, 100, "[%s: LLRF Method] Range Error", cav);
    return -1;
  }

  memcpy( tmp1, cav_pu + i_start, (Nsamples)*sizeof(double));
  n = Nsamples;
  vdLog10( n, tmp1, magnitude );
  int incx = 1;
  double scale = 10.0;
  dscal(&n, &scale, magnitude, &incx);

  /*
   *  calculate tau and quality factor by linear regression
  */
  double intercept, slope, tau, Ql;
  linear_regression_llrf(dt, magnitude, Nsamples, &intercept, &slope);

  /* VALA: tau */
  //tau = -1/slope;        // loge  --> dt [us]
  tau = -10/slope/log(10); // log10 --> dt [us]
  *(double*)precord->vala=tau;
  precord->neva=1;

  //VALB: Q loaded
  Ql = 2*M_PI*F0*tau;
  *(double*)precord->valb=Ql;
  precord->nevb=1;

  //VALC: kt by Pf
  double kt_fwd;
  kt_fwd = 1e-6*(2/leff)*sqrt(R_over_Q*Ql*1e3*cavity_fwd[i_rfend_cav]/fabs(cav_pu[i_rfend_cav]))*(1-exp(-M_PI*F0*1e3*t_endpulse/Ql));
  *(double*)precord->valc=kt_fwd;
  precord->nevc=1;

  //VALD: kt[%] by Pf
  double kt_fwd_diff;
  kt_fwd_diff = 100*fabs((kt-kt_fwd)/kt);
  *(double*)precord->vald=kt_fwd_diff;
  precord->nevd=1;

  //VALE: Qt by Pf
  double qt_fwd;
  qt_fwd = pow(kt_fwd*1e6*leff,2)/R_over_Q;
  *(double*)precord->vale=qt_fwd;
  precord->neve=1;

  //VALF: Qt[%] by Pf
  double qt_fwd_diff;
  qt_fwd_diff = 100*fabs((qt_ref-qt_fwd)/qt_ref);
  *(double*)precord->valf=qt_fwd_diff;
  precord->nevf=1;

  /* VALG: Eacc by reference kt */
  /* VALH: Eacc by Pf */
  double *eacc_ref = (double*)precord->valg;
  double *eacc_fwd = (double*)precord->valh;

  precord->nevg=Nin;
  precord->nevh=Nin;

  n = Nin;
  vdAbs(n, cav_pu, tmp2);
  vdSqrt(n, tmp2, eacc_ref);
  memcpy( eacc_fwd, eacc_ref, Nin*sizeof(double));

  dscal(&n, &kt, eacc_ref, &incx);
  dscal(&n, &kt_fwd, eacc_fwd, &incx);

  double s_elapsed = (dsecnd() - s_initial) * 1000;

  strcpy((char*)precord->vali, llrf_algoMsg);
  // update algo time PV
  *((double*)precord->valj) = s_elapsed;
  precord->nevi=1;

  /* Done! */
  return 0;
}
epicsRegisterFunction(llrf_calibration_init);
epicsRegisterFunction(llrf_calibration_algo);

#include "cavityoperation.h"

/*  cavity low gradient calibration -- Tom Powers' method */


/* Parameters:
 *
 *  INA: cavity Mag
 *  INB: cavity reflect
 *  INC: decimation enable from cav ch
 *  IND: decimation factor from cav ch
 *  INE: decimation enable from ref ch
 *  INF: decimation factor from ref ch
 *
 *  ING: cavity gradient enable
 *  INH: cold cable att
 *  INI: cavity kt
 *
 *  INJ: main digitizer readback
 *  INK: VPD motor position
 *  INL: RF pulse width	[ms]
 *  INM: cavity gradient efficient length [m]
 *  INN: cavity geometry R/Q
 *
 * **********************************************
 * Output:
 *
 *  VALA: @param [out] [scalar] cavity gradient
 *  VALB: @param [out] [scalar] cavity qt
 *  VALC: @param [out] [scalar] cavity kt
 *
*/

static char cm_algoMsg[120];

static int cm_calibration_algo(aSubRecord *precord)
{

  // Get cavity string
  const char *cav;
  cav = (char*)precord->l;

  // requirements to start calibration:
  const short calibReq_tp	= *(double*)precord->m;

  if (calibReq_tp == 1) {
    snprintf(cm_algoMsg, 100, "%s Invalid calibration requirements", cav);
    return -1;
  }

  epicsInt32 INPSIZE = precord->nea;
  // INB,INC,IND,INE and INF should have the same number of points
  if (precord->neb!=INPSIZE) {
    snprintf(cm_algoMsg, 100, "%s Invalid input array ", cav);
    return -1;
  }

  // input waveform
  const double *cav_pu  = (double*)precord->a;
  const double *ref_mag  = (double*)precord->b;

  // cavity kt
  const double kt  = *(double*)precord->e;

  // cavity effective length [m]
  const double leff  = *(double*)precord->f;
  // cavity R/Q
  const double R_over_Q  = *(double*)precord->g;

  //system frequency [MHz]
  const double F0    = *(double*)precord->h;
  // time between samples MHZ h is tickTime in KHz.
  const double dt    = *(double*)precord->i * 1000.0;

  /*
     * LLRF, EVR parameters & configuration
  */
  // decimation for cavity field AI0
  const int i_rfend_cav  = *(int*)precord->j;
  // decimation for cavity  cavity reflect AI6
  const int i_rfend_ref  = *(int*)precord->k;


  if (i_rfend_cav>INPSIZE || i_rfend_ref>INPSIZE || i_rfend_cav!=i_rfend_ref) {
    snprintf(cm_algoMsg, 100, "%s Invalid input array", cav);
    return -1;
  }

  /*
   * should see some RF inside cavity for calculation
  */
  const int winEacc = 16;
  double sumEacc = 0;
  for (int a = 0; a < winEacc; a++) {
      sumEacc += kt*sqrt(cav_pu[abs(a+i_rfend_cav-winEacc+1)]);
  }
  double avgEacc = sumEacc/winEacc;

  if (isnan(avgEacc)  || avgEacc < 1 ) {
    snprintf(cm_algoMsg, 100, "%s Decimation Error", cav);
    return -1;
  }

   // enery inside cavity when RF End
  double U = 0;
  for (int m = i_rfend_cav-1; m < INPSIZE; m++) {
    U += ref_mag[m]*1e3*dt;
  }

  // sanity check
  if( isnan(U) || U < 0 ) {
    snprintf(cm_algoMsg, 100, "%s Incorrect Energy level", cav);
    return -1;
  }

  double eacc = (1/leff)*sqrt(2*M_PI*F0*U*R_over_Q)*1e-6;
  double Qt = pow(eacc*1e6,2)*pow(leff,2)/(cav_pu[i_rfend_cav-2]*R_over_Q);
  double kt_calc = 1e-6*(1/leff)*sqrt(Qt*R_over_Q);
  // VALA: cavity gradient
  precord->neva=1;
  *(double*)precord->vala=eacc;
  // VALB: cavity Qt
  precord->nevb=1;
  *(double*)precord->valb=Qt;
  // VALC: cavity kt
  precord->nevc=1;
  *(double*)precord->valc=kt_calc;

  strcpy((char*)precord->vald, cm_algoMsg);

  return 0;
}

epicsRegisterFunction(cm_calibration_algo);

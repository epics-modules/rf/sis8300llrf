/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfPIChannel.cpp
 * @brief Implementation of PI input channel in NDS.
 * @author urojec
 * @date 26.5.2014
 * 
 * This class exposes all the settings available for the PI controller 
 * part on the device. There are always two instances of the class - one 
 * for I and one for Q PI controller. The settings are the same for both 
 * controllers.  
 */

#include <math.h>

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"
#include "sis8300llrfdrv_types.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfControllerChannelGroup.h"
#include "sis8300llrfPIChannel.h"

#define ROUNDUP_TWOHEX(val) ((unsigned) (val + 0x1F) &~0x1F)

std::string sis8300llrfPIChannel::
                    PV_REASON_PI_GAIN_K          = "PIGainK";
std::string sis8300llrfPIChannel::
                    PV_REASON_PI_GAIN_TS_DIV_TI  = "PIGainTsDivTi";
std::string sis8300llrfPIChannel::
                    PV_REASON_PI_D_A2            = "PIDa2";
std::string sis8300llrfPIChannel::
                    PV_REASON_PI_D_B0            = "PIDb0";
std::string sis8300llrfPIChannel::
                    PV_REASON_PI_D_B1            = "PIDb1";
std::string sis8300llrfPIChannel::
                    PV_REASON_PI_D_B2            = "PIDb2";
std::string sis8300llrfPIChannel::
                    PV_REASON_SAT_MAX            = "PISaturationMax";
std::string sis8300llrfPIChannel::
                    PV_REASON_SAT_MIN            = "PISaturationMin";
std::string sis8300llrfPIChannel::
                    PV_REASON_FIXED_SP_VAL       = "PIFixedSPVal";
std::string sis8300llrfPIChannel::
                    PV_REASON_FIXED_FF_VAL       = "PIFixedFFVal";
std::string sis8300llrfPIChannel::
                    PV_REASON_PI_OVERFLOW_STATUS = "PIOverflowStatus";
std::string sis8300llrfPIChannel::
                    PV_REASON_SEL_OUTPUT         = "PISelOutput";

//TODO: pi type to string

/**
 * @brief PI channel constructor.
 */
sis8300llrfPIChannel::sis8300llrfPIChannel(sis8300llrfdrv_pi_type piType) :
        sis8300llrfChannel(
            SIS8300LLRFDRV_PI_PARAM_NUM, 
            SIS8300LLRFDRV_PI_PARAM_INT_FIRST) {

    _PIType = piType;

    switch (_PIType) {
    case pi_I:
        _GenStatusBit = gen_status_pi_overflow_I;
        strcpy(_ChanStringIdentifier, "PI I");
        break;
    case pi_Q:
        _GenStatusBit = gen_status_pi_overflow_Q;
        strcpy(_ChanStringIdentifier, "PI Q");
        break;
    default:
        NDS_CRT("Invalid option for PI channel! Must be I or Q!");
        break;
    }

    registerOnEnterStateHandler(nds::CHANNEL_STATE_RESETTING,
        boost::bind(&sis8300llrfPIChannel::onEnterReset, this));
}

/**
 * @brief AI channel destructor.
 *
 * Free channel data buffer.
 */
sis8300llrfPIChannel::~sis8300llrfPIChannel() {}

/**
 * @see   sis8300llrfChannel:onEnterReset
 * @brief Aditianaly to parent, also reset the RMS values and check the
 *        PI overflow status
 */
ndsStatus sis8300llrfPIChannel::onEnterReset() {
    
    if (sis8300llrfChannel::onEnterReset() != ndsSuccess) {
        return ndsError;
    }
    
    if (checkStatuses() != ndsSuccess) {
        return ndsError;
    }
    
    return ndsSuccess;
}


/**
 * @see #sis8300llrfChannel::readParameter
 */
inline int sis8300llrfPIChannel::readParameter(
                int paramIdx, double *paramVal) {
    if (paramIdx != pi_param_sel_out_src)
        return sis8300llrfdrv_get_pi_param(
                    _DeviceUser, _PIType, 
                    (sis8300llrfdrv_pi_param) paramIdx, paramVal);
    else {
        double sel_out_val;
        int status;

        status = sis8300llrfdrv_get_pi_param(
                    _DeviceUser, _PIType, 
                    (sis8300llrfdrv_pi_param) paramIdx, &sel_out_val);

        switch ((int) sel_out_val) {
            case 0:
            case 1:
            case 2:
                *paramVal = sel_out_val;
                break;
            case 4:
                *paramVal = 3;
                break;
            case 8:
                *paramVal = 4;
                break;
            default:
                NDS_ERR("Invalid read value for PI Sel Out Src");
                return ndsError;
        }
        return status;
    }
}

/**
 * @see #sis8300llrfChannel::writeParameter
 */
inline int sis8300llrfPIChannel::writeParameter(
                int paramIdx, double *paramErr) {
    if (paramIdx != pi_param_sel_out_src)
        return sis8300llrfdrv_set_pi_param(
                    _DeviceUser, _PIType,
                    (sis8300llrfdrv_pi_param) paramIdx, 
                    _ParamVals[paramIdx], paramErr);
    else {
        double sel_out_val;
        switch ((int)_ParamVals[paramIdx]) {
            case 0:
            case 1:
            case 2:
                sel_out_val = _ParamVals[pi_param_sel_out_src];
                break;
            case 3:
                sel_out_val = 4;
                break;
            case 4:
                sel_out_val = 8;
                break;
            default:
                NDS_ERR("Invalid option for PI Out Ctrl Sel");
                return ndsError;
        }

        return sis8300llrfdrv_set_pi_param(
                    _DeviceUser, _PIType,
                    (sis8300llrfdrv_pi_param) paramIdx, 
                    sel_out_val, paramErr);
    }
}

/**
 * @brief All the statuses that need to be checked on leave processing
 *        and enter reset
 */
inline ndsStatus sis8300llrfPIChannel::checkStatuses() {
    epicsInt32 valInt32;
    
    if (getPIOverflowStatus(NULL, &valInt32) != ndsSuccess) {
        return ndsError;
    }

    return ndsSuccess;
}

/**
 * @brief Get PI controller type
 *
 * @return pi_angle or pi_mag, @see #sis8300llrfdrv_pi_type
 */
sis8300llrfdrv_pi_type sis8300llrfPIChannel::getPIType() {
    return _PIType;
}

/**
 * @brief Read current PI overflow status from the device
 *
 * @param [in]  pasynUser Asyn user context struct
 * @param [out] value     On success this will be 1 if overflow occured 
 *                        and 0 if not
 *
 * @retval ndsSuccess    Value read successfully.
 * @retval ndsError      Could not retrieve the data from the device
 */
ndsStatus sis8300llrfPIChannel::getPIOverflowStatus(
                asynUser *pasynUser, epicsInt32 *value) {
    unsigned overflowStatus;
    int status;

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    status = sis8300llrfdrv_get_general_status(
                _DeviceUser, _GenStatusBit, &overflowStatus);
    SIS8300NDS_STATUS_ASSERT(
        "sis8300llrfdrv_get_general_status", status);

    *value = (epicsInt32) overflowStatus;

    doCallbacksUInt32Digital(*value, _interruptIdPIOverflowStatus);

    return ndsSuccess;
}


/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfPIChannel::registerHandlers(
                nds::PVContainers* pvContainers) {
    
    /* Device Settings */
    
    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfPIChannel::PV_REASON_PI_GAIN_K,
            &sis8300llrfPIChannel::setPIGainK,
            &sis8300llrfPIChannel::getPIGainK,
            &_interruptIds[pi_param_gain_K]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfPIChannel::PV_REASON_PI_GAIN_TS_DIV_TI,
            &sis8300llrfPIChannel::setPIGainTsDivTi,
            &sis8300llrfPIChannel::getPIGainTsDivTi,
            &_interruptIds[pi_param_gain_TSdivTI]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfPIChannel::PV_REASON_PI_D_A2,
            &sis8300llrfPIChannel::setPIDa2,
            &sis8300llrfPIChannel::getPIDa2,
            &_interruptIds[pi_param_D_a2]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfPIChannel::PV_REASON_PI_D_B0,
            &sis8300llrfPIChannel::setPIDb0,
            &sis8300llrfPIChannel::getPIDb0,
            &_interruptIds[pi_param_D_b0]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfPIChannel::PV_REASON_PI_D_B1,
            &sis8300llrfPIChannel::setPIDb1,
            &sis8300llrfPIChannel::getPIDb1,
            &_interruptIds[pi_param_D_b1]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfPIChannel::PV_REASON_PI_D_B2,
            &sis8300llrfPIChannel::setPIDb2,
            &sis8300llrfPIChannel::getPIDb2,
            &_interruptIds[pi_param_D_b2]);

    NDS_PV_REGISTER_FLOAT64(sis8300llrfPIChannel::PV_REASON_SAT_MAX,
            &sis8300llrfPIChannel::setPISaturationMax,
            &sis8300llrfPIChannel::getPISaturationMax,
            &_interruptIds[pi_param_sat_max]);

    NDS_PV_REGISTER_FLOAT64(sis8300llrfPIChannel::PV_REASON_SAT_MIN,
            &sis8300llrfPIChannel::setPISaturationMin,
            &sis8300llrfPIChannel::getPISaturationMin,
            &_interruptIds[pi_param_sat_min]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfPIChannel::PV_REASON_FIXED_SP_VAL,
            &sis8300llrfPIChannel::setPIFixedSPVal,
            &sis8300llrfPIChannel::getPIFixedSPVal,
            &_interruptIds[pi_param_fixed_sp_val]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfPIChannel::PV_REASON_FIXED_FF_VAL,
            &sis8300llrfPIChannel::setPIFixedFFVal,
            &sis8300llrfPIChannel::getPIFixedFFVal,
            &_interruptIds[pi_param_fixed_ff_val]);

    NDS_PV_REGISTER_INT32(
           sis8300llrfPIChannel::PV_REASON_PI_OVERFLOW_STATUS,
	   &sis8300llrfPIChannel::setInt32,
           &sis8300llrfPIChannel::getPIOverflowStatus,
           &_interruptIdPIOverflowStatus
     );

    NDS_PV_REGISTER_INT32(
            sis8300llrfPIChannel::PV_REASON_SEL_OUTPUT,
            &sis8300llrfPIChannel::setPISelOutput,
            &sis8300llrfPIChannel::getPISelOutput,
            &_interruptIds[pi_param_sel_out_src]);

    return sis8300llrfChannel::registerHandlers(pvContainers);
}



/* === GETTERS AND SETTERS FOR CONTROLLER SETTINGS === */
/* ============ RELATED TO PI CONTROLLER ============= */
/*
 * The getters are meant to be used by records that will keep track of 
 * setup used in the pulse that just passed. These records should be 
 * set to process at IO interrupt, because all the callbacks will get 
 * called when receiving the PULSE_DONE interrupt.
 * */

/**
 * @brief Set PI controller gain K
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the K gain to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPIGainK(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_gain_K] = (double) value;

    _ParamChanges[pi_param_gain_K] = 1;
    return commitParameters();
}
/**
 * @brief Get PI controller K gain
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the K gain value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPIGainK(
                asynUser *pasynUser, epicsFloat64 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[pi_param_gain_K];

    return ndsSuccess;
}
/**
 * @brief Set PI controller gain Ts/Ti
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the Ts/Ti gain to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPIGainTsDivTi(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_gain_TSdivTI] = (double) value;

    _ParamChanges[pi_param_gain_TSdivTI] = 1;
    return commitParameters();
}
/**
 * @brief Get PI controller Ts/Ti gain
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the Ts/Ti gain value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPIGainTsDivTi(
                asynUser *pasynUser, epicsFloat64 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[pi_param_gain_TSdivTI];
    return ndsSuccess;
}


/**
 * @brief Set PI controller D part filter coefficient a2
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the coefficient a2 to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPIDa2(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_D_a2] = (double) value;

    _ParamChanges[pi_param_D_a2] = 1;
    return commitParameters();
}
/**
 * @brief Get PI controller D part filter coefficient a2
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold of the coefficient a2 value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPIDa2(
                asynUser *pasynUser, epicsFloat64 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[pi_param_D_a2];

    return ndsSuccess;
}

/**
 * @brief Set PI controller D part filter coefficient b0
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the coefficient b0 to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPIDb0(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_D_b0] = (double) value;

    _ParamChanges[pi_param_D_b0] = 1;
    return commitParameters();
}
/**
 * @brief Get PI controller D part filter coefficient b0
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold of the coefficient b0 value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPIDb0(
                asynUser *pasynUser, epicsFloat64 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[pi_param_D_b0];

    return ndsSuccess;
}

/**
 * @brief Set PI controller D part filter coefficient b1
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the coefficient b1 to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPIDb1(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_D_b1] = (double) value;

    _ParamChanges[pi_param_D_b1] = 1;
    return commitParameters();
}
/**
 * @brief Get PI controller D part filter coefficient b1
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold of the coefficient b1 value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPIDb1(
                asynUser *pasynUser, epicsFloat64 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[pi_param_D_b1];

    return ndsSuccess;
}

/**
 * @brief Set PI controller D part filter coefficient b2
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the coefficient b2 to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPIDb2(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_D_b2] = (double) value;

    _ParamChanges[pi_param_D_b2] = 1;
    return commitParameters();
}
/**
 * @brief Get PI controller D part filter coefficient b2
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold of the coefficient b2 value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPIDb2(
                asynUser *pasynUser, epicsFloat64 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[pi_param_D_b2];

    return ndsSuccess;
}

/**
 * @brief Set PI controller maximum saturation value
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the maximum saturation to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPISaturationMax(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_sat_max] = (double) value;

    _ParamChanges[pi_param_sat_max] = 1;
    return commitParameters();
}
/**
 * @brief Get PI controller maximum saturation setting
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the max saturation value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPISaturationMax(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[pi_param_sat_max];

    return ndsSuccess;
}
/**
 * @brief Set PI controller minimum saturation value
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the minimum saturation to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPISaturationMin(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_sat_min] = (double) value;

    _ParamChanges[pi_param_sat_min] = 1;
    return commitParameters();
}
/**
 * @brief Get PI controller minimuam saturation setting
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the min saturation value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPISaturationMin(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[pi_param_sat_min];

    return ndsSuccess;
}
/**
 * @brief Set PI controller fixed SP value
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the fixed SP to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPIFixedSPVal(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_fixed_sp_val] = (double) value;

    _ParamChanges[pi_param_fixed_sp_val] = 1;
    return commitParameters();
}
/**
 * @brief Get PI controller fixed SP value setting
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the current fixed SP value on 
 *                          success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPIFixedSPVal(
                asynUser *pasynUser, epicsFloat64 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[pi_param_fixed_sp_val];
    return ndsSuccess;
}
/**
 * @brief Set PI controller fixed FF value
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the fixed FF to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPIFixedFFVal(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[pi_param_fixed_ff_val] = (double) value;

    _ParamChanges[pi_param_fixed_ff_val] = 1;
    return commitParameters();
}
/**
 * @brief Get PI controller fixed FF value setting
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the current fixed FF value on 
 *                          success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPIFixedFFVal(
                asynUser *pasynUser, epicsFloat64 *value) {
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[pi_param_fixed_ff_val];
    return ndsSuccess;
}

/**
 * @brief Select Output
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        0, 1, 2, 4 or 8
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIChannel::setPISelOutput(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value < 0 || value > 4) {
        NDS_ERR("Invalid option for PI Sel Output");
        return ndsError;
    }
    _ParamVals[pi_param_sel_out_src] = value;

    _ParamChanges[pi_param_sel_out_src] = 1;
    return commitParameters();
}
/**
 * @brief Check the selected output source
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will be 1 if enabled and 0 if disabled on 
 *                          success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfPIChannel::getPISelOutput(
                asynUser *pasynUser, epicsInt32 *value) {
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[pi_param_sel_out_src];
    return ndsSuccess;
}

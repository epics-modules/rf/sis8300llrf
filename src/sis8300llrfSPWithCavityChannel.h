/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfSPWithCavityChannel.h
 * @brief Header file defining the LLRF Fixed SP channel class
 * @author gabriel.fedel@ess.eu
 * @date 5.5.2020
 */

#ifndef _sis8300llrfSPWithCavityChannel_h
#define _sis8300llrfSPWithCavityChannel_h

#include "scalingSqrt.h"
#include "sis8300llrfControlTableChannel.h"
#include "sis8300llrfPIMagAngChannel.h"
#include "sis8300llrfCavityChannel.h"
/**
 * @brief sis8300 LLRF specific nds::ADIOChannel class from which
 *           all llrf specific channels are derived
 */
class sis8300llrfSPWithCavityChannel: public sis8300llrfPIMagAngChannel {
public:
    sis8300llrfSPWithCavityChannel(sis8300llrfPIChannel * PIChannels[2], sis8300llrfControlTableChannel * CtrlTableCh, sis8300llrfCavityChannel * CavChannel, sis8300llrfCalibrationProvider * calibrationProvider);

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    virtual ndsStatus setMagTable(
                        asynUser *pasynUser, epicsFloat64 *value, size_t nelem);
    virtual ndsStatus getMagTable(
                asynUser* pasynUser, epicsFloat64 *value, 
                size_t nelem, size_t *nIn);
    virtual ndsStatus setSqrtFitEnable(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setCalibEGU(asynUser *pasynUser, epicsFloat64 *value, size_t nelem);

    virtual double calibrateEGU2Raw(double val);
protected:
    scalingSqrt * calibratorSqrt; // used for output
    sis8300llrfCavityChannel * m_CavChannel;
    bool _useSqrtFit;
    static std::string PV_REASON_CALIB_SQRT_FIT_EN;
    int _interruptIdCalibSqrtFitEnable;

    virtual ndsStatus writeToHardware();
    virtual ndsStatus updateMagAngRBV();
    virtual void reloadCalibPVs();
};

#endif /* _sis8300llrfSPWithCavityChannel_h */

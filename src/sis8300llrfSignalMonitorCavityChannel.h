/*
 * sis8300llrf
 * Copyright (C) 2021 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfSignalMonitorCavityChannel.h
 * @brief Header file defining the LLRF Signal monitor for cavity channel
 * @author gabrielfedel
 */

#ifndef _sis8300llrfSignalMonitorCavityChannel_h
#define _sis8300llrfSignalMonitorCavityChannel_h

#include "sis8300llrfSignalMonitorChannel.h"
#include <math.h>
#include "sis8300llrfCavityChannel.h"

/**
 * @brief Modulator filter specific implementation of
 *        @see #sis8300llrfChannel Class
 */
class sis8300llrfSignalMonitorCavityChannel: public sis8300llrfSignalMonitorChannel {
public:
    sis8300llrfSignalMonitorCavityChannel( sis8300llrfCavityChannel * CavChan): sis8300llrfSignalMonitorChannel(CavChan){
        m_CavChan = CavChan;
    };
    
    virtual ndsStatus getMagMinMax(
                        asynUser *pasynUser,  epicsFloat64 *value);
    virtual ndsStatus getMagAngCurrent(epicsFloat64 *mag_val, epicsFloat64 *ang_val);
    virtual ndsStatus getMagAverage(
                        asynUser *pasynUser,  epicsFloat64 *value);
protected:
    /*This variable is used to access calibration values from AI channel
     * so the same calibration will be used on monitoring values*/
    sis8300llrfCavityChannel * m_CavChan;
};

#endif /* _sis8300llrfSignalMonitorCavityChannel_h */


/*
 * sis8300llrf
 * Copyright (C) 2021 European Spallation Source - ESS

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file _sis8300llrfCavityChannel_h
 * @brief Header file defining the LLRF Cavity channel class
 * @author gabrielfedel
 * @date 1.11.2021
 */

#ifndef _sis8300llrfCavityChannel_h
#define _sis8300llrfCavityChannel_h

#include "sis8300llrfAIChannel.h"

/**
 * @brief llrf specific AI channel, used for AI0 and AI1 - reference and
 * cavity. It prevents disabling the channel and provides signal
 * magnitude and angle reads.
 */
class sis8300llrfCavityChannel: public sis8300llrfAIChannel {

public:
    sis8300llrfCavityChannel(epicsFloat64 FSampling);

    /* overriden because unsupported */
    virtual ndsStatus getCalibGradEnable(asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setCalibGradEnable(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setCalibGradA(asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus setCalibGradKt(asynUser *pasynUser, epicsFloat64 value);


    virtual ndsStatus setLeff(asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus setRoverQ(asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus setLowPowerQext(asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus setWindow(asynUser *pasynUser,  epicsInt32 value);

    // real and imaginary part of complex constant
    virtual ndsStatus setCccXReal(asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus setCccXImag(asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus setCccYReal(asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus setCccYImag(asynUser *pasynUser, epicsFloat64 value);
    //methods to be used by other classes to check if the channels is calibrated
    virtual bool isGradCalibrated() {return m_calib_grad_en;};

    virtual double calibrateSanityCheck(double value) {
        double calib_poly = calibrator->calibrate(value);
        if (std::isnan(calib_poly) || calib_poly < 0)
            return 0;
        return  calib_poly;
    };

    // cavitu pu(w): apply cold cable attenuation on raw calibrated data without gradient.
    virtual double calibrateCavityPu(double value) {
        return  value * pow(10, m_calib_grad_a/10);
    };

    virtual double calibratePolyAndGrad(double value) {
        double calib_poly_grad = calibrateSanityCheck(value) ;
        if (calib_poly_grad == 0)
            return 0;
        return m_calib_grad_kt * sqrt( calib_poly_grad * pow(10, m_calib_grad_a/10)  );
    };

    virtual double calibrateGrad(double value) {
        if (std::isnan(value) || value < 0)
            return 0;
        return m_calib_grad_kt * sqrt( value * pow(10, m_calib_grad_a/10)  );
    };

    // do the oposite function
    virtual double calibrateGradInv(double value) {
        return pow(value/m_calib_grad_kt,2)/pow(10,(m_calib_grad_a/10));
    };

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
protected:
    static std::string PV_REASON_CALIB_GRAD_EN;
    static std::string PV_REASON_CALIB_GRAD_A;
    static std::string PV_REASON_CALIB_GRAD_KT;

		// Cavity detuning params
    static std::string PV_REASON_LEFF;
    static std::string PV_REASON_ROVERQ;
    static std::string PV_REASON_LOWPOWERQEXT;
    static std::string PV_REASON_WINDOW;
    static std::string PV_REASON_CCCXREAL;
    static std::string PV_REASON_CCCXIMAG;
    static std::string PV_REASON_CCCYREAL;
    static std::string PV_REASON_CCCYIMAG;

    int _interruptIdCalibGradEnable;
    int _interruptIdCalibGradA;
    int _interruptIdCalibGradKt;

    int _interruptIdLeff;
    int _interruptIdRoverQ;
    int _interruptIdLowPowerQext;
    int _interruptIdWindow;
    int _interruptIdCccXReal;
    int _interruptIdCccYReal;
    int _interruptIdCccXImag;
    int _interruptIdCccYImag;

    bool m_calib_grad_en;
    double m_calib_grad_a;
    double m_calib_grad_kt;

    double m_leff;
    double m_roverq;
    double m_lowpowerqext;
    double m_cccxreal;
    double m_cccyreal;
    double m_cccximag;
    double m_cccyimag;
    long m_window;

    virtual ndsStatus onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to);
};

#endif /* _sis8300llrfCavityChannel_h */

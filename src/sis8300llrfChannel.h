/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfChannel.h
 * @brief Header file defining the LLRF PI channel class
 * @author urojec
 * @date 5.6.2014
 */

#ifndef _sis8300llrfChannel_h
#define _sis8300llrfChannel_h

#include <ndsADIOChannel.h>

#define SIS8300LLRFNDS_CHAN_STRING 32


/**
 * @brief sis8300 LLRF specific nds::ADIOChannel class from which
 *           all llrf specific channels are derived
 */
class sis8300llrfChannel: public nds::ADIOChannel {
public:
    sis8300llrfChannel(int ParamNum, int intParamFirst);
    virtual ~sis8300llrfChannel();

    virtual void onRegistered();

    virtual ndsStatus initialize();

    virtual ndsStatus commitParameters();
    virtual ndsStatus forceCommitParameters();
    virtual ndsStatus readParameters();
    virtual ndsStatus markAllParametersChanged();

    virtual ndsStatus updateFilter();


    virtual ndsStatus setEnabled(asynUser* pasynUser, epicsInt32 value);

    /* overriden because unsupported */
    virtual ndsStatus handleOnMsg(
        asynUser *pasynUser, const nds::Message& value);
    virtual ndsStatus handleOffMsg(
        asynUser *pasynUser, const nds::Message& value);
    virtual ndsStatus handleResetMsg(
        asynUser *pasynUser, const nds::Message& value);

protected:
    sis8300drv_usr *_DeviceUser; /**< User context for sis8300drv, 
                                      set by channel group. */

    char _ChanStringIdentifier[SIS8300LLRFNDS_CHAN_STRING]; /**< Used 
                              for messages to display the channel name*/

    unsigned _Initialized;  /**< gets set to 1 when channel is 
                                 initialized */

    virtual ndsStatus onEnterDisabled(
        nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus onEnterReset();
    virtual ndsStatus onEnterError(
        nds::ChannelStates from, nds::ChannelStates to);


    /* These two arebasically the only place where
     * @see #sis8300llrfVMControlChannel
     * @see #sis8300llrfSignalMonitorChannel
     * @see #sis8300llrfPIChannel
     * @see #sis8300llrfModRippleFilterChannel
     * @see #sis8300llrfIQSamplingChannel
     *
     * differentiate. With these two, practically all that has to
     * be defined int the listed channels are setters and getters for 
     * all the paramters they have in control
     */
    virtual int readParameter(int paramIdx, double *paramVal);
    virtual int writeParameter(int paramIdx, double *paramErr);
    
    /* */
    virtual ndsStatus writeToHardware();

    double *_ParamVals;
    int    *_ParamChanges;
    int    *_interruptIds;

    int _ParamNum;
    int _IntParamFirst;
    
    void freeParamBuffers();
};

#endif /* _sis8300llrfChannel_h */

/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfPIErrorChannel.cpp
 * @brief File defining the LLRF PIError channel class that handles
 * PI error waveform, their settings and statistics
 * @author gabriel.fedel@ess.eu
 * @date 9.4.2020
 *
 * The class also reads out data that changes on 
 * pulse-to-pulse basis, which includes: PI error waveform and PI 
 * overflow status. This is done in the #onLeaveProcessing function.

 * 
 * RMS statistics:
 * Apart from device related settings and readouts, this class also 
 * tracks the RMS values of the PI error waveform that is recieved for 
 * each pulse and does a cumulative average. The average is reset when 
 * ANY (not just related to this class) parameters change, which is 
 * determined from the update reason in #onEneterProcessing. The
 * RMS averages can also be reset on demand.

 */
#include "sis8300drv.h"
#include "sis8300llrfdrv.h"
#include "sis8300llrfdrv_types.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfPIErrorChannel.h"

#define ROUNDUP_TWOHEX(val) ((unsigned) (val + 0x1F) &~0x1F)

std::string sis8300llrfPIErrorChannel::
                    PV_REASON_PIERROR_DAQ_FMT      = "DAQFormat";

/**
 * @brief PIError channel constructor.
 */
sis8300llrfPIErrorChannel::sis8300llrfPIErrorChannel(epicsFloat64 FSampling, epicsInt32 nearIqN, sis8300llrfPIMagAngChannel * SPChannel) :
        sis8300llrfAuxChannel(
            FSampling,
            nearIqN,
            1),
        _SPChannel(SPChannel){ //Internal

}

/**
 * @see   sis8300llrfChannel:onEnterReset
 * @brief Aditianaly to parent, also reset the RMS values and check the
 *        PI overflow status
 */
ndsStatus sis8300llrfPIErrorChannel::onEnterReset() {
    if (sis8300llrfChannel::onEnterReset() != ndsSuccess) {
        return ndsError;
    }
    
    if (checkStatuses() != ndsSuccess) {
        return ndsError;
    }
    
    return ndsSuccess;
}

/**
 * @brief Read all the internal data from memory and push to PV
 *
 * @return ndsSuccess   Data read successfully
 * @return ndsError     Read failed, chan goes to ERR state or
 *                      couldn't allocate memory.
 *
 * This will read all the PI error data, convert to float and push
 * to the PV. It will also calculate and update the RMS PVs.
 *
 * */
ndsStatus sis8300llrfPIErrorChannel::onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);
  
    int status;
    unsigned iterSrc, iterDest;
    unsigned nsamples;
    epicsInt16 * rawDataI16;
    epicsFloat64       *IMagData; /**< waveform with I or Magnitude*/
    epicsFloat64       *QAngData; /**< waveform with Q or Angle */
    epicsFloat64 ConvFactIMag, ConvFactQAng, magCalib;
    epicsInt32 daq_fmt;
    double param;
    bool pm;

    if (sis8300llrfAuxChannel::onLeaveProcessing(from, to) == ndsError)
        return ndsError;
    
    if (_SamplesAcquired == 0) {
        return ndsSuccess;
    }

    pm = checkPM();
    // If there it is not enable the transfer and there is no interlock 
    // We shouldn't get anything from firmware
    if (!pm && !_EnableTransf){
        if (_XAxis != NULL)
            delete [] _XAxis;
        _XAxis = NULL;
        _SamplesAcquired = 0;
        doCallbacksInt32(_SamplesAcquired, _interruptIdAUXNSamplesAcq);
        doCallbacksFloat64Array(_XAxis, _SamplesAcquired, _interruptIdAUXXAxis);
        doCallbacksFloat64Array(NULL, (size_t)_SamplesAcquired, _interruptIdAUXIMag);
        doCallbacksFloat64Array(NULL, (size_t)_SamplesAcquired, _interruptIdAUXQAng);
        return ndsSuccess;
    }

    /* we need to read in 512 bit blocks - firmware imposed limitation */
    NDS_DBG("Rounding _SamplesAcquired from %d", _SamplesAcquired);
    nsamples = ROUNDUP_TWOHEX(_SamplesAcquired);
    NDS_DBG("_SamplesAcquired rounded to %u", nsamples);

    rawDataI16 = new (std::nothrow) epicsInt16[nsamples * 2]; //*2 because is 32bits per sample
    IMagData = new (std::nothrow) epicsFloat64[_SamplesAcquired];
    QAngData = new (std::nothrow) epicsFloat64[_SamplesAcquired];

    if (!rawDataI16 || !IMagData || !QAngData) 
        return ndsError;


    status = sis8300llrfdrv_read_aux_channel(
                _DeviceUser, 0, _DownOrIntern, rawDataI16, 
                nsamples);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_read_aux_channel", status);

    //get dat format - 0: MA / 1: IQ / 2: DC
    readParameter(aux_param_daq_fmt, &param);
    daq_fmt = (epicsInt32) param;

    switch (daq_fmt) {
        case 0://MA 
            ConvFactQAng = 1.0 / (epicsFloat64) (1 << sis8300llrfdrv_Qmn_intern_PI_err_ang_sample.frac_bits_n);
            if (!_SPChannel->isCalibrationEnabled()) //without calibration
                for (iterSrc = 0, iterDest = 0; iterDest < (unsigned)_SamplesAcquired; iterSrc += 2, iterDest ++) {
                    sis8300llrfdrv_Qmn_2_double((uint32_t)(uint16_t)rawDataI16[iterSrc], 
                            sis8300llrfdrv_Qmn_intern_PI_err_mag_sample, &IMagData[iterDest]);
                    QAngData[iterDest] = ((epicsFloat64)(((epicsInt16)rawDataI16[iterSrc + 1]) * ConvFactQAng)) * 180/M_PI;
                }
            else  // with calibration
                for (iterSrc = 0, iterDest = 0; iterDest < (unsigned)_SamplesAcquired; iterSrc += 2, iterDest ++) {
                    sis8300llrfdrv_Qmn_2_double((uint32_t)(uint16_t)rawDataI16[iterSrc], 
                            sis8300llrfdrv_Qmn_intern_PI_err_mag_sample, &IMagData[iterDest]);
                    QAngData[iterDest] = ((epicsFloat64)(((epicsInt16)rawDataI16[iterSrc + 1]) * ConvFactQAng)) * 180/M_PI;
                }
        break;
        case 1: //IQ
            ConvFactIMag = ConvFactQAng = 1.0 / (epicsFloat64) (1 << sis8300llrfdrv_Qmn_intern_PI_err_IQ_sample.frac_bits_n);
            for (iterSrc = 0, iterDest = 0; iterDest < (unsigned)_SamplesAcquired; iterSrc += 2, iterDest ++) {
                IMagData[iterDest] = (epicsFloat64)(((epicsInt16)rawDataI16[iterSrc]) * ConvFactIMag);
                QAngData[iterDest] = (epicsFloat64)(((epicsInt16)rawDataI16[iterSrc + 1]) * ConvFactIMag);
            }
        break;
    }
    // Push waveform
    doCallbacksFloat64Array(QAngData, (size_t)_SamplesAcquired, _interruptIdAUXQAng);
    doCallbacksFloat64Array(IMagData, (size_t)_SamplesAcquired, _interruptIdAUXIMag);

    if (pm){
        //Push data for PM PVs
        doCallbacksFloat64Array(IMagData, (size_t)_SamplesAcquired, _interruptIdAUXPMCmp0);
        doCallbacksFloat64Array(QAngData, (size_t)_SamplesAcquired, _interruptIdAUXPMCmp1);
        doCallbacksFloat64Array(_XAxis, _SamplesAcquired, _interruptIdAUXPMXAxis);
    }

    delete [] rawDataI16;
    delete [] IMagData;
    delete [] QAngData;

    return ndsSuccess;
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfPIErrorChannel::registerHandlers(
                nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_INT32(
           sis8300llrfPIErrorChannel::PV_REASON_PIERROR_DAQ_FMT,
           &sis8300llrfPIErrorChannel::setDAQFormat,
           &sis8300llrfPIErrorChannel::getDAQFormat,
           &_interruptIds[aux_param_daq_fmt]
     );

    return sis8300llrfAuxChannel::registerHandlers(pvContainers);
}

/**
 * @brief Set the PIError channel acquisition format
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Format:     
 * 0 : Mag/Ang
 * 1 : IQ
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfPIErrorChannel::setDAQFormat(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value > 1)
        return ndsError;

    _ParamVals[aux_param_daq_fmt] = (int) value;

    _ParamChanges[aux_param_daq_fmt] = 1;
    return commitParameters();
}
/**
 * @brief Check the acquisition format
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] value       Format:
 * 0 : Mag/Ang
 * 1 : IQ
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfPIErrorChannel::getDAQFormat(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[aux_param_daq_fmt];

    return ndsSuccess;
}

/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfControlTableChannelGroup.h
 * @brief Header file defining the LLRF control table (FF and SP) 
 * channelGroup class.
 * @author urojec
 * @date 23.5.2014
 */

#ifndef _sis8300llrfControlTableChannelGroup_h
#define _sis8300llrfControlTableChannelGroup_h

#include "sis8300llrfChannelGroup.h"

/**
 * @brief Control table specific sis8300llrfChannelGroup
 *
 */
class sis8300llrfControlTableChannelGroup: public sis8300llrfChannelGroup {

public:
    sis8300llrfControlTableChannelGroup(
        const std::string& name, sis8300drv_usr *newDeviceUser,
        sis8300llrfdrv_ctrl_table ctrlTableType);
    virtual ~sis8300llrfControlTableChannelGroup();

    virtual ndsStatus initialize();

    virtual ndsStatus readParameters();
    virtual ndsStatus markAllParametersChanged();

    sis8300llrfdrv_ctrl_table getCtrlTableType();
    virtual ndsStatus checkNewPulseType(int pulse_type);

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    virtual ndsStatus setSamplesCount(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getMaxNelm(asynUser *pasynUser, epicsInt32 *value);

protected:
    sis8300llrfdrv_ctrl_table _CtrlTableType; /**< Table type, either SP 
                                                * or FF */
    int _SamplesCountChanged;

    epicsInt32 _MaxSamplesCount;             /**< Maximum number of 
                                               * elements a table can 
                                               * have, as read from HW 
                                               * at init*/

    /* for asynReasons */
    static std::string PV_REASON_MAX_SAMPLESCNT;

    int _interruptIdMaxNsamples;
    
    /* write to hardware */
    virtual ndsStatus writeToHardware();
};

#endif /* _sis8300llrfControlTableChannelGroup_h */

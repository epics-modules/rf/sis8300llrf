/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfModRippleFilChannel.cpp
 * @brief Implementation of sis8300llrf interlock channel in NDS
 * @author urojec
 * @date 23.1.2015
 * 
 * This class contains all the settings related to interlocks. It also 
 * provides functions to read out interlocks on demand and will 
 * automatically get the interlock status after each pulse done 
 * interrupt from the device.
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfControllerChannelGroup.h"
#include "sis8300llrfILOCKChannel.h"

std::string sis8300llrfILOCKChannel::
                PV_REASON_ILOCK_CONDITION = "ILOCKCond";


/**
 * @brief sis8300llrfILOCKChannel constructor
 */
sis8300llrfILOCKChannel::sis8300llrfILOCKChannel() 
                                : sis8300llrfChannel(0, 0) {

    _ILOCKCondition = ilock_disabled;

    registerOnLeaveStateHandler(nds::CHANNEL_STATE_PROCESSING,
        boost::bind(&sis8300llrfILOCKChannel::onLeaveProcessing, 
        this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_RESETTING,
        boost::bind(&sis8300llrfILOCKChannel::onEnterReset, this));

    strcpy(_ChanStringIdentifier, "ILOCK channel");
}


/**
 * @brief sis8300llrfILOCKChannel destructor
 */
sis8300llrfILOCKChannel::~sis8300llrfILOCKChannel() {}

ndsStatus sis8300llrfILOCKChannel::initialize() {

    _HarlinkNum = 
        (unsigned) (getChannelNumber() - SIS8300LLRFNDS_ILOCK_CHAN0_ADDR);
    NDS_INF("Creating harlink chan %i", _HarlinkNum);
    if (_HarlinkNum > (SIS8300DRV_NUM_HAR_CHANNELS - 1)) {
        NDS_CRT("Wrong harlink num on ILOCK channel, channel num is: %i", 
            getChannelNumber());
    }

    _Initialized = 1;

    markAllParametersChanged();
    commitParameters();

    return ndsSuccess;
}

/**
 * @brief Read out data that has to be updated after every pulse
 *
 * @param [in]  from    From channel state
 * @param [in]  to      To Channels state
 *
 * @return ndsSuccess   Always
 */
ndsStatus sis8300llrfILOCKChannel::onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    
    return checkStatuses();
}

/**
 * @see #sis8300llrfChannel::onEnterReset
 */
ndsStatus sis8300llrfILOCKChannel::onEnterReset() {
    
    if (sis8300llrfChannel::onEnterReset() != ndsSuccess) {
        return ndsError;
    }
    
    return checkStatuses();
}

/**
 * @see #sis8300llrfCahnnel::writeToHardware
 */
ndsStatus sis8300llrfILOCKChannel::writeToHardware() {
    int status;
    sis8300llrfdrv_ilock_condition ilockConditionRbv;
    
    sis8300llrfChannelGroup *cg = 
            dynamic_cast<sis8300llrfChannelGroup*> (getChannelGroup());

    if (_ILOCKConditionChanged) {
        NDS_DBG("Calling set ILOCK condition with %i, %i", 
            (int)_HarlinkNum, (int) _ILOCKCondition);
        status = sis8300llrfdrv_set_ilock_condition(
                    _DeviceUser, _HarlinkNum, _ILOCKCondition);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_set_ilock_condition", status);

        status = sis8300llrfdrv_get_ilock_condition(
                    _DeviceUser, _HarlinkNum, &ilockConditionRbv);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_get_ilock_condition", status);
        
        doCallbacksInt32(
            (epicsInt32) ilockConditionRbv, _interruptIdILOCKCondition);

        if (ilockConditionRbv !=  _ILOCKCondition) {
            NDS_ERR("Readbac val from hw %i is not the same as write"
                "val %i for harlink %i", (int) ilockConditionRbv, 
                (int) _ILOCKCondition, (int) _HarlinkNum);
            SIS8300NDS_MSGERR("readback from hw is not the same as"
                "write val!");
            return ndsError;
        }

		/* only if write was successfull */
        _ILOCKConditionChanged = 0;
        
        //cg->lock();
        cg->setUpdateReason(SIS8300LLRFDRV_UPDATE_REASON_NEW_PARAMS);
        //cg->unlock();
    }

    return sis8300llrfChannel::writeToHardware();
}

/**
 * @see #sis8300llrfCahnnel::readParameters
 */
ndsStatus sis8300llrfILOCKChannel::readParameters() {
    NDS_TRC("%s", __func__);

    int status;

    sis8300llrfdrv_ilock_condition condition;

    status = sis8300llrfdrv_get_ilock_condition(
                _DeviceUser, _HarlinkNum, &condition);
    SIS8300NDS_STATUS_ASSERT(
        "sis8300llrfdrv_get_ilock_condition", status);

    doCallbacksInt32((epicsInt32)condition, _interruptIdILOCKCondition);

    return ndsSuccess;
}

/**
 * @see #sis8300llrfChannel::markAllParamtersAsChanged
 */
ndsStatus sis8300llrfILOCKChannel::markAllParametersChanged() {
    NDS_DBG("%s", __func__);

    _ILOCKConditionChanged = 1;

    return ndsSuccess;
}

/**
 * @brief All the statuses that need to be checked on leave processing
 *        and enter reset
 */
inline ndsStatus sis8300llrfILOCKChannel::checkStatuses() {
    epicsInt32 val;
    
    if (getValueInt32(NULL, &val) != ndsSuccess) {
        return ndsError;
    }
    
    return ndsSuccess;
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfILOCKChannel::registerHandlers(
                nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_INT32(
            sis8300llrfILOCKChannel::PV_REASON_ILOCK_CONDITION,
            &sis8300llrfILOCKChannel::setILOCKCondition,
            &sis8300llrfILOCKChannel::getILOCKCondition,
            &_interruptIdILOCKCondition);

    return sis8300llrfChannel::registerHandlers(pvContainers);
}

/* === GETTERS AND SETTERS FOR CONTROLLER SETTINGS === */
/* =========== RELATED TO ILOCK SETTINGS ============= */
/*
 * The getters are meant to be used by records that will keep track of 
 * setup used in the pulse that just passed. These records should be 
 * set to process at IO interrupt, because all the callbacks will get 
 * called when receiving the PULSE_DONE interrupt.
 * */

/**
 * @brief Set the interlock condition
 *
 * @param [in] pasynUser    Asyn user struct.
 * @param [in] value        The interlock condition to set.
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfILOCKChannel::setILOCKCondition(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ILOCKCondition = (sis8300llrfdrv_ilock_condition) value;
    _ILOCKConditionChanged = 1;

    return commitParameters();
}

/**
 * @brief Get the interolck condition
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the current interlock condition on 
 *                          success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * varable)
 */
ndsStatus sis8300llrfILOCKChannel::getILOCKCondition(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ILOCKCondition;

    return ndsSuccess;
}

/**
 * @brief Read harlink input value (=interlock status) directly from the 
 * device
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold ILOCK status on success
 *                          (1 for high, 0 for low)
 * 
 * @return ndsSuccess   Data retrieved successfully
 * @return ndsError     Wrong device state or could not read from 
 *                      hardware
 */
ndsStatus sis8300llrfILOCKChannel::getValueInt32(
                asynUser *pasynUser, epicsInt32 *value) {
    int status;
    unsigned uRegVal;

    if (_device->getCurrentState() == nds::DEVICE_STATE_OFF ||
            getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    status = sis8300drv_read_harlink(_DeviceUser, &uRegVal);
    SIS8300NDS_STATUS_ASSERT("sis8300drv_read_harlink", status);

    uRegVal &= 0x1 << _HarlinkNum;
    doCallbacksInt32(
        (epicsInt32) (uRegVal >> _HarlinkNum), _interruptIdValueInt32);

    *value = (epicsInt32) uRegVal;

    return ndsSuccess;
}

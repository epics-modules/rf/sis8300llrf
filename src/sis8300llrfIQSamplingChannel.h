/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfIOControlChannel.h
 * @brief Header file defining the LLRF Input and Output control (VM and 
 * IQ) Channel class
 * @author urojec
 * @date 23.5.2014
 */

#ifndef _sis8300llrfIQSamplingChannel_h
#define _sis8300llrfIQSmplChannel_h

#include "sis8300llrfChannel.h"

/**
 * @brief sis8300 LLRF specific nds::ADIOChannel class that supports PI
 *           channel
 */
class sis8300llrfIQSamplingChannel: public sis8300llrfChannel {
public:
    sis8300llrfIQSamplingChannel(unsigned nearIqM, unsigned nearIqN);
    virtual ~sis8300llrfIQSamplingChannel();

    virtual ndsStatus readParameters();

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    virtual ndsStatus setIQAngleOffset(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getIQAngleOffset(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setIQAngleOffsetEnabled(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getIQAngleOffsetEnabled(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setIQCavityInputDelay(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getIQCavityInputDelay(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setIQCavityInputDelayEnabled(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getIQCavityInputDelayEnabled(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus getNearIqParamM(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus getNearIqParamN(
                        asynUser *pasynUser, epicsInt32 *value);

protected:
    unsigned _NearIqParamM;         /**< Loacal setting for non IQ 
                                      * sampling parameter M */
    int      _NearIqParamMChanged;  /**< Internal flag to keep track of 
                                      * changes in M parameter value */
    unsigned _NearIqParamN;         /**< Loacal setting for non IQ 
                                      * sampling parameter N */
    int      _NearIqParamNChanged;  /**< Internal flag to keep track of 
                                      * changes in N parameter value */

    /* for asynReasons */
    static std::string PV_REASON_IQ_ANGLE_OFFSET_EN;
    static std::string PV_REASON_IQ_ANGLE_OFFSET;
    static std::string PV_REASON_IQ_CAVITY_INPUT_DELAY_EN;
    static std::string PV_REASON_IQ_CAVITY_INPUT_DELAY;
    static std::string PV_REASON_NEAR_IQ_PARAM_M;
    static std::string PV_REASON_NEAR_IQ_PARAM_N;

    int _interruptIdNearIqParamM;
    int _interruptIdNearIqParamN;

    /* read/write parameter */
    virtual int readParameter(int paramIdx, double *paramVal);
    virtual int writeParameter(int paramIdx, double *paramErr);
    
    /* write to hardware */
    virtual ndsStatus writeToHardware();
};

#endif /* _sis8300llrfIQSamplingChannel_h */

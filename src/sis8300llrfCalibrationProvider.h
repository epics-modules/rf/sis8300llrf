/*
 * sis8300llrf
 * Copyright (C) 2023 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfCalibrationProvider.h
 * @brief Header file for LLRF Calibration Provider.
 * @author mateusz.nabywaniec@ess.eu
 * @date 27.06.2023
 * 
 */


#ifndef _sis8300llrfCalibrationProvider_h
#define _sis8300llrfCalibrationProvider_h

#include "scalingPoly.h"

class sis8300llrfCalibrationProvider {
    public:
        sis8300llrfCalibrationProvider();
        virtual ~sis8300llrfCalibrationProvider();
        virtual void setOrderEGU2Raw(int order);
        virtual void setOrderRaw2EGU(int order);
        virtual bool isCalibratorEGU2RawEnabled();
        virtual bool isCalibratorRaw2EGUEnabled();
        virtual void setCalibrationEGU2RawEnable(bool value);
        virtual void setCalibrationRaw2EGUEnable(bool value);
        virtual bool isCalibratorEGU2RawValid();
        virtual bool isCalibratorRaw2EGUValid();
        virtual scalingPoly* getCalibratorEGU2Raw();
        virtual scalingPoly* getCalibratorRaw2EGU();

        virtual double calibrateEGU2Raw(double val) {
            return calibratorEGU2Raw->calibrate(val);
        };

        virtual double calibrateRaw2EGU(double val) {
            return calibratorRaw2EGU->calibrate(val);
        };

        virtual bool isCalibrationEnabled(){
            return isCalibratorEGU2RawEnabled() && isCalibratorRaw2EGUEnabled();
        };

    protected:
        /* calibration variables */
        scalingPoly * calibratorEGU2Raw; // used for output
        scalingPoly * calibratorRaw2EGU; // the same as input channels
        const int          maxCalibCoef = 20;
};

#endif /* _sis8300llrCalibrationProvider_h */

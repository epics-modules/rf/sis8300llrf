/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfFFChannel.cpp
 * @brief Implementation of specific features of FF Channel.
 * @author mateusz.nabywaniec@ess.eu
 * @date 14.7.2023
 *
 * 
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfChannel.h"
#include "sis8300llrfPIChannel.h"
#include "sis8300llrfControlTableChannel.h"

#include "sis8300llrfFFChannel.h"
#include <math.h>

std::string sis8300llrfFFChannel::
                    PV_REASON_CALIB_SQRT_FIT_EN          = "CalibFFSqrtFitEnable";

/**
 * @brief FF channel constructor.
 *
 * @param [in] PIChannels       Array of PI Channels to read and set I/Q
 * @param [in] SPorFF         Indicate if this channel is for SetPoint(0) or FeedForward(1)
 *
 */
sis8300llrfFFChannel::sis8300llrfFFChannel(sis8300llrfPIChannel * PIChannels[2], sis8300llrfControlTableChannel * CtrlTableCh, sis8300llrfCalibrationProvider * calibrationProvider): sis8300llrfPIMagAngChannel(PIChannels, 1, CtrlTableCh, calibrationProvider), _useSqrtFit(0){
    NDS_TRC("%s", __func__);

    calibratorSqrt    = new scalingSqrt();
}


ndsStatus sis8300llrfFFChannel::setSqrtFitEnable(
        asynUser *pasynUser, epicsInt32 value){
    _useSqrtFit = value;
    reloadCalibPVs();

    return ndsSuccess;
}

double sis8300llrfFFChannel::calibrateEGU2Raw(double value) {
    if (_useSqrtFit) // use sqrt calibrator
        return calibratorSqrt->calibrate(value);
    else
        return calibrationProvider->calibrateEGU2Raw(value);
}

void sis8300llrfFFChannel::reloadCalibPVs(){ 
    bool valid, enable;
    valid = calibrationProvider->isCalibratorEGU2RawValid() && calibrationProvider->isCalibratorRaw2EGUValid();
    enable = valid  && calibrationProvider->isCalibratorEGU2RawEnabled() && calibrationProvider->isCalibratorRaw2EGUEnabled();
    calibrationProvider->setCalibrationEGU2RawEnable(enable);
    calibrationProvider->setCalibrationRaw2EGUEnable(enable);

    doCallbacksInt32(enable, _interruptIdCalibEnable);

    scalingPoly* calibratorEGU2Raw = calibrationProvider->getCalibratorEGU2Raw();
    scalingPoly* calibratorRaw2EGU = calibrationProvider->getCalibratorRaw2EGU();  

    if (valid) {
        if (calibratorRaw2EGU->getCoeffsSize() >= 3)
            // update coefficients on calibratorSqrt
            calibratorSqrt->setCoefficients(calibratorRaw2EGU->getCoeffs()); 
        if (_useSqrtFit){// use calibratorSqrt
            doCallbacksFloat64Array(calibratorSqrt->getFittedLine(), 
                    calibratorSqrt->getFittedLineSize(), 
                    _interruptIdCalibFittedLineEGU2Raw);
            doCallbacksFloat64Array(NULL, 0,  _interruptIdCalibResidEGU2Raw); //TODO
            doCallbacksFloat64Array(NULL, 0,  _interruptIdCalibCoefEGU2Raw); //TODO
            doCallbacksFloat64(0, _interruptIdCalibChisqEGU2Raw); //TODO
        } 
        else{ // use EGU2Raw calibrator
            doCallbacksFloat64Array(calibratorEGU2Raw->getFittedLine(), 
                       calibratorEGU2Raw->getFittedLineSize(), 
                       _interruptIdCalibFittedLineEGU2Raw);
            doCallbacksFloat64Array(calibratorEGU2Raw->getResiduals(), 
                    calibratorEGU2Raw->getResidualsSize(), 
                    _interruptIdCalibResidEGU2Raw);
            doCallbacksFloat64Array(calibratorEGU2Raw->getCoeffs(), 
                    calibratorEGU2Raw->getCoeffsSize(), 
                    _interruptIdCalibCoefEGU2Raw);
            doCallbacksFloat64(calibratorEGU2Raw->getChisq(), 
                    _interruptIdCalibChisqEGU2Raw);

        }

        doCallbacksFloat64Array(calibratorRaw2EGU->getFittedLine(), 
                calibratorRaw2EGU->getFittedLineSize(), 
                _interruptIdCalibFittedLineRaw2EGU);
        doCallbacksFloat64Array(calibratorRaw2EGU->getResiduals(), 
                calibratorRaw2EGU->getResidualsSize(), 
                _interruptIdCalibResidRaw2EGU);
        doCallbacksFloat64Array(calibratorRaw2EGU->getCoeffs(), 
                calibratorRaw2EGU->getCoeffsSize(), 
                _interruptIdCalibCoefRaw2EGU);
        doCallbacksFloat64(calibratorRaw2EGU->getChisq(), 
                _interruptIdCalibChisqRaw2EGU);
    }
    else{
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibFittedLineEGU2Raw);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibResidEGU2Raw);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibCoefEGU2Raw);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibFittedLineRaw2EGU);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibResidRaw2EGU);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibCoefRaw2EGU);
    }
}

// Load EGU
ndsStatus sis8300llrfFFChannel::setCalibEGU(
        asynUser *pasynUser, epicsFloat64 *value, size_t nelem){
    NDS_TRC("%s", __func__);

    calibratorSqrt->setX(value, nelem);

    return sis8300llrfPIMagAngChannel::setCalibEGU(pasynUser, value, nelem);
}

ndsStatus sis8300llrfFFChannel::registerHandlers(
                nds::PVContainers* pvContainers) {
    NDS_PV_REGISTER_INT32(
           sis8300llrfFFChannel::PV_REASON_CALIB_SQRT_FIT_EN,
           &sis8300llrfFFChannel::setSqrtFitEnable,
           &sis8300llrfFFChannel::getInt32,
           &_interruptIdCalibSqrtFitEnable
     ); 

    return sis8300llrfPIMagAngChannel::registerHandlers(pvContainers);
}

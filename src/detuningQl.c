#include "cavityoperation.h"

static epicsInt32 INPSIZE;
static epicsInt32 INPALIGN;

static const MKL_INT incx = 1;
static  MKL_INT n;

static double  *Vcav_I, *Vcav_Q;
static double  *Vfor_I, *Vfor_Q, *Vref_I, *Vref_Q, *Vfor_ref_mag_diff;
static double  *Vfor_I_cali, *Vfor_Q_cali;

static double  *Delta_Vcav_I, *Delta_Vcav_Q;
static double  *detuning, *ql, *timearray;
static double  *dtg_Ql_tmp1, *dtg_Ql_tmp2, *dtg_Ql_tmp3, *dtg_Ql_tmp4, *dtg_Ql_tmp5,*dtg_Ql_tmp6,*dtg_Ql_tmp7,*dtg_Ql_tmp8,*dtg_Ql_tmp9, *dtg_Ql_tmp10;

static double  *abs_tmp, *cav_mag_cal, *cav_cos_tmp, *cav_sin_tmp, *cav_ang_cal;
static double  *for_mag_cal, *for_ang_cal, *for_cos_tmp, *for_sin_tmp, *for_mag_cal;
static double  *ref_mag_cal, *ref_ang_cal, *ref_cos_tmp, *ref_sin_tmp, *ref_mag_cal;

static double  *cav_pu;
static double  X_real, X_imag, Y_real, Y_imag;

char algoMsg[120];

static const double  DEGRAD = (double )(M_PI/180.0);
static const double  ANG_SCALE = 1/(2*M_PI);
static const double  MAG_SCALAR = (double )1e6;
static const MKL_INT INCX = 1;


void calibratedcavIQ(const double  *ang, const double  *pu,
                      double  leff, double  kt)
{

  double  cav_mag_scale = leff*kt*MAG_SCALAR;

  //not safe to inline scale input data: need a local copy
  memcpy(cav_ang_cal,ang,INPSIZE*sizeof(double ));

  vdAbs(n,pu,abs_tmp);
  vdSqrt(n,abs_tmp,cav_mag_cal);
  dscal(&n,&cav_mag_scale,cav_mag_cal,&INCX);

  //Angle part
  dscal(&n,&DEGRAD,cav_ang_cal,&INCX);
  //I Angle
  vdCos(n,cav_ang_cal,cav_cos_tmp);
  //Q Angle
  vdSin(n,cav_ang_cal,cav_sin_tmp);
  //VcavI
  vdMul(n,cav_mag_cal,cav_cos_tmp,Vcav_I);
  //VcavQ
  vdMul(n,cav_mag_cal,cav_sin_tmp,Vcav_Q);

}

void calibratedIQ(const double *mag,const double *ang,double *mag_cal,double *ang_cal,
                    double *cos_tmp,double *sin_tmp,const double mag_scale,double *vi,double *vq)
{

  //not safe to inline scale input data:need a local copy
  memcpy(ang_cal,ang,INPSIZE*sizeof(double ));

  //square root vector
  vdSqrt(n,mag,mag_cal);
  //scale mag
  dscal(&n,&mag_scale,mag_cal,&INCX);

  //Angle part
  dscal(&n,&DEGRAD,ang_cal,&INCX);
  //I Angle
  vdCos(n,ang_cal,cos_tmp);
  //Q Angle
  vdSin(n,ang_cal,sin_tmp);
  //Vi
  vdMul(n,mag_cal,cos_tmp,vi);
  //Vq
  vdMul(n,mag_cal,sin_tmp,vq);
}


void forward_signal_recalibration(double _dt)
{
  /*
  *start forward/reflect voltage calibration procedure:
  *
  *1.cavityPU,forward and reflect IQ part
  *2.internal 2*2 matrix elements D11/D12/D21/D22
  *3.another internal 2*1 matrix elements E11/E21
  *4.calculateXandYreal/imaginarypart
  *
  */
  //cavityPU,forward,reflectIQ
  //D11,D12,D21,D22 elements real and imaginary part
  //E11,E21 elements real and imaginary part

  double d11_real=0,d11_imag=0,d12_real=0,d12_imag=0;
  double d21_real=0,d21_imag=0,d22_real=0,d22_imag=0;
  double e11_real=0,e11_imag=0;
  double e21_real=0,e21_imag=0;

  for(unsigned int i=0;i<INPSIZE;i++)
  {
    d11_real+=(Vfor_I[i]*Vfor_I[i])-(Vfor_Q[i]*Vfor_Q[i]);//a
    d11_imag+=Vfor_I[i]*Vfor_Q[i];//b
    d12_real+=Vfor_I[i]*Vref_I[i]-Vfor_Q[i]*Vref_Q[i];//c
    d12_imag+=Vfor_I[i]*Vref_Q[i]+Vfor_Q[i]*Vref_I[i];//d
    d21_real+=Vref_I[i]*Vfor_I[i]-Vref_Q[i]*Vfor_Q[i];//e
    d21_imag+=Vref_I[i]*Vfor_Q[i]+Vfor_I[i]*Vref_Q[i];//f
    d22_real+=(Vref_I[i]*Vref_I[i])-(Vref_Q[i]*Vref_Q[i]);//g
    d22_imag+=Vref_I[i]*Vref_Q[i];//h
    e11_real+=Vfor_I[i]*Vcav_I[i]-Vfor_Q[i]*Vcav_Q[i];//i
    e11_imag+=Vfor_I[i]*Vcav_Q[i]+Vfor_Q[i]*Vcav_I[i];//j
    e21_real+=Vref_I[i]*Vcav_I[i]-Vref_Q[i]*Vcav_Q[i];//k
    e21_imag+=Vref_I[i]*Vcav_Q[i]+Vref_Q[i]*Vcav_I[i];//l
  }

  //intermediate calculation variable definition
  double var_A=(d11_real*d22_real)-(2*d11_imag*2*d22_imag)-(d12_real*d12_real)+(d12_imag*d12_imag);
  double var_B=(d11_real*2*d22_imag)+(2*d11_imag*d22_real)-(d12_real*d12_imag)-(d12_imag*d12_real);
  double var_C=(d22_real*e11_real)-(2*d22_imag*e11_imag)-(d12_real*e21_real)+(d12_imag*e21_imag);
  double var_D=(d22_real*e11_imag)+(2*d22_imag*e11_real)-(d12_real*e21_imag)-(d12_imag*e21_real);
  double var_E=-(d12_real*e11_real)+(d12_imag*e11_imag)+(d11_real*e21_real)-(2*d11_imag*e21_imag);
  double var_F=-(d12_real*e11_imag)-(d12_imag*e11_real)+(d11_real*e21_imag)+(2*d11_imag*e21_real);

  double tmpAB=(var_A*var_A)+(var_B*var_B);
  X_real=(var_A*var_C+var_B*var_D)/tmpAB;
  X_imag=(var_A*var_D-var_B*var_C)/tmpAB;
  Y_real=(var_A*var_E+var_B*var_F)/tmpAB;
  Y_imag=(var_A*var_F-var_B*var_E)/tmpAB;

  //forward signal recalibration
  for(unsigned int k=0;k<INPSIZE;k++){
    Vfor_I_cali[k]=X_real*Vfor_I[k]-X_imag*Vfor_Q[k];
    Vfor_Q_cali[k]=X_real*Vfor_Q[k]+X_imag*Vfor_I[k];
    timearray[k]=k*_dt*0.001;
  }

}

void  cav_iq_derivative(double _dt,epicsInt32 _window){
  /*
  * Differential calculation by Vcav_I and Vcav_Q
  */

  for(unsigned int m=0;m<(INPSIZE-(_window-1));m++){
    double sumIxy=0.0,sumx=0.0,sumIy=0.0,sumx2=0.0;
    double sumQxy=0.0,sumQy=0.0;
    for(int n=0;n<_window;n++){
      double _arrI=Vcav_I[m+n];
      double _arrQ=Vcav_Q[m+n];
      double _xaxis=(m+n);

      sumIxy+=_xaxis*_arrI;
      sumIy+=_arrI;
      sumQxy+=_xaxis*_arrQ;
      sumQy+=_arrQ;
      sumx+=_xaxis;
      sumx2+=_xaxis*_xaxis;
    }

    double dtsqr=_dt*_dt;
    double dtwin=_window*_dt;

    double denom=(_window*dtsqr*sumx2-dtsqr*sumx*sumx);
    double calibX=_dt*sumx;
    Delta_Vcav_I[m]=(dtwin*sumIxy-calibX*sumIy)/denom;
    Delta_Vcav_Q[m]=(dtwin*sumQxy-calibX*sumQy)/denom;
  }
}

void  compute_detuningQl(double _K,double _F0)
{

  double tmpk_scale=2*M_PI*_K;
  double fo_scale=(double )(_F0*M_PI);

  /*detuning operations*/
  vdMul(n,Vcav_I,Delta_Vcav_Q,dtg_Ql_tmp1);
  vdMul(n,Vcav_Q,Delta_Vcav_I,dtg_Ql_tmp2);
  vdSub(n,dtg_Ql_tmp1,dtg_Ql_tmp2,dtg_Ql_tmp3);

  vdMul(n,Vfor_I_cali,Vcav_Q,dtg_Ql_tmp4);
  vdMul(n,Vfor_Q_cali,Vcav_I,dtg_Ql_tmp5);
  vdSub(n,dtg_Ql_tmp4,dtg_Ql_tmp5,dtg_Ql_tmp6);

  dscal(&n,&tmpk_scale,dtg_Ql_tmp6,&INCX);
  vdAdd(n,dtg_Ql_tmp3,dtg_Ql_tmp6,dtg_Ql_tmp7);
  dscal(&n,&MAG_SCALAR,dtg_Ql_tmp7,&INCX);

  vdSqr(n,Vcav_I,dtg_Ql_tmp8);
  vdSqr(n,Vcav_Q,dtg_Ql_tmp9);
  vdAdd(n,dtg_Ql_tmp8,dtg_Ql_tmp9,dtg_Ql_tmp10);

  //detuning
  vdDiv(n,dtg_Ql_tmp7,dtg_Ql_tmp10,detuning);
  dscal(&n,&ANG_SCALE,detuning,&INCX);


  /*Ql calculation*/

  vdAdd(n,dtg_Ql_tmp8,dtg_Ql_tmp9,dtg_Ql_tmp1);
  dscal(&n,&fo_scale,dtg_Ql_tmp1,&INCX);

  vdMul(n,Vfor_I_cali,Vcav_I,dtg_Ql_tmp2);
  vdMul(n,Vfor_Q_cali,Vcav_Q,dtg_Ql_tmp3);
  vdAdd(n,dtg_Ql_tmp2,dtg_Ql_tmp3,dtg_Ql_tmp4);
  dscal(&n,&tmpk_scale,dtg_Ql_tmp4,&INCX);

  vdMul(n,Vcav_I,Delta_Vcav_I,dtg_Ql_tmp5);
  vdMul(n,Vcav_Q,Delta_Vcav_Q,dtg_Ql_tmp6);
  vdAdd(n,dtg_Ql_tmp5,dtg_Ql_tmp6,dtg_Ql_tmp7);
  vdSub(n,dtg_Ql_tmp4,dtg_Ql_tmp7,dtg_Ql_tmp8);

  //ql
  vdDiv(n,dtg_Ql_tmp1,dtg_Ql_tmp8,ql);

}

/*Intel VML Error handling*/
static int errorHandler(DefVmlErrorContext *pContext)
{
  //division error
  snprintf(algoMsg,50,"[VMLERROR at:%s:%d:%d]",pContext->cFuncName,pContext->iCode,pContext->iIndex);
  return  0;
};

static int detuningQl_init(aSubRecord *precord)
{
  mkl_set_num_threads(1);
  vmlSetMode(VML_LA|VML_ERRMODE_DEFAULT);

  INPSIZE = *(epicsInt32*)precord->q;

  /*some sanity check,it can be redundant but need to make sure
  *the input buffer is the correct size for mem alloc here.
  */
  INPALIGN = (epicsInt32)FIX_LD(INPSIZE);

  n = (MKL_INT)INPSIZE;

  abs_tmp=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  cav_mag_cal=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  for_mag_cal=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  ref_mag_cal=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);

  cav_ang_cal=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  for_ang_cal=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  ref_ang_cal=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  //tempconversionvars

  cav_cos_tmp=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  cav_sin_tmp=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);

  for_cos_tmp=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  for_sin_tmp=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);

  ref_cos_tmp=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  ref_sin_tmp=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);

  //I/Q buffers
  Vcav_I=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  Vcav_Q=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);

  Vfor_I=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  Vfor_Q=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);

  Vref_I=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  Vref_Q=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  //Cavity I/Q cav_iq_derivative
  Delta_Vcav_I=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  Delta_Vcav_Q=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);

  //Calibrated forward I/Q
  Vfor_I_cali=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  Vfor_Q_cali=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);

  //detuning and Ql and time waveforms
  detuning=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  ql=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  timearray=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);

  dtg_Ql_tmp1=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  dtg_Ql_tmp2=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  dtg_Ql_tmp3=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  dtg_Ql_tmp4=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  dtg_Ql_tmp5=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  dtg_Ql_tmp6=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  dtg_Ql_tmp7=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  dtg_Ql_tmp8=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  dtg_Ql_tmp9=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);
  dtg_Ql_tmp10=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);

  // temporary: to be remove with quench perfromance computation goes to FIM IOC
  Vfor_ref_mag_diff=(double *)mkl_malloc(INPALIGN*sizeof(double ),64);

  if(!abs_tmp||!for_mag_cal||!ref_mag_cal||!cav_mag_cal||!cav_ang_cal
	||!for_ang_cal||!ref_ang_cal||!cav_cos_tmp||!cav_sin_tmp||!for_cos_tmp
	||!for_sin_tmp||!ref_cos_tmp||!ref_sin_tmp||!Vcav_I||!Vcav_Q||!Vfor_I
	||!Vfor_Q||!Vref_I||!Vref_Q||!Delta_Vcav_I||!Delta_Vcav_Q ||!Vfor_I_cali
	||!Vfor_Q_cali||!detuning||!ql||!timearray||!dtg_Ql_tmp1||!dtg_Ql_tmp2
	||!dtg_Ql_tmp3||!dtg_Ql_tmp4||!dtg_Ql_tmp5||!dtg_Ql_tmp6||!dtg_Ql_tmp7
	||!dtg_Ql_tmp8||!dtg_Ql_tmp9||!dtg_Ql_tmp10||!Vfor_ref_mag_diff)
  {
    return -1;
  }
  sleep(1);
  return  0;
}

static int detuningQl_algo(aSubRecord *precord)
{

  double s_initial = dsecnd();

  VMLErrorCallBack errcb;
  errcb=errorHandler;
  vmlSetErrorCallBack(errcb);

  //Get cavity string
  const char*cav;
  cav=(char*)precord->r;

  /*The dynamic routine is called once during init to CP links
  *this causes nes to be 0 as the asub record is already instantiated.
  *when FSM is ON thus,a,b,c,d,e,f waveforms are valid with datasize==nes
  *We read nea to ensure all waveforms are same lenght
  */

  INPSIZE=precord->nea;

  //INB,INC,IND,INE and INF should have the same number of points
  if(precord->neb!=INPSIZE||precord->nec!=INPSIZE||precord->ned!=INPSIZE||precord->nee!=INPSIZE||precord->nef!=INPSIZE){
    snprintf(algoMsg,100,"%s: Error- Invalid input array!\n",cav);
    return  -1;
  }

  //Internal cavity PU waveform
  cav_pu=(double *)precord->a;

  //Cavity Ang,For and Ref Mag/Ang waveform
  double *cav_ang=(double *)precord->b;
  double *for_mag=(double *)precord->c;
  double *for_ang=(double *)precord->d;
  double *ref_mag=(double *)precord->e;
  double *ref_ang=(double *)precord->f;

  //system frequency[MHz]
  const double F0=*(double *)precord->g;
  //time between samples MHZh is tick Time in KHz.
  double dt=*(double *)precord->h*1000;
  //cavity kt
  const double kt=*(double *)precord->i;

  //cavity effective length[m]
  const double leff=*(double *)precord->j;
  //cavity R/Q
  const double R_over_Q=*(double *)precord->k;
  //cavity external Q
  const double Qext=*(double *)precord->l;
  //Derivative window
  const epicsInt32 window=*(epicsInt32*)precord->m;
  //parameter K
  const double K=F0/Qext;
  //parameter B(with beam)
  const double B=R_over_Q*F0/2;

  long i_rfend_cav=*(long *)precord->n;
  long i_rfend_fwd=*(long *)precord->o;
  long i_rfend_ref=*(long *)precord->p;

  //check the index of RF endpulse for cavity pu,cavity forward and reflect
  if(i_rfend_cav>INPSIZE||i_rfend_fwd>INPSIZE||i_rfend_ref>INPSIZE||i_rfend_cav!=i_rfend_fwd||i_rfend_cav!=i_rfend_ref){
    snprintf(algoMsg,100,"%s:Invalid input array!\n",cav);
    return  -1;
  }

  /*
  * should see some RF inside cavity for calculation 2MV/m
  */
  const double avgEaccMin = *(double*)precord->s;
  const int winEacc=16;
  double sumEacc=0;

  for(int a=0;a<winEacc;a++){
    sumEacc+=kt*sqrt(cav_pu[abs(a+i_rfend_cav-winEacc+1)]);
  }

  double avgEacc=sumEacc/winEacc;
  if(isnan(avgEacc) || avgEacc < avgEaccMin){
    snprintf(algoMsg,100,"%s Decimation Error %f !\n",cav, avgEacc);
    return  -1;
  }
  /*
  *start forward/reflect voltage calibration procedures
  */

  //Calibrte and converttoI/Q
  calibratedcavIQ(cav_ang,cav_pu,leff,kt);

  double R_over_Qext=sqrt(R_over_Q*Qext*1e3);
  //forward
  calibratedIQ(for_mag,for_ang,for_mag_cal,for_ang_cal,for_cos_tmp,for_sin_tmp,R_over_Qext,Vfor_I,Vfor_Q);
  //Reflected
  calibratedIQ(ref_mag,ref_ang,ref_mag_cal,ref_ang_cal,ref_cos_tmp,ref_sin_tmp,R_over_Qext,Vref_I,Vref_Q);

  //compplex constant and forward signal recalibration
  forward_signal_recalibration(dt);
  //cavity I/Q derivative

  cav_iq_derivative(dt,window);
  //calculate detuning and Ql
  compute_detuningQl(K,F0);

  /* Temporary  forwardand reflected power differention for quench 
     performance evaluation ( see ICSHWI-19540), this code will be moved to FIM IOC.*/
  vdSub(n,for_mag, ref_mag, Vfor_ref_mag_diff);

  //Update Detuning,Ql,Cccx,Cccy,CavI/Q derivative,algotime
  precord->neva=INPSIZE;
  precord->nevb=INPSIZE;
  precord->nevc=INPSIZE;
  precord->nevd=INPSIZE;
  precord->neve=INPSIZE;
  precord->nevf=INPSIZE;
  precord->nevg=INPSIZE;
  precord->nevh=INPSIZE;
  precord->nevi=INPSIZE;
  precord->nevj=INPSIZE;
  precord->nevk=INPSIZE;
  precord->nevl=INPSIZE;
  precord->nevm=INPSIZE;
  precord->nevt=INPSIZE;
  //Update Detuning,Ql and time series PVs
  memcpy((double *)precord->vala,detuning,INPSIZE*sizeof(double ));
  memcpy((double *)precord->valb,ql,INPSIZE*sizeof(double ));
  memcpy((double *)precord->valc,timearray,INPSIZE*sizeof(double ));

  memcpy((double *)precord->vald,Vcav_I,INPSIZE*sizeof(double ));
  memcpy((double *)precord->vale,Vcav_Q,INPSIZE*sizeof(double ));
  memcpy((double *)precord->valf,Vfor_I,INPSIZE*sizeof(double ));
  memcpy((double *)precord->valg,Vfor_Q,INPSIZE*sizeof(double ));
  memcpy((double *)precord->valh,Vref_I,INPSIZE*sizeof(double ));
  memcpy((double *)precord->vali,Vref_Q,INPSIZE*sizeof(double ));

  memcpy((double *)precord->valj,Delta_Vcav_I,INPSIZE*sizeof(double ));
  memcpy((double *)precord->valk,Delta_Vcav_Q,INPSIZE*sizeof(double ));

  memcpy((double *)precord->vall,Vfor_I_cali,INPSIZE*sizeof(double ));
  memcpy((double *)precord->valm,Vfor_Q_cali,INPSIZE*sizeof(double ));

  // Temporary: to be removed when quench performance computing goes  to FIM IOC
  memcpy((double *)precord->valt,Vfor_ref_mag_diff,INPSIZE*sizeof(double ));

  //Update Complex calibration constant PVs
  *(double *)precord->valn=X_real;
  *(double *)precord->valo=X_imag;
  *(double *)precord->valp=Y_real;
  *(double *)precord->valq=Y_imag;
  strcpy((char*)precord->valr,algoMsg);

  double s_elapsed=(dsecnd()-s_initial)*1000;

  //update algo time PV
  *((double *)precord->vals)=s_elapsed;
  precord->nevs=1;

  return  0;
}

epicsRegisterFunction(detuningQl_init);
epicsRegisterFunction(detuningQl_algo);

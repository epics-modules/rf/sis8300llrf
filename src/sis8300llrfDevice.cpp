/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfDevice.cpp
 * @brief Implementation of LLRF device in NDS.
 * @author urojec
 * @date 30.5.2014
 */
#include <unistd.h>

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"
#include "sis8300llrfdrv_types.h"

#include "sis8300llrfCalibrationProvider.h"
#include "sis8300llrfAIChannel.h"
#include "sis8300llrfCavityChannel.h"
#include "sis8300llrfChannel.h"
#include "sis8300llrfPIChannel.h"
#include "sis8300llrfIQSamplingChannel.h"
#include "sis8300llrfVMControlChannel.h"
#include "sis8300llrfControlTableChannel.h"
#include "sis8300llrfRippleFilterChannel.h"
#include "sis8300llrfNotchFilterChannel.h"
#include "sis8300llrfLowPassFiltChannel.h"
#include "sis8300llrfILOCKChannel.h"
#include "sis8300RegisterChannel.h"
#include "sis8300llrfSignalMonitorChannel.h"
#include "sis8300llrfRefCompMonitorChannel.h"
#include "sis8300llrfDownSampledChannel.h"
#include "sis8300llrfDownSampledCavityChannel.h"
#include "sis8300llrfInternalChannel.h"
#include "sis8300llrfPIErrorChannel.h"
#include "sis8300llrfPIMagAngChannel.h"
#include "sis8300llrfSPWithCavityChannel.h"
#include "sis8300llrfFFChannel.h"
#include "sis8300llrfSignalMonitorCavityChannel.h"

#include "sis8300llrfDevice.h"

nds::RegisterDriver<sis8300llrfDevice> exportedDevice("sis8300llrf");

std::string sis8300llrfDevice::PV_REASON_PULSE_DONE_COUNT     = "PulseDoneCount";
std::string sis8300llrfDevice::PV_REASON_PULSE_MISSED         = "PulseMissed";
std::string sis8300llrfDevice::PV_REASON_UPDATE_REASON        = "UpdateReason";
std::string sis8300llrfDevice::PV_REASON_ARM                  = "Arm";
std::string sis8300llrfDevice::PV_REASON_PULSE_DONE           = "PulseDone";
std::string sis8300llrfDevice::PV_REASON_STATUS               = "Status";
std::string sis8300llrfDevice::PV_REASON_SETUP_ACTIVE         = "SetupActive";
std::string sis8300llrfDevice::PV_REASON_F_SAMPLING           = "FSampling";
std::string sis8300llrfDevice::PV_REASON_F_SYSTEM             = "FSystem";
std::string sis8300llrfDevice::PV_REASON_ILOCK_LPS            = "ILockLPS";
std::string sis8300llrfDevice::PV_REASON_FSM_FW               = "FSMFirmware";
std::string sis8300llrfDevice::PV_REASON_ILOCK_CAUSE          = "IlckCause";
std::string sis8300llrfDevice::PV_REASON_RFEND_CAUSE          = "RFEndCause";
std::string sis8300llrfDevice::PV_REASON_DAQEND_CAUSE         = "DAQEndCause";
std::string sis8300llrfDevice::PV_REASON_MAX_RF_LENGTH        = "MaxRFLength";
std::string sis8300llrfDevice::PV_REASON_MAX_DAQ_LENGTH        = "MaxDAQLength";
std::string sis8300llrfDevice::PV_REASON_LPS_DEAD_TIME         = "LPSDeadTime";
std::string sis8300llrfDevice::PV_REASON_LPS_ENABLE            = "LPSEnable";
std::string sis8300llrfDevice::PV_REASON_FREQ_SAMP_MEAS        = "FreqSampMeas";
std::string sis8300llrfDevice::PV_REASON_FORCE_ILOCK           = "ForceIlock";
std::string sis8300llrfDevice::PV_REASON_ACQ_TIME              = "AcqTime";
std::string sis8300llrfDevice::PV_REASON_MAIN_INSTANCE         = "MainInstance";

/**
 * @brief Device constructor.
 * @param [in] name Device name.
 *
 * LLRF device constructor. For details refer to NDS documentation.
 */
sis8300llrfDevice::sis8300llrfDevice(const std::string& name) :
        sis8300Device(name) {


    registerOnEnterStateHandler(nds::DEVICE_STATE_ERROR,
            boost::bind(&sis8300llrfDevice::onEnterError, this, _1, _2));

    registerOnRequestStateHandler(nds::DEVICE_STATE_INIT, nds::DEVICE_STATE_ON,
            boost::bind(&sis8300llrfDevice::onSwitchOn, this, _1, _2));
    registerOnEnterStateHandler(nds::DEVICE_STATE_ON,
            boost::bind(&sis8300llrfDevice::onEnterOn, this, _1, _2));
    registerOnLeaveStateHandler(nds::DEVICE_STATE_ON,
            boost::bind(&sis8300llrfDevice::onLeaveOn, this, _1, _2));

    /* Do similar for reset as it is done for processing -> first call
     * the device onEnterReset and than send all CGs to resetting
     *
     * order of registration IS IMPORTAINT!
     */
    registerOnEnterStateHandler(nds::DEVICE_STATE_RESETTING,
            boost::bind(&sis8300llrfDevice::onEnterReset, this, _1, _2));

    registerOnEnterStateHandler(nds::DEVICE_STATE_RESETTING,
            boost::bind(&sis8300llrfDevice::llrfCgStateSet, this, nds::CHANNEL_STATE_RESETTING));


    /* add check for Interlock to all request transitions
     * (request INIT and request ON)
     */
    registerOnRequestStateHandler(nds::DEVICE_STATE_RESETTING, nds::DEVICE_STATE_INIT,
            boost::bind(&sis8300llrfDevice::checkInterlock, this));
    registerOnRequestStateHandler(nds::DEVICE_STATE_INIT, nds::DEVICE_STATE_ON,
            boost::bind(&sis8300llrfDevice::checkInterlock, this));


    prohibitTransition(nds::DEVICE_STATE_ON, nds::DEVICE_STATE_INIT);
    prohibitTransition(nds::DEVICE_STATE_ON, nds::DEVICE_STATE_OFF);
    prohibitTransition(nds::DEVICE_STATE_OFF, nds::DEVICE_STATE_RESETTING);
    prohibitTransition(nds::DEVICE_STATE_RESETTING, nds::DEVICE_STATE_ON);

    /* handle INIT msg */
    registerMessageWriteHandler("INIT",
            boost::bind(&sis8300llrfDevice::handleInitMsg, this, _1, _2));

    /* Main task taking care of the loop when controller is on
         */
    _ControlLoopTask = nds::ThreadTask::create(
            nds::TaskManager::generateName("controlLoopTask"),
            epicsThreadGetStackSize(epicsThreadStackMedium),
            epicsThreadPriorityHigh,
            boost::bind(&sis8300llrfDevice::controlLoopTask, this, _1));


    _readLoopTaskRun = 1;
    /* task taking care of read data updated on fix interval
         */
    _readLoopTask = nds::ThreadTask::create(
            nds::TaskManager::generateName("readLoopTask"),
            epicsThreadGetStackSize(epicsThreadStackMedium),
            epicsThreadPriorityLow,
            boost::bind(&sis8300llrfDevice::readLoopTask, this, _1));

    _DeviceUser = NULL;

    _UpdateReason = 0;
    _SetupActive  = 0;
    _FSMFirmware  = 0;
    _IlockCause = 0;
    _MaxRFLengthChange = 0;
    _MaxRFLength = 0;
    _MaxDAQLengthChange = 0;
    _MaxDAQLength = 0;
    _LPSDeadTimeChange = 0;
    _LPSDeadTime = 0;
    _LPSEnable = 1;
    _LPSEnableChange = 1;
    _PerfTiming = 0;
}

/**
 * @brief LLRF device destructor. For details refer to NDS documentation.
 * 
 * @param [in] name Device name.
 *
 */
sis8300llrfDevice::~sis8300llrfDevice() {
    _readLoopTaskRun = 0;
    if (_DeviceUser) {
        sis8300llrfdrv_sw_reset(_DeviceUser);
    }
}

/**
 * @brief Create driver structures.
 *
 * @param [in] portName AsynPort name.
 * @param [in] params   Refer to NDS documentation.
 *
 * @retval ndsSuccess createStructure always returns success.
 *
 * The function creates all the channel groups and registers channels with them.
 * This is achieved by calling the following functions:
 *  - #registerAIChannels
 *  - #registerControlTableChannels
 *  - #registerControllerChannels
 *  - #registerSignalMonitorChannels
 *  - #registerRegisterChannels
 *  - #registerAuxiliaryChannels
 *
 * Because we require custom channel groups and channels we override the createStructure
 * from base class. We also don't register trigger channel groups and AO channel groups
 *
 * This function also allocates space for #_DeviceUser and copies the value of the
 * FILE parameter into it.
 */
ndsStatus sis8300llrfDevice::createStructure(const char* portName, const char* params) {
    NDS_TRC("%s", __func__);
    
    ndsStatus status;
    nds::ChannelGroup *cg;

    _DeviceUser = (sis8300drv_usr *) calloc(1, sizeof(sis8300drv_usr));
    _DeviceUser->file = getStrParam("FILE", "").c_str();

    _FSampling = (epicsFloat64) atof((getStrParam("FSAMPLING", "0")).c_str());
    if (_FSampling == 0) 
        _FSampling = FSAMPLING_DEFAULT;
    NDS_DBG("F Sampling %f", _FSampling);

    _FSystem = (epicsFloat64) atof((getStrParam("FSYSTEM", "0")).c_str());
    NDS_DBG("F System %f", _FSystem);

    _NearIQM = (unsigned) atoi((getStrParam("NEARIQM", "0")).c_str());
    if (_NearIQM == 0) {
        NDS_CRT("NEARIQM should be set on ndsCreateDevice");
        return ndsError;
    }
    NDS_DBG("Near IQ M %d", _NearIQM);

    _NearIQN = (unsigned) atoi((getStrParam("NEARIQN", "0")).c_str());
    if (_NearIQN == 0) {
        NDS_CRT("NEARIQN should be set on ndsCreateDevice");
        return ndsError;
    }
    NDS_DBG("Near IQ M %d", _NearIQN);

    //Enable or disable first digitizer
    _LPSEnable = (unsigned) atoi((getStrParam("LPSEN", "1")).c_str());
    NDS_DBG("LPS Enable %d", _LPSEnable);

    //Has a cavity channel
    _HasCavCh = (unsigned) atoi((getStrParam("HASCAV", "0")).c_str());
    NDS_DBG("Has cavity channel %d", _HasCavCh);

    //Get maximum size for tables
    _CtrlTblMax = (unsigned) atoi((getStrParam("TABLE_SMNM_MAX", SIS8300LLRFDRV_CTRL_TBL_MAX_LEN_DEFAULT)).c_str());
    NDS_DBG("Control Table maximum size %d", _CtrlTblMax);

    calibrationProviderFF = new sis8300llrfCalibrationProvider();
    calibrationProviderSP = new sis8300llrfCalibrationProvider();

    /* this one has to be the first, so that the asyn addr matches the one in parent */
    status = registerAIChannels();
    if (status != ndsSuccess) {
        return status;
    }

    status = registerControllerChannels();
    if (status != ndsSuccess) {
        return status;
    }

    status = registerControlTableChannels(ctrl_table_sp);
    if (status != ndsSuccess) {
        return status;
    }

    status = registerControlTableChannels(ctrl_table_ff);
    if (status != ndsSuccess) {
        return status;
    }

    status = registerPIMagAngChannels();
    if (status != ndsSuccess) {
        return status;
    }

    status = registerSignalMonitorChannels();
    if (status != ndsSuccess) {
        return status;
    }

    status = registerRefCompMonitorChannels();
    if (status != ndsSuccess) {
        NDS_ERR("Failed to register Reference Compensation Monitor Channels");
        return status;
    }


    status = registerRegisterChannels();
    if (status != ndsSuccess) {
        NDS_ERR("Failed to register Register Channels");
    }
    else {
        getChannelGroup(SIS8300NDS_REGCG_NAME, &cg);
        _CgReg = dynamic_cast<sis8300RegisterChannelGroup *>(cg);
    }

    status = registerAuxChannels();
    if (status != ndsSuccess) {
        return status;
    }

    
    /* in the same order that they are registered - matches asyn addr*/
     _llrfChannelGroups[0] = _CgCtrl;
     _llrfChannelGroups[1] = _CgSP;
     _llrfChannelGroups[2] = _CgFF;
     _llrfChannelGroups[3] = _CgSigmon;
     _llrfChannelGroups[4] = _CgRefCompMon;
     _llrfChannelGroups[5] = _CgAux;

    return ndsSuccess;
}

/**
 * @brief Register analog input channel group and channels.
 *
 * @retval ndsSuccess Channel group or channels registered successfully.
 * @retval ndsSuccess Channel group or channels registration failed.
 *
 * Registers a #sis8300llrfAIChannelGroup named #SIS8300NDS_AICG_NAME
 * and then registers #SIS8300DRV_NUM_AI_CHANNELS times #sis8300AIChannel
 * with the channel group.
 */
ndsStatus sis8300llrfDevice::registerAIChannels() {
    int iter;
    ndsStatus status;
    sis8300llrfAIChannel * channel;

    _CgAI = new sis8300llrfAIChannelGroup(SIS8300NDS_AICG_NAME);
    status = registerChannelGroup(_CgAI);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", SIS8300NDS_AICG_NAME);
        return ndsError;
    }

    if (_HasCavCh) {
        channel = new sis8300llrfCavityChannel(_FSampling);
        status = _CgAI->registerChannel(channel);

        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for %s channel 0", SIS8300NDS_AICG_NAME);
            return ndsError;
        }
        _AIChannels[0] = channel;

        for (iter = 1; iter < SIS8300DRV_NUM_AI_CHANNELS; iter++) {
            channel = new sis8300llrfAIChannel(_FSampling);
            status = _CgAI->registerChannel(channel);

            if (status != ndsSuccess) {
                NDS_ERR("registerChannel error for %s channel %d.", SIS8300NDS_AICG_NAME, iter);
                return ndsError;
            }
            _AIChannels[iter] = channel;
        }
    }
    else
        for (iter = 0; iter < SIS8300DRV_NUM_AI_CHANNELS; iter++) {
            channel = new sis8300llrfAIChannel(_FSampling);
            status = _CgAI->registerChannel(channel);

            if (status != ndsSuccess) {
                NDS_ERR("registerChannel error for %s channel %d.", SIS8300NDS_AICG_NAME, iter);
                return ndsError;
            }
            _AIChannels[iter] = channel;
        }

    _CgAI->setDeviceUser(_DeviceUser);

    return ndsSuccess;
}

/**
 * @brief Register controller channel group and channels.
 *
 * @retval ndsSuccess Channel group or channels registered successfully.
 * @retval ndsSuccess Channel group or channels registration failed.
 *
 * Registers a #sis8300llrfControllerChannelGroup named
 * #SIS8300LLRFNDS_CONTROLLER_CG_NAME and then registers
 * two #sis8300llrfPIChannel and two #sis8300llrfIOControlChannel
 * channels
 */
ndsStatus sis8300llrfDevice::registerControllerChannels() {
    ndsStatus status;
    int i;

    _CgCtrl = new sis8300llrfControllerChannelGroup(SIS8300LLRFNDS_CONTROLLER_CG_NAME, _DeviceUser);
    status = registerChannelGroup(_CgCtrl);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }

    /* register all controler channels. Order is importaint, @see
     * #SIS8300LLRFNDS_PI_I_CHAN_ADDR, #SIS8300LLRFNDS_PI_Q_CHAN_ADDR,
     * #SIS8300LLRFNDS_IO_IQ_CHAN_ADDR, #SIS8300LLRFNDS_IO_VM_CHAN_ADDR
     */

    _PIChannels[0] = new sis8300llrfPIChannel(pi_I);
    status = _CgCtrl->registerChannel(_PIChannels[0]);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel PI magnitude.",
                SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }
    
    _PIChannels[1] = new sis8300llrfPIChannel(pi_Q);
    status = _CgCtrl->registerChannel(_PIChannels[1]);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel PI angle.",
                SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }

    status = _CgCtrl->registerChannel(new sis8300llrfIQSamplingChannel(_NearIQM, _NearIQN));
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel IQ control.",
                SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }

    status = _CgCtrl->registerChannel(new sis8300llrfVMChannel(calibrationProviderFF));
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel VM control.",
                SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }

    status = _CgCtrl->registerChannel(new sis8300llrfRippleFilterChannel(_FSampling, _NearIQN));
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel modulator ripple filter setup.",
                SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }

    /* Register 4 channels for interlock */
    for (i = 0; i < SIS8300DRV_NUM_HAR_CHANNELS; i++) {
        status = _CgCtrl->registerChannel(new sis8300llrfILOCKChannel());
        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for %s channel ILOCK %i.",
                    SIS8300LLRFNDS_CONTROLLER_CG_NAME, i);
            return ndsError;
        }
    }
    
    status = _CgCtrl->registerChannel(new sis8300llrfNotchFilterChannel(_FSampling, _NearIQN));
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel notch filter setup.",
                SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }

    status = _CgCtrl->registerChannel(new sis8300llrfLowPassFiltChannel(_FSampling, _NearIQN));
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel low pass filter setup.",
                SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }

    return ndsSuccess;
}


/*
 * @brief: Register PI Calibration channels
 * */
ndsStatus sis8300llrfDevice::registerPIMagAngChannels() {
    ndsStatus status;
    nds::Channel *chan;

    //Set point
    status = getChannel(_CgSP->getName(), 0, &chan);
    
    //If there is a cavity channel , use a different class
    if (_HasCavCh) 
        _PIMagAngChannels[0] = new sis8300llrfSPWithCavityChannel(_PIChannels, (dynamic_cast<sis8300llrfControlTableChannel *>(chan)),static_cast<sis8300llrfCavityChannel *>(_AIChannels[0]), calibrationProviderSP);
    else
        _PIMagAngChannels[0] = new sis8300llrfPIMagAngChannel(_PIChannels, 0, (dynamic_cast<sis8300llrfControlTableChannel *>(chan)), calibrationProviderSP);
    status = _CgCtrl->registerChannel(_PIMagAngChannels[0]);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel PI SP Calibration",
                SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }

    //Feed Forward
    status = getChannel(_CgFF->getName(), 0, &chan);
    _PIMagAngChannels[1] = new sis8300llrfFFChannel(_PIChannels, (dynamic_cast<sis8300llrfControlTableChannel *>(chan)), calibrationProviderFF);
    status = _CgCtrl->registerChannel(_PIMagAngChannels[1]);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s channel PI FF Calibration",
                SIS8300LLRFNDS_CONTROLLER_CG_NAME);
        return ndsError;
    }

    return ndsSuccess;
}


/**
 * @brief Register control table channel group and channels.
 *
 * @retval ndsSuccess Channel group or channels registered successfully.
 * @retval ndsSuccess Channel group or channels registration failed.
 *
 * Registeres a #sis8300llrfControlTableChannelGroup named
 * #SIS8300LLRFNDS_SPCG_NAME or #SIS8300LLRFNDS_FFCG_NAME
 * and then registeres #pulseTypes times #sis8300llrfControlTableChannel
 * with the channel group.
 */
ndsStatus sis8300llrfDevice::registerControlTableChannels(sis8300llrfdrv_ctrl_table ctrlTableType) {
    ndsStatus status;
    sis8300llrfControlTableChannelGroup *cg;

    switch (ctrlTableType) {
        case ctrl_table_sp:
            _CgSP = new sis8300llrfControlTableChannelGroup(SIS8300LLRFNDS_SPCG_NAME, _DeviceUser, ctrlTableType);
            cg = _CgSP;
            break;
        case ctrl_table_ff:
            _CgFF = new sis8300llrfControlTableChannelGroup(SIS8300LLRFNDS_FFCG_NAME, _DeviceUser, ctrlTableType);
            cg = _CgFF;
            break;
        default:
            NDS_ERR("Create ControlTableChannelGroup called with wrong control table type.");
            return ndsError;
    }

    status = registerChannelGroup(cg);
    if (status != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", (cg->getName()).c_str());
        return ndsError;
    }

    status = cg->registerChannel(new sis8300llrfControlTableChannel(_CtrlTblMax));
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s.",
                (cg->getName()).c_str());
        return ndsError;
    }

    return ndsSuccess;
}

/**
 * @brief Register signal monitoring channel group and channels.
 *
 * @retval ndsSuccess Channel group or channels registered successfully.
 * @retval ndsSuccess Channel group or channels registration failed.
 *
 * Registers a #sis8300llrfChannelGroup named #SIS8300LLRFNDS_SIGMON_CG_NAME
 * and then registers #SIS8300DRV_NUM_AI_CHANNELS times #sis8300llrfSigmonChannel
 * with the channel group.
 */
ndsStatus sis8300llrfDevice::registerSignalMonitorChannels() {
    int iter;
    ndsStatus status;

    _CgSigmon = new sis8300llrfChannelGroup(SIS8300LLRFNDS_SIGMON_CG_NAME, _DeviceUser);
    
    if (registerChannelGroup(_CgSigmon) != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", SIS8300LLRFNDS_SIGMON_CG_NAME);
        return ndsError;
    }
    

    if (_HasCavCh) {
        // first channel will have a different class
        status = _CgSigmon->registerChannel(new sis8300llrfSignalMonitorCavityChannel(static_cast<sis8300llrfCavityChannel *>(_AIChannels[0])));
        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for sigmon channel 0.");
            return ndsError;
        }
        for (iter = 1; iter < SIS8300DRV_NUM_AI_CHANNELS; iter++) {
            //store respective AI channel on signal monitor channel
            status = _CgSigmon->registerChannel(new sis8300llrfSignalMonitorChannel(_AIChannels[iter]));
            if (status != ndsSuccess) {
                NDS_ERR("registerChannel error for %s channel %d.", SIS8300LLRFNDS_SIGMON_CG_NAME, iter);
                return ndsError;
            }
        }
    }
    else
        for (iter = 0; iter < SIS8300DRV_NUM_AI_CHANNELS; iter++) {
            //store respective AI channel on signal monitor channel
            status = _CgSigmon->registerChannel(new sis8300llrfSignalMonitorChannel(_AIChannels[iter]));
            if (status != ndsSuccess) {
                NDS_ERR("registerChannel error for %s channel %d.", SIS8300LLRFNDS_SIGMON_CG_NAME, iter);
                return ndsError;
            }
        }

    return ndsSuccess;
}


/**
 * @brief Register reference compensation monitoring channel group and channels.
 *
 * @retval ndsSuccess Channel group or channels registered successfully.
 * @retval ndsError Channel group or channels registration failed.
 *
 * Registers a #sis8300llrfChannelGroup named #SIS8300LLRFNDS_REFCOMP_CG_NAME
 * and then register one time #sis8300llrfRefCompMonitorChannel
 * with the channel group.
 */
ndsStatus sis8300llrfDevice::registerRefCompMonitorChannels() {
    ndsStatus status;

    _CgRefCompMon = new sis8300llrfChannelGroup(SIS8300LLRFNDS_REFCOMP_CG_NAME, _DeviceUser);
    
    if (registerChannelGroup(_CgRefCompMon) != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", SIS8300LLRFNDS_REFCOMP_CG_NAME);
        return ndsError;
    }
   
    //Use fixed cavity and reference lien for now
    status = _CgRefCompMon->registerChannel(new
        sis8300llrfRefCompMonitorChannel());
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for %s", SIS8300LLRFNDS_REFCOMP_CG_NAME);
        return ndsError;
    }

    return ndsSuccess;
}



/**
 * @brief Register Auxiliary group for Down-Sampled and Internal channels.
 *
 * @retval ndsSuccess Channel group or channels registered successfully.
 * @retval ndsSuccess Channel group or channels registration failed.
 *
 * Registers a #sis8300llrfChannelGroup named #SIS8300LLRFNDS_AUX_CG_NAME
 * and then registers #SIS8300DRV_NUM_AI_CHANNELS times #sis8300llrfDownSampledChannel
 * and #SIS8300LLRFDRV_INTERN_CHANNELS times #sis8300llrfInternalChannel
 * with the channel group.
 */
ndsStatus sis8300llrfDevice::registerAuxChannels() {
    int iter;
    ndsStatus status;

    _CgAux  = new sis8300llrfAuxChannelGroup(SIS8300LLRFNDS_AUX_CG_NAME, _DeviceUser);
    
    if (registerChannelGroup(_CgAux) != ndsSuccess) {
        NDS_ERR("registerChannelGroup %s error.", SIS8300LLRFNDS_AUX_CG_NAME);
        return ndsError;
    }

    if (_HasCavCh) {
        // first channel will have a different class
        status = _CgAux->registerChannel(new sis8300llrfDownSampledCavityChannel(_FSampling, _NearIQN, static_cast<sis8300llrfCavityChannel *>(_AIChannels[0])));
        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for down-sampled channel 0.");
            return ndsError;
        }
        for (iter = 1; iter < SIS8300DRV_NUM_AI_CHANNELS; iter++) {
            status = _CgAux->registerChannel(new sis8300llrfDownSampledChannel(_FSampling, _NearIQN, _AIChannels[iter]));
            if (status != ndsSuccess) {
                NDS_ERR("registerChannel error for down-sampled channel %d.", iter);
                return ndsError;
            }
        }
    }
    else
        for (iter = 0; iter < SIS8300DRV_NUM_AI_CHANNELS; iter++) {
            status = _CgAux->registerChannel(new sis8300llrfDownSampledChannel(_FSampling, _NearIQN, _AIChannels[iter]));
            if (status != ndsSuccess) {
                NDS_ERR("registerChannel error for down-sampled channel %d.", iter);
                return ndsError;
            }
        }

    //First internal channel is PI Err, its have a specific class for it
    status = _CgAux->registerChannel(new sis8300llrfPIErrorChannel(_FSampling, _NearIQN, _PIMagAngChannels[0]));
    if (status != ndsSuccess) {
        NDS_ERR("registerChannel error for PI error channel.");
        return ndsError;
    }

    for (iter = 1; iter < SIS8300LLRFDRV_INTERN_CHANNELS; iter++) {
        status = _CgAux->registerChannel(new sis8300llrfInternalChannel(_FSampling, _NearIQN));
        if (status != ndsSuccess) {
            NDS_ERR("registerChannel error for Internal channel %d.", iter);
            return ndsError;
        }
    }


    return ndsSuccess;
}


/**
 * @brief Update the Firmware FSM and Interlock status
 *
 * @return ndsSuccess Interlock is not active
 *
 * Read from firmware the current Firmware FSM and Interlock cause.
 * This is an internal method.
 */
ndsStatus sis8300llrfDevice::getFirmwareStatus() {
    int status;
    unsigned oldFSM, oldCause;
    oldFSM = _FSMFirmware;
    oldCause = _IlockCause;

    status = sis8300llrfdrv_get_fsm_state(_DeviceUser,  &_FSMFirmware);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_fsm_state", status);
    if (_FSMFirmware != oldFSM)
        doCallbacksInt32((epicsInt32)_FSMFirmware, _interruptIdFSMFirmware);

    status = sis8300llrfdrv_get_ilock_cause(_DeviceUser,  &_IlockCause);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_ilock_cause", status);
    if (_IlockCause != oldCause)
        doCallbacksInt32((epicsInt32)_IlockCause, _interruptIdIlockCause);

    if (_FSMFirmware == fsm_state_interlock_daq) {
        NDS_ERR("Firmware is on Interlock state! %d", _FSMFirmware);
        error();
        return ndsError;
    }

    return ndsSuccess;
}

/**
 * @brief Update the Firmware Information
 *
 * @return ndsSuccess
 *
 * Get general information from firmware
 * This is an internal method.
 * TODO: check when this should be called
 */
ndsStatus sis8300llrfDevice::getFirmwareInfo() {
    unsigned rfend_cause, daqend_cause;
    int status;

    /*RF End Cause*/
    status = sis8300llrfdrv_get_rfend_cause(_DeviceUser, &rfend_cause);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_rfend_cause", status);
    doCallbacksInt32((epicsInt32) rfend_cause, _interruptIdRFEndCause);
    /*DAQ End Cause*/
    status = sis8300llrfdrv_get_daqend_cause(_DeviceUser, &daqend_cause);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_daqend_cause", status);
    doCallbacksInt32((epicsInt32) daqend_cause, _interruptIdDAQEndCause);

    return ndsSuccess;
}

/**
 * @brief Check Interlock state and send the device to error if it is active
 *
 * @return ndsError   Interlock is active
 * @return ndsSuccess Interlock is not active
 *
 * This is a state transition handler called when INIT or ON state is
 * requested and prevents the transition if Interlock is active.
 * It updates also the ilock cause and fsm state PVs
 */
ndsStatus sis8300llrfDevice::checkInterlock() {
    getFirmwareStatus();

    if (_IlockCause != ilock_no_interlock) {
        NDS_ERR("Firmware is on Interlock state!");
        error();
        return ndsError;
    }

    return ndsSuccess;
}


/**
 * @brief State handler for requesting the INIT state.
 *
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 * @retval ndsError Failed to open device or read info.
 *
 * Calls the parents onSwitchInit handler and reads basic info,
 * LLRF specific information from the device. It also checks if the libray
 * and firmware versions match and refuses the transition on mismatch.
 */
ndsStatus sis8300llrfDevice::onSwitchInit(nds::DeviceStates from, nds::DeviceStates to) {
    NDS_TRC("%s", __func__);

    int status;
    char buffer[SIS8300NDS_STRING_BUFFER_SIZE];
    unsigned fwDevice, fwMajor, fwMinor, fwPatch;

    status = sis8300Device::onSwitchInit(from, to);
    if ((ndsStatus) status != ndsSuccess) {
        return ndsBlockTransition;
    }

    status = sis8300llrfdrv_get_fw_version(_DeviceUser, &fwMajor, &fwMinor, &fwPatch);
    SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_get_fw_version", status);
    NDS_INF("sis8300llrfdrv_get_fw_version returned version V:%d.%d.%d",
            fwMajor, fwMinor, fwPatch);
    status = sis8300llrfdrv_get_fw_id(_DeviceUser, &fwDevice);
    SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_get_fw_id", status);
    NDS_INF("sis8300llrfdrv_get_fw_id returned id %#06x", fwDevice);

    if (fwDevice != SIS8300LLRFDRV_HW_ID) {
        NDS_ERR("Card hardware id: %#06x not compatible with library hardware id: %#06x",
                fwDevice, SIS8300LLRFDRV_HW_ID);
        SIS8300NDS_MSGERR("Hardware id not compatible with library hw id");
        return ndsBlockTransition;
    }
    snprintf(buffer, SIS8300NDS_STRING_BUFFER_SIZE  - 1, "%#06x", fwDevice);
    _model = buffer;
    doCallbacksOctetStr(_model, ASYN_EOM_END, _interruptIdModel, _portAddr);
    
    snprintf(buffer, SIS8300NDS_STRING_BUFFER_SIZE - 1, "%03u.%03u.%03u", fwMajor, fwMinor, fwPatch);
    _firmwareVersion = buffer;
    doCallbacksOctetStr(_firmwareVersion, ASYN_EOM_END, _interruptIdFirmware);
        
    /* Check firmware version is supported by the software */
    status = check_fw_version(fwMajor, fwMinor, fwPatch);
    if (status != ndsSuccess) {    
        NDS_CRT("Firmware version unsupported: Version %d.%d is not currently supported.", fwMajor, fwMinor);
        return ndsError;
    }
    SIS8300NDS_STATUS_ASSERT("sis8300llrfDevice::check_fw_version", status);

    /* TODO: sw id? */

    /* TODO: remove, this is a workaround */
    _CgReg->setDeviceUser(_DeviceUser);

    _readLoopTask->start();
    NDS_DBG("%s is in %s.", _readLoopTask->getName().c_str(), _readLoopTask->getStateStr().c_str());

    return ndsSuccess;
}

/**
 * @brief State handler for entering INIT state.
 *
 * @param [in] from Source state.
 * @param [in] to   Destination state.
 *
 * @return ndsError   Setup failed
 * @return ndsSuccess Device successfully initialized.
 *
 * The function prevents the parnet handler to initialize the ADC and DAC!
 * Than does LLRF specific setup. The setup depends on which state we arrived
 * from. If we did not arrive from OFF or RESETTING than an error is returned.
 */
ndsStatus sis8300llrfDevice::onEnterInit(nds::DeviceStates from, nds::DeviceStates to) {
    NDS_TRC("%s", __func__);

    int status, i;
    epicsInt32  samplesCountMax;
    ndsStatus   statusSamplesCount;

    unsigned fwMajor, fwMinor, fwPatch;
    //firmware version used to determine if certain registers are required for operation 
    status = sis8300llrfdrv_get_fw_version(_DeviceUser, &fwMajor, &fwMinor, &fwPatch);

    
    /* To be sure of the device state, does not hurt */
    status = sis8300llrfdrv_sw_reset(_DeviceUser);
    SIS8300NDS_STATUS_CHECK("sis8300llrfdrv_sw_reset", status);

    /* Debugging and logging */
    _CgReg->readAllChannels();

    /* Initialize the board */
    status = sis8300llrfdrv_mem_ctrl_set_custom_mem_map(_DeviceUser);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_mem_ctrl_set_custom_mem_map", status);
    
    status = sis8300drv_init_adc(_DeviceUser);
    SIS8300NDS_STATUS_ASSERT("sis8300drv_init_adc", status);

    status = sis8300llrfdrv_setup_dac(_DeviceUser);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_setup_dac", status);

    /* Initialize the generic NDS part
     * parent onEnterInit handler does this. But it also initializes the DAC and ADC and
     * we want to prevent this here. So we copy this part of AI channel group initialization
     * from the parent */
    if (_CgAI != NULL) {
        NDS_DBG("Initializing channel group %s.", _CgAI->getName().c_str());
        statusSamplesCount = _CgAI->checkSamplesConfig(-1, -1, &samplesCountMax);
        if (statusSamplesCount != ndsSuccess) {
            _CgAI->setSamplesCount(NULL, samplesCountMax);
        }
        _CgAI->markAllParametersChanged();
        _CgAI->commitParameters();
        _CgAI->commitAllChannels();
    }

    /* Initialize the llrf NDS part */
    if (from == nds::DEVICE_STATE_OFF) {
        
        for(i = 0; i < SIS8300LLRFNDS_CHAN_GROUP_NUM; i++) {
            if (_llrfChannelGroups[i]->initialize() != ndsSuccess) {
                NDS_ERR("Could not initialize CG %s", _llrfChannelGroups[i]->getName().c_str());
                SIS8300NDS_MSGERR("Could not initialize CG");
                error();
                return ndsError;
            }
            else {
                NDS_INF("Sucessfully inititalized CG %s", _llrfChannelGroups[i]->getName().c_str());
            }
        }

    }
    else if (from == nds::DEVICE_STATE_RESETTING) {

        /* When CGs and CHs enter the disabled state they will rewrite all
         * the values to the controller */
        llrfCgStateSet(nds::CHANNEL_STATE_DISABLED);
    }
    else {
        NDS_ERR("Arriving to INIT from an unsupported state, %i", (int) from);
        SIS8300NDS_MSGERR("Arriving to INIT from an unsupported state");
        error();
        return ndsError;
    }

    //This should be here because the loop mode should be set after the setOperatin Mode
    if (_CgCtrl != NULL) {
        _CgCtrl->markAllParametersChanged();
        _CgCtrl->commitParameters();
    } 

    //Mark device parameters as changed
    _MaxRFLengthChange = 1;
    _MaxDAQLengthChange = 1;
    _LPSDeadTimeChange = 1;
    _LPSEnableChange = 1;

    //commit parameters from Device
    commitParameters();

	sis8300Device::restoreAllValues();
    NDS_INF("%s is in %s.", _ControlLoopTask->getName().c_str(), _ControlLoopTask->getStateStr().c_str());


    return getFirmwareStatus();
}

/**
 * @brief Override fast init
 *
 * @param [in] from ignored
 * @param [in] to   ignored
 *
 * @return ndsSuccess always
 */
ndsStatus sis8300llrfDevice::fastInit(nds::DeviceStates from, nds::DeviceStates to) {
    return ndsSuccess;
}

/**
 * @brief State handler for request INIT->ON request
 *
 * @param [in] from Source state.
 * @param [in] to   Destination state.
 *
 * @retval ndsSuccess         Transition allowed.
 * @retval ndsBlockTransition Parameters are not ok
 *
 * Checks if the control tables  are ok.
 * If not, the transition is blocked
 */
ndsStatus sis8300llrfDevice::onSwitchOn(nds::DeviceStates from, nds::DeviceStates to) {
    NDS_TRC("%s", __func__);

    if (_SetupActive) {
        NDS_ERR("Calibration must be done in INIT state");
        SIS8300NDS_MSGERR("Calibration must be done in INIT state");
        return ndsBlockTransition;
    }

    return ndsSuccess;
}


/**
 * @brief state handler for transition to ON
 *
 * @param [in]  from    Source state
 * @param [in]  to      Destination state (ON)
 *
 * @retval ndsSuccess   Transition successfull
 * @retval ndsError     Something failed
 *
 * This will also automatically start all CGs groups so no need to do it
 * separately. 
 * The function will set the init done bit on the device which will start the
 * control loop on the device. If anything fails, the device will move to error state.
 */
ndsStatus sis8300llrfDevice::onEnterOn(nds::DeviceStates from, nds::DeviceStates to) {
    NDS_TRC("%s", __func__);

    int status;

    status = sis8300llrfdrv_init_done(_DeviceUser);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_init_done", status);

    _UpdateReason = (epicsInt32) SIS8300LLRFDRV_UPDATE_REASON_INIT_DONE;
    doCallbacksInt32(_UpdateReason , _interruptIdUpdateReason);
    
    _CgReg->readAllChannels();

    _ControlLoopTask->start();
    NDS_INF("%s is in %s.", _ControlLoopTask->getName().c_str(), _ControlLoopTask->getStateStr().c_str());

    return ndsSuccess;
}

/**
 * @brief State handler for transition out of ON.
 *
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Always.
 *
 * Parent closes the device, since it does not have the INIT state.
 * We don't want to do this.
 *
 * Similar to switch on, parent's onLeaveOn translates to this
 * onEnterOff with from == INIT
 */
ndsStatus sis8300llrfDevice::onLeaveOn(nds::DeviceStates from, nds::DeviceStates to) {
    NDS_TRC("%s", __func__);
    
    _CgReg->readAllChannels();

    /* TODO: start waiting for PMS? - no we check for this on all state transitions
     * so it is ok. Only in ON do we need to consantly have this monitored */

    return ndsSuccess;
}


ndsStatus sis8300llrfDevice::onEnterOffState(nds::DeviceStates from, nds::DeviceStates to) {

    llrfCgStateSet(nds::CHANNEL_STATE_DISABLED);

    return sis8300Device::onEnterOffState(from, to);
}


/**
 * @brief State handler for entering the RESET state
 *
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 * @retval ndsError   Problem accessing device
 *
 * The function releases all waiters for the interrupt and
 * requests a sw reset of custom logic. If OFF state was requested,
 * it requests the transition of the device to OFF.
 */
ndsStatus sis8300llrfDevice::onEnterReset(nds::DeviceStates from, nds::DeviceStates to) {
    NDS_TRC("%s", __func__);
    
    int status;

    /* Release waiters - _PulseSetupTask */
    status = sis8300drv_release_irq(_DeviceUser, irq_type_usr);
    SIS8300NDS_STATUS_ASSERT("sis8300drv_release_irq", status);

    /* Wait for _ControlLoopTaslk to terminate */
    usleep(1);

    status = sis8300llrfdrv_sw_reset(_DeviceUser);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_sw_reset", status);


    /* Check what happened to Interlock state, it should have gone away */
    getFirmwareStatus();

    if (_IlockCause != ilock_no_interlock) {
        NDS_ERR("Interlock not cleared on RESET!");
        SIS8300NDS_MSGERR("Interlock not cleared on RESET!");
        error();
        return ndsError;
    }

    //clear Interlock
    status = sis8300llrfdrv_clear_latched_statuses(_DeviceUser, SIS8300LLRFDRV_STATUS_CLR_GENERAL);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_clear_latched_statuses", status);

    doCallbacksInt32((epicsInt32) SIS8300LLRFNDS_STATUS_CLEAR, _interruptIdStatus);
    
    NDS_INF("%s is in %s.", _ControlLoopTask->getName().c_str(), _ControlLoopTask->getStateStr().c_str());

    _CgAI->off();
    
    // Reset Pulse Done Counter
    // No need to read back from hardware as device pulse count is incremental not absolute
    _PulseDoneCount = 0;
    doCallbacksInt32((epicsInt32)_PulseDoneCount, _interruptIdPulseDoneCount); 
    return ndsSuccess;
}

/**
 * @brief State handler for entering the ERROR state
 *
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Always.
 *
 * Parent closes the device, but parent does not have INIT state. Here, we have a
 * possibility of a sw reset, which means that we will leave the on state, but will
 * not want to close the device
 *
 * Similar to switch on, parent's onLeaveOn translates to this onEnterOff with from == INIT
 */
ndsStatus sis8300llrfDevice::onEnterError(nds::DeviceStates from, nds::DeviceStates to) {

    unsigned status;

    /* Release waiters - _PulseSetupTask */
    status = sis8300drv_release_irq(_DeviceUser, irq_type_usr);

    /* Check for Interlock */
    //getFirmwareStatus();

    /* stop all CGs and CHs */
    stopAllGroups();

    NDS_INF("Pulse setup task is in: %s", _ControlLoopTask->getStateStr().c_str());

    if (status != status_success)
        return ndsError;

    return ndsSuccess;
}

/**
 * @brief Message handler for RESET state
 *
 * @param [in] pasynUser  Asyn user
 * @param [in] value      Message value
 *
 * @return ndsSuccess always
 *
 * The function directly sets the RESET state
 */
ndsStatus sis8300llrfDevice::handleResetMsg(asynUser *pasynUser, const nds::Message& value) {

    NDS_TRC("%s", __func__);

    if (getCurrentState() == nds::DEVICE_STATE_OFF) {
        NDS_ERR("Cannot set RESET in OFF state");
        SIS8300NDS_MSGERR("Cannot request RESET in OFF state");
        return ndsBlockTransition;
    }

    getCurrentStateObj()->setMachineState(this, nds::DEVICE_STATE_RESETTING);

    return ndsSuccess;
}

/**
 * @brief Message handler for OFF state
 *
 * @param [in] pasynUser  Asyn user
 * @param [in] value      Message value
 *
 * @return ndsSuccess Always
 *
 * If the device is in ON state, which means the custom logic is busy,
 * the device is sent to RESETTING and #_OffRequested flag goes high,
 * which ensures that the device will go to OFF after reset. If the
 * device is not on, it goes directly to OFF.
 */
ndsStatus sis8300llrfDevice::handleOffMsg(asynUser *pasynUser, const nds::Message& value) {

    NDS_TRC("%s", __func__);

    if (getCurrentState() == nds::DEVICE_STATE_ON) {
        SIS8300NDS_MSGERR("OFF cannot be requested from ON");
        NDS_ERR("OFF cannot be requested from ON");
        return ndsBlockTransition;
    }
    
    getCurrentStateObj()->requestState(this, nds::DEVICE_STATE_OFF);

    return ndsSuccess;
}

/**
 * @brief message handler for INIT msg
 *
 * @param [in] pasynUser  Asyn user
 * @param [in] value      Message value
 *
 * @return ndsSuccess         Transition allowed
 * @return ndsBlockTransition Transition not allowed
 *
 * Transition to INIT is only allowed from OFF or
 * RESETTING
 */
ndsStatus sis8300llrfDevice::handleInitMsg(asynUser *pasynUser, const nds::Message& value) {

    NDS_TRC("%s", __func__);

    if (getCurrentState() != nds::DEVICE_STATE_OFF &&
        getCurrentState() != nds::DEVICE_STATE_RESETTING) {
        SIS8300NDS_MSGERR("INIT state can be requested only from OFF or RESETTING");
        NDS_ERR("INIT state can be requested only from OFF or RESETTING");
        return ndsBlockTransition;
    }

    getCurrentStateObj()->requestState(this, nds::DEVICE_STATE_INIT);

    return ndsSuccess;
}

/**
 * @brief Override parent because it overrides our messages
 */
ndsStatus sis8300llrfDevice::handleOnMsg(asynUser *pasynUser, const nds::Message& value) {

    if (getCurrentState() != nds::DEVICE_STATE_INIT) {
        SIS8300NDS_MSGERR("ON state can only be requested from INIT");
        NDS_ERR("ON state can only be requested from INIT");
        return ndsBlockTransition;
    }

    return on();
}


/**
 * @brief Get current time in miliseconds
 */
epicsFloat64 sis8300llrfDevice::timemsec() {
    struct timespec ts;

    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (double)ts.tv_sec * 1.0e3 + ts.tv_nsec / 1.0e6;
}

/**
 * @brief task that waits for PULSE_DONE interrupt and setsup the new pulse parameters
 *
 *
 * This task is continuously running when the sis8300llrfDevice is in on state.
 * This is because during the on state, the controller is in action. After passing
 * of each pulse, the controller emits a PULSE_DONE interrupt. The task catches the
 * interrupt upon which it
 *
 * 1. Checks if the device is on (meaning the custom logic is running) and
 *    terminates if not
 * 2. Sends all the channel groups into disabled state
 * 3. Determines update reason and sends it to hw
 * 4. Sends all the channel groups to processing state
 *
 */
ndsStatus sis8300llrfDevice::controlLoopTask(nds::TaskServiceBase &service) {
    NDS_TRC("%s", __func__);

    unsigned pulseCntDevice;
    int status, irq_status, i;

    _PulseDoneCount = 0;

    while (getCurrentState() == nds::DEVICE_STATE_ON) {
        NDS_DBG("Entered loop task");
/*****
 * LOCKING THE DEVICE AND CG PORT!!!
 *****/
        lock();
        //This avoids the loop task start when is at reset or init state
        if (getCurrentState() != nds::DEVICE_STATE_ON)
        {
            unlock();
            continue;
        }
        NDS_INF("Before clear latched");
        status = sis8300llrfdrv_clear_latched_statuses(_DeviceUser, SIS8300LLRFDRV_STATUS_CLR_GENERAL | SIS8300LLRFDRV_STATUS_CLR_SIGMON);
        SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_clear_gop", status, this);

        logGenericPredefinedRegisters();
       
        //TODO: I think here we will never get an interlock, as the statuses were cleared above
        getFirmwareStatus();
        if (_FSMFirmware >= fsm_state_lps_daq){
            NDS_DBG("Get interlock here 1");
        }
        if (_FSMFirmware >= fsm_state_lps_daq && _IlockCause != ilock_no_interlock) {
            NDS_ERR("Firmware is on Interlock state!");
            error();
            unlock();
            return ndsError;
        }

        NDS_DBG("Before clear pulse done");
        /* We clear this every time an interrupt is recieved, so that we 
         * can raise an alarm if we are missing pulses */
        status = sis8300llrfdrv_clear_pulse_done_count(_DeviceUser);
        SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_clear_pulse_done_count", status, this);
        
//        if (_OperatingMode == mode_normal) {
            status = sis8300llrfdrv_arm_device(_DeviceUser);
            SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_arm_device", status, this);
//        }
//        else {
//            status = sis8300llrfdrv_arm_device_unlocked(_DeviceUser);
//            SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_arm_device_unlocked", status, this);
//        }

        doCallbacksInt32((epicsInt32) 1, _interruptIdArm);
        doCallbacksInt32((epicsInt32) SIS8300LLRFNDS_STATUS_ARMED, _interruptIdStatus);

        NDS_DBG("Before Unlock");
        unlock();
/*****
 * UNLOCKING THE DEVICE AND CG PORT TO WAIT FOR IRQ
 *****/
        if (_PerfTiming != 0){
            _PerfTiming = timemsec() - _PerfTiming;
            doCallbacksFloat64(_PerfTiming, _interruptIdAcqTime);
        }
        irq_status = sis8300llrfdrv_wait_pulse_done_pms(_DeviceUser, SIS8300LLRFNDS_IRQ_WAIT_TIME);
        // Start calculate of the time here
        _PerfTiming = timemsec();

        NDS_DBG("After wait pulse");

        if (irq_status == status_irq_release) {
            if (getCurrentState() != nds::DEVICE_STATE_RESETTING) {
                NDS_ERR("Unknown source released waiting for PULSE DONE");
                SIS8300NDS_MSGERR("Unknown source released waiting for PULSE DONE");
                error();
            }
            return ndsSuccess;
        }
        else {
            SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_wait_pulse_done_pms", irq_status);
        }
/*****
 * LOCKING THE DEVICE AND CG PORT!!!
 *****/
        lock();

        //This avoids the loop task run when is at reset or init state
        if (getCurrentState() != nds::DEVICE_STATE_ON)
        {
            unlock();
            continue;
        }

        getFirmwareStatus();
        getFirmwareInfo();
        if (_FSMFirmware >= fsm_state_lps_daq){
            NDS_DBG("Get interlock here 2");
        }

        if (_FSMFirmware == fsm_state_interlock && _IlockCause != ilock_no_interlock) {
            NDS_ERR("Firmware is on Interlock state!");
            error();
            unlock();
            return ndsSuccess;
        } else {
            doCallbacksInt32((epicsInt32) 1, _interruptIdPulseDone);
        }
        
        logGenericPredefinedRegisters();

        if (stopAllGroups() != ndsSuccess) {
            error();
            unlock();
            return ndsSuccess;
        }

        status = sis8300llrfdrv_get_pulse_done_count(_DeviceUser, &pulseCntDevice);
        SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_get_pulse_done_count", status, this);

        //THINK: go to error on missed pulse?
        doCallbacksInt32((epicsInt32)pulseCntDevice - 1, _interruptIdPulseMissed);
        _PulseDoneCount += pulseCntDevice;
        doCallbacksInt32((epicsInt32)_PulseDoneCount, _interruptIdPulseDoneCount);

        getFirmwareStatus();

        if (_FSMFirmware >= fsm_state_lps_daq){
            NDS_DBG("Get interlock here 3");
        }
        if (_FSMFirmware == fsm_state_interlock && _IlockCause != ilock_no_interlock) {
            NDS_ERR("Firmware is on Interlock state!");
            error();
            unlock();
            return ndsSuccess;
        }
        
        _UpdateReason = 0x0;

        commitParameters();

        for (i = 0; i < SIS8300LLRFNDS_CHAN_GROUP_NUM; i++) {
            _UpdateReason |= _llrfChannelGroups[i]->getUpdateReason();
        }
        _UpdateReason |= _CgAI->getUpdateReason();
        
        NDS_DBG("CAlling update params with reason %#8x", _UpdateReason);
        status = sis8300llrfdrv_update(_DeviceUser, _UpdateReason);
        SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_update", status, this);
        doCallbacksInt32((epicsInt32) _UpdateReason, _interruptIdUpdateReason);

        getFirmwareStatus();

        if (_FSMFirmware >= fsm_state_lps_daq){
            NDS_DBG("Get interlock here 4");
        }
        if (_FSMFirmware == fsm_state_interlock && _IlockCause != ilock_no_interlock) {
            NDS_ERR("Firmware is on Interlock state!");
            error();
            unlock();
            return ndsSuccess;
        }

        if (startAllGroups() != ndsSuccess) {
            error();
            unlock();
            return ndsSuccess;
        }

        getFirmwareStatus();
        if (_FSMFirmware >= fsm_state_lps_daq){
            NDS_DBG("Get interlock here 5");
            //If it is on some of the intermediate lps states should wait until
            //got soft or hard interlock
            while (_FSMFirmware == fsm_state_lps_daq ||  _FSMFirmware == fsm_state_lps_wait ||  _FSMFirmware == fsm_state_softilock_daq) {
                usleep(10);
                getFirmwareStatus();
            }
        }
        if (_FSMFirmware == fsm_state_interlock && _IlockCause != ilock_no_interlock) {
            NDS_ERR("Firmware is on Interlock state!");
            error();
            unlock();
            return ndsSuccess;
        }

        unlock();
/*****
 * UNLOCKING THE DEVICE AND CG PORT!!!
 *****/
    }

    return ndsSuccess;
}

//Loop to acquire values updated with a fixed interval
ndsStatus sis8300llrfDevice::readLoopTask(nds::TaskServiceBase &service) {
    NDS_TRC("%s", __func__);

    int status;
    double freq_samp_meas;

    NDS_DBG("Entered Thread");

    while (_readLoopTaskRun) {
        if (getCurrentState() != nds::DEVICE_STATE_OFF){
            status = sis8300llrfdrv_get_freq_samp(_DeviceUser, &freq_samp_meas);
            SIS8300NDS_STATUS_ASSERT_UNLOCK("sis8300llrfdrv_get_freq_samp", status, this);
            doCallbacksFloat64(freq_samp_meas, _interruptIdFreqSampMeas);
        }
        usleep(10000);
    }

    return ndsSuccess;
}



/* ==================== GETTERS, SETTERS, REGISTER HANDLERS ==================== */
/**
 * @brief Registers handlers for interfacing with records. For more information,
 * refer to NDS documentation.
 */
ndsStatus sis8300llrfDevice::registerHandlers(nds::PVContainers* pvContainers) {
    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_SETUP_ACTIVE,
            &sis8300llrfDevice::setSetupActive,
            &sis8300llrfDevice::getSetupActive,
            &_interruptIdSetupActive);

    /* only status records that process on I/O,
     * without getters and setters
     */
    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_PULSE_DONE_COUNT,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getInt32,
            &_interruptIdPulseDoneCount);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_PULSE_MISSED,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getInt32,
            &_interruptIdPulseMissed);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_UPDATE_REASON,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getUpdateReason,
            &_interruptIdUpdateReason);
            
    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_STATUS,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getInt32,
            &_interruptIdStatus);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_ARM,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getInt32,
            &_interruptIdArm);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_PULSE_DONE,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getInt32,
            &_interruptIdPulseDone);
            
    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfDevice::PV_REASON_F_SAMPLING,
            &sis8300llrfDevice::setFloat64, &sis8300llrfDevice::getFSampling,
            &_interruptIdFSampling);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfDevice::PV_REASON_F_SYSTEM,
            &sis8300llrfDevice::setFloat64, &sis8300llrfDevice::getFSystem,
            &_interruptIdFSystem);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_ILOCK_LPS,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getInt32,
            &_interruptIdILockLps);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_FSM_FW,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getFSMFirmware,
            &_interruptIdFSMFirmware);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_ILOCK_CAUSE,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getIlockCause,
            &_interruptIdIlockCause);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_RFEND_CAUSE,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getInt32,
            &_interruptIdRFEndCause);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_DAQEND_CAUSE,
            &sis8300llrfDevice::setInt32, &sis8300llrfDevice::getInt32,
            &_interruptIdDAQEndCause);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfDevice::PV_REASON_MAX_RF_LENGTH,
            &sis8300llrfDevice::setMaxRFLength, &sis8300llrfDevice::getMaxRFLength,
            &_interruptIdMaxRFLength);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfDevice::PV_REASON_MAX_DAQ_LENGTH,
            &sis8300llrfDevice::setMaxDAQLength, &sis8300llrfDevice::getMaxDAQLength,
            &_interruptIdMaxDAQLength);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfDevice::PV_REASON_LPS_DEAD_TIME,
            &sis8300llrfDevice::setLPSDeadTime, &sis8300llrfDevice::getLPSDeadTime,
            &_interruptIdLPSDeadTime);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_LPS_ENABLE,
            &sis8300llrfDevice::setLPSEnable, &sis8300llrfDevice::getLPSEnable,
            &_interruptIdLPSEnable);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfDevice::PV_REASON_FREQ_SAMP_MEAS,
            &sis8300llrfDevice::setFloat64, &sis8300llrfDevice::getFloat64,
            &_interruptIdFreqSampMeas);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_FORCE_ILOCK,
            &sis8300llrfDevice::setForceIlock, &sis8300llrfDevice::getInt32,
            &_interruptIdForceIlock);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfDevice::PV_REASON_ACQ_TIME,
            &sis8300llrfDevice::setFloat64, &sis8300llrfDevice::getFloat64,
            &_interruptIdAcqTime);

    NDS_PV_REGISTER_INT32(
            sis8300llrfDevice::PV_REASON_MAIN_INSTANCE,
            &sis8300llrfDevice::setMainInstance, &sis8300llrfDevice::getMainInstance,
            &_interruptIdMainInstance);

    return sis8300Device::registerHandlers(pvContainers);
}

/**
 * @brief Get the update reason that was sent to the device before the last
 *        arm. 
 * 
 * @param
 * 
 * The update reason is used to force the controller to load new values from
 * shadow registers or to reload control tables when the controller is running
 * = when this device is in ON state.
 */
ndsStatus sis8300llrfDevice::getUpdateReason(asynUser *pasynUser, epicsInt32 *value) {
	if (getCurrentState() == nds::DEVICE_STATE_IOC_INIT) {
        return ndsError;
    }

    *value = (epicsInt32) _UpdateReason;

    return ndsSuccess;
}

ndsStatus sis8300llrfDevice::getFSampling(asynUser *pasynUser, epicsFloat64 *value) {
    *value = _FSampling;

    return ndsSuccess;
}

ndsStatus sis8300llrfDevice::getFSystem(asynUser *pasynUser, epicsFloat64 *value) {
    *value = _FSystem;

    return ndsSuccess;
}

ndsStatus sis8300llrfDevice::getFSMFirmware(asynUser *pasynUser, epicsInt32 *value) {
    unsigned fsm_state;
    int status;

    status = sis8300llrfdrv_get_fsm_state(_DeviceUser,  &fsm_state);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_fsm_state", status);

    _FSMFirmware = fsm_state;
    *value = _FSMFirmware;

    return ndsSuccess;
}

/* @brief Get Interlock cause from firmware*/
ndsStatus sis8300llrfDevice::getIlockCause(asynUser *pasynUser, epicsInt32 *value) {
    int status;

    status = sis8300llrfdrv_get_ilock_cause(_DeviceUser,  &_IlockCause);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_ilock_cause", status);

    *value = _IlockCause;

    return ndsSuccess;
}


/**
 * @brief This flag is used to indicate that the device is in setup mode. 
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [in]  value       Set to 1 = ENABLE SETUP, 0 = DISABLE SETUP
 * 
 * @return ndsSuccess   Setup activated
 * @return ndsError     If setup is requested from any other state than INIT
 *                      or if the current operating mode is not normal.
 * 
 * This mode is used for initial setup of the device, where loop
 * parameters and input signals are being set up.
 */
ndsStatus sis8300llrfDevice::setSetupActive(asynUser *pasynUser, epicsInt32 value) {

    if (value) {

        if (getCurrentState() != nds::DEVICE_STATE_INIT) {
            SIS8300NDS_MSGERR("Setup can only be activated when in INIT");
            NDS_ERR("Setup can only be activated when in INIT");
            return ndsError;
        }

//        if (_OperatingMode != mode_normal) {
//            SIS8300NDS_MSGERR("Setup can only be activated in NORMAL operating mode");
//            NDS_ERR("Setup can only be activated when in NORMAL operating mode");
//            return ndsError;
//        }
    }

    _SetupActive = value ? 1 : 0;

    doCallbacksInt32(_SetupActive, _interruptIdSetupActive);

    return ndsSuccess;
}
/**
 * @brief Check if the device is in setup mode
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Ons success this will be 1 = ENABLED, 0 = DISABLED
 * 
 * @return ndsError     If call is made during IOC INIT
 * @return ndsSuccess   All other cases
 */
ndsStatus sis8300llrfDevice::getSetupActive(asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::DEVICE_STATE_IOC_INIT) {
        return ndsError;
    }

    *value = _SetupActive;

    return ndsSuccess;
}

/**
 * @brief Start processing of all channel groups
 *
 * @return statusSuccess    Groups started
 * @return statusError      One or more grpous returned error
 * 
 */
inline ndsStatus sis8300llrfDevice::startAllGroups() {
    NDS_TRC("%s", __func__);

    int i;
    ndsStatus statusCombined = ndsSuccess;
    nds::ChannelStates cgState;

    for(i = 0; i < SIS8300LLRFNDS_CHAN_GROUP_NUM; i++) {
        
        cgState = _llrfChannelGroups[i]->getCurrentState();
        
        if (cgState == nds::CHANNEL_STATE_ERROR) {
            NDS_ERR("Start CG failed: %s in ERR state!", 
                _llrfChannelGroups[i]->getName().c_str());
            SIS8300NDS_MSGERR("Start CG failed, CG in ERR!");
            statusCombined = ndsError;
        }
        else if (cgState != nds::CHANNEL_STATE_PROCESSING) {
            if (_llrfChannelGroups[i]->start() != ndsSuccess) {
                NDS_ERR("Start CG failed: %s", 
                    _llrfChannelGroups[i]->getName().c_str());
                SIS8300NDS_MSGERR("Start CG failed!");
                statusCombined = ndsError;
            }
        }
    }
    
    cgState = _CgAI->getCurrentState();
    
    if (cgState == nds::CHANNEL_STATE_ERROR) {
        NDS_ERR("Start CG failed: %s, group in ERR state.", 
            _CgAI->getName().c_str());
        SIS8300NDS_MSGERR("Start CG failed, CG is in ERROR.");
        statusCombined = ndsError;
    }
    else if (cgState != nds::CHANNEL_STATE_PROCESSING) {
        if (_CgAI->start() != ndsSuccess) {
            NDS_ERR("Start CG failed: %s", _CgAI->getName().c_str());
            SIS8300NDS_MSGERR("Start CG failed!");
            statusCombined = ndsError;
        }
    }

    return statusCombined;
}

/**
 * @brief Stop processing of all channel groups
 *
 * @return statusSuccess    Groups stoppped
 * @return statusError      One or more groups returned error
 */
inline ndsStatus sis8300llrfDevice::stopAllGroups() {
    NDS_TRC("%s", __func__);

    int i;
    ndsStatus statusCombined = ndsSuccess;
    nds::ChannelStates cgState;

    for(i = 0; i < SIS8300LLRFNDS_CHAN_GROUP_NUM; i++) {
        
        cgState = _llrfChannelGroups[i]->getCurrentState();
        
        if (cgState == nds::CHANNEL_STATE_ERROR) {
            NDS_ERR("Stop CG failed: %s in ERR state!", 
                _llrfChannelGroups[i]->getName().c_str());
            SIS8300NDS_MSGERR("Stop CG failed, CG in ERR!");
            statusCombined = ndsError;
        }
        else if (cgState != nds::CHANNEL_STATE_DISABLED) {
            if (_llrfChannelGroups[i]->stop() != ndsSuccess) {
                NDS_ERR("Stop failed: %s", 
                    _llrfChannelGroups[i]->getName().c_str());
                SIS8300NDS_MSGERR("Stop CG failed!");
                statusCombined = ndsError;
            }
        }
    }
    
    cgState = _CgAI->getCurrentState();
    
    if (cgState == nds::CHANNEL_STATE_ERROR) {
        NDS_ERR("Stop failed: %s, group in ERR state.", 
            _CgAI->getName().c_str());
        SIS8300NDS_MSGERR("Stop CG failed, CG is in ERROR.");
        statusCombined = ndsError;
    }
    else if (cgState != nds::CHANNEL_STATE_DISABLED) {
        if (_CgAI->stop() != ndsSuccess) {
            NDS_ERR("STOP FAILED: %s", _CgAI->getName().c_str());
            SIS8300NDS_MSGERR("Stop CG failed");
            statusCombined = ndsError;
        }
    }

    return statusCombined;
}

/**
 * @brief Request a specific state for al lthe llrf groups
 *
 * @param [in] state to request
 *
 * @return ndsSuccess   Always
 */
ndsStatus sis8300llrfDevice::llrfCgStateRequest(nds::ChannelStates state){
    NDS_TRC("%s", __func__);

    int i;

    for(i = 0; i < SIS8300LLRFNDS_CHAN_GROUP_NUM; i++) {
        _llrfChannelGroups[i]->getCurrentStateObj()->requestState(
                _llrfChannelGroups[i], state);
    }

    return ndsSuccess;
}

/**
 * @brief Set a specific state for all lthe llrf groups
 *
 * @param [in] state to request
 *
 * @return ndsSuccess   Always
 */
ndsStatus sis8300llrfDevice::llrfCgStateSet(nds::ChannelStates state){
    NDS_TRC("%s", __func__);

    int i;

    for(i = 0; i < SIS8300LLRFNDS_CHAN_GROUP_NUM; i++) {
        _llrfChannelGroups[i]->getCurrentStateObj()->setMachineState(
                _llrfChannelGroups[i], state);
    }

    return ndsSuccess;
}


/**
 * @brief commit Device changed parameters
 */
ndsStatus sis8300llrfDevice::commitParameters(){
    int status;

    NDS_TRC("%s", __func__);
    
    if ( getCurrentState() != nds::DEVICE_STATE_INIT &&
         getCurrentState() != nds::DEVICE_STATE_ON) {
        NDS_DBG("Device not committing parameters.");
        return ndsSuccess;
    }


    if (_MaxRFLengthChange) {
        epicsUInt32 MaxRFLengthRead, MaxRFCyc;
        _MaxRFLengthChange = 0;

        MaxRFCyc = (epicsUInt32)(_MaxRFLength * (1000*_FSampling));

        NDS_DBG("Convert Max RF Length from %f ms to %d cycles", 
            _MaxRFLength, MaxRFCyc);

        status = sis8300llrfdrv_set_max_rf_length(
                    _DeviceUser,  MaxRFCyc);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_set_max_rf_length", status);
        NDS_DBG("Max RF Pulse Length set to  %i", 
            MaxRFCyc);

        status = sis8300llrfdrv_get_max_rf_length(
                        _DeviceUser,&MaxRFLengthRead);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_get_max_rf_length", status);

        if (MaxRFLengthRead != MaxRFCyc) {
            NDS_ERR("Written value is different from read value for MaxRFLEngth. Written value %d Read Value %d", MaxRFCyc, MaxRFLengthRead);
            return ndsError;
        }
        _MaxRFLength = (epicsFloat64)(MaxRFLengthRead * (1/(_FSampling*1000)));
        doCallbacksFloat64(_MaxRFLength, _interruptIdMaxRFLength);
    }

    if (_MaxDAQLengthChange) {
        epicsUInt32 MaxDAQLengthRead, MaxDAQCyc;
        _MaxDAQLengthChange = 0;

        MaxDAQCyc = (epicsUInt32)(_MaxDAQLength * (1000*_FSampling));

        NDS_DBG("Convert Max DAQ Length from %f ms to %d cycles", 
            _MaxDAQLength, MaxDAQCyc);

        status = sis8300llrfdrv_set_max_daq_length(
                    _DeviceUser,  MaxDAQCyc);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_set_max_daq_length", status);
        NDS_DBG("Max DAQ Pulse Length set to  %i", 
            MaxDAQCyc);

        status = sis8300llrfdrv_get_max_daq_length(
                        _DeviceUser,&MaxDAQLengthRead);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_get_max_daq_length", status);

        if (MaxDAQLengthRead != MaxDAQCyc) {
            NDS_ERR("Written value is different from read value for MaxDAQLEngth. Written value %d Read Value %d", MaxDAQCyc, MaxDAQLengthRead);
            return ndsError;
        }
        _MaxDAQLength = (epicsFloat64)(MaxDAQLengthRead * (1/(_FSampling*1000)));
        doCallbacksFloat64(_MaxDAQLength, _interruptIdMaxDAQLength);
    }

    if (_LPSDeadTimeChange) {
        epicsUInt32 LPSDeadTimeRead, LPSDeadTimeCyc;
        _LPSDeadTimeChange = 0;

        LPSDeadTimeCyc = (epicsUInt32)(_LPSDeadTime * (1000*_FSampling));

        NDS_DBG("Convert LPS DeadTime from %f ms to %d cycles", 
            _LPSDeadTime, LPSDeadTimeCyc);

        status = sis8300llrfdrv_set_dead_time_soft_lps(
                    _DeviceUser,  LPSDeadTimeCyc);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_set_dead_time_soft_lps", status);
        NDS_DBG("LPS Dead Time Cyc set to  %i", 
            LPSDeadTimeCyc);

        status = sis8300llrfdrv_get_dead_time_soft_lps(
                        _DeviceUser,&LPSDeadTimeRead);
        SIS8300NDS_STATUS_ASSERT(
            "sis8300llrfdrv_get_lps_dead_time", status);

        if (LPSDeadTimeRead != LPSDeadTimeCyc) {
            NDS_ERR("Written value is different from read value for LPS Dead Time. Written value %d Read Value %d", LPSDeadTimeCyc, LPSDeadTimeRead);
            return ndsError;
        }
        _LPSDeadTime = (epicsFloat64)(LPSDeadTimeRead * (1/(_FSampling*1000)));
        doCallbacksFloat64(_LPSDeadTime, _interruptIdLPSDeadTime);
    }
    //Reload values to set the firmware to behaves like the old version (1.0.0)
    status = sis8300llrfdrv_set_inp_sync_en(
                    _DeviceUser,0);
    SIS8300NDS_STATUS_ASSERT(
        "sis8300llrfdrv_set_inp_sync_en", status);

    status = sis8300llrfdrv_disable_new_trigger(
                    _DeviceUser);
    SIS8300NDS_STATUS_ASSERT(
        "sis8300llrfdrv_disable_new_trigger", status);
    if(_LPSEnableChange) {
        status = sis8300llrfdrv_set_signal_from_lps(_DeviceUser, _LPSEnable);
        SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_set_signal_from_lps", status);
        doCallbacksInt32(_LPSEnable, _interruptIdLPSEnable);
    }

    return ndsSuccess;
}

/**
 * @brief set Max RF Length 
 * 
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Max RF Length in ms
 * 
 * @return ndsError     If the channel is not initalized
 * @return @see #commitParameters
 */
ndsStatus sis8300llrfDevice::setMaxRFLength(
                asynUser *pasynUser, epicsFloat64 value) {
    _MaxRFLength = value;
    _MaxRFLengthChange = 1;
    
    if ( getCurrentState() != nds::DEVICE_STATE_INIT) {
        NDS_DBG("Not committing Max RF Length.");
        return ndsSuccess;
    }

    return commitParameters();
}


/**
 * @brief Get Max RF Length
 * 
 * @param [in]  pasynUser    Asyn user context struct
 * @param [out] value        Max RF LEngth in ms
 *  
 * @return ndsSuccess
 */
ndsStatus sis8300llrfDevice::getMaxRFLength(
                asynUser *pasynUser, epicsFloat64 *value) {
    *value = _MaxRFLength;

    return ndsSuccess;
}


/**
 * @brief set Max DAQ Length 
 * 
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Max DAQ Length in ms
 * 
 * @return ndsError     If the channel is not initalized
 * @return @see #commitParameters
 */
ndsStatus sis8300llrfDevice::setMaxDAQLength(
                asynUser *pasynUser, epicsFloat64 value) {
    _MaxDAQLength = value;
    _MaxDAQLengthChange = 1;
    
    if ( getCurrentState() != nds::DEVICE_STATE_INIT) {
        NDS_DBG("Not committing Max DAQ Length.");
        return ndsSuccess;
    }

    return commitParameters();
}


/**
 * @brief Get Max DAQ Length
 * 
 * @param [in]  pasynUser    Asyn user context struct
 * @param [out] value        Max DAQ LEngth in ms
 *  
 * @return ndsSuccess
 */
ndsStatus sis8300llrfDevice::getMaxDAQLength(
                asynUser *pasynUser, epicsFloat64 *value) {
    *value = _MaxDAQLength;

    return ndsSuccess;
}

/**
 * @brief set LPS Dead Time for soft Interlock 
 * 
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Dead time in  ms
 * 
 * @return ndsError     If the channel is not initalized
 * @return @see #commitParameters
 */
ndsStatus sis8300llrfDevice::setLPSDeadTime(
                asynUser *pasynUser, epicsFloat64 value) {
    _LPSDeadTime = value;
    _LPSDeadTimeChange = 1;
    
    if ( getCurrentState() != nds::DEVICE_STATE_INIT) {
        NDS_DBG("Not committing LPS Dead Time.");
        return ndsSuccess;
    }

    return commitParameters();
}


/**
 * @brief Get Max LPS Dead Time
 * 
 * @param [in]  pasynUser    Asyn user context struct
 * @param [out] value        Dead Time in ms
 *  
 * @return ndsSuccess
 */
ndsStatus sis8300llrfDevice::getLPSDeadTime(
                asynUser *pasynUser, epicsFloat64 *value) {
    *value = _LPSDeadTime;

    return ndsSuccess;
}


/**
 * @brief set LPS interlock enable
 * 
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Dead time in  ms
 * 
 * @return ndsError     If the channel is not initalized
 * @return @see #commitParameters
 */
ndsStatus sis8300llrfDevice::setLPSEnable(
                asynUser *pasynUser, epicsInt32 value) {
    _LPSEnable = value;
    _LPSEnableChange = 1;
    
    if ( getCurrentState() != nds::DEVICE_STATE_INIT) {
        NDS_DBG("Not committing LPS Enable.");
        return ndsSuccess;
    }

    return commitParameters();
}


/**
 * @brief Get LPS interlock enable
 * 
 * @param [in]  pasynUser    Asyn user context struct
 * @param [out] value        Dead Time in ms
 *  
 * @return ndsSuccess
 */
ndsStatus sis8300llrfDevice::getLPSEnable(
                asynUser *pasynUser, epicsInt32 *value) {
    *value = _LPSEnable;

    return ndsSuccess;
}




/**
 * @brief Force an Interlock using firwmware register
 * 
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Any value
 * 
 * @return ndsError    the register couldn't be written 
 * @return @see #commitParameters
 */
ndsStatus sis8300llrfDevice::setForceIlock(
                asynUser *pasynUser, epicsInt32 value) {
    int status;

    status = sis8300llrfdrv_force_ilock(_DeviceUser);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_force_ilock", status);
    return ndsSuccess;
}

/**
 * @brief Set device to instance type
 * 
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Is Main instance
 * 
 * @return ndsError     If cannot change the instance type
 * @return ndsSuccess
 */
ndsStatus sis8300llrfDevice::setMainInstance(asynUser *pasynUser, epicsInt32 value) {
    int         status;

    NDS_TRC("%s",__func__);

    /* Prevent changing instance type if not at IOC init. */
    if (getCurrentState() != nds::DEVICE_STATE_INIT) {
        NDS_ERR("Change on instance type should be done on INIT state ");
        return ndsError;
    }

    NDS_DBG("Setting Main Instance to %d", value);
    _MainInstance = value;

    //By default set VM Output disable when change the instance type
    sis8300Device::setVMOutputEnabled(NULL, 0);
    status = sis8300llrfdrv_set_instance_type(_DeviceUser, value);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_set_instance_type", status);

    doCallbacksInt32(_MainInstance, _interruptIdMainInstance);
    return ndsSuccess;
}

/**
 * @brief Get if device is a main instance
 * 
 * @param [in]  pasynUser    Asyn user context struct
 * @param [out] value        Is main instance
 *  
 * @return ndsSuccess      Always
 */
ndsStatus sis8300llrfDevice::getMainInstance(asynUser *pasynUSer, epicsInt32 *value) {
    NDS_TRC("%s", __func__);

    *value = _MainInstance;
    return ndsSuccess;
}


/**
 * @brief Force logging of a set of predefined raw registers if they are loaded
 */
inline void sis8300llrfDevice::logGenericPredefinedRegisters() {
    /* This uses the sis8300RegisterChannel to get the raw register value.
     * The channel number corresponds to register address
     */
    ndsStatus status;

    nds::Channel *chan;

    epicsUInt32 value;
    epicsUInt32 mask = 0xffffffff;
    int reg_addr;

    status = getChannel(_CgReg->getName(), (int) 0x10, &chan);
    if (status != ndsSuccess) {
        //channel does not exist, the template is most likely not loaded.
        //abort
        return;
    }

    (dynamic_cast<sis8300RegisterChannel *>(chan))->getValueUInt32Digital(NULL, &value, mask);

    //get the values of adc channel addresses
    for (reg_addr = 0x120; reg_addr <= 0x129; reg_addr++) {
        status = getChannel(_CgReg->getName(), reg_addr, &chan);
        if (status != ndsSuccess) {
            return;
        }
        (dynamic_cast<sis8300RegisterChannel *>(chan))->getValueUInt32Digital(NULL, &value, mask);
    }

    status = getChannel(_CgReg->getName(), (int) 0x220, &chan);
    if (status != ndsSuccess) {
        return;
    }
    (dynamic_cast<sis8300RegisterChannel *>(chan))->getValueUInt32Digital(NULL, &value, mask);

    return;
}

/** Brief Get firmware version of the SIS8300 device has available software support
*
* @param[in]	fwMajor				Firmware major version
* @param[in]	fwMinor				Firmware minor version
* @return	ndsSuccess              Firmware version is suported by current IOC version. 
* @return	ndsError                Firmware version is unsupported by current IOC version.
*/
ndsStatus sis8300llrfDevice::check_fw_version(unsigned fwMajor, unsigned fwMinor, unsigned fwPatch) {
  NDS_TRC("%s", __func__);
  NDS_DBG("Checking fw vesion v%u.%u.%u is compatible with current IOC release.", fwMajor, fwMinor, fwPatch);
  /* Supported firmware versions
   * Rows correspond to major versions (0,1,2 etc.)
   * Columns have the following format
   * Column 0 is the first minor number supported by the current IOC version for the corresponding major version.
   * Column 1 is the last minor number supported by the current IOC version for the corresponding major version.
   * Column 2 is zero if no minor versions corresponding to this major number are supported by the current IOC version and non-zero where the specified minor version range is valid."
  */
  unsigned fwSupported[][3] = {
  {7,7,1},	// Major version 0.
  {0,2,1},
  {0,0,0},
  {0,0,0},
  {0,0,0} // Major version n.
  };
  /* Maximum supported major version (derived from fwSupported array) */
  unsigned sMajorMax;
  bool bMaxAny;

  /* Minor version range supported */
  unsigned sMinorFirst, sMinorLast;
  if (sizeof(*fwSupported) > 0) {
    sMajorMax = sizeof(fwSupported)/sizeof(*fwSupported) - 1;
    bMaxAny=fwSupported[fwMajor][2] > 0;
    NDS_DBG("Firmware major max supported is %u and valid minor versions present = %u", sMajorMax, bMaxAny); 
  } else {
    NDS_ERR("Could not retrieve supported firmware information within IOC. Contact IOC developer for support.");
    return ndsError;
  }
  /* Check major version is supported */
  if (fwMajor > sMajorMax || !bMaxAny ) {
    NDS_DBG("Firmware major version number %u unsupported by current IOC release", fwMajor);
    NDS_ERR("Firmware version v%u.%u is unsupported by current IOC version. Contact IOC developer for support.", fwMajor, fwMinor);
    return ndsError;
  }
  /* Check minor version support */
  sMinorLast = *(*(fwSupported + fwMajor) + 1);
  sMinorFirst = *(*(fwSupported + fwMajor));
  NDS_DBG("For firmware major version %u, minor versions %u to %u are supported.", fwMajor, sMinorFirst, sMinorLast);
  if (fwMinor >= sMinorFirst && fwMinor <= sMinorLast) {
    NDS_INF("Firmware version v%u.%u is compatible with current IOC version.", fwMajor, fwMinor);
    return ndsSuccess;
  } else {
    NDS_DBG("Firmware minor version number %u unsupported by current IOC release", fwMinor);
    NDS_ERR("Firmware version v%u.%u is unsupported by current IOC version. Contact IOC developer for support.", fwMajor, fwMinor);
    return ndsError;
  }
}

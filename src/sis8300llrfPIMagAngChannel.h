/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfPIMagAngChannel.h
 * @brief Header file defining the LLRF Fixed SP channel class
 * @author gabriel.fedel@ess.eu
 * @date 5.5.2020
 */

#ifndef _sis8300llrfPIMagAngChannel_h
#define _sis8300llrfPIMagAngChannel_h

//TODO: change this to be read from startup script
#define SIS8300LLRF_MAXCALIBSAMPLE 5000
#define SIS8300LLRF_MAG_MIN 0
#define SIS8300LLRF_MAG_MAX 1
#define SIS8300LLRF_I_MIN -1
#define SIS8300LLRF_I_MAX 0.999969482422
#define SIS8300LLRF_Q_MIN -1
#define SIS8300LLRF_Q_MAX 0.999969482422

#include "sis8300llrfControlTableChannel.h"
#include "sis8300llrfCalibrationProvider.h"
/**
 * @brief sis8300 LLRF specific nds::ADIOChannel class from which
 *           all llrf specific channels are derived
 */
class sis8300llrfPIMagAngChannel: public sis8300llrfChannel {
public:
    sis8300llrfPIMagAngChannel(sis8300llrfPIChannel * PIChannels[2], int SP_or_FF, sis8300llrfControlTableChannel * CtrlTableCh, sis8300llrfCalibrationProvider * calibrationProvider);
    virtual ~sis8300llrfPIMagAngChannel();

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    virtual ndsStatus setFixMag(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getFixMag(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus getFixMagRaw(
                        asynUser *pasynUser, epicsFloat64 *value);
    virtual ndsStatus setFixAng(
                        asynUser *pasynUser, epicsFloat64 value);
    virtual ndsStatus getFixAng(
                        asynUser *pasynUser, epicsFloat64 *value);

    virtual ndsStatus getCalibEnable(asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setCalibEnable(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setCalibEGU(asynUser *pasynUser, epicsFloat64 *value, size_t nelem);
    virtual ndsStatus setCalibRaw(asynUser *pasynUser, epicsFloat64 *value, size_t nelem);
    virtual ndsStatus setCalibOrderEGU2Raw(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setCalibOrderRaw2EGU(asynUser *pasynUser, epicsInt32 value);

    virtual ndsStatus setMagTable(
                        asynUser *pasynUser, epicsFloat64 *value, size_t nelem);
    virtual ndsStatus setAngTable(
                        asynUser *pasynUser, epicsFloat64 *value, size_t nelem);
    virtual ndsStatus getMagTable(
                asynUser* pasynUser, epicsFloat64 *value, 
                size_t nelem, size_t *nIn);
    virtual ndsStatus getAngTable(
                asynUser* pasynUser, epicsFloat64 *value, 
                size_t nelem, size_t *nIn);

    virtual bool isCalibrationEnabled();
    sis8300llrfCalibrationProvider* getCalibrationProvider();

protected:
    sis8300llrfCalibrationProvider* calibrationProvider;

    /* for asynReasons */
    static std::string PV_REASON_FIX_MAG;
    static std::string PV_REASON_FIX_MAG_RAW;
    static std::string PV_REASON_FIX_ANG;
    static std::string PV_REASON_MAG_TABLE;
    static std::string PV_REASON_ANG_TABLE;
    static std::string PV_REASON_MAG_TABLE_RAW;

    static std::string PV_REASON_CALIB_EN;
    static std::string PV_REASON_CALIB_EGU;
    static std::string PV_REASON_CALIB_RAW;
    static std::string PV_REASON_CALIB_ORDER_EGU2RAW;
    static std::string PV_REASON_CALIB_ORDER_RAW2EGU;
    static std::string PV_REASON_CALIB_FITTED_LINE_EGU2RAW;
    static std::string PV_REASON_CALIB_FITTED_LINE_RAW2EGU;
    static std::string PV_REASON_CALIB_RESID_EGU2RAW;
    static std::string PV_REASON_CALIB_RESID_RAW2EGU;
    static std::string PV_REASON_CALIB_COEF_EGU2RAW;
    static std::string PV_REASON_CALIB_COEF_RAW2EGU;
    static std::string PV_REASON_CALIB_CHISQ_EGU2RAW;
    static std::string PV_REASON_CALIB_CHISQ_RAW2EGU;


    int _interruptIdMag;
    int _interruptIdMagRaw;
    int _interruptIdAng;
    int _interruptIdCalibEnable;
    int _interruptIdCalibEGU;
    int _interruptIdCalibRaw;
    int _interruptIdCalibOrderEGU2Raw;
    int _interruptIdCalibOrderRaw2EGU;
    int _interruptIdCalibFittedLineEGU2Raw;
    int _interruptIdCalibFittedLineRaw2EGU;
    int _interruptIdCalibResidEGU2Raw;
    int _interruptIdCalibResidRaw2EGU;
    int _interruptIdCalibCoefEGU2Raw;
    int _interruptIdCalibCoefRaw2EGU;
    int _interruptIdCalibChisqEGU2Raw;
    int _interruptIdCalibChisqRaw2EGU;

    int _MagAngChanged;

    int _interruptIdMagTable;
    int _interruptIdAngTable;
    int _interruptIdMagTableRaw;

    sis8300llrfPIChannel ** _PIChannels;
    int                     _SPorFF;

    epicsFloat64        _Mag;
    epicsFloat64        _MagRBV;
    epicsFloat64        _MagRaw;
    epicsFloat64        _Ang;
    epicsFloat64        _AngRBV;

    //control table variables
    sis8300llrfControlTableChannel * _CtrlTableCh;
    epicsInt32      _MagTableCount;      /**< Number of elements on _MagTable*/
    epicsInt32      _AngTableCount;      /**< Number of elements on _AngTable*/
    epicsFloat64        *_MagTable;
    epicsFloat64        *_AngTable;


    void reloadCalibPVs();

    virtual ndsStatus writeToHardware();
//    virtual ndsStatus onEnterDisabled(nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus updateMagAngRBV();
    virtual ndsStatus convertTablesToIQ();
    virtual ndsStatus loadMagAngTables();
};

#endif /* _sis8300llrfPIMagAngChannel_h */

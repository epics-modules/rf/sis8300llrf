/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfPIMagAngChannel.cpp
 * @brief Implementation of Calibration for PI Channel.
 * @author gabriel.fedel@ess.eu
 * @date 5.5.2020
 *
 * This class implements the calibration process for PI Channel, this
 * calibration is made using the Fixed values (SP and FF). This class
 * also provides the PI values in Magnitude and Angle.
 *
 * There are a set of functionalities provided to calculate some statistical
 * values used by calibration
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfChannel.h"
#include "sis8300llrfPIChannel.h"
#include "sis8300llrfControlTableChannel.h"

#include "sis8300llrfPIMagAngChannel.h"
#include <math.h>

std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_FIX_MAG          = "FixedMag";
std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_FIX_MAG_RAW      = "FixedMagRaw";
std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_FIX_ANG          = "FixedAng";

std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_MAG_TABLE         = "MagTable";
std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_ANG_TABLE         = "AngTable";
std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_MAG_TABLE_RAW     = "MagTableRaw";

std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_CALIB_EN          = "CalibEnable";
std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_CALIB_EGU        = "CalibEGU";
std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_CALIB_RAW        = "CalibRaw";
std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_CALIB_ORDER_EGU2RAW      = "CalibOrderEGU2Raw";
std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_CALIB_ORDER_RAW2EGU      = "CalibOrderRaw2EGU";
std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_CALIB_FITTED_LINE_EGU2RAW = "CalibFittedLineEGU2Raw";
std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_CALIB_FITTED_LINE_RAW2EGU = "CalibFittedLineRaw2EGU";
std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_CALIB_RESID_EGU2RAW = "CalibResidEGU2Raw";
std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_CALIB_RESID_RAW2EGU = "CalibResidRaw2EGU";
std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_CALIB_COEF_EGU2RAW = "CalibCoefEGU2Raw";
std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_CALIB_COEF_RAW2EGU = "CalibCoefRaw2EGU";
std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_CALIB_CHISQ_EGU2RAW = "CalibChisqEGU2Raw";
std::string sis8300llrfPIMagAngChannel::
                    PV_REASON_CALIB_CHISQ_RAW2EGU = "CalibChisqRaw2EGU";


/**
 * @brief PI Calibration channel constructor.
 *
 * @param [in] PIChannels       Array of PI Channels to read and set I/Q
 * @param [in] SPorFF         Indicate if this channel is for SetPoint(0) or FeedForward(1)
 *
 */
sis8300llrfPIMagAngChannel::sis8300llrfPIMagAngChannel(sis8300llrfPIChannel * PIChannels[2], int SPorFF, sis8300llrfControlTableChannel * CtrlTableCh, sis8300llrfCalibrationProvider * calibrationProvider): sis8300llrfChannel(0, 0){
    NDS_TRC("%s", __func__);

    registerOnEnterStateHandler(nds::CHANNEL_STATE_DISABLED,
        boost::bind(&sis8300llrfPIMagAngChannel::onEnterDisabled, this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_RESETTING,
        boost::bind(&sis8300llrfPIMagAngChannel::onEnterReset, this));
            
    registerOnEnterStateHandler(nds::CHANNEL_STATE_ERROR,
        boost::bind(&sis8300llrfPIMagAngChannel::onEnterError, this, _1, _2));

    _PIChannels = PIChannels;
    _SPorFF = SPorFF; //0 -> SP , 1 -> FF

    this->calibrationProvider = calibrationProvider;    

    _Mag = 0;
    _MagRBV = 0;
    _Ang = 0;
    _AngRBV = 0;
    _MagRaw = 0;
    _MagAngChanged = 0;

    _CtrlTableCh = CtrlTableCh;
    _MagTable = NULL; //new epicsFloat64[SIS8300LLRF_MAXCALIBSAMPLE];
    _AngTable = NULL; //new epicsFloat64[SIS8300LLRF_MAXCALIBSAMPLE];
    _MagTableCount = 0;
    _AngTableCount = 0;
}

/**
 * @brief LLRF channel destructor.
 *
 * Free channel data buffer.
 */
sis8300llrfPIMagAngChannel::~sis8300llrfPIMagAngChannel() {
    if (_MagTable != NULL)
        delete[] _MagTable;
    if (_AngTable != NULL)
        delete[] _AngTable;
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfPIMagAngChannel::registerHandlers(
                nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_FLOAT64(
           sis8300llrfPIMagAngChannel::PV_REASON_FIX_MAG,
           &sis8300llrfPIMagAngChannel::setFixMag,
           &sis8300llrfPIMagAngChannel::getFixMag,
           &_interruptIdMag);

    NDS_PV_REGISTER_FLOAT64(
           sis8300llrfPIMagAngChannel::PV_REASON_FIX_MAG_RAW,
           &sis8300llrfPIMagAngChannel::setFloat64,
           &sis8300llrfPIMagAngChannel::getFixMagRaw,
           &_interruptIdMagRaw);

    NDS_PV_REGISTER_FLOAT64(
           sis8300llrfPIMagAngChannel::PV_REASON_FIX_ANG,
           &sis8300llrfPIMagAngChannel::setFixAng,
           &sis8300llrfPIMagAngChannel::getFixAng,
           &_interruptIdAng);

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfPIMagAngChannel::PV_REASON_MAG_TABLE,
            &sis8300llrfPIMagAngChannel::setMagTable,
            &sis8300llrfPIMagAngChannel::getMagTable,
            &_interruptIdMagTable);

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfPIMagAngChannel::PV_REASON_ANG_TABLE,
            &sis8300llrfPIMagAngChannel::setAngTable,
            &sis8300llrfPIMagAngChannel::getAngTable,
            &_interruptIdAngTable);

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfPIMagAngChannel::PV_REASON_MAG_TABLE_RAW,
            NULL,
            &sis8300llrfPIMagAngChannel::getFloat64Array,
            &_interruptIdMagTableRaw);

    NDS_PV_REGISTER_INT32(
           sis8300llrfPIMagAngChannel::PV_REASON_CALIB_EN,
           &sis8300llrfPIMagAngChannel::setCalibEnable,
           &sis8300llrfPIMagAngChannel::getCalibEnable,
           &_interruptIdCalibEnable
     );

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfPIMagAngChannel::PV_REASON_CALIB_EGU,
            &sis8300llrfPIMagAngChannel::setCalibEGU,
            &sis8300llrfPIMagAngChannel::getFloat64Array,
//            &sis8300llrfPIMagAngChannel::getCalibEGU,
            &_interruptIdCalibEGU);

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfPIMagAngChannel::PV_REASON_CALIB_RAW,
            &sis8300llrfPIMagAngChannel::setCalibRaw,
            &sis8300llrfPIMagAngChannel::getFloat64Array,
//            &sis8300llrfPIMagAngChannel::getCalibRaw,
            &_interruptIdCalibRaw);

    NDS_PV_REGISTER_INT32(
           sis8300llrfPIMagAngChannel::PV_REASON_CALIB_ORDER_EGU2RAW,
           &sis8300llrfPIMagAngChannel::setCalibOrderEGU2Raw,
           &sis8300llrfPIMagAngChannel::getInt32,
//           &sis8300llrfPIMagAngChannel::getCalibOrder,
           &_interruptIdCalibOrderEGU2Raw
     );

    NDS_PV_REGISTER_INT32(
           sis8300llrfPIMagAngChannel::PV_REASON_CALIB_ORDER_RAW2EGU,
           &sis8300llrfPIMagAngChannel::setCalibOrderRaw2EGU,
           &sis8300llrfPIMagAngChannel::getInt32,
//           &sis8300llrfPIMagAngChannel::getCalibOrder,
           &_interruptIdCalibOrderRaw2EGU
     );

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfPIMagAngChannel::PV_REASON_CALIB_FITTED_LINE_EGU2RAW,
            &sis8300llrfPIMagAngChannel::setFloat64Array,
            &sis8300llrfPIMagAngChannel::getFloat64Array,
            &_interruptIdCalibFittedLineEGU2Raw);

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfPIMagAngChannel::PV_REASON_CALIB_FITTED_LINE_RAW2EGU,
            &sis8300llrfPIMagAngChannel::setFloat64Array,
            &sis8300llrfPIMagAngChannel::getFloat64Array,
            &_interruptIdCalibFittedLineRaw2EGU);

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfPIMagAngChannel::PV_REASON_CALIB_RESID_EGU2RAW,
            &sis8300llrfPIMagAngChannel::setFloat64Array,
            &sis8300llrfPIMagAngChannel::getFloat64Array,
            &_interruptIdCalibResidEGU2Raw);

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfPIMagAngChannel::PV_REASON_CALIB_RESID_RAW2EGU,
            &sis8300llrfPIMagAngChannel::setFloat64Array,
            &sis8300llrfPIMagAngChannel::getFloat64Array,
            &_interruptIdCalibResidRaw2EGU);

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfPIMagAngChannel::PV_REASON_CALIB_COEF_EGU2RAW,
            &sis8300llrfPIMagAngChannel::setFloat64Array,
            &sis8300llrfPIMagAngChannel::getFloat64Array,
            &_interruptIdCalibCoefEGU2Raw);

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfPIMagAngChannel::PV_REASON_CALIB_COEF_RAW2EGU,
            &sis8300llrfPIMagAngChannel::setFloat64Array,
            &sis8300llrfPIMagAngChannel::getFloat64Array,
            &_interruptIdCalibCoefRaw2EGU);

    NDS_PV_REGISTER_FLOAT64(
           sis8300llrfPIMagAngChannel::PV_REASON_CALIB_CHISQ_EGU2RAW,
           &sis8300llrfPIMagAngChannel::setFloat64,
           &sis8300llrfPIMagAngChannel::getFloat64,
           &_interruptIdCalibChisqEGU2Raw);

    NDS_PV_REGISTER_FLOAT64(
           sis8300llrfPIMagAngChannel::PV_REASON_CALIB_CHISQ_RAW2EGU,
           &sis8300llrfPIMagAngChannel::setFloat64,
           &sis8300llrfPIMagAngChannel::getFloat64,
           &_interruptIdCalibChisqRaw2EGU);

    return sis8300llrfChannel::registerHandlers(pvContainers);
}

/**************************************************************/

/**
 * @brief Set Fixed Magnitude
 */
ndsStatus sis8300llrfPIMagAngChannel::setFixMag(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);
    _Mag = value;
    _MagAngChanged = 1;

    return commitParameters();
}


/**
 * @brief Get Fixed Magnitude
 */
ndsStatus sis8300llrfPIMagAngChannel::getFixMag(
                asynUser *pasynUser, epicsFloat64 *value) {
    NDS_TRC("%s", __func__);
    if (updateMagAngRBV() != ndsSuccess)
        return ndsError;

    *value = _MagRBV;

    return ndsSuccess;
}

/**
 * @brief Get Fixed Magnitude Raw
 */
ndsStatus sis8300llrfPIMagAngChannel::getFixMagRaw(
                asynUser *pasynUser, epicsFloat64 *value) {
    NDS_TRC("%s", __func__);
    if (updateMagAngRBV() != ndsSuccess)
        return ndsError;

    *value = _MagRaw;

    return ndsSuccess;
}


/**
 * @brief Set Fixed Angle
 */
ndsStatus sis8300llrfPIMagAngChannel::setFixAng(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);
    _Ang = value;
    _MagAngChanged = 1;

    return commitParameters();
}


/**
 * @brief Get Fixed Angle
 */
ndsStatus sis8300llrfPIMagAngChannel::getFixAng(
                asynUser *pasynUser, epicsFloat64 *value) {
    NDS_TRC("%s", __func__);
    // read values on RBV
    if (updateMagAngRBV() != ndsSuccess)
        return ndsError;

    *value = _AngRBV;

    return ndsSuccess;
}

ndsStatus sis8300llrfPIMagAngChannel::updateMagAngRBV() {
    epicsFloat64 I, Q;

    //set point
    if (!_SPorFF){
        // read values on RBV
        if (_PIChannels[0]->getPIFixedSPVal(NULL, &I) != ndsSuccess)
            return ndsError;

        if (_PIChannels[1]->getPIFixedSPVal(NULL, &Q) != ndsSuccess)
            return ndsError;
    }else { //feed forward table
        // read values on RBV
        if (_PIChannels[0]->getPIFixedFFVal(NULL, &I) != ndsSuccess)
            return ndsError;

        if (_PIChannels[1]->getPIFixedFFVal(NULL, &Q) != ndsSuccess)
            return ndsError;
    }

    //convert IQ to Mag/Ang
    _MagRaw = sqrt(pow(I, 2) + pow(Q, 2));
    _AngRBV = atan2(Q, I);


    if (calibrationProvider->isCalibratorRaw2EGUEnabled())
        _MagRBV = calibrationProvider->calibrateRaw2EGU(_MagRaw);
    else
        _MagRBV = _MagRaw;

    doCallbacksFloat64(_MagRBV, _interruptIdMag);
    doCallbacksFloat64(_AngRBV, _interruptIdAng);
    doCallbacksFloat64(_MagRaw, _interruptIdMagRaw);

    return ndsSuccess;
}

//calibrate -> write IQ -> read IQ -> calibrate
ndsStatus sis8300llrfPIMagAngChannel::writeToHardware() {
    if (_MagAngChanged){
        //calibration
        if (calibrationProvider->isCalibratorEGU2RawEnabled())
            _MagRaw = calibrationProvider->calibrateEGU2Raw(_Mag);
        else
            _MagRaw = _Mag;

        if (_MagRaw > SIS8300LLRF_MAG_MAX) 
            _MagRaw = SIS8300LLRF_MAG_MAX;
            
        if (_MagRaw < SIS8300LLRF_MAG_MIN)
            _MagRaw = SIS8300LLRF_MAG_MIN;

        // update IQ
        epicsFloat64 I, Q;

        //convert to IQ
        I = _MagRaw*cos(_Ang);
        Q = _MagRaw*sin(_Ang);

        if (I > SIS8300LLRF_I_MAX)
            I = SIS8300LLRF_I_MAX;
        if (I < SIS8300LLRF_I_MIN)
            I = SIS8300LLRF_I_MIN;
        if (Q > SIS8300LLRF_Q_MAX)
            Q =  SIS8300LLRF_Q_MAX;
        if (Q < SIS8300LLRF_Q_MIN)
            Q =  SIS8300LLRF_Q_MIN;

        if (!_SPorFF){ //Set Point
            //set IQ
            if ((_PIChannels[0]->setPIFixedSPVal(NULL, I) != ndsSuccess) || (_PIChannels[1]->setPIFixedSPVal(NULL, Q) != ndsSuccess)) {
                NDS_ERR("Invalid IQ value");
                return ndsError;
            }
        }
        else {//Feed Forward 
            //set IQ
            if ((_PIChannels[0]->setPIFixedFFVal(NULL, I) != ndsSuccess) || (_PIChannels[1]->setPIFixedFFVal(NULL, Q) != ndsSuccess)) {
                NDS_ERR("Invalid IQ value");
                return ndsError;
            }
        }
        // read values on RBV
        if (updateMagAngRBV() != ndsSuccess)
            return ndsError;
        
        _MagAngChanged = 0;
    }

    return ndsSuccess;
}


/**************************/
/* Calibration functions */
/**************************/

/**
 * @brief Check if calibration is enabled
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] value       Enable/Disabled
 *
 */

ndsStatus sis8300llrfPIMagAngChannel::getCalibEnable(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);
    *value = (epicsInt32) calibrationProvider -> isCalibratorRaw2EGUEnabled() && calibrationProvider -> isCalibratorEGU2RawEnabled();

    return ndsSuccess;
}

/**
 * @brief Set Enable Calibration
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Any value    
 */
ndsStatus sis8300llrfPIMagAngChannel::setCalibEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);
    bool enable = calibrationProvider -> isCalibratorEGU2RawValid() && calibrationProvider->isCalibratorRaw2EGUValid() && value;

    calibrationProvider -> setCalibrationEGU2RawEnable(enable);
    calibrationProvider -> setCalibrationRaw2EGUEnable(enable);

    //TODO update readback
    doCallbacksInt32(enable, _interruptIdCalibEnable);

    return ndsSuccess;
}


void sis8300llrfPIMagAngChannel::reloadCalibPVs(){ 
    bool valid, enable;
    valid = calibrationProvider -> isCalibratorEGU2RawValid() && calibrationProvider -> isCalibratorRaw2EGUValid();
    enable = valid  && calibrationProvider -> isCalibratorEGU2RawEnabled() && calibrationProvider -> isCalibratorRaw2EGUEnabled();
    calibrationProvider -> setCalibrationEGU2RawEnable(enable);
    calibrationProvider -> setCalibrationRaw2EGUEnable(enable);

    doCallbacksInt32(enable, _interruptIdCalibEnable);
   
    if (valid) {
        scalingPoly* calibratorEGU2Raw = calibrationProvider->getCalibratorEGU2Raw();
        scalingPoly* calibratorRaw2EGU = calibrationProvider->getCalibratorRaw2EGU();
        
        doCallbacksFloat64Array(calibratorEGU2Raw->getFittedLine(), 
                   calibratorEGU2Raw->getFittedLineSize(), 
                   _interruptIdCalibFittedLineEGU2Raw);
        doCallbacksFloat64Array(calibratorEGU2Raw->getResiduals(), 
                calibratorEGU2Raw->getResidualsSize(), 
                _interruptIdCalibResidEGU2Raw);
        doCallbacksFloat64Array(calibratorEGU2Raw->getCoeffs(), 
                calibratorEGU2Raw->getCoeffsSize(), 
                _interruptIdCalibCoefEGU2Raw);
        doCallbacksFloat64(calibratorEGU2Raw->getChisq(), 
                _interruptIdCalibChisqEGU2Raw);
        doCallbacksFloat64Array(calibratorRaw2EGU->getFittedLine(), 
                calibratorRaw2EGU->getFittedLineSize(), 
                _interruptIdCalibFittedLineRaw2EGU);
        doCallbacksFloat64Array(calibratorRaw2EGU->getResiduals(), 
                calibratorRaw2EGU->getResidualsSize(), 
                _interruptIdCalibResidRaw2EGU);
        doCallbacksFloat64Array(calibratorRaw2EGU->getCoeffs(), 
                calibratorRaw2EGU->getCoeffsSize(), 
                _interruptIdCalibCoefRaw2EGU);
        doCallbacksFloat64(calibratorRaw2EGU->getChisq(), 
                _interruptIdCalibChisqRaw2EGU);
    }
    else{
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibFittedLineEGU2Raw);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibResidEGU2Raw);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibCoefEGU2Raw);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibFittedLineRaw2EGU);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibResidRaw2EGU);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibCoefRaw2EGU);
    }
}



// Load EGU
ndsStatus sis8300llrfPIMagAngChannel::setCalibEGU(
        asynUser *pasynUser, epicsFloat64 *value, size_t nelem){
    NDS_TRC("%s", __func__);

    calibrationProvider->getCalibratorEGU2Raw()->setX(value, nelem);
    calibrationProvider->getCalibratorRaw2EGU()->setY(value, nelem);

    //read status from calibrator and update the status PV and fittedLine
    reloadCalibPVs();

    return ndsSuccess;
}


// Load Raw
ndsStatus sis8300llrfPIMagAngChannel::setCalibRaw(
        asynUser *pasynUser, epicsFloat64 *value, size_t nelem){
    NDS_TRC("%s", __func__);

    calibrationProvider->getCalibratorEGU2Raw()->setY(value, nelem);
    calibrationProvider->getCalibratorRaw2EGU()->setX(value, nelem);
    //read status from calibrator and update the status PV and fittedLine
    reloadCalibPVs();

    return ndsSuccess;
}

/**
 * @brief Set Calibration Order
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Any value    
 */
ndsStatus sis8300llrfPIMagAngChannel::setCalibOrderEGU2Raw(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value <= 0)
        return ndsError;
    calibrationProvider->getCalibratorEGU2Raw()->setOrder((unsigned int)value);
    //read status from calibrator and update the status PV and fittedLine
    reloadCalibPVs();

    return ndsSuccess;
}


/**
 * @brief Set Calibration Order
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Any value    
 */
ndsStatus sis8300llrfPIMagAngChannel::setCalibOrderRaw2EGU(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value <= 0)
        return ndsError;
    calibrationProvider->getCalibratorRaw2EGU()->setOrder((unsigned int)value);
    //read status from calibrator and update the status PV and fittedLine
    reloadCalibPVs();

    return ndsSuccess;
}



/****************************************************/
/******************** Tables methods ****************/
/****************************************************/

/*
 * @brief: Convert Magnitude and Angle tables to I and Q, and set on ControllerTable
 * */
ndsStatus sis8300llrfPIMagAngChannel::convertTablesToIQ() {
    NDS_TRC("%s", __func__);

    NDS_DBG("Before");
    //convert to I/Q
    if (_MagTable == NULL || _MagTableCount == 0 ||_AngTable == NULL || _AngTableCount == 0){
        _CtrlTableCh->setITable(NULL, NULL, 0);
        _CtrlTableCh->setQTable(NULL, NULL, 0);
        return ndsSuccess;
    }
    epicsInt32 IQTableCount;
    if (_MagTableCount > _AngTableCount)
        IQTableCount = _AngTableCount;
    else
        IQTableCount = _MagTableCount;


    NDS_DBG("MagTable Coung %d , AngTable Count %d, IQTableCount, %d ", _MagTableCount, _AngTableCount, IQTableCount);

    epicsFloat64 * ITable;
    epicsFloat64 * QTable;

    ITable = new epicsFloat64[IQTableCount];
    QTable = new epicsFloat64[IQTableCount];

    for (int i = 0; i < IQTableCount; i++) {
        ITable[i] = _MagTable[i]*cos(_AngTable[i] * M_PI / 180);
        QTable[i] = _MagTable[i]*sin(_AngTable[i] * M_PI / 180);
    }

    _CtrlTableCh->setITable(NULL, ITable, IQTableCount);
    _CtrlTableCh->setQTable(NULL, QTable, IQTableCount);


    delete[] ITable;
    delete[] QTable;

    NDS_DBG("Leaving convert to IQ");
    return ndsSuccess;
}

/*
 * @brief: Set Magnitude Table
 * */
ndsStatus sis8300llrfPIMagAngChannel::setMagTable(
        asynUser *pasynUser, epicsFloat64 *value, size_t nelem){
    NDS_TRC("%s", __func__);

    if (_MagTable != NULL)
        delete[] _MagTable;

    _MagTable = new epicsFloat64[nelem];

    memcpy(_MagTable, value,  nelem * sizeof(epicsFloat64));
    _MagTableCount = nelem;


    //calibration
    if (calibrationProvider->isCalibratorEGU2RawEnabled()){
            for (int i = 0; i < _MagTableCount; i++){
                _MagTable[i] = calibrationProvider->calibrateEGU2Raw(_MagTable[i]);
                if (_MagTable[i] > SIS8300LLRF_MAG_MAX) 
                    _MagTable[i] = SIS8300LLRF_MAG_MAX;
                    
                if (_MagTable[i] < SIS8300LLRF_MAG_MIN)
                    _MagTable[i] = SIS8300LLRF_MAG_MIN;
            }
        }
    else {
        for (int i = 0; i < _MagTableCount; i++){
            if (_MagTable[i] > SIS8300LLRF_MAG_MAX) 
                _MagTable[i] = SIS8300LLRF_MAG_MAX;
                
            if (_MagTable[i] < SIS8300LLRF_MAG_MIN)
                _MagTable[i] = SIS8300LLRF_MAG_MIN;
        }
    }

    doCallbacksFloat64Array(_MagTable, _MagTableCount, _interruptIdMagTableRaw);
    return convertTablesToIQ();
}

/*
 * @brief: Set Angle Table
 * */
ndsStatus sis8300llrfPIMagAngChannel::setAngTable(
        asynUser *pasynUser, epicsFloat64 *value, size_t nelem){
    NDS_TRC("%s", __func__);

    if (_AngTable != NULL)
        delete[] _AngTable;

    _AngTable = new epicsFloat64[nelem];

    memcpy(_AngTable, value,  nelem * sizeof(epicsFloat64));
    _AngTableCount = nelem;

    doCallbacksFloat64Array(_AngTable, _AngTableCount, _interruptIdAngTable);
    return convertTablesToIQ();
}

/*
 * @brief: Load Mag and Ang table from IQ tables
 * */
ndsStatus sis8300llrfPIMagAngChannel::loadMagAngTables() {
    epicsFloat64 * ITable;
    epicsFloat64 * QTable;

    epicsInt32 count;
    size_t ICount, QCount, iqcount; 

    _CtrlTableCh->getSamplesCount(NULL, &count);
    NDS_DBG("Samples Count %d", count);

    // Ignore tables with 1 element
    if (count == 1)
        return ndsSuccess;

    // Numbers lower than 32 are caunsing a very weird allocation error
    // TODO: find the cause of the issue
    if (count < 32)
        iqcount = 32;
    else
        iqcount = count;


    ITable = new epicsFloat64[iqcount];
    QTable = new epicsFloat64[iqcount];

    NDS_DBG("Before get tables");
    _CtrlTableCh->getITable(NULL, ITable, iqcount, &ICount);
    _CtrlTableCh->getQTable(NULL, QTable, iqcount, &QCount);
    NDS_DBG("After get tables");

    //Alocate MagTable and Angle Table
    if (_MagTable != NULL)
        delete[] _MagTable;

    NDS_DBG("Before mag tables");

    _MagTable = new epicsFloat64[count];


    if (_AngTable != NULL)
        delete[] _AngTable;

    NDS_DBG("Before ang tables");
    _AngTable = new epicsFloat64[count];


    for (int i = 0; i < count; i++){
        //convert IQ to Mag/Ang
        _MagTable[i] = sqrt(pow(ITable[i], 2) + pow(QTable[i], 2));
        _AngTable[i] = atan2(QTable[i], ITable[i]) * 180 / M_PI;
    }
    _MagTableCount = count;
    _AngTableCount = count;

    delete[] ITable;
    delete[] QTable;

    NDS_DBG("Before Return");
    return ndsSuccess;
}


ndsStatus sis8300llrfPIMagAngChannel::getMagTable(
                asynUser* pasynUser, epicsFloat64 *value, 
                size_t nelem, size_t *nIn) {
   if (loadMagAngTables() != ndsSuccess)
       return ndsError;

    NDS_DBG("After load Mag tables");

   if (nelem < (size_t) _MagTableCount)
       return ndsError;

    //recalibrate value
    if (calibrationProvider->isCalibratorRaw2EGUEnabled()){
       for (int i = 0; i <  _MagTableCount; i++)
           value[i] = calibrationProvider->calibrateRaw2EGU(_MagTable[i]);
    } else 
        memcpy(value, _MagTable,  _MagTableCount * sizeof(epicsFloat64));


    NDS_DBG("After copy Mag tables");

   *nIn = _MagTableCount;
   return ndsSuccess;
}


ndsStatus sis8300llrfPIMagAngChannel::getAngTable(
                asynUser* pasynUser, epicsFloat64 *value, 
                size_t nelem, size_t *nIn) {
   if (loadMagAngTables() != ndsSuccess)
       return ndsError;

    NDS_DBG("After load Ang tables");

   if (nelem < (size_t)_AngTableCount)
       return ndsError;

   for (int i = 0; i <  _AngTableCount; i++)
       value[i] = _AngTable[i];

    NDS_DBG("After copy ang table");

   *nIn = _AngTableCount;
   return ndsSuccess;
}


bool sis8300llrfPIMagAngChannel::isCalibrationEnabled(){
    return calibrationProvider->isCalibrationEnabled();
};

sis8300llrfCalibrationProvider* sis8300llrfPIMagAngChannel::getCalibrationProvider(){
    return calibrationProvider;
};

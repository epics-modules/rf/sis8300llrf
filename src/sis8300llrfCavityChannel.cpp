/*
 * sis8300llrf
 * Copyright (C) 2021 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300CavityllrfChannel.cpp
 * @brief Implementation of LLRF cavity channel in NDS.
 * @author gabrielfedel
 * @date 1.11.2021
 *
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfCavityChannel.h"
#include "ndsADIOChannel.h"

std::string sis8300llrfCavityChannel::
                    PV_REASON_CALIB_GRAD_EN          = "CalibGradEn";
std::string sis8300llrfCavityChannel::
                    PV_REASON_CALIB_GRAD_A           = "CalibGradA";
std::string sis8300llrfCavityChannel::
                    PV_REASON_CALIB_GRAD_KT          = "CalibGradKt";



// Detuning and QL calibration and conversion parameters
std::string sis8300llrfCavityChannel::
                    PV_REASON_LEFF                   = "Leff";
std::string sis8300llrfCavityChannel::
                    PV_REASON_ROVERQ                 = "RoverQ";
std::string sis8300llrfCavityChannel::
                    PV_REASON_LOWPOWERQEXT           = "LowPowerQext";
std::string sis8300llrfCavityChannel::
                    PV_REASON_WINDOW                 = "Window";

std::string sis8300llrfCavityChannel::
                    PV_REASON_CCCXREAL                 = "CccXReal";
std::string sis8300llrfCavityChannel::
                    PV_REASON_CCCYREAL                 = "CccYReal";
std::string sis8300llrfCavityChannel::
                    PV_REASON_CCCXIMAG                 = "CccXImag";
std::string sis8300llrfCavityChannel::
                    PV_REASON_CCCYIMAG                 = "CccYImag";

sis8300llrfCavityChannel::sis8300llrfCavityChannel(epicsFloat64 FSampling) : sis8300llrfAIChannel(FSampling) {
    m_calib_grad_en = false;
    m_calib_grad_a = 0;
    m_calib_grad_kt = 1;

    m_leff = 1;
    m_roverq = 1;
    m_lowpowerqext = 1;
    m_cccxreal = 0.0;
    m_cccyreal = 0.0;
    m_cccximag = 0.0;
    m_cccyimag = 0.0;
    m_window = 100;
}



/**
 * @brief State handler for transition from PROCESSING. This method override the
 * onLeaveProcessing from sis8300CavityChannel including the polynomial calibration
 * @param [in] from Source state.
 * @param [in] to Destination state.
 * Acquisition can only be started if channel group is in DISABLED state
 * @retval ndsSuccess Successful transition.
 *
 * Check if the acquisition finished successfully. If so then retrieve the
 * raw buffer from the device and convert to volts. For obtaining the raw values
 * the second half of the voltage buffer is used.
 */
ndsStatus sis8300llrfCavityChannel::onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to) {
    sis8300AIChannelGroup   *cg;
    int                     status, iter;
    epicsInt32              nsamples;

    NDS_TRC("%s", __func__);

    if (!_EnableTransf) { //The data transfer is disabled
        doCallbacksFloat32Array(NULL, 0, _interruptIdBufferFloat32);
        return ndsSuccess;
    }

    nsamples = _SamplesCountUsed;

    /* If acquisition didn't finish completely do nothing. */
    cg = dynamic_cast<sis8300AIChannelGroup *>(getChannelGroup());
    if (!cg->isDaqFinished()) {
        NDS_DBG("Daq not completed channel %d skipping buffer update.", getChannelNumber());
        return ndsSuccess;
    }
    NDS_DBG("Calling sis8300drv_read_ai for channel %d", getChannelNumber());
    status = sis8300drv_read_ai(_DeviceUser, getChannelNumber(), _ChannelDataRaw);
    SIS8300NDS_STATUS_CHECK("sis8300drv_read_ai", status);

    // gradiente calibration
    if (calibrator->isEnabled() and m_calib_grad_en) {
        for (iter = 0; iter < nsamples; iter++) {
            _ChannelData[iter] = calibratePolyAndGrad((double)(((epicsInt16)_ChannelDataRaw[iter]) / _convFact));
        }
    }
    else if (calibrator->isEnabled()) { // polynomial calibration
        for (iter = 0; iter < nsamples; iter++) {
            _ChannelData[iter] = calibrator->calibrate((double)(((epicsInt16)_ChannelDataRaw[iter]) / _convFact));
        }
    }
    else // no calibration
    {
        for (iter = 0; iter < nsamples; iter++) {
            _ChannelData[iter] = (double)(((epicsInt16)_ChannelDataRaw[iter]) / _convFact);
        }
    }

    doCallbacksFloat64Array(_ChannelData, (size_t)nsamples, _interruptIdBufferFloat64);

    return ndsSuccess;
}

/**************************/
/* Calibration functions */
/**************************/
/**
 * @brief Check if calibration is enabled
 *
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] value       Enable/Disabled
 *
 */

ndsStatus sis8300llrfCavityChannel::getCalibGradEnable(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);
    *value = (epicsInt32) m_calib_grad_en;

    return ndsSuccess;
}

/**
 * @brief Set Enable Calibration
 *
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Any value
 */
ndsStatus sis8300llrfCavityChannel::setCalibGradEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    m_calib_grad_en = value;
    return ndsSuccess;
}


/**
 * @brief Set Gradient Calibration parameter A
 *
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Any value
 */
ndsStatus sis8300llrfCavityChannel::setCalibGradA(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    m_calib_grad_a = value;

    return ndsSuccess;
}

/**
 * @brief Set Gradient Calibration parameter Kt
 *
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Any value
 */
ndsStatus sis8300llrfCavityChannel::setCalibGradKt(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    m_calib_grad_kt = value;

    return ndsSuccess;
}


/**
 * @brief set cavity efficient length [m]
 *
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Any value
 */
ndsStatus sis8300llrfCavityChannel::setLeff(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    m_leff = value;

    return ndsSuccess;
}

/**
 * @brief cavity geometry R/Q
 *
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Any value
 */
ndsStatus sis8300llrfCavityChannel::setRoverQ(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    m_roverq = value;

    return ndsSuccess;
}

/**
 * @brief cavity external Q value from cavity calibration
 *
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Any value
 */
ndsStatus sis8300llrfCavityChannel::setLowPowerQext(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    m_lowpowerqext = value;

    return ndsSuccess;
}

/**
 * @brief cavity external Q value from cavity calibration
 *
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Any value
 */
ndsStatus sis8300llrfCavityChannel::setWindow(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    m_window = value;

    return ndsSuccess;
}

ndsStatus sis8300llrfCavityChannel::setCccXReal(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);
    m_cccxreal = value;

    return ndsSuccess;
}

ndsStatus sis8300llrfCavityChannel::setCccYReal(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);
    m_cccyreal = value;

    return ndsSuccess;
}

ndsStatus sis8300llrfCavityChannel::setCccXImag(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);
    m_cccximag = value;

    return ndsSuccess;
}

ndsStatus sis8300llrfCavityChannel::setCccYImag(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);
    m_cccyimag = value;

    return ndsSuccess;
}


/**
 * @brief Registers handlers for interfacing with records. For more
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfCavityChannel::registerHandlers(
                nds::PVContainers* pvContainers) {
    NDS_PV_REGISTER_INT32(
           sis8300llrfCavityChannel::PV_REASON_CALIB_GRAD_EN,
           &sis8300llrfCavityChannel::setCalibGradEnable,
           &sis8300llrfCavityChannel::getCalibGradEnable,
           &_interruptIdCalibGradEnable
     );

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfCavityChannel::PV_REASON_CALIB_GRAD_A,
            &sis8300llrfCavityChannel::setCalibGradA,
            &sis8300llrfCavityChannel::getFloat64,
            &_interruptIdCalibGradA
    );

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfCavityChannel::PV_REASON_CALIB_GRAD_KT,
            &sis8300llrfCavityChannel::setCalibGradKt,
            &sis8300llrfCavityChannel::getFloat64,
            &_interruptIdCalibGradKt
    );
		//Additional cavity params
    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfCavityChannel::PV_REASON_LEFF,
            &sis8300llrfCavityChannel::setLeff,
            &sis8300llrfCavityChannel::getFloat64,
            &_interruptIdLeff
    );

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfCavityChannel::PV_REASON_ROVERQ,
            &sis8300llrfCavityChannel::setRoverQ,
            &sis8300llrfCavityChannel::getFloat64,
            &_interruptIdRoverQ
    );

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfCavityChannel::PV_REASON_LOWPOWERQEXT,
            &sis8300llrfCavityChannel::setLowPowerQext,
            &sis8300llrfCavityChannel::getFloat64,
            &_interruptIdLowPowerQext
    );

    NDS_PV_REGISTER_INT32(
            sis8300llrfCavityChannel::PV_REASON_WINDOW,
            &sis8300llrfCavityChannel::setWindow,
            &sis8300llrfCavityChannel::getInt32,
            &_interruptIdWindow
    );

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfCavityChannel::PV_REASON_CCCXREAL,
            &sis8300llrfCavityChannel::setCccXReal,
            &sis8300llrfCavityChannel::getFloat64,
            &_interruptIdCccXReal
    );

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfCavityChannel::PV_REASON_CCCYREAL,
            &sis8300llrfCavityChannel::setCccYReal,
            &sis8300llrfCavityChannel::getFloat64,
            &_interruptIdCccYReal
    );

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfCavityChannel::PV_REASON_CCCXIMAG,
            &sis8300llrfCavityChannel::setCccXImag,
            &sis8300llrfCavityChannel::getFloat64,
            &_interruptIdCccXImag
    );

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfCavityChannel::PV_REASON_CCCYIMAG,
            &sis8300llrfCavityChannel::setCccYImag,
            &sis8300llrfCavityChannel::getFloat64,
            &_interruptIdCccYImag
    );

    return sis8300llrfAIChannel::registerHandlers(pvContainers);
}

/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfInternalChannel.cpp
 * @brief File defining the LLRF Internal channel class that handles
 * internal values and their settings
 * @author gabriel.fedel@ess.eu
 * @date 8.4.2020
 * 
 * TODO: add long description
 */
#include "sis8300drv.h"
#include "sis8300llrfdrv.h"
#include "sis8300llrfdrv_types.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfInternalChannel.h"

#define ROUNDUP_TWOHEX(val) ((unsigned) (val + 0x1F) &~0x1F)


/**
 * @brief Internal channel constructor.
 */
sis8300llrfInternalChannel::sis8300llrfInternalChannel(epicsFloat64 FSampling, epicsInt32 nearIqN) :
        sis8300llrfAuxChannel(
            FSampling,
            nearIqN,
            1) { //Internal
}


sis8300llrfdrv_Qmn intern_qmn[SIS8300LLRFDRV_INTERN_CHANNELS] = {
    sis8300llrfdrv_Qmn_intern_PI_err_IQ_sample, //It's not used here
    sis8300llrfdrv_Qmn_intern_ref_comp_sample,
    sis8300llrfdrv_Qmn_intern_inp_filt_sample,
    sis8300llrfdrv_Qmn_intern_PI_out_sample,
    sis8300llrfdrv_Qmn_intern_llrf_out_sample,
    sis8300llrfdrv_Qmn_intern_PI_err_ILC_sample,
};

/**
 * @brief Read all the internal data from memory and push to PV
 *
 * @return ndsSuccess   Data read successfully
 * @return ndsError     Read failed, chan goes to ERR state or
 *                      couldn't allocate memory.
 *
 * This will read all the internal data, convert to float and push
 * to the PV.
 *
 * */
ndsStatus sis8300llrfInternalChannel::onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);
   
    int status, iterSrc, iterDest;
    unsigned nsamples;
    epicsInt16 * rawDataI16;
    epicsFloat64       *IMagData; /**< waveform with I or Magnitude*/
    epicsFloat64       *QAngData; /**< waveform with Q or Angle */
    epicsFloat64 ConvFact;
    bool pm;

    if (sis8300llrfAuxChannel::onLeaveProcessing(from, to) == ndsError)
        return ndsError;
    
    if (_SamplesAcquired == 0) {
        return ndsSuccess;
    }

    pm = checkPM();
    // If there it is not enable the transfer and there is no interlock 
    // We shouldn't get anything from firmware
    if (!pm && !_EnableTransf){
        if (_XAxis != NULL)
            delete [] _XAxis;
        _XAxis = NULL;
        _SamplesAcquired = 0;
        doCallbacksInt32(_SamplesAcquired, _interruptIdAUXNSamplesAcq);
        doCallbacksFloat64Array(_XAxis, _SamplesAcquired, _interruptIdAUXXAxis);
        doCallbacksFloat64Array(NULL, (size_t)_SamplesAcquired, _interruptIdAUXIMag);
        doCallbacksFloat64Array(NULL, (size_t)_SamplesAcquired, _interruptIdAUXQAng);
        return ndsSuccess;
    }

    /* we need to read in 512 bit blocks - firmware imposed limitation */
    NDS_DBG("Rounding _SamplesAcquired from %d", _SamplesAcquired);
    nsamples = ROUNDUP_TWOHEX(_SamplesAcquired);
    NDS_DBG("_SamplesAcquired rounded to %u", nsamples);

    rawDataI16 = new (std::nothrow) epicsInt16[nsamples * 2]; //*2 because is 32bits per sample
    IMagData = new (std::nothrow) epicsFloat64[_SamplesAcquired];
    QAngData = new (std::nothrow) epicsFloat64[_SamplesAcquired];

    if (!rawDataI16 || !IMagData || !QAngData) 
        return ndsError;


    status = sis8300llrfdrv_read_aux_channel(
                _DeviceUser, _Channel , _DownOrIntern, rawDataI16, 
                nsamples);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_read_aux_channel", status);

    ConvFact = 1.0 / (epicsFloat64) (1 << intern_qmn[_Channel].frac_bits_n);
    for (iterSrc = 0, iterDest = 0; iterDest < _SamplesAcquired; iterSrc += 2, iterDest ++) {
        IMagData[iterDest] = (epicsFloat64)(((epicsInt16)rawDataI16[iterSrc]) * ConvFact);
        QAngData[iterDest] = (epicsFloat64)(((epicsInt16)rawDataI16[iterSrc + 1]) * ConvFact);
    }
    doCallbacksFloat64Array(QAngData, (size_t)_SamplesAcquired, _interruptIdAUXQAng);
    doCallbacksFloat64Array(IMagData, (size_t)_SamplesAcquired, _interruptIdAUXIMag);

    if (pm){
        //Push data for PM PVs
        doCallbacksFloat64Array(IMagData, (size_t)_SamplesAcquired, _interruptIdAUXPMCmp0);
        doCallbacksFloat64Array(QAngData, (size_t)_SamplesAcquired, _interruptIdAUXPMCmp1);
        doCallbacksFloat64Array(_XAxis, _SamplesAcquired, _interruptIdAUXPMXAxis);
    }

    delete [] rawDataI16;
    delete [] IMagData;
    delete [] QAngData;

    return ndsSuccess;
}

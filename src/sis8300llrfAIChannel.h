/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file _sis8300llrfAIChannel_h
 * @brief Header file defining the LLRF AI channel class
 * @author urojec
 * @date 12.5.2015
 */

#ifndef _sis8300llrfAIChannel_h
#define _sis8300llrfAIChannel_h

#include "sis8300AIChannel.h"
#include "scalingPoly.h"

/**
 * @brief llrf specific AI channel, used for AI0 and AI1 - reference and 
 * cavity. It prevents disabling the channel and provides signal 
 * magnitude and angle reads.
 */
class sis8300llrfAIChannel: public sis8300AIChannel {

public:
    sis8300llrfAIChannel(epicsFloat64 FSampling);
    sis8300llrfAIChannel();
    virtual ~sis8300llrfAIChannel();

    /* overriden because unsupported */
    virtual ndsStatus setEnabled(asynUser* pasynUser, epicsInt32 value);
    
    virtual ndsStatus getCalibEnable(asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setCalibEnable(asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setCalibEGU(asynUser *pasynUser, epicsFloat64 *value, size_t nelem);
    virtual ndsStatus setCalibRaw(asynUser *pasynUser, epicsFloat64 *value, size_t nelem);
    virtual ndsStatus setCalibOrder(asynUser *pasynUser, epicsInt32 value);

    //methods to be used by other classes to check if the channels is calibrated
    virtual bool isCalibrated() {return calibrator->isEnabled();};
    virtual double calibrate(double value) {
        return calibrator->calibrate(value);
    };

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
protected:
    scalingPoly        *calibrator;
    const int          maxCalibCoef = 20;

    static std::string PV_REASON_CALIB_EN;
    static std::string PV_REASON_CALIB_EGU;
    static std::string PV_REASON_CALIB_RAW;
    static std::string PV_REASON_CALIB_ORDER;
    static std::string PV_REASON_CALIB_FITTED_LINE;
    static std::string PV_REASON_CALIB_RESID;
    static std::string PV_REASON_CALIB_COEF;
    static std::string PV_REASON_CALIB_CHISQ;

    int _interruptIdCalibEnable;
    int _interruptIdCalibEGU;
    int _interruptIdCalibRaw;
    int _interruptIdCalibOrder;
    int _interruptIdCalibFittedLine;
    int _interruptIdCalibResid;
    int _interruptIdCalibCoef;
    int _interruptIdCalibChisq;

    virtual ndsStatus onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to);
    virtual void reloadCalibPVs();
};

#endif /* _sis8300llrfAIChannel_h */

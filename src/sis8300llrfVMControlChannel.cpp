/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfIOControlChannel.cpp
 * @brief Implementation of LLRF Input-Output control channel in NDS.
 * @author urojec
 * @date 2.6.2014
 * 
 * this class contains all the settings available for Vector Modulator (VM).
 * 
 * Since vector modulator has a magnitude limiter that can activate when the
 * output magnitude gos over a predefined limit, the channel also reads
 * the status of this limiter every time it leaves processing state (status
 * for the past pulse). The limiter status can also be read out on demand.
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfControllerChannelGroup.h"

#include "sis8300llrfVMControlChannel.h"


std::string sis8300llrfVMChannel::
                PV_REASON_OUTPUT_MAGNITUDE_LIMIT_VAL 	= "MagnitudeLimitVal";
std::string sis8300llrfVMChannel::
                PV_REASON_OUTPUT_MAGNITUDE_LIMIT_EN 	= "MagnitudeLimitEnable";
std::string sis8300llrfVMChannel::
                PV_REASON_OUTPUT_MAGNITUDE_LIMIT_STATUS = "MagnitudeLimitStatus";
std::string sis8300llrfVMChannel::
                PV_REASON_OUTPUT_INVERSE_I 				= "InvertOutputI";
std::string sis8300llrfVMChannel::
                PV_REASON_OUTPUT_INVERSE_Q 				= "InvertOutputQ";
std::string sis8300llrfVMChannel::
                PV_REASON_SWAP_IQ 						= "SwapIQEn";
std::string sis8300llrfVMChannel::
                PV_REASON_PREDISTORT_EN 				= "PreDistEn";
std::string sis8300llrfVMChannel::
                PV_REASON_PREDISTORT_RC00				= "PreDistRC00";
std::string sis8300llrfVMChannel::
                PV_REASON_PREDISTORT_RC01				= "PreDistRC01";
std::string sis8300llrfVMChannel::
                PV_REASON_PREDISTORT_RC10				= "PreDistRC10";
std::string sis8300llrfVMChannel::
                PV_REASON_PREDISTORT_RC11				= "PreDistRC11";
std::string sis8300llrfVMChannel::
                PV_REASON_PREDISTORT_DC_OFFSET_I		= "PreDistDCOI";
std::string sis8300llrfVMChannel::
                PV_REASON_PREDISTORT_DC_OFFSET_Q  		= "PreDistDCOQ";
std::string sis8300llrfVMChannel::
                PV_REASON_OUTPUT_LINEARIZATION_LUT_EN   = "LinLUTEnable";
std::string sis8300llrfVMChannel::
                PV_REASON_OUTPUT_INNLOOP_EN             = "InnLoopEnable";
std::string sis8300llrfVMChannel::
                PV_REASON_OUTPUT_ANGOFFSET_EN           = "AngOffsetEnable";
std::string sis8300llrfVMChannel::
                PV_REASON_OUTPUT_FORCE_MAG              = "ForceMagnitude";
std::string sis8300llrfVMChannel::
                PV_REASON_OUTPUT_FORCE_ANG              = "ForceAngle";
std::string sis8300llrfVMChannel::
                PV_REASON_OUTPUT_ANGLE_OFF              = "AngleOffset";

/**
 * @brief VM channel constructor.
 */
sis8300llrfVMChannel::sis8300llrfVMChannel(sis8300llrfCalibrationProvider * calibrationProvider)
    : sis8300llrfChannel(
            SIS8300LLRFDRV_VM_PARAM_NUM, 
            SIS8300LLRFDRV_VM_PARAM_INT_FIRST) {
        
    registerOnLeaveStateHandler(nds::CHANNEL_STATE_PROCESSING,
        boost::bind(&sis8300llrfVMChannel::onLeaveProcessing, 
        this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_RESETTING,
        boost::bind(&sis8300llrfVMChannel::onEnterReset, this));

    strcpy(_ChanStringIdentifier, "VM control");

    _ParamVals[vm_param_inverse_i_en] = 1;
    _ParamVals[vm_param_inverse_q_en] = 1;

    this->calibrationProvider = calibrationProvider;
}

/**
 * @brief VM Channel destructor
 */
sis8300llrfVMChannel::~sis8300llrfVMChannel() {}

/**
 * @brief Read out data that has to be updated after every pulse
 *
 * @param [in]  from    From channel state
 * @param [in]  to      To Channels state
 *
 * @return ndsSuccess   Always
 */
ndsStatus sis8300llrfVMChannel::onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    
    return checkStatuses();
}

/**
 * @see #sis8300llrfChannel::onEnterReset
 */
ndsStatus sis8300llrfVMChannel::onEnterReset() {
    
    if (sis8300llrfChannel::onEnterReset() != ndsSuccess) {
        return ndsError;
    }
    
    return checkStatuses();
}

/**
 * @see #sis8300llrfChannel::readParameter
 */
inline int sis8300llrfVMChannel::readParameter(
                int paramIdx, double *paramVal) {
    int status = sis8300llrfdrv_get_vm_param(
                        _DeviceUser, 
                        (sis8300llrfdrv_vm_param) paramIdx, paramVal);
    // If parameter is vm_param_mag_lim_val, then apply calibration
    if (paramIdx == vm_param_mag_lim_val){
        if (!status && calibrationProvider->isCalibratorRaw2EGUEnabled()){
            *paramVal = calibrationProvider->calibrateRaw2EGU(*paramVal);
        }
    }
    return status;
}

/**
 * @see #sis8300llrfChannel::writeParameter
 */
inline int sis8300llrfVMChannel::writeParameter(
                int paramIdx, double *paramErr) {
    return sis8300llrfdrv_set_vm_param(
                _DeviceUser, (sis8300llrfdrv_vm_param) paramIdx, 
                _ParamVals[paramIdx], paramErr);
}

/**
 * @brief All the statuses that need to be checked on leave processing
 *        and enter reset
 */
inline ndsStatus sis8300llrfVMChannel::checkStatuses() {
    epicsInt32 valInt32;
    
    if (getOutputMagnitudeLimitStatus(NULL, &valInt32) != ndsSuccess) {
        return ndsError;
    }
    doCallbacksInt32(valInt32, _interruptIdOutputMagnitudeLimitStatus);
    
    return ndsSuccess;
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfVMChannel::registerHandlers(
                nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfVMChannel::PV_REASON_OUTPUT_MAGNITUDE_LIMIT_VAL,
            &sis8300llrfVMChannel::setOutputMagnitudeLimit,
            &sis8300llrfVMChannel::getOutputMagnitudeLimit,
            &_interruptIds[vm_param_mag_lim_val]);

    NDS_PV_REGISTER_INT32(
            sis8300llrfVMChannel::PV_REASON_OUTPUT_MAGNITUDE_LIMIT_EN,
            &sis8300llrfVMChannel::setOutputMagnitudeLimitEnable,
            &sis8300llrfVMChannel::getOutputMagnitudeLimitEnable,
            &_interruptIds[vm_param_mag_lim_en]);

    NDS_PV_REGISTER_INT32(sis8300llrfVMChannel::PV_REASON_OUTPUT_INVERSE_I,
            &sis8300llrfVMChannel::setOutputInverseIEnable,
            &sis8300llrfVMChannel::getOutputInverseIEnable,
            &_interruptIds[vm_param_inverse_i_en]);

    NDS_PV_REGISTER_INT32(sis8300llrfVMChannel::PV_REASON_OUTPUT_INVERSE_Q,
            &sis8300llrfVMChannel::setOutputInverseQEnable,
            &sis8300llrfVMChannel::getOutputInverseQEnable,
            &_interruptIds[vm_param_inverse_q_en]);

    NDS_PV_REGISTER_INT32(
            sis8300llrfVMChannel::PV_REASON_SWAP_IQ,
            &sis8300llrfVMChannel::setSwapIQEnable,
            &sis8300llrfVMChannel::getSwapIQQEnable,
            &_interruptIds[vm_param_swap_iq]);

    NDS_PV_REGISTER_INT32(
            sis8300llrfVMChannel::PV_REASON_PREDISTORT_EN,
            &sis8300llrfVMChannel::setPredistortEnable,
            &sis8300llrfVMChannel::getPredistortEnable,
            &_interruptIds[vm_param_predistort_en]);
            
    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfVMChannel::PV_REASON_PREDISTORT_RC00,
            &sis8300llrfVMChannel::setPredistortRC00,
            &sis8300llrfVMChannel::getPredistortRC00,
            &_interruptIds[vm_param_predist_rc00]);
            
    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfVMChannel::PV_REASON_PREDISTORT_RC10,
            &sis8300llrfVMChannel::setPredistortRC10,
            &sis8300llrfVMChannel::getPredistortRC10,
            &_interruptIds[vm_param_predist_rc10]);
            
    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfVMChannel::PV_REASON_PREDISTORT_RC01,
            &sis8300llrfVMChannel::setPredistortRC01,
            &sis8300llrfVMChannel::getPredistortRC01,
            &_interruptIds[vm_param_predist_rc01]);
            
    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfVMChannel::PV_REASON_PREDISTORT_RC11,
            &sis8300llrfVMChannel::setPredistortRC11,
            &sis8300llrfVMChannel::getPredistortRC11,
            &_interruptIds[vm_param_predist_rc11]);
            
    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfVMChannel::PV_REASON_PREDISTORT_DC_OFFSET_I,
            &sis8300llrfVMChannel::setPredistortDCOffsetI,
            &sis8300llrfVMChannel::getPredistortDCOffsetI,
            &_interruptIds[vm_param_predist_dcoffset_i]);
            
    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfVMChannel::PV_REASON_PREDISTORT_DC_OFFSET_Q,
            &sis8300llrfVMChannel::setPredistortDCOffsetQ,
            &sis8300llrfVMChannel::getPredistortDCOffsetQ,
            &_interruptIds[vm_param_predist_dcoffset_q]);

    NDS_PV_REGISTER_INT32(
            sis8300llrfVMChannel::PV_REASON_OUTPUT_MAGNITUDE_LIMIT_STATUS,
            &sis8300llrfVMChannel::setInt32,
            &sis8300llrfVMChannel::getOutputMagnitudeLimitStatus,
            &_interruptIdOutputMagnitudeLimitStatus);

    NDS_PV_REGISTER_INT32(
            sis8300llrfVMChannel::PV_REASON_OUTPUT_LINEARIZATION_LUT_EN,
            &sis8300llrfVMChannel::setLinearizationLUTEnable,
            &sis8300llrfVMChannel::getLinearizationLUTEnable,
            &_interruptIds[vm_param_lin_lut_en]
            );

    NDS_PV_REGISTER_INT32(
            sis8300llrfVMChannel::PV_REASON_OUTPUT_INNLOOP_EN,
            &sis8300llrfVMChannel::setInnerLoopEnable,
            &sis8300llrfVMChannel::getInnerLoopEnable,
            &_interruptIds[vm_param_inn_loop_en]
            );

    NDS_PV_REGISTER_INT32(
            sis8300llrfVMChannel::PV_REASON_OUTPUT_ANGOFFSET_EN,
            &sis8300llrfVMChannel::setAngleOffsetEnable,
            &sis8300llrfVMChannel::getAngleOffsetEnable,
            &_interruptIds[vm_param_ang_off_en]
            );

    NDS_PV_REGISTER_INT32(
            sis8300llrfVMChannel::PV_REASON_OUTPUT_FORCE_MAG,
            &sis8300llrfVMChannel::setForceMagnitude,
            &sis8300llrfVMChannel::getForceMagnitude,
            &_interruptIds[vm_param_force_mag]
            );

    NDS_PV_REGISTER_INT32(
            sis8300llrfVMChannel::PV_REASON_OUTPUT_FORCE_ANG,
            &sis8300llrfVMChannel::setForceAngle,
            &sis8300llrfVMChannel::getForceAngle,
            &_interruptIds[vm_param_force_ang]
            );

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfVMChannel::PV_REASON_OUTPUT_ANGLE_OFF,
            &sis8300llrfVMChannel::setAngleOffset,
            &sis8300llrfVMChannel::getAngleOffset,
            &_interruptIds[vm_param_angleoffset]);


    return sis8300llrfChannel::registerHandlers(pvContainers);
}

/* ====VECTOR MODULATOR STATUS READOUTS FROM HARDWARE ==== */

/**
 * @brief Read the current magnitude limiter status from hardware.
 * 
 * @param [in]  pasynUser Asyn user context struct
 * @param [out] value     On success this will be 1 if limiter is active and 0 if not
 * 
 * @return  ndsSuccess  Data retrieved successfully
 * @return  ndsError    During IOC INIt of if read from device failed.
 * 
 */
ndsStatus sis8300llrfVMChannel::getOutputMagnitudeLimitStatus(
                asynUser *pasynUser, epicsInt32 *value) {
    int status;
    unsigned uRegVal;

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    status = sis8300llrfdrv_get_general_status(
                _DeviceUser, gen_status_vm_mag_limiter_active, &uRegVal);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_get_general_status", status);
    
    *value = (epicsInt32) uRegVal;

    return ndsSuccess;
}

/* === GETTERS AND SETTERS FOR CONTROLLER SETTINGS === */
/* ========== RELATED TO VECTOR MODULATOR ============ */
/*
 * The getters are meant to be used by records that will keep track of 
 * setup used in the pulse that just passed. These records should be 
 * set to process at IO interrupt, because all the callbacks will get 
 * called when receiving the PULSE_DONE interrupt.
 * */

/**
 * @brief Set output magnitude limiter limit value
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of magnitude limit to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setOutputMagnitudeLimit(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    //Calibrate to Raw
    if (calibrationProvider->isCalibratorEGU2RawEnabled()){
        double value_calibrated = calibrationProvider->calibrateEGU2Raw((double) value);
        _ParamVals[vm_param_mag_lim_val] = value_calibrated;
    }
    else{
        _ParamVals[vm_param_mag_lim_val] = (double) value;
    }

    _ParamChanges[vm_param_mag_lim_val] = 1;
    return commitParameters();
}
/**
 * @brief Get output magnitude limiter limit
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the limiter value on success
 * 
 * @return ndsSuccess  Success
 * @return ndsError    If a call is made during IOC INIT to prevent wrong 
 *                     settings 
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getOutputMagnitudeLimit(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    // Calibrate to EGU
    if (calibrationProvider->isCalibratorRaw2EGUEnabled()){
        double value_calibrated = calibrationProvider->calibrateRaw2EGU(_ParamVals[vm_param_mag_lim_val]);
        *value = (epicsFloat64) value_calibrated;
    }
    else{
        *value = (epicsFloat64) _ParamVals[vm_param_mag_lim_val];
    }

    return ndsSuccess;
}
/**
 * @brief Emable output magnitude limiter. It will trigger if the magnitude is
 *        larger than the value set with #setOutputMagnitudeLimit
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setOutputMagnitudeLimitEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_mag_lim_en] = value ? 1 : 0;

    _ParamChanges[vm_param_mag_lim_en] = 1;
    return commitParameters();
}
/**
 * @brief Check if magnitude limiter is enabled
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       On success, this will be 1 if enabled and 0 
 *                          if disabled
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getOutputMagnitudeLimitEnable(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[vm_param_mag_lim_en];

    return ndsSuccess;
}
/**
 * @brief Enable I output inversion.
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setOutputInverseIEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_inverse_i_en] = value ? 1 : 0;

    _ParamChanges[vm_param_inverse_i_en] = 1;
    return commitParameters();
}
/**
 * @brief Check if inversion of I part is enabled
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       On success, this will be 1 if enabled and 0 
 *                          if disabled
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getOutputInverseIEnable(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[vm_param_inverse_i_en];

    return ndsSuccess;
}
/**
 * @brief Enable Q output inversion.
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setOutputInverseQEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_inverse_q_en] = value ? 1 : 0;

    _ParamChanges[vm_param_inverse_q_en] = 1;
    return commitParameters();
}
/**
 * @brief Check if inversion of Q part is enabled
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       On success, this will be 1 if enabled and 0 
 *                          if disabled
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getOutputInverseQEnable(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[vm_param_inverse_q_en];

    return ndsSuccess;
}
/**
 * @brief Enable swapping of I and Q component
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setSwapIQEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_swap_iq] = value ? 1 : 0;

    _ParamChanges[vm_param_swap_iq] = 1;
    return commitParameters();
}
/**
 * @brief Check if swapping of I and Q part is enabled
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       On success, this will be 1 if enabled and 0 
 *                          if disabled
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getSwapIQQEnable(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[vm_param_swap_iq];

    return ndsSuccess;
}
/**
 * @brief Enable presditortion of the output to VM
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setPredistortEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_predistort_en] = value ? 1 : 0;

    _ParamChanges[vm_param_predistort_en] = 1;
    return commitParameters();
}
/**
 * @brief Check if predistortion of the output to VM is enabled
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       On success, this will be 1 if enabled and 0 
 *                          if disabled
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getPredistortEnable(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[vm_param_predistort_en];

    return ndsSuccess;
}
/**
 * @brief Set pre-distortion matrix RC00
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the matrix element to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setPredistortRC00(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_predist_rc00] = (double) value;

    _ParamChanges[vm_param_predist_rc00] = 1;
    return commitParameters();
}
/**
 * @brief Get the pre-distortion matrix RC00
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the matrix element value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getPredistortRC00(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[vm_param_predist_rc00];

    return ndsSuccess;
}
/**
 * @brief Set pre-distortion matrix RC01
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the matrix element to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setPredistortRC01(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_predist_rc01] = (double) value;

    _ParamChanges[vm_param_predist_rc01] = 1;
    return commitParameters();
}
/**
 * @brief Get the pre-distortion matrix RC01
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the matrix element value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getPredistortRC01(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[vm_param_predist_rc01];

    return ndsSuccess;
}
/**
 * @brief Set pre-distortion matrix RC10
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the matrix element to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setPredistortRC10(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_predist_rc10] = (double) value;

    _ParamChanges[vm_param_predist_rc10] = 1;
    return commitParameters();
}
/**
 * @brief Get the pre-distortion matrix RC10
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the matrix element value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getPredistortRC10(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[vm_param_predist_rc10];

    return ndsSuccess;
}
/**
 * @brief Set pre-distortion matrix RC11
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of the matrix element to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setPredistortRC11(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_predist_rc11] = (double) value;

    _ParamChanges[vm_param_predist_rc11] = 1;
    return commitParameters();
}
/**
 * @brief Get the pre-distortion matrix RC11
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the matrix element value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getPredistortRC11(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[vm_param_predist_rc11];

    return ndsSuccess;
}
/**
 * @brief Set predistortion DC offset for I part
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of predistortion DC offset to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setPredistortDCOffsetI(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_predist_dcoffset_i] = (double) value;

    _ParamChanges[vm_param_predist_dcoffset_i] = 1;
    return commitParameters();
}
/**
 * @brief Get the pre-distortion offset for I part
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold offset value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getPredistortDCOffsetI(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[vm_param_predist_dcoffset_i];

    return ndsSuccess;
}
/**
 * @brief Set predistortion DC offset for Q part
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Value of predistortion DC offset to set
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setPredistortDCOffsetQ(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_predist_dcoffset_q] = (double) value;

    _ParamChanges[vm_param_predist_dcoffset_q] = 1;
    return commitParameters();
}
/**
 * @brief Get the pre-distortion offset for Q part
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold offset value on success
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getPredistortDCOffsetQ(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[vm_param_predist_dcoffset_q];

    return ndsSuccess;
}

/**
 * @brief Enable Linearization LUT.
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setLinearizationLUTEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_lin_lut_en] = value ? 1 : 0;

    _ParamChanges[vm_param_lin_lut_en] = 1;
    return commitParameters();
}

/**
 * @brief Check if Linearization LUT is enabled
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       On success, this will be 1 if enabled and 0 
 *                          if disabled
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getLinearizationLUTEnable(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[vm_param_lin_lut_en];

    return ndsSuccess;
}

/**
 * @brief Enable Inner Loop.
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setInnerLoopEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_inn_loop_en] = value ? 1 : 0;

    _ParamChanges[vm_param_inn_loop_en] = 1;
    return commitParameters();
}

/**
 * @brief Check if Inner Loop is enabled
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       On success, this will be 1 if enabled and 0 
 *                          if disabled
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getInnerLoopEnable(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[vm_param_inn_loop_en];

    return ndsSuccess;
}

/**
 * @brief Enable Angle Offset.
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setAngleOffsetEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_ang_off_en] = value ? 1 : 0;

    _ParamChanges[vm_param_ang_off_en] = 1;
    return commitParameters();
}

/**
 * @brief Check if Angle Offset is enabled
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       On success, this will be 1 if enabled and 0 
 *                          if disabled
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getAngleOffsetEnable(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[vm_param_ang_off_en];

    return ndsSuccess;
}

/**
 * @brief Force output magnitude.
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setForceMagnitude(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_force_mag] = value ? 1 : 0;

    _ParamChanges[vm_param_force_mag] = 1;
    return commitParameters();
}

/**
 * @brief Check if Force output magnitude is enable
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       On success, this will be 1 if enabled and 0 
 *                          if disabled
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getForceMagnitude(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[vm_param_force_mag];

    return ndsSuccess;
}

/**
 * @brief Force output angle.
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setForceAngle(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_force_ang] = value ? 1 : 0;

    _ParamChanges[vm_param_force_ang] = 1;
    return commitParameters();
}

/**
 * @brief Check if Force output angle is enable
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       On success, this will be 1 if enabled and 0 
 *                          if disabled
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getForceAngle(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[vm_param_force_ang];

    return ndsSuccess;
}

/**
 * @brief Set output block angle offset
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Angle Offset
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control 
 * loop is running, this will get written down when the pulse is 
 * finished, if the device is in INIT state it will be written down 
 * immediately.
 */
ndsStatus sis8300llrfVMChannel::setAngleOffset(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[vm_param_angleoffset] = (double) value;

    _ParamChanges[vm_param_angleoffset] = 1;
    return commitParameters();
}
/**
 * @brief Get output block angle offset
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Angle Offset
 * 
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 * 
 * This will return the value of the local setting (this class's private 
 * variable)
 */
ndsStatus sis8300llrfVMChannel::getAngleOffset(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[vm_param_angleoffset];

    return ndsSuccess;
}

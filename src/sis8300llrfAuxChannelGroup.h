/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfAuxChannelGroup.h
 * @brief Header file defining the LLRF Auxiliary channel group class.
 * @author gabrielfedel@ess.eu
 * @date 7.4.2020
 */

#ifndef _sis8300llrfAuxChannelGroup_h
#define _sis8300llrfAuxChannelGroup_h

#include "sis8300llrfChannelGroup.h"
#include "ndsChannelStateMachine.h"

/**
 * @brief Auxiliary channel group  specific sis8300llrfChannelGroup
 *
 */
class sis8300llrfAuxChannelGroup: public sis8300llrfChannelGroup {

public:
    sis8300llrfAuxChannelGroup(
        const std::string& name, sis8300drv_usr *newDeviceUser);
    virtual ~sis8300llrfAuxChannelGroup();
    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);

    virtual ndsStatus onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus onEnterDisabled(nds::ChannelStates from, nds::ChannelStates to);
    virtual ndsStatus onEnterProcessing(nds::ChannelStates from, nds::ChannelStates to);
protected:
    epicsFloat64 timemsec();

    epicsFloat64 _PerfTiming;

    int _interruptIdAUXPerfTiming;

    unsigned _Init;

    /* for asynReasons */
    static std::string PV_REASON_AUX_PERFTIMING;
};

#endif /* _sis8300llrfAuxChannelGroup_h */

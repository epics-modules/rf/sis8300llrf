/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @brief Implementation of the generic LLRF channelGroup class
 * @author urojec
 * @date 26.5.2014
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfChannel.h"
#include "sis8300llrfChannelGroup.h"

std::string sis8300llrfChannelGroup::PV_REASON_CH_DATA_READY = 
                                           "ChannelDataReady";

/**
 * @brief sis8300llrfChannelGroup constructor.
 * @param [in] name Channel Group name.
 *
 * The function registers handlers for required state transitions and initializes
 * parametrs. A resetting state is also added, to support the software reset
 * available on the controller.
 */
sis8300llrfChannelGroup::sis8300llrfChannelGroup(
    const std::string& name, sis8300drv_usr *newDeviceUser) :
        nds::ChannelGroup(name) {

    if(!newDeviceUser) {
        NDS_CRT("Cannot create group %s, device user is NULL!", 
            name.c_str());
    }

    _DeviceUser = newDeviceUser;

    registerOnEnterStateHandler(nds::CHANNEL_STATE_PROCESSING,
        boost::bind(&sis8300llrfChannelGroup::onEnterProcessing, 
        this, _1, _2));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_DISABLED,
        boost::bind(&sis8300llrfChannelGroup::onEnterDisabled, 
        this, _1, _2));

    /* Do similar for reset as it is done for disabled -> first send all 
     * chans and than call this channel's handler
     *
     * order of registration IS IMPORTAINT!
     */
    registerOnEnterStateHandler(nds::CHANNEL_STATE_RESETTING,
            boost::bind(&sis8300llrfChannelGroup::resetChannels, this));

    registerOnEnterStateHandler(nds::CHANNEL_STATE_RESETTING,
            boost::bind(&sis8300llrfChannelGroup::onEnterReset, this));

    _isEnabled    = 1;
    _UpdateReason = 0x0;
    _Initialized  = 0;
}

/**
 * @brief llrfChannelGroup destructor
 *
 */
sis8300llrfChannelGroup::~sis8300llrfChannelGroup() {
}

/**
 *  * @brief Registers handlers for interfacing with records. For more 
 *   * information, refer to NDS documentation.
 *    */
ndsStatus sis8300llrfChannelGroup::registerHandlers(
                nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_INT32(
        sis8300llrfChannelGroup::PV_REASON_CH_DATA_READY,
        NULL,
        NULL,
        &_interruptIdChannelDataReady);

    return nds::ChannelGroup::registerHandlers(pvContainers);
}



/**
 * @brief Calls initialize on all the channels
 *
 * @return  ndsError    if there is no device ipened, of device is in 
 *                      OFF, if any of the channels fail to initialize
 * @return ndsSuccess   Successful initialization of all channels
 *
 * This is used when channel group needs initial information from the
 * hw, so it cannot be done at IOC_INIT, because the device is not
 * yet open
 * The function calls initialize on all channels
 */
ndsStatus sis8300llrfChannelGroup::initialize() {
    NDS_TRC("%s", __func__);
    ndsStatus status, statusCombined;


    statusCombined = status = ndsSuccess;

    markAllParametersChanged();

    /* Lock asyn port. All channels are on the same port so only one 
     * should be locked */
    (dynamic_cast<nds::Channel *>(_nodes.begin()->second))->lock();

    for (ChannelContainer::iterator iter = _nodes.begin(); 
                    iter != _nodes.end(); ++iter) {
        status = 
            (dynamic_cast<sis8300llrfChannel*>(iter->second))->initialize();
        if (status != ndsSuccess) {
            statusCombined = ndsError;
        }
    }
    /* unlock the channels port */
    (dynamic_cast<nds::Channel *>(_nodes.begin()->second))->unlock();

    NDS_INF("Initialization of channels from %s %s.", 
        getName().c_str(), (status == ndsSuccess) ? "OK" : "FAIL");

    status = commitParameters();
    if (status != ndsSuccess) {
        return ndsError;
    }

    _Initialized = 1;

    return statusCombined;
}

/**
 * @brief State handler for entering PROCESSING state
 *
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Reset the changes paramter, and start tracking them from scratch
 */
ndsStatus sis8300llrfChannelGroup::onEnterProcessing(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    _UpdateReason = 0x0;

    return ndsSuccess;
}

/**
 * @brief handler for entering PROCESSING state
 *
 * @retval ndsSuccess Successful transition.
 *
 * When the group enters disabled state, new parameter values get 
 * written to the controller. This handler will always get called after 
 * the parent onEnterDisabled handler, that switches the channels 
 * off -> channels will go to disabled first.
 *
 * Write any new values to hw and check if parameters have changed on 
 * any of the channels
 */
ndsStatus sis8300llrfChannelGroup::onEnterDisabled(
                nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    if (from == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsSuccess;
    }
    else if (from == nds::CHANNEL_STATE_RESETTING) {
        disableChannels();
    }

    /* Trigger channel data ready to allow processing of channel data
     * before channels go to processing again */
    doCallbacksInt32(0, _interruptIdChannelDataReady);

    return commitParameters();
}

/**
 * @brief State change handler for enetering RESETTING state. IF
 *        #_AutoReset is enabled it will auto transition to disabled
 *
 * @return ndsError    resetFailed
 * @return ndsSuccess  reset OK
 */
ndsStatus sis8300llrfChannelGroup::onEnterReset() {
    ndsStatus status;

    status = readParameters();
    if (status != ndsSuccess) {
        return ndsError;
    }

    status = markAllParametersChanged();
    if (status != ndsSuccess) {
        return status;
    }

    return ndsSuccess;
}

/**
 * @brief Request RESET on all channels in the CG
 *
 * The RESTTING state cannot be directly accessed from anywhere. So
 * sometimes we must ask nicely
 */
ndsStatus sis8300llrfChannelGroup::resetChannels()
{
    (dynamic_cast<nds::Channel *>(_nodes.begin()->second))->lock();

    for(ChannelContainer::iterator itr=_nodes.begin(); 
                itr!=_nodes.end(); ++itr ) {
        //itr->second->getCurrentStateObj()->requestState(itr->second, nds::CHANNEL_STATE_RESETTING);
        itr->second->getCurrentStateObj()->setMachineState(
                            itr->second, nds::CHANNEL_STATE_RESETTING);
    }

    (dynamic_cast<nds::Channel *>(_nodes.begin()->second))->unlock();

    return ndsSuccess;
}

/**
 * @brief Reqest DISABLE on all channels in the CG
 *
 * When channel is in RESETTING, DESABLED state musty be requested - the 
 * default ChannelGroup handler to switchChannelsOff will not work
 */
ndsStatus sis8300llrfChannelGroup::disableChannels()
{
    (dynamic_cast<nds::Channel *>(_nodes.begin()->second))->lock();

    for(ChannelContainer::iterator itr=_nodes.begin(); 
                itr!=_nodes.end(); ++itr ) {
        //itr->second->getCurrentStateObj()->requestState(itr->second, nds::CHANNEL_STATE_DISABLED);
        itr->second->getCurrentStateObj()->setMachineState(
                            itr->second, nds::CHANNEL_STATE_DISABLED);
    }

    (dynamic_cast<nds::Channel *>(_nodes.begin()->second))->unlock();
    return ndsSuccess;
}

/**
 * @brief write new values to the controller
 *
 * @return ndsSuccess always
 *
 * Write new values to the controller. Typically a few
 * Conditions are checked such as id the device is in INIT or ON and if
 * the corresponding CG is in disabled.
 * This is usually done after a new paramter value is set (trough a 
 * setter) or when the CH enters the DISABLED state.
 * The function checks is any of the parmetrs have changed at all before
 * writing to hw and calling callbacks. It can be used in combination
 * with @see #markAllPArmatersChanged to force a rewrite of all the 
 * values to the hw.
 *
 * By default the onEnterDisabled handler calls this function
 *
 */
ndsStatus sis8300llrfChannelGroup::commitParameters() {
    
    if (!_Initialized ||
        getCurrentState() != nds::CHANNEL_STATE_DISABLED ||
        (_device->getCurrentState() != nds::DEVICE_STATE_INIT &&
        _device->getCurrentState() != nds::DEVICE_STATE_ON)) {
        NDS_DBG("%s not committing parameters.", getName().c_str());
        
        return ndsSuccess;
    }
    
    return writeToHardware();
}

ndsStatus sis8300llrfChannelGroup::forceCommitParameters() {
    NDS_TRC("%s", __func__);
    ndsStatus status, statusCombined;

    statusCombined = status = ndsSuccess;
    
    if (!_Initialized ||
        (_device->getCurrentState() != nds::DEVICE_STATE_INIT &&
        _device->getCurrentState() != nds::DEVICE_STATE_ON)) {
        NDS_DBG("%s not committing parameters.", getName().c_str());
        
        return ndsSuccess;
    }
    
    /* Lock asyn port. All channels are on the same port so only one 
     * should be locked */
    (dynamic_cast<nds::Channel *>(_nodes.begin()->second))->lock();

    for (ChannelContainer::iterator iter = _nodes.begin(); 
                    iter != _nodes.end(); ++iter) {
        status = 
            (dynamic_cast<sis8300llrfChannel*>(iter->second))
                ->forceCommitParameters();
        if (status != ndsSuccess) {
            statusCombined = ndsError;
        }
    }
    /* unlock the channels port */
    (dynamic_cast<nds::Channel *>(_nodes.begin()->second))->unlock();

    status = writeToHardware();
    if (status != ndsSuccess) {
        statusCombined = ndsError;
    }
    
    NDS_INF("Force commit parameters from %s %s.", getName().c_str(), 
        (statusCombined == ndsSuccess) ? "OK" : "FAIL");

    return statusCombined;
}

ndsStatus sis8300llrfChannelGroup::writeToHardware() {
    return ndsSuccess;
}

/**
 * @brief Read all the paramters from the controller
 *
 * This is typically done when the CG leaves processing after
 * every PULSE_DONE interrupt from the cotroller. This will
 * read all the wwaveforms and values taht change on
 * pulse-2-pulse basis. It will not read settings such as paramters,
 * since they are user input
 *
 * Deriving classes are meant to override this to read parameters
 * that they are responsible for
 */
ndsStatus sis8300llrfChannelGroup::readParameters() {
    return ndsSuccess;
}

/**
 * @brief Mark all parameters as changed to force rewrite to the device.
 * 
 * @return ndsSuccess Always
 */
ndsStatus sis8300llrfChannelGroup::markAllParametersChanged() {

    for(ChannelContainer::iterator iter = _nodes.begin(); 
                iter != _nodes.end(); ++iter) {
        dynamic_cast<sis8300llrfChannel *>(iter->second)
                ->markAllParametersChanged();
    }

    return ndsSuccess;
}

/**
 * @return device user context
 */
sis8300drv_usr* sis8300llrfChannelGroup::getDeviceUser() {
    return _DeviceUser;
}

/**
 * @brief Check if paramters for any of the channels in the cg have 
 *        changed
 *
 * @return The current update reason 
 * 
 * Update reason is used to inform the controller if it needs to
 * load new values from shadow registers or relaod the control tables.
 * Every channel group holds the update reason for the channels it
 * contains. Channels report their changes to the channel group and
 * than the device ( @see #sis8300llrfDevice ) queries the channel group 
 * for the update reason.
 */
unsigned sis8300llrfChannelGroup::getUpdateReason() {
    return _UpdateReason;
}

/**
 * @brief Set a specific update reason
 */
void sis8300llrfChannelGroup::setUpdateReason(unsigned reason) {
    _UpdateReason |= reason;
}

/**
 * @brief Get samples count
 *
 * Override parent to disable getting the wrong sampes count
 * at IOC INIT
 */
ndsStatus sis8300llrfChannelGroup::getSamplesCount(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = _SamplesCount;

    return ndsSuccess;
}

/**
 * @brief Enable or disable channel group.
 *
 * @param [in] pasynUser Asyn user struct.
 * @param [in] value      Disable (0) or enable (non-0) the channel.
 *
 * @retval ndsError This is never allowed
 */
ndsStatus sis8300llrfChannelGroup::setEnabled(
                asynUser* pasynUser, epicsInt32 value) {

    if (pasynUser) {
        NDS_ERR( "Set enabled is not allowed externally for CG.");
        return ndsError;
    }

    return nds::ChannelGroup::setEnabled(pasynUser, value);
}

/**
 * @brief Override from parent, because action is unsupported
 * 
 * State Transitions of the LLRF specific channels are handled 
 * by the device ( @see #sis8300llrfDevice ) and can not be
 * manyally requested by user.
 */
ndsStatus sis8300llrfChannelGroup::handleOnMsg(
                asynUser *pasynUser, const nds::Message& value) {
    NDS_ERR("LLRF channel cannot be started manually.");
    return ndsError;
}
/**
 * @brief Override from parent, because action is unsupported
 * 
 * State Transitions of the LLRF specific channel groups are handled 
 * by the device ( @see #sis8300llrfDevice ) and can not be
 * manyally requested by user.
 */
ndsStatus sis8300llrfChannelGroup::handleOffMsg(
                asynUser *pasynUser, const nds::Message& value) {
    NDS_ERR("LLRF channel cannot be stopped manually.");
    return ndsError;
}
/**
 * @brief Override from parent, because action is unsupported
 * 
 * State Transitions of the LLRF specific channel groups are handled 
 * by the device ( @see #sis8300llrfDevice ) and can not be
 * manyally requested by user.
 */
ndsStatus sis8300llrfChannelGroup::handleResetMsg(
                asynUser *pasynUser, const nds::Message& value) {
    NDS_ERR("LLRF channel cannot be reseted manually.");
    return ndsError;
}
/**
 * @brief Override from parent, because action is unsupported
 * 
 * State Transitions of the LLRF specific channel groups are handled 
 * by the device ( @see #sis8300llrfDevice ) and can not be
 * manyally requested by user.
 */
ndsStatus sis8300llrfChannelGroup::handleStartMsg(
                asynUser *pasynUser, const nds::Message& value) {
    NDS_ERR("LLRF channel cannot be stopped manually.");
    return ndsError;
}
/**
 * @brief Override from parent, because action is unsupported
 * 
 * State Transitions of the LLRF specific channel groups are handled 
 * by the device ( @see #sis8300llrfDevice ) and can not be
 * manyally requested by user.
 */
ndsStatus sis8300llrfChannelGroup::handleStopMsg(
                asynUser *pasynUser, const nds::Message& value) {
    NDS_ERR("LLRF channel cannot be reseted manually.");
    return ndsError;
}

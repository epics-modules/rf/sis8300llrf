/*
 * m-epics-sis8300llrf
 * Copyright (C) 2022 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * @file sis8300llrfRippleFilterChannel.cpp
 * @brief Implementation of sis8300llrf Modulator ripple filter channel in NDS
 * @author andreas.persson@ess.eu
 * @date 22.8.2022
 *
 * This class holds all the settings related to the modulator ripple filter that
 * are available on the device. It is more or less a collection of setters and getters,
 * since this unit has no readouts that have to be made on pulse-to-pulse basis.
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfControllerChannelGroup.h"
#include "sis8300llrfRippleFilterChannel.h"

#include <cmath>

std::string sis8300llrfRippleFilterChannel::
                    PV_REASON_CONST_A3   = "RipFilConstA3";
std::string sis8300llrfRippleFilterChannel::
                    PV_REASON_CONST_A6   = "RipFilConstA6";
std::string sis8300llrfRippleFilterChannel::
                    PV_REASON_CONST_B1   = "RipFilConstB1";
std::string sis8300llrfRippleFilterChannel::
                    PV_REASON_CONST_B2   = "RipFilConstB2";
std::string sis8300llrfRippleFilterChannel::
                    PV_REASON_CONST_B3   = "RipFilConstB3";
std::string sis8300llrfRippleFilterChannel::
                    PV_REASON_CONST_B4   = "RipFilConstB4";
std::string sis8300llrfRippleFilterChannel::
                    PV_REASON_CONST_B5   = "RipFilConstB5";
std::string sis8300llrfRippleFilterChannel::
                    PV_REASON_CONST_B6   = "RipFilConstB6";
std::string sis8300llrfRippleFilterChannel::
                    PV_REASON_FREQ_ZERO  = "RipFilFreqZero";
std::string sis8300llrfRippleFilterChannel::
                    PV_REASON_DAMP_ZERO  = "RipFilDampZero";
std::string sis8300llrfRippleFilterChannel::
                    PV_REASON_FREQ_POLE  = "RipFilFreqPole";
std::string sis8300llrfRippleFilterChannel::
                    PV_REASON_DAMP_POLE  = "RipFilDampPole";
std::string sis8300llrfRippleFilterChannel::
                    PV_REASON_EN         = "RipFilEn";

/**
 * @brief sis8300llrfRippleFilterChannel constructor
 */
sis8300llrfRippleFilterChannel::sis8300llrfRippleFilterChannel(
    epicsFloat64 FSampling, unsigned NearIQN) :
        sis8300llrfChannel(
            SIS8300LLRFDRV_RIPPLE_FILTER_PARAM_NUM,
            SIS8300LLRFDRV_RIPPLE_FILTER_PARAM_INT_FIRST) {

    _ZeroFrequency = 0;
    _ZeroDamping = 0;
    _PoleFrequency = 0;
    _PoleDamping = 0;
    _FSampling = FSampling;
    _NearIQN = NearIQN;

    sprintf(_ChanStringIdentifier, "Ripple filter");
}


/**
 * @brief sis8300llrfRippleFilterChannel destructor
 */
sis8300llrfRippleFilterChannel::~sis8300llrfRippleFilterChannel() {}

/**
 * @see #sis8300llrfChannel::readParameter
 */
inline int sis8300llrfRippleFilterChannel::readParameter(
                int paramIdx, double *paramVal) {
    return sis8300llrfdrv_get_ripple_filter_param(_DeviceUser,
            (sis8300llrfdrv_ripple_filter_param) paramIdx, paramVal);
}

/**
 * @see #sis8300llrfChannel::writeParameter
 */
inline int sis8300llrfRippleFilterChannel::writeParameter(
                int paramIdx, double *paramErr) {
    return sis8300llrfdrv_set_ripple_filter_param(_DeviceUser,
                (sis8300llrfdrv_ripple_filter_param) paramIdx,
                _ParamVals[paramIdx], paramErr);
}

/**
 * @brief Registers handlers for interfacing with records. For more
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfRippleFilterChannel::registerHandlers(
                nds::PVContainers* pvContainers) {

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRippleFilterChannel::PV_REASON_CONST_A3,
            &sis8300llrfRippleFilterChannel::setFloat64,
            &sis8300llrfRippleFilterChannel::getConstantA3,
            &_interruptIds[ripple_fil_const_a3]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRippleFilterChannel::PV_REASON_CONST_A6,
            &sis8300llrfRippleFilterChannel::setFloat64,
            &sis8300llrfRippleFilterChannel::getConstantA6,
            &_interruptIds[ripple_fil_const_a6]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRippleFilterChannel::PV_REASON_CONST_B1,
            &sis8300llrfRippleFilterChannel::setFloat64,
            &sis8300llrfRippleFilterChannel::getConstantB1,
            &_interruptIds[ripple_fil_const_b1]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRippleFilterChannel::PV_REASON_CONST_B2,
            &sis8300llrfRippleFilterChannel::setFloat64,
            &sis8300llrfRippleFilterChannel::getConstantB2,
            &_interruptIds[ripple_fil_const_b2]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRippleFilterChannel::PV_REASON_CONST_B3,
            &sis8300llrfRippleFilterChannel::setFloat64,
            &sis8300llrfRippleFilterChannel::getConstantB3,
            &_interruptIds[ripple_fil_const_b3]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRippleFilterChannel::PV_REASON_CONST_B4,
            &sis8300llrfRippleFilterChannel::setFloat64,
            &sis8300llrfRippleFilterChannel::getConstantB4,
            &_interruptIds[ripple_fil_const_b4]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRippleFilterChannel::PV_REASON_CONST_B5,
            &sis8300llrfRippleFilterChannel::setFloat64,
            &sis8300llrfRippleFilterChannel::getConstantB5,
            &_interruptIds[ripple_fil_const_b5]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRippleFilterChannel::PV_REASON_CONST_B6,
            &sis8300llrfRippleFilterChannel::setFloat64,
            &sis8300llrfRippleFilterChannel::getConstantB6,
            &_interruptIds[ripple_fil_const_b6]);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRippleFilterChannel::PV_REASON_FREQ_ZERO,
            &sis8300llrfRippleFilterChannel::setZeroFrequency,
            &sis8300llrfRippleFilterChannel::getZeroFrequency,
            &_interruptIdFreqZero);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRippleFilterChannel::PV_REASON_DAMP_ZERO,
            &sis8300llrfRippleFilterChannel::setZeroDamping,
            &sis8300llrfRippleFilterChannel::getZeroDamping,
            &_interruptIdDampZero);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRippleFilterChannel::PV_REASON_FREQ_POLE,
            &sis8300llrfRippleFilterChannel::setPoleFrequency,
            &sis8300llrfRippleFilterChannel::getPoleFrequency,
            &_interruptIdFreqPole);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfRippleFilterChannel::PV_REASON_DAMP_POLE,
            &sis8300llrfRippleFilterChannel::setPoleDamping,
            &sis8300llrfRippleFilterChannel::getPoleDamping,
            &_interruptIdDampPole);

    NDS_PV_REGISTER_INT32(
           sis8300llrfRippleFilterChannel::PV_REASON_EN,
           &sis8300llrfRippleFilterChannel::setEnable,
           &sis8300llrfRippleFilterChannel::getEnable,
           &_interruptIds[ripple_fil_en]);

    return sis8300llrfChannel::registerHandlers(pvContainers);
}

/* === GETTERS AND SETTERS FOR CONTROLLER SETTINGS === */
/* ======= RELATED TO MODULATOR RIPPLE FILTER ======== */
/*
 * The getters are meant to be used by records that will keep track of
 * setup used in the pulse that just passed. These records should be
 * set to process at IO interrupt, because all the callbacks will get
 * called when receiving the PULSE_DONE interrupt.
 */

/**
 * @brief Get ripple filter constant A3
 *
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the A3 constant value on success
 *
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 *
 * This will return the value of the local setting (this class's private
 * variable)
 */
ndsStatus sis8300llrfRippleFilterChannel::getConstantA3(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[ripple_fil_const_a3];

    return ndsSuccess;
}
/**
 * @brief Get ripple filter constant A6
 *
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the A6 constant value on success
 *
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 *
 * This will return the value of the local setting (this class's private
 * variable)
 */
ndsStatus sis8300llrfRippleFilterChannel::getConstantA6(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[ripple_fil_const_a6];

    return ndsSuccess;
}
/**
 * @brief Get ripple filter constant B1
 *
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the B1 constant value on success
 *
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 *
 * This will return the value of the local setting (this class's private
 * variable)
 */
ndsStatus sis8300llrfRippleFilterChannel::getConstantB1(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[ripple_fil_const_b1];

    return ndsSuccess;
}
/**
 * @brief Get ripple filter constant B2
 *
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the B2 constant value on success
 *
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 *
 * This will return the value of the local setting (this class's private
 * variable)
 */
ndsStatus sis8300llrfRippleFilterChannel::getConstantB2(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[ripple_fil_const_b2];

    return ndsSuccess;
}
/**
 * @brief Get ripple filter constant B3
 *
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the B3 constant value on success
 *
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 *
 * This will return the value of the local setting (this class's private
 * variable)
 */
ndsStatus sis8300llrfRippleFilterChannel::getConstantB3(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[ripple_fil_const_b3];

    return ndsSuccess;
}
/**
 * @brief Get ripple filter constant B4
 *
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the B4 constant value on success
 *
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 *
 * This will return the value of the local setting (this class's private
 * variable)
 */
ndsStatus sis8300llrfRippleFilterChannel::getConstantB4(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[ripple_fil_const_b4];

    return ndsSuccess;
}
/**
 * @brief Get ripple filter constant B5
 *
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the B5 constant value on success
 *
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 *
 * This will return the value of the local setting (this class's private
 * variable)
 */
ndsStatus sis8300llrfRippleFilterChannel::getConstantB5(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[ripple_fil_const_b5];

    return ndsSuccess;
}
/**
 * @brief Get ripple filter constant B6
 *
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the B6 constant value on success
 *
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 *
 * This will return the value of the local setting (this class's private
 * variable)
 */
ndsStatus sis8300llrfRippleFilterChannel::getConstantB6(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsFloat64) _ParamVals[ripple_fil_const_b6];

    return ndsSuccess;
}
/**
 * @brief Enable ripple filter
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Set 1 to enable and 0 to disable
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control
 * loop is running, this will get written down when the pulse is
 * finished, if the device is in INIT state it will be written down
 * immediately.
 */
ndsStatus sis8300llrfRippleFilterChannel::setEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    _ParamVals[ripple_fil_en] = (double) value;

    _ParamChanges[ripple_fil_en] = 1;
    return commitParameters();
}
/**
 * @brief Check if ripple filter is enabled
 *
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will be 1 if enabled and 0 if disabled on success
 *
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 *
 * This will return the value of the local setting (this class's private
 * variable)
 */
ndsStatus sis8300llrfRippleFilterChannel::getEnable(
                asynUser *pasynUser, epicsInt32 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = (epicsInt32) _ParamVals[ripple_fil_en];

    return ndsSuccess;
}
/**
 * @brief Set ripple filter zero frequency
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Zero frequency value
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control
 * loop is running, this will get written down when the pulse is
 * finished, if the device is in INIT state it will be written down
 * immediately.
 */
ndsStatus sis8300llrfRippleFilterChannel::setZeroFrequency(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ZeroFrequency = value;
    _ZeroFrequencyChanged = 1;

    return commitParameters();
}
/**
 * @brief Get ripple filter zero frequency
 *
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the zero frequency value on success
 *
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 *
 * This will return the value of the local setting (this class's private
 * variable)
 */
ndsStatus sis8300llrfRippleFilterChannel::getZeroFrequency(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = _ZeroFrequency;

    return ndsSuccess;
}
/**
 * @brief Set ripple filter zero damping
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Zero damping value
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control
 * loop is running, this will get written down when the pulse is
 * finished, if the device is in INIT state it will be written down
 * immediately.
 */
ndsStatus sis8300llrfRippleFilterChannel::setZeroDamping(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _ZeroDamping = value;
    _ZeroDampingChanged = 1;

    return commitParameters();
}
/**
 * @brief Get ripple filter zero damping
 *
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the zero damping value on success
 *
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 *
 * This will return the value of the local setting (this class's private
 * variable)
 */
ndsStatus sis8300llrfRippleFilterChannel::getZeroDamping(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = _ZeroDamping;

    return ndsSuccess;
}
/**
 * @brief Set ripple filter pole frequency
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Pole frequency value
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control
 * loop is running, this will get written down when the pulse is
 * finished, if the device is in INIT state it will be written down
 * immediately.
 */
ndsStatus sis8300llrfRippleFilterChannel::setPoleFrequency(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _PoleFrequency = value;
    _PoleFrequencyChanged = 1;

    return commitParameters();
}
/**
 * @brief Get ripple filter pole frequency
 *
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the pole frequency value on success
 *
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 *
 * This will return the value of the local setting (this class's private
 * variable)
 */
ndsStatus sis8300llrfRippleFilterChannel::getPoleFrequency(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = _PoleFrequency;

    return ndsSuccess;
}
/**
 * @brief Set ripple filter pole damping
 *
 * @param [in] pasynUser    Asyn user context struct
 * @param [in] value        Zero damping value
 *
 * @return @see #commitParameters
 *
 * Set a private variable that stores the data locally. If the control
 * loop is running, this will get written down when the pulse is
 * finished, if the device is in INIT state it will be written down
 * immediately.
 */
ndsStatus sis8300llrfRippleFilterChannel::setPoleDamping(
                asynUser *pasynUser, epicsFloat64 value) {
    NDS_TRC("%s", __func__);

    _PoleDamping = value;
    _PoleDampingChanged = 1;

    return commitParameters();
}
/**
 * @brief Get ripple filter pole damping
 *
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the pole damping value on success
 *
 * @return ndsError    Prevent wrong setting during IOC INIT calls
 * @return ndsSuccess  Always when not in IOC INIT state
 *
 * This will return the value of the local setting (this class's private
 * variable)
 */
ndsStatus sis8300llrfRippleFilterChannel::getPoleDamping(
                asynUser *pasynUser, epicsFloat64 *value) {

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    *value = _PoleDamping;

    return ndsSuccess;
}
/**
 * @brief Overides commitParameters to manage extra filter parameters
 *
 * @return ndsError    If update or commitParameters return error
 * @return ndsSuccess
 *
 * This method is necessary since frequency and damping are handled differently
 * from other PVs. These PVs don't have registers to write, instead they
 * will set constants A and B.
 */
ndsStatus sis8300llrfRippleFilterChannel::commitParameters() {
    ndsStatus status;

    NDS_TRC("%s", __func__);

    sis8300llrfControllerChannelGroup *cg =
        dynamic_cast<sis8300llrfControllerChannelGroup*> (getChannelGroup());

    if ( !_Initialized ||
         cg->getCurrentState() != nds::CHANNEL_STATE_DISABLED  ||
         (_device->getCurrentState() != nds::DEVICE_STATE_INIT &&
          _device->getCurrentState() != nds::DEVICE_STATE_ON)) {

        NDS_DBG("%s not committing parameters.", _ChanStringIdentifier);
        return ndsSuccess;
    }

    // If frequency or damping has changed
    if (_ZeroFrequencyChanged || _ZeroDampingChanged ||
        _PoleFrequencyChanged || _PoleDampingChanged)
    {
        status = updateFilter();
        if (status == ndsSuccess) {
            if (_ZeroFrequencyChanged) {
                doCallbacksFloat64(_ZeroFrequency, _interruptIdFreqZero);
                _ZeroFrequencyChanged = 0;
            }
            if (_ZeroDampingChanged) {
                doCallbacksFloat64(_ZeroDamping, _interruptIdDampZero);
                _ZeroDampingChanged = 0;
            }
            if (_PoleFrequencyChanged) {
                doCallbacksFloat64(_PoleFrequency, _interruptIdFreqPole);
                _PoleFrequencyChanged = 0;
            }
            if (_PoleDampingChanged) {
                doCallbacksFloat64(_PoleDamping, _interruptIdDampPole);
                _PoleDampingChanged = 0;
            }
        }
        return status;
    }

    return sis8300llrfChannel::commitParameters();
}
/**
 * @brief Update the ripple filter constants
 *
 * This method updates the coefficients of polynomials A and B.
 * It is called when frequency or damping settings change.
 */
ndsStatus sis8300llrfRippleFilterChannel::updateFilter() {
    double Ts;
    double wp, wz;
    double rp, rz;
    double f1, f2;
    double g1, g2;
    double d1, d2, d3, d4;
    double w1, w2;

    Ts = 1 / (_FSampling / _NearIQN);

    wp = 2 * M_PI * _PoleFrequency;
    wz = 2 * M_PI * _ZeroFrequency;

    rp = exp(-_PoleDamping * wp * Ts);
    rz = exp(-_ZeroDamping * wz * Ts);

    f1 = -2 * rp * cos(wp * Ts * sqrt(1 - _PoleDamping*_PoleDamping));
    f2 = rp * rp;

    g1 = -2 * rz * cos(wz * Ts * sqrt(1 - _ZeroDamping*_ZeroDamping));
    g2 = rz * rz;

    // Scattered look-ahead (SLA) transformation
    // See https://confluence.esss.lu.se/x/TEUXEg
    //
    // [  1  0  0  0  0 ]   [ 1]   [1]
    // [ f1  1  0  0  0 ]   [d1]   [0]
    // [ f2 f1  1  0  0 ] * [d2] = [0]
    // [  0  0 f2 f1  1 ]   [d3]   [0]
    // [  0  0  0 f2 f1 ]   [d4]   [0]

    // Solve for D
    d1 = -f1;
    d2 = -f2 - f1*d1;
    d3 = -f2*d2 / (f1 - f2/f1);
    d4 = -f2*d3 / f1;

    //                            [ 1]
    // [w1]   [ 0 f2 f1  1  0 ]   [d1]
    // [w2] = [ 0  0  0  0 f2 ] * [d2]
    //                            [d3]
    //                            [d4]
    w1 = f2*d1 + f1*d2 + d3;
    w2 = f2*d4;

    // Transfer function
    //
    //  B    (1 + d1*z^-1 + d2*z^-2 + d3*z^-3 + d4*z^-4) * (1 + g1*z^-1 + g2*z^-2)
    // --- = ---------------------------------------------------------------------
    //  A                         1 - w1*z^-3 + w2*z^-6

    // Numerator after polynomial multiplication
    _ParamVals[ripple_fil_const_b1] = checkRange(g1 + d1);
    _ParamVals[ripple_fil_const_b2] = checkRange(d1*g1 + g2 + d2);
    _ParamVals[ripple_fil_const_b3] = checkRange(d2*g1 + d1*g2 + d3);
    _ParamVals[ripple_fil_const_b4] = checkRange(d3*g1 + d2*g2 + d4);
    _ParamVals[ripple_fil_const_b5] = checkRange(d4*g1 + d3*g2);
    _ParamVals[ripple_fil_const_b6] = checkRange(d4*g2);

    // Denominator. Why is w2 negated?
    // This is according to spec. Possibly a firmware quirk.
    _ParamVals[ripple_fil_const_a3] = checkRange(-w1);
    _ParamVals[ripple_fil_const_a6] = checkRange(-w2);

    _ParamChanges[ripple_fil_const_b1] = 1;
    _ParamChanges[ripple_fil_const_b2] = 1;
    _ParamChanges[ripple_fil_const_b3] = 1;
    _ParamChanges[ripple_fil_const_b4] = 1;
    _ParamChanges[ripple_fil_const_b5] = 1;
    _ParamChanges[ripple_fil_const_b6] = 1;
    _ParamChanges[ripple_fil_const_a3] = 1;
    _ParamChanges[ripple_fil_const_a6] = 1;

    return sis8300llrfChannel::commitParameters();
}
/**
 * @brief Check and fix numeric range of filter coefficients
 *
 * @param [in]  value   value to check
 *
 * @return value in the range of signed Q(2,22)
 *
 * If the input value is outside the register range, the
 * maximum or minimum value is returned.
 */
double sis8300llrfRippleFilterChannel::checkRange(double value) {
    const double q2_22_min = -2;
    const double q2_22_max = 2 - pow(2, -21);

    if (value < q2_22_min)
        return q2_22_min;

    if (value > q2_22_max)
        return q2_22_max;

    return value;
}

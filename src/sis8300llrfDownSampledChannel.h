/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfDownSampledChannel.h
 * @brief Header file defining the LLRF DownSampled channel class that handles
 * downsampled values and their settings
 * @author gabriel.fedel@ess.eu
 * @date 31.3.2020
 */

#ifndef _sis8300llrfDownSampledChannel_h
#define _sis8300llrfDownSampledChannel_h

#include "sis8300llrfAuxChannel.h"
#include "sis8300llrfAIChannel.h"

#define SIS8300LLRF_MAXCALIBCOEF 10
/**
 * @brief sis8300 LLRF specific nds::ADIOChannel class that supports
 *  DownSampled channel
 */
class sis8300llrfDownSampledChannel: public sis8300llrfAuxChannel {
public:
    sis8300llrfDownSampledChannel(epicsFloat64 FSampling, epicsInt32 nearIqN, sis8300llrfAIChannel * AIChan);
    virtual ~sis8300llrfDownSampledChannel();

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    virtual ndsStatus setDAQFormat(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getDAQFormat(
                        asynUser *pasynUser, epicsInt32 *value);
protected:
    epicsInt16 * _rawDataI16;

    epicsFloat64       *_IMagData; /**< waveform with I or Magnitude*/
    epicsFloat32       *_IMagDataW; /**< waveform with I or Magnitude for cavity PU*/
    epicsFloat64       *_QAngData; /**< waveform with Q or Angle */

    static std::string PV_REASON_DWNSMPL_DAQ_FMT;

    virtual ndsStatus onLeaveProcessing(
                nds::ChannelStates from, nds::ChannelStates to);
    sis8300llrfAIChannel * _AIChan;
};

#endif /* _sis8300llrfDownSampledChannel_h */

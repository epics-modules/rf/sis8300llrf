/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfSPWithCavityChannel.cpp
 * @brief Implementation of Calibration for PI Channel.
 * @author gabriel.fedel@ess.eu
 * @date 5.5.2020
 *
 * This class is a specialization from PIMagAngChannel focused for when the 
 * cavity channel is configured. This class allows to operate the SP values
 * using the gradient calibration.
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfChannel.h"
#include "sis8300llrfPIChannel.h"
#include "sis8300llrfControlTableChannel.h"

#include "sis8300llrfSPWithCavityChannel.h"
#include <math.h>

std::string sis8300llrfSPWithCavityChannel::
                    PV_REASON_CALIB_SQRT_FIT_EN          = "CalibSqrtFitEnable";

/**
 * @brief PI Calibration channel constructor.
 *
 * @param [in] PIChannels       Array of PI Channels to read and set I/Q
 * @param [in] SPorFF         Indicate if this channel is for SetPoint(0) or FeedForward(1)
 *
 */
sis8300llrfSPWithCavityChannel::sis8300llrfSPWithCavityChannel(sis8300llrfPIChannel * PIChannels[2], sis8300llrfControlTableChannel * CtrlTableCh, sis8300llrfCavityChannel * CavChannel, sis8300llrfCalibrationProvider * calibrationProvider): sis8300llrfPIMagAngChannel(PIChannels, 0, CtrlTableCh, calibrationProvider), _useSqrtFit(0){
    NDS_TRC("%s", __func__);

    m_CavChannel = CavChannel;
    calibratorSqrt    = new scalingSqrt();
}

/**************************************************************/

ndsStatus sis8300llrfSPWithCavityChannel::setSqrtFitEnable(
        asynUser *pasynUser, epicsInt32 value){
    _useSqrtFit = value;
    reloadCalibPVs();

    return ndsSuccess;
}

double sis8300llrfSPWithCavityChannel::calibrateEGU2Raw(double value) {
    if (_useSqrtFit) // use sqrt calibrator
        return calibratorSqrt->calibrate(value);
    else
        return calibrationProvider->calibrateEGU2Raw(value);


}

void sis8300llrfSPWithCavityChannel::reloadCalibPVs(){ 
    bool valid, enable;
    valid = calibrationProvider-> isCalibratorEGU2RawValid() && calibrationProvider-> isCalibratorRaw2EGUValid();
    enable = valid  && calibrationProvider-> isCalibratorEGU2RawEnabled() && calibrationProvider-> isCalibratorRaw2EGUEnabled();
    calibrationProvider->setCalibrationEGU2RawEnable(enable);
    calibrationProvider->setCalibrationRaw2EGUEnable(enable);

    doCallbacksInt32(enable, _interruptIdCalibEnable);
   
    if (valid) {
        scalingPoly* calibratorEGU2Raw = calibrationProvider->getCalibratorEGU2Raw();
        scalingPoly* calibratorRaw2EGU = calibrationProvider->getCalibratorRaw2EGU();

        if (calibratorRaw2EGU->getCoeffsSize() >= 3)
            // update coefficients on calibratorSqrt
            calibratorSqrt->setCoefficients(calibratorRaw2EGU->getCoeffs()); 
        if (_useSqrtFit){// use calibratorSqrt
            doCallbacksFloat64Array(calibratorSqrt->getFittedLine(), 
                    calibratorSqrt->getFittedLineSize(), 
                    _interruptIdCalibFittedLineEGU2Raw);
            doCallbacksFloat64Array(NULL, 0,  _interruptIdCalibResidEGU2Raw); //TODO
            doCallbacksFloat64Array(NULL, 0,  _interruptIdCalibCoefEGU2Raw); //TODO
            doCallbacksFloat64(0, _interruptIdCalibChisqEGU2Raw); //TODO
        } 
        else{ // use EGU2Raw calibrator
            doCallbacksFloat64Array(calibratorEGU2Raw->getFittedLine(), 
                       calibratorEGU2Raw->getFittedLineSize(), 
                       _interruptIdCalibFittedLineEGU2Raw);
            doCallbacksFloat64Array(calibratorEGU2Raw->getResiduals(), 
                    calibratorEGU2Raw->getResidualsSize(), 
                    _interruptIdCalibResidEGU2Raw);
            doCallbacksFloat64Array(calibratorEGU2Raw->getCoeffs(), 
                    calibratorEGU2Raw->getCoeffsSize(), 
                    _interruptIdCalibCoefEGU2Raw);
            doCallbacksFloat64(calibratorEGU2Raw->getChisq(), 
                    _interruptIdCalibChisqEGU2Raw);

        }

        doCallbacksFloat64Array(calibratorRaw2EGU->getFittedLine(), 
                calibratorRaw2EGU->getFittedLineSize(), 
                _interruptIdCalibFittedLineRaw2EGU);
        doCallbacksFloat64Array(calibratorRaw2EGU->getResiduals(), 
                calibratorRaw2EGU->getResidualsSize(), 
                _interruptIdCalibResidRaw2EGU);
        doCallbacksFloat64Array(calibratorRaw2EGU->getCoeffs(), 
                calibratorRaw2EGU->getCoeffsSize(), 
                _interruptIdCalibCoefRaw2EGU);
        doCallbacksFloat64(calibratorRaw2EGU->getChisq(), 
                _interruptIdCalibChisqRaw2EGU);
    }
    else{
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibFittedLineEGU2Raw);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibResidEGU2Raw);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibCoefEGU2Raw);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibFittedLineRaw2EGU);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibResidRaw2EGU);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibCoefRaw2EGU);
    }
}

ndsStatus sis8300llrfSPWithCavityChannel::updateMagAngRBV() {
    epicsFloat64 I, Q;

    //set point
    if (!_SPorFF){
        // read values on RBV
        if (_PIChannels[0]->getPIFixedSPVal(NULL, &I) != ndsSuccess)
            return ndsError;

        if (_PIChannels[1]->getPIFixedSPVal(NULL, &Q) != ndsSuccess)
            return ndsError;
    }else { //feed forward table
        // read values on RBV
        if (_PIChannels[0]->getPIFixedFFVal(NULL, &I) != ndsSuccess)
            return ndsError;

        if (_PIChannels[1]->getPIFixedFFVal(NULL, &Q) != ndsSuccess)
            return ndsError;
    }

    //convert IQ to Mag/Ang
    _MagRaw = sqrt(pow(I, 2) + pow(Q, 2));
    _AngRBV = atan2(Q, I);

    // if gradient calibration is enabled
    if (calibrationProvider->isCalibratorRaw2EGUEnabled() && m_CavChannel->isGradCalibrated()) {
        _MagRBV = m_CavChannel->calibrateGrad(calibrationProvider->calibrateRaw2EGU(_MagRaw));
       if (_MagRBV != _MagRBV) // Nan
           _MagRBV = 0;
    }
    else
        if (calibrationProvider->isCalibratorRaw2EGUEnabled())
            _MagRBV = calibrationProvider->calibrateRaw2EGU(_MagRaw);
        else
            _MagRBV = _MagRaw;

    doCallbacksFloat64(_MagRBV, _interruptIdMag);
    doCallbacksFloat64(_AngRBV, _interruptIdAng);
    doCallbacksFloat64(_MagRaw, _interruptIdMagRaw);

    return ndsSuccess;
}

//calibrate -> write IQ -> read IQ -> calibrate
ndsStatus sis8300llrfSPWithCavityChannel::writeToHardware() {
    if (_MagAngChanged){
        //calibration

        if (isCalibrationEnabled() && m_CavChannel->isGradCalibrated()) {
            _MagRaw = calibrateEGU2Raw(m_CavChannel->calibrateGradInv(_Mag));
        } else
            if (isCalibrationEnabled())
                _MagRaw = calibrateEGU2Raw(_Mag);
            else
                _MagRaw = _Mag;

        if (_MagRaw > SIS8300LLRF_MAG_MAX) 
            _MagRaw = SIS8300LLRF_MAG_MAX;
            
        if (_MagRaw < SIS8300LLRF_MAG_MIN)
            _MagRaw = SIS8300LLRF_MAG_MIN;

        // update IQ
        epicsFloat64 I, Q;

        //convert to IQ
        I = _MagRaw*cos(_Ang);
        Q = _MagRaw*sin(_Ang);

        if (I > SIS8300LLRF_I_MAX)
            I = SIS8300LLRF_I_MAX;
        if (I < SIS8300LLRF_I_MIN)
            I = SIS8300LLRF_I_MIN;
        if (Q > SIS8300LLRF_Q_MAX)
            Q =  SIS8300LLRF_Q_MAX;
        if (Q < SIS8300LLRF_Q_MIN)
            Q =  SIS8300LLRF_Q_MIN;

        if (!_SPorFF){ //Set Point
            //set IQ
            if ((_PIChannels[0]->setPIFixedSPVal(NULL, I) != ndsSuccess) || (_PIChannels[1]->setPIFixedSPVal(NULL, Q) != ndsSuccess)) {
                NDS_ERR("Invalid IQ value");
                return ndsError;
            }
        }
        else {//Feed Forward 
            //set IQ
            if ((_PIChannels[0]->setPIFixedFFVal(NULL, I) != ndsSuccess) || (_PIChannels[1]->setPIFixedFFVal(NULL, Q) != ndsSuccess)) {
                NDS_ERR("Invalid IQ value");
                return ndsError;
            }
        }
        // read values on RBV
        if (updateMagAngRBV() != ndsSuccess)
            return ndsError;
        
        _MagAngChanged = 0;
    }

    return ndsSuccess;
}

// Load EGU
ndsStatus sis8300llrfSPWithCavityChannel::setCalibEGU(
        asynUser *pasynUser, epicsFloat64 *value, size_t nelem){
    NDS_TRC("%s", __func__);

    calibratorSqrt->setX(value, nelem);

    return sis8300llrfPIMagAngChannel::setCalibEGU(pasynUser, value, nelem);
}

/****************************************************/
/******************** Tables methods ****************/
/****************************************************/



/*
 * @brief: Set Magnitude Table
 * */
ndsStatus sis8300llrfSPWithCavityChannel::setMagTable(
        asynUser *pasynUser, epicsFloat64 *value, size_t nelem){
    NDS_TRC("%s", __func__);

    if (_MagTable != NULL)
        delete[] _MagTable;

    _MagTable = new epicsFloat64[nelem];

    memcpy(_MagTable, value,  nelem * sizeof(epicsFloat64));
    _MagTableCount = nelem;


    //calibration
    if (isCalibrationEnabled() && m_CavChannel->isGradCalibrated()) {
        for (int i = 0; i < _MagTableCount; i++){
            _MagTable[i] = calibrateEGU2Raw(m_CavChannel->calibrateGradInv(_MagTable[i])); //TODO improve calling different methods depending on if use or not sqrtFit
            if (_MagTable[i] > SIS8300LLRF_MAG_MAX) 
                _MagTable[i] = SIS8300LLRF_MAG_MAX;
                
            if (_MagTable[i] < SIS8300LLRF_MAG_MIN)
                _MagTable[i] = SIS8300LLRF_MAG_MIN;
        }
    } else {
        if (isCalibrationEnabled()){
                for (int i = 0; i < _MagTableCount; i++){
                    _MagTable[i] = calibrateEGU2Raw(_MagTable[i]);//TODO improve calling different methods depending on if use or not sqrtFit
                    if (_MagTable[i] > SIS8300LLRF_MAG_MAX) 
                        _MagTable[i] = SIS8300LLRF_MAG_MAX;
                        
                    if (_MagTable[i] < SIS8300LLRF_MAG_MIN)
                        _MagTable[i] = SIS8300LLRF_MAG_MIN;
                }
            }
        else {
            for (int i = 0; i < _MagTableCount; i++){
                if (_MagTable[i] > SIS8300LLRF_MAG_MAX) 
                    _MagTable[i] = SIS8300LLRF_MAG_MAX;
                    
                if (_MagTable[i] < SIS8300LLRF_MAG_MIN)
                    _MagTable[i] = SIS8300LLRF_MAG_MIN;
            }
        }
    }

    doCallbacksFloat64Array(_MagTable, _MagTableCount, _interruptIdMagTableRaw);
    return convertTablesToIQ();
}

ndsStatus sis8300llrfSPWithCavityChannel::getMagTable(
                asynUser* pasynUser, epicsFloat64 *value, 
                size_t nelem, size_t *nIn) {
   if (loadMagAngTables() != ndsSuccess)
       return ndsError;

    NDS_DBG("After load Mag tables");

   if (nelem < (size_t) _MagTableCount)
       return ndsError;

    //recalibrate value
    // if gradient calibration is enabled
    if (calibrationProvider->isCalibratorRaw2EGUEnabled() && m_CavChannel->isGradCalibrated()) {
       for (int i = 0; i <  _MagTableCount; i++){
           value[i] = m_CavChannel->calibrateGrad(calibrationProvider->calibrateRaw2EGU(_MagTable[i]));
           if (value[i] != value[i]) // Nan
               value[i] = 0;
        }
    }
    else
        if (calibrationProvider->isCalibratorRaw2EGUEnabled()){
           for (int i = 0; i <  _MagTableCount; i++)
               value[i] = calibrationProvider->calibrateRaw2EGU(_MagTable[i]);
        } else 
            memcpy(value, _MagTable,  _MagTableCount * sizeof(epicsFloat64));

    NDS_DBG("After copy Mag tables");

   *nIn = _MagTableCount;
   return ndsSuccess;
}


ndsStatus sis8300llrfSPWithCavityChannel::registerHandlers(
                nds::PVContainers* pvContainers) {
    NDS_PV_REGISTER_INT32(
           sis8300llrfSPWithCavityChannel::PV_REASON_CALIB_SQRT_FIT_EN,
           &sis8300llrfSPWithCavityChannel::setSqrtFitEnable,
           &sis8300llrfSPWithCavityChannel::getInt32,
           &_interruptIdCalibSqrtFitEnable
     ); 

    return sis8300llrfPIMagAngChannel::registerHandlers(pvContainers);
}

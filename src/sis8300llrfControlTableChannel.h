/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfControlTableChannel.h
 * @brief Header file defining the LLRF control table (FF and SP) channel 
 * class.
 * @author urojec, ursa.rojec@cosylab.com
 * @date 23.5.2014
 */

#ifndef _sis8300llrfControlTableChannel_h
#define _sis8300llrfControlTableChannel_h

#include "ndsManager.h"
#include "ndsTaskManager.h"
#include "ndsThreadTask.h"

#include "sis8300llrfChannel.h"

/**
 * @brief sis8300 LLRF specific nds::ADIOChannel class that supports 
 * ControlTable setup
 *
 * Each control Table acts as an analog output channel. Channel number 
 * corresponds to pulse type.
 */
class sis8300llrfControlTableChannel: public sis8300llrfChannel {
public:
    sis8300llrfControlTableChannel(int maxTableSize);
    virtual ~sis8300llrfControlTableChannel();

    virtual ndsStatus initialize();

    virtual int tableSet();
    virtual ndsStatus markAllParametersChanged();

    virtual ndsStatus registerHandlers(nds::PVContainers* pvContainers);
    virtual ndsStatus setSamplesCount(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getSamplesCount(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setTableMode(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getTableMode(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setTableSpeed(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getTableSpeed(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setConstantEnable(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus getConstantEnable(
                        asynUser *pasynUser, epicsInt32 *value);
    virtual ndsStatus setWriteTable(
                        asynUser *pasynUser, epicsInt32 value);
    virtual ndsStatus setQTable(
                        asynUser* pasynUser, epicsFloat64 *value, 
                        size_t nelem);
    virtual ndsStatus getQTable(
                        asynUser* pasynUser, epicsFloat64 *value, 
                        size_t nelem, size_t *nIn);
    virtual ndsStatus setITable(
                        asynUser* pasynUser, epicsFloat64 *value, 
                        size_t nelem);
    virtual ndsStatus getITable(
                        asynUser* pasynUser, epicsFloat64 *value, 
                        size_t nelem, size_t *nIn);
    virtual ndsStatus getRawTable(
                        asynUser *pasynUser, epicsInt32 *value, 
                        size_t nelem, size_t *nIn);

protected:
    epicsFloat64 timemsec();
    sis8300llrfdrv_ctrl_table _CtrlTableType; /**< Table type, either SP 
                                                *  or FF */
    epicsInt32 *_RawTablePrel;      /**< Holds the preliminary table in hw format */
    epicsInt32 *_RawTableFinal;     /**< Holds the final table in hw format */
    epicsInt32 _TableMode;        /**< Can be hold last (normal) or 
                                      *  circular (special operation) */

    epicsInt32 _TableSpeed;        /**< 0-0xFF */
    epicsInt32 _ConstantEnable;        /**< 0-1 */
    epicsInt32 _SamplesCount;       /**< Table size for this pulse type */
    epicsInt32 _MaxSamplesCount;    /**< Maximum allowed number of table 
                                      *  elements. Equals to 
                                      *  _SamplesCount from Channel 
                                      * group */

    epicsFloat64 *_ITbl;            /**< Holds the preliminary table in hw format */
    epicsFloat64 *_QTbl;            /**< Holds the preliminary table in hw format */

    unsigned int  _ITblNelm; 
                          
    unsigned int  _QTblNelm; 

    int        _WriteTable;         /**< Parameter to trigger write
                                      * of tables */
    int        _SamplesCountChange; /** < Parameter Tracking Samples 
                                      * count changes */
    int        _TableModeChange;  /** < Parameter tracking Table 
                                      * mode changes */
    int        _TableSpeedChange;  /** < Parameter tracking Table 
                                      * speed changes */
    int        _ConstantEnableChange;    /** < Enable/Disable fixed value */
    int        _TableSet;           /**< Set to 1 the first time table 
                                      * is written - so that we know 
                                      * table is set up */

    unsigned int  _QNelm;                     /**< Number of elements in the Angle 
                                      * table */
    unsigned int  _INelm;                     /**< Number of elements in the angle 
                                      * table */

    static epicsFloat64 _FFSPConvFact;  /**< For converting samples from/to 
                                          * device's fixed point to/from 
                                          * double */
    
    static epicsFloat64 _FFSPSampleMin;  /**< For checking minimum limit
                                           *  on samples */
    
    static epicsFloat64 _FFSPSampleMax;  /**< For checking maximum limits
                                           *  on samples */

    /* write to hardware */
    virtual ndsStatus writeToHardware();

    /* for asynReasons */
    static std::string PV_REASON_Q_TABLE;
    static std::string PV_REASON_I_TABLE;
    static std::string PV_REASON_RAW_TABLE;
    static std::string PV_REASON_TABLE_MODE;
    static std::string PV_REASON_TABLE_SPEED;
    static std::string PV_REASON_CONSTANT_ENABLE;
    static std::string PV_REASON_WRITE_TABLE;
    static std::string PV_REASON_WRITE_TABLE_TIME;
    static std::string PV_REASON_I_TABLE_INT;
    static std::string PV_REASON_Q_TABLE_INT;

    int _interruptIdQTable;
    int _interruptIdITable;
    int _interruptIdRawTable;
    int _interruptIdTableMode;
    int _interruptIdConstantEnable;
    int _interruptIdTableSpeed;
    int _interruptIdWriteTable; /* Dummy interrupt, not used. Required by nds */
    int _interruptIdWriteTableTime;
    int _interruptIdQTableInt;
    int _interruptIdITableInt;

    virtual void freeBuffers();


    nds::ThreadTask *_writeTableLoopTask;              /**< NDS task/thread to write table to the firmware */
    ndsStatus writeTableLoopTask(nds::TaskServiceBase &service);
    int _writeTableLoopTaskRun;
};

#endif /* _sis8300llrfControlTableChannel_h */

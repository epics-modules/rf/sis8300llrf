/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfChannel.cpp
 * @brief Implementation of LLRF specific channel in NDS.
 * @author urojec
 * @date 5.6.2014
 * 
 * This channel is only used for cavity (AI0) and reference (AI1) 
 * signals and adds the functionality of reading out the current
 * Magnitude and Angle (MA) point. IQ point is also callculated
 * from the MA point by software. 
 * 
 * The class also makes sure that the two channels never get disabled.
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfAIChannel.h"
#include "ndsADIOChannel.h"
                             
std::string sis8300llrfAIChannel::
                    PV_REASON_CALIB_EN          = "CalibEnable";
std::string sis8300llrfAIChannel::
                    PV_REASON_CALIB_EGU        = "CalibEGU";
std::string sis8300llrfAIChannel::
                    PV_REASON_CALIB_RAW        = "CalibRaw";
std::string sis8300llrfAIChannel::
                    PV_REASON_CALIB_ORDER      = "CalibOrder";
std::string sis8300llrfAIChannel::
                    PV_REASON_CALIB_FITTED_LINE = "CalibFittedLine";
std::string sis8300llrfAIChannel::
                    PV_REASON_CALIB_RESID = "CalibResid";
std::string sis8300llrfAIChannel::
                    PV_REASON_CALIB_COEF = "CalibCoef";
std::string sis8300llrfAIChannel::
                    PV_REASON_CALIB_CHISQ = "CalibChisq";
/**
 * @brief llrf channel constructor.
 */
sis8300llrfAIChannel::sis8300llrfAIChannel() : sis8300AIChannel() {
 //   _isEnabled = 1;
}


sis8300llrfAIChannel::sis8300llrfAIChannel(epicsFloat64 FSampling) : sis8300AIChannel(FSampling) {
//    _isEnabled = 1;

    calibrator = new scalingPoly(maxCalibCoef);
    //Default order is 2
    calibrator->setOrder(2);
}

/**
 * @brief LLRF AI channel destructor.
 *
 * Free channel data buffer.
 */
sis8300llrfAIChannel::~sis8300llrfAIChannel() {
 //   this->~sis8300AIChannel();
}

/**
 * @brief Override parent, because we are not allowed to disable 
 *        reference and cavity channel. The PI loop does not 
 *        even start with AI0 disabled.
 */
ndsStatus sis8300llrfAIChannel::setEnabled(
    asynUser *pasynUser, epicsInt32 value) {

    NDS_TRC("%s",__func__);
    if ( (getChannelNumber() == 0 || getChannelNumber() == 1)  && value == 0) {
        NDS_ERR("AI Channel 0 (CAV) and REF (1) are always enabled");
        return ndsError;
    }

    return sis8300AIChannel::setEnabled(pasynUser, 1);
}

/**
 * @brief State handler for transition from PROCESSING. This method override the
 * onLeaveProcessing from sis8300AIChannel including the polynomial calibration
 * @param [in] from Source state.
 * @param [in] to Destination state.
 * Acquisition can only be started if channel group is in DISABLED state
 * @retval ndsSuccess Successful transition.
 *
 * Check if the acquisition finished successfully. If so then retrieve the
 * raw buffer from the device and convert to volts. For obtaining the raw values
 * the second half of the voltage buffer is used.
 */
ndsStatus sis8300llrfAIChannel::onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to) {
    sis8300AIChannelGroup   *cg;
    int                     status, iter;
    epicsInt32              nsamples;

    NDS_TRC("%s", __func__);

    if (!_EnableTransf) { //The data transfer is disabled
        doCallbacksFloat32Array(NULL, 0, _interruptIdBufferFloat32);
        return ndsSuccess;
    }

    nsamples = _SamplesCountUsed;

    /* If acquisition didn't finish completely do nothing. */
    cg = dynamic_cast<sis8300AIChannelGroup *>(getChannelGroup());
    if (!cg->isDaqFinished()) {
        NDS_DBG("Daq not completed channel %d skipping buffer update.", getChannelNumber());
        return ndsSuccess;
    }
    NDS_DBG("Calling sis8300drv_read_ai for channel %d", getChannelNumber());
    status = sis8300drv_read_ai(_DeviceUser, getChannelNumber(), _ChannelDataRaw);
    SIS8300NDS_STATUS_CHECK("sis8300drv_read_ai", status);

    // there is no linear conversion for LLRF, only polynomial, if enabled
    if (calibrator->isEnabled()) {
        for (iter = 0; iter < nsamples; iter++) {
            _ChannelData[iter] = calibrator->calibrate((double)(((epicsInt16)_ChannelDataRaw[iter]) / _convFact));
        }
    }
    else
    {
        for (iter = 0; iter < nsamples; iter++) {
            _ChannelData[iter] = (double)(((epicsInt16)_ChannelDataRaw[iter]) / _convFact);
        }
    }

    doCallbacksFloat64Array(_ChannelData, (size_t)nsamples, _interruptIdBufferFloat64);

    return ndsSuccess;
}

/**************************/
/* Calibration functions */
/**************************/
/**
 * @brief Check if calibration is enabled
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [out] value       Enable/Disabled
 *
 */

ndsStatus sis8300llrfAIChannel::getCalibEnable(
                asynUser *pasynUser, epicsInt32 *value) {
    NDS_TRC("%s", __func__);
    *value = (epicsInt32) calibrator->isEnabled();

    return ndsSuccess;
}


void sis8300llrfAIChannel::reloadCalibPVs(){ 
    bool valid, enable;
    valid = calibrator->isValid();
    enable = valid && calibrator->isEnabled();
    calibrator->setEnable(enable);
    doCallbacksInt32(enable, _interruptIdCalibEnable);
   
    if (valid) {
        doCallbacksFloat64Array(calibrator->getFittedLine(), calibrator->getFittedLineSize(), _interruptIdCalibFittedLine);
        doCallbacksFloat64Array(calibrator->getResiduals(), calibrator->getResidualsSize(), _interruptIdCalibResid);
        doCallbacksFloat64Array(calibrator->getCoeffs(), calibrator->getCoeffsSize(), _interruptIdCalibCoef);
        doCallbacksFloat64(calibrator->getChisq(), _interruptIdCalibChisq);
    }
    else{
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibFittedLine);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibResid);
        doCallbacksFloat64Array(NULL, 0, _interruptIdCalibCoef);
        doCallbacksFloat64(0, _interruptIdCalibChisq);
    }
}

/**
 * @brief Set Enable Calibration
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Any value    
 */
ndsStatus sis8300llrfAIChannel::setCalibEnable(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    bool enable = calibrator->isValid() && value;
    calibrator->setEnable(enable);
    
    doCallbacksInt32(enable, _interruptIdCalibEnable);
    //TODO update readback
    return ndsSuccess;
}


// Load EGU
ndsStatus sis8300llrfAIChannel::setCalibEGU(
        asynUser *pasynUser, epicsFloat64 *value, size_t nelem){
    NDS_TRC("%s", __func__);

    calibrator->setY(value, nelem);
    //read status from calibrator and update the status PV and fittedLine
    reloadCalibPVs();

    return ndsSuccess;
}


// Load Raw
ndsStatus sis8300llrfAIChannel::setCalibRaw(
        asynUser *pasynUser, epicsFloat64 *value, size_t nelem){
    NDS_TRC("%s", __func__);

    calibrator->setX(value, nelem);
    //read status from calibrator and update the status PV and fittedLine
    reloadCalibPVs();

    return ndsSuccess;
}

/**
 * @brief Set Calibration Order
 * 
 * @param [in]  asynUser    Asyn user context struct
 * @param [in]  value       Any value    
 */
ndsStatus sis8300llrfAIChannel::setCalibOrder(
                asynUser *pasynUser, epicsInt32 value) {
    NDS_TRC("%s", __func__);

    if (value <= 0)
        return ndsError;
    calibrator->setOrder((unsigned int)value);
    //read status from calibrator and update the status PV and fittedLine
    reloadCalibPVs();

    return ndsSuccess;
}

/**
 * @brief Registers handlers for interfacing with records. For more 
 * information, refer to NDS documentation.
 */
ndsStatus sis8300llrfAIChannel::registerHandlers(
                nds::PVContainers* pvContainers) {
    NDS_PV_REGISTER_INT32(
           sis8300llrfAIChannel::PV_REASON_CALIB_EN,
           &sis8300llrfAIChannel::setCalibEnable,
           &sis8300llrfAIChannel::getCalibEnable,
           &_interruptIdCalibEnable
     );

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfAIChannel::PV_REASON_CALIB_EGU,
            &sis8300llrfAIChannel::setCalibEGU,
            &sis8300llrfAIChannel::getFloat64Array,
//            &sis8300llrfAIChannel::getCalibEGU,
            &_interruptIdCalibEGU);

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfAIChannel::PV_REASON_CALIB_RAW,
            &sis8300llrfAIChannel::setCalibRaw,
            &sis8300llrfAIChannel::getFloat64Array,
//            &sis8300llrfAIChannel::getCalibRaw,
            &_interruptIdCalibRaw);

    NDS_PV_REGISTER_INT32(
           sis8300llrfAIChannel::PV_REASON_CALIB_ORDER,
           &sis8300llrfAIChannel::setCalibOrder,
           &sis8300llrfAIChannel::getInt32,
//           &sis8300llrfAIChannel::getCalibOrder,
           &_interruptIdCalibOrder
     );

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfAIChannel::PV_REASON_CALIB_FITTED_LINE,
            &sis8300llrfAIChannel::setFloat64Array,
            &sis8300llrfAIChannel::getFloat64Array,
            &_interruptIdCalibFittedLine);

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfAIChannel::PV_REASON_CALIB_RESID,
            &sis8300llrfAIChannel::setFloat64Array,
            &sis8300llrfAIChannel::getFloat64Array,
            &_interruptIdCalibResid);

    NDS_PV_REGISTER_FLOAT64ARRAY(
            sis8300llrfAIChannel::PV_REASON_CALIB_COEF,
            &sis8300llrfAIChannel::setFloat64Array,
            &sis8300llrfAIChannel::getFloat64Array,
            &_interruptIdCalibCoef);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfAIChannel::PV_REASON_CALIB_CHISQ,
            &sis8300llrfAIChannel::setFloat64,
            &sis8300llrfAIChannel::getFloat64,
            &_interruptIdCalibChisq);

    return sis8300AIChannel::registerHandlers(pvContainers);
}


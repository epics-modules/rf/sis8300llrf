/*
 * m-epics-sis8300llrf
 * Copyright (C) 2020 European Spallation Source

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfAuxChannelGroup.h
 * @brief File defining the LLRF Auxiliary channel group class.
 * This group is used by Down-Sampled and Internal Channels
 * @author gabrielfedel@ess.eu
 * @date 7.4.2020
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"
#include "sis8300llrfdrv_types.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfAuxChannelGroup.h"
#include "sis8300llrfAuxChannel.h"



std::string sis8300llrfAuxChannelGroup::PV_REASON_AUX_PERFTIMING = "AuxPerfTiming";

/**
 * @brief Control Table ChannelGroup constructor.
 * 
 * @param [in] name Channel Group name.
 * 
 *
 */
sis8300llrfAuxChannelGroup::sis8300llrfAuxChannelGroup(
            const std::string& name, sis8300drv_usr *newDeviceUser) :
        sis8300llrfChannelGroup(name, newDeviceUser) {

    registerOnLeaveStateHandler(nds::CHANNEL_STATE_PROCESSING,
            boost::bind(&sis8300llrfAuxChannelGroup::onLeaveProcessing, this, _1, _2));

    _Init = 1;
}

/**
 * @brief AuxChannelGroup destructor
 */
sis8300llrfAuxChannelGroup::~sis8300llrfAuxChannelGroup() {}

/**
 * @brief Register handlers, register Perf timing
 */
ndsStatus sis8300llrfAuxChannelGroup::registerHandlers(nds::PVContainers* pvContainers) {

    nds::ChannelGroup::registerHandlers(pvContainers);

    NDS_PV_REGISTER_FLOAT64(
            sis8300llrfAuxChannelGroup::PV_REASON_AUX_PERFTIMING,
            &sis8300llrfAuxChannelGroup::setFloat64,
            &sis8300llrfAuxChannelGroup::getFloat64,
            &_interruptIdAUXPerfTiming);


    return sis8300llrfChannelGroup::registerHandlers(pvContainers);
}


/**
 * @brief Get current time in miliseconds
 */
epicsFloat64 sis8300llrfAuxChannelGroup::timemsec() {
    struct timespec ts;

    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (double)ts.tv_sec * 1.0e3 + ts.tv_nsec / 1.0e6;
}

/**
 * @brief State handler for transition from PROCESSING.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Get the time before acquisition.
 */
ndsStatus sis8300llrfAuxChannelGroup::onLeaveProcessing(nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    _PerfTiming = timemsec();

    return ndsSuccess;
}

/**
 * @brief State handler for transition to DISABLED.
 * @param [in] from Source state.
 * @param [in] to Destination state.
 *
 * @retval ndsSuccess Successful transition.
 *
 * Calculate the time since leave processing (acquisition time), and update 
 * PerfTiming PV.
 */
ndsStatus sis8300llrfAuxChannelGroup::onEnterDisabled(nds::ChannelStates from, nds::ChannelStates to) {
    NDS_TRC("%s", __func__);

    _PerfTiming = timemsec() - _PerfTiming;
    doCallbacksFloat64(_PerfTiming, _interruptIdAUXPerfTiming);

    return sis8300llrfChannelGroup::onEnterDisabled(from, to);
}

ndsStatus sis8300llrfAuxChannelGroup::onEnterProcessing(nds::ChannelStates from, nds::ChannelStates to) {
    int status;
    NDS_TRC("%s", __func__);


    //update memory positions
    status = sis8300llrfdrv_update_mem_position_aux_channel(_DeviceUser);
    SIS8300NDS_STATUS_ASSERT("sis8300llrfdrv_update_mem_position_aux_channel", status);

    return sis8300llrfChannelGroup::onEnterProcessing(from, to);
}

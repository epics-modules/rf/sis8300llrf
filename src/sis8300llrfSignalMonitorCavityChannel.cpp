/*
 * m-epics-sis8300llrf
 * Copyright (C) 2014-2015  Cosylab

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file sis8300llrfModRippleFilChannel.cpp
 * @brief Implementation of sis8300llrf Modulator ripple filter channel in NDS
 * @author urojec
 * @date 23.1.2015
 * 
 * Class that exposes all the settings available for signal monitors. 
 * 
 * The class also reads out pulse-to-pulse data, which includes ALARM, PMS 
 * and interlock (ILOCK) status for the specific channel, current
 * magnitude value and minimum or maximum amplitude in the last monitor active phase.
 * If the alarm is set to trigger (#setMonitorAlaramCondition) over treshold the 
 * maximum amplitude is returned, if it is set to below treshold, than minimum 
 * amplitude is returned.
 * Monitor active phase is defined with monitor start (#setMonitorStartEvent)
 * and monitor stop (#setMonitorStopEvent) events. 
 */

#include "sis8300drv.h"
#include "sis8300llrfdrv.h"

#include "sis8300llrfDevice.h"
#include "sis8300llrfChannelGroup.h"
#include "sis8300llrfSignalMonitorCavityChannel.h"

/* ==== SIGNAL MONITOR STATUS READOUTS FROM HARDWARE ==== */


/**
 * @brief Get the current magnitude and angle value for this channel
 * 
 * @param [out] mag_val     Will hold the current magnitude value on 
 *                          success
 * @param [out] ang_val     Will hold the current angle value on 
 *                          success
 * 
 * @return  ndsSuccess      Data retrieved successfully
 * @return  ndsError        If we are in 
 *                          IOC INIT STATE or if data could not be read 
 *                          from device.
 *
 * Read magnitude and angle at the same time and update both PVs
 */
ndsStatus sis8300llrfSignalMonitorCavityChannel::getMagAngCurrent(epicsFloat64 *mag_val, epicsFloat64 *ang_val) {
    NDS_TRC("%s", __func__);
    double mag, ang;
    int status;

    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    status = sis8300llrfdrv_get_sigmon_mag_ang(
                _DeviceUser, (int) getChannelNumber(), &mag, &ang);
    SIS8300NDS_STATUS_CHECK(
        "sis8300llrfdrv_get_sigmon_mag_ang", status);
   
    *mag_val = (epicsFloat64) mag;
    *ang_val = (epicsFloat64) ang;

    //update raw values
    doCallbacksFloat64(*mag_val, _interruptIdMagCurrentRaw);
    doCallbacksFloat64(*ang_val, _interruptIdAngCurrentRaw);

    //calibrate values using the same calibration from respective AI channel
    if (m_CavChan->isCalibrated() && m_CavChan->isGradCalibrated()) //with gradient calibration
        *mag_val = m_CavChan->calibratePolyAndGrad(*mag_val);
    else if (m_CavChan->isCalibrated()) // only polynomial calibration
        *mag_val = m_CavChan->calibrate(*mag_val);

    //update calibrated values
    doCallbacksFloat64(*mag_val, _interruptIdMagCurrent);
    doCallbacksFloat64(*ang_val, _interruptIdAngCurrent);

    return ndsSuccess;
}


/**
 * @brief Get the minimum or maximum magnitude for this channel during  
 *        the last signal monitor active period.
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the min or max magnitude value on 
 *                          success
 * 
 * @return  ndsSuccess      Data retrieved successfully
 * @return  ndsError        If we are in 
 *                          IOC INIT STATE or if data could not be read 
 *                          from device.
 * 
 * This will return the minimum magnitude if the signal monitor is set 
 * to trigger below treshold and maximum magnitude if it is set to 
 * trigger below treshold (#setMonitorAlaramCondition).
 */
ndsStatus sis8300llrfSignalMonitorCavityChannel::getMagMinMax(
                asynUser *pasynUser,  epicsFloat64 *value) {
    double doubleVal;
    int status; 
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    status = sis8300llrfdrv_get_sigmon_mag_minmax(
                _DeviceUser, (int) getChannelNumber(), &doubleVal);
    SIS8300NDS_STATUS_CHECK(
        "sis8300llrfdrv_get_sigmon_mag_minmax", status);
    
    *value = (epicsFloat64) doubleVal;

    //update raw value
    doCallbacksFloat64(*value, _interruptIdMagMinMaxRaw);

    if (m_CavChan->isCalibrated() && m_CavChan->isGradCalibrated()) //with gradient calibration
        *value = m_CavChan->calibratePolyAndGrad(*value);
    else if (m_CavChan->isCalibrated()) // only polynomial calibration
        *value = m_CavChan->calibrate(*value);
   
    //update calibrated value
    doCallbacksFloat64(*value, _interruptIdMagMinMax);
    return ndsSuccess;
}

/**
 * @brief Get the average magnitude value for this channel inside
 * configured window
 * 
 * @param [in]  pasynUser   Asyn user context struct
 * @param [out] value       Will hold the current magnitude value on 
 *                          success
 * 
 * @return  ndsSuccess      Data retrieved successfully
 * @return  ndsError        If we are in 
 *                          IOC INIT STATE or if data could not be read 
 *                          from device.
 */
ndsStatus sis8300llrfSignalMonitorCavityChannel::getMagAverage(
                asynUser *pasynUser,  epicsFloat64 *value) {
    double doubleVal;
    int status; 
    
    if (getCurrentState() == nds::CHANNEL_STATE_IOC_INITIALIZATION) {
        return ndsError;
    }

    status = sis8300llrfdrv_get_sigmon_mag_avg(
                _DeviceUser, (int) getChannelNumber(), &doubleVal);
    SIS8300NDS_STATUS_CHECK(
        "sis8300llrfdrv_get_sigmon_mag_avg", status);
    
    *value = (epicsFloat64) doubleVal;

    //update raw value
    doCallbacksFloat64(*value, _interruptIdMagAverageRaw);

    if (m_CavChan->isCalibrated() && m_CavChan->isGradCalibrated()) //with gradient calibration
        *value = m_CavChan->calibratePolyAndGrad(*value);
    else if (m_CavChan->isCalibrated()) // only polynomial calibration
        *value = m_CavChan->calibrate(*value);
    
    //update calibrated value
    doCallbacksFloat64(*value, _interruptIdMagAverage);

    return ndsSuccess;
}

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <aSubRecord.h>

// Reset a waveform
static long waveform_reset(aSubRecord *psr) {
    long const reset = *((long*)psr->a);
    long cur_size = *((long*)psr->b);
    
    if (reset == 0) {
        psr->neva = cur_size;
        return 0;
    } else {
        psr->neva = 0;
        return 0;
    }
}

// Copy a waveform
static long waveform_copy(aSubRecord *psr) {
    //Load input waveform
    double * in     = (double*)psr->a;

    long const in_len = *((long*)psr->b);

    // check if there is space on output waveform
    if (in_len > psr->nova) {
        return 1;
    }

    //copy waveform
    memcpy((double *) psr->vala, in, in_len * sizeof(double));

    //set size of the output
    psr->neva = in_len;

    return 0;
}


//wrap all values of a waveform around -180 and +180 
static long angle_wrapper(aSubRecord *psr) {
    int i;
    double val;
    //Load input waveform
    double * in     = (double*)psr->a;
    long const in_len = *((long*)psr->b);
    //output waveform
    double * out = (double *) psr->vala;


    for(i = 0; i < in_len; i++){
        if (in[i] > 180)
            val = in[i] - 360;
        else if (in[i] < -180)
            val = in[i] + 360;
        else
            val = in[i];
        out[i] = val;
    }

    //set size of the output
    psr->neva = in_len;

    return 0;
}

//wrap all values of a waveform around a specific interval
static long waveform_wrapper(aSubRecord *psr) {
    int i;
    double val;
    //Load input waveform
    double * in     = (double*)psr->a;
    long const in_len = *((long*)psr->b);
    //load limits
    double low = *((double*)psr->c);
    double high = *((double*)psr->d);
    //output waveform
    double * out = (double *) psr->vala;

    for(i = 0; i < in_len; i++){
        if (in[i] > high)
            val = high;
        else if (in[i] < low)
            val = low;
        else
            val = in[i];
        out[i] = val;
    }

    //set size of the output
    psr->neva = in_len;

    return 0;
}


// Register the functions for access by EPICS
#include <registryFunction.h>
#include <epicsExport.h>

epicsRegisterFunction(waveform_reset);
epicsRegisterFunction(waveform_copy);
epicsRegisterFunction(angle_wrapper);
epicsRegisterFunction(waveform_wrapper);

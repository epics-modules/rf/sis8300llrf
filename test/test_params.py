from helper import read_reg
from helper import caget_assert

def test_params(board, prefixdig, ssh):
    assert caget_assert(prefixdig + ":FSM") is not None  # if fails, prefix invalid
    assert (read_reg(board, 0x0, ssh)).rfind("error") == -1 # if fail board invalid

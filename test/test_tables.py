#!/usr/bin/env python3
from random import random, randint
from time import sleep

from helper import FF_REG, SP_REG, read_reg, float_to_fixed, get_min_max_mn, read_mem, fixed_to_float
from helper import caput_assert, caget_assert
import numpy as np
import pytest

NELM = 16

# Normal mode

# SP -  I
# SP -  Q
# FF -  I
# FF -  Q

def set_table(prefixdig, ctrl, qi, qmn, size = 16, wrt_to_mem = True):
    """Set values for one table (the same value)
    ctrl = SP / FF
    qi = I / Q
    size = number of elements
    qmn = (m, n, signed)
    """
    (min, max) = get_min_max_mn(*qmn)

    val = round(random()*(max-min)+min, qmn[1])
    while (val > max or val < min):
        val = round(random()*(max-min)+min, qmn[1])

    vals = []
    vals.extend([val]*size)

    pv = prefixdig + ":"+ ctrl +"Tbl"

    caput_assert(pv + "-" + qi, vals)

    if (wrt_to_mem):
        print("Wrote to Mem!")
        # write table to the memory
        caput_assert(pv + "-TblToFW", 1)
    else:
        print("Won't write to Mem!")

    sleep(1)
    # update readback
    caput_assert(pv + "-" + qi + "-RB.PROC", 1)

    return val

def load_readback(prefixdig, ctrl, qi):
    pv = prefixdig + ":"+ ctrl +"Tbl"
    caput_assert(pv + "-" + qi + "-RB.PROC", 1)

class TestTables:
#    """Check if the tables on firmware are empty"""
#    def test_empty_tables(self, board, prefixdig, ssh):
#        ## SP tables
#        load_readback(prefixdig, "SP", "Q")
#        load_readback(prefixdig, "SP", "I")
#        sleep(1)
#        len_i = len(caget_assert(prefixdig + ":SPTbl-I-RB"))
#        len_q = len(caget_assert(prefixdig + ":SPTbl-Q-RB"))
#        assert len_i > 0 and len_q > 0
#        ## SP tables
#        load_readback(prefixdig, "FF", "Q")
#        load_readback(prefixdig, "FF", "I")
#        sleep(1)
#        len_i = len(caget_assert(prefixdig + ":FFTbl-I-RB"))
#        len_q = len(caget_assert(prefixdig + ":FFTbl-Q-RB"))
#        assert len_i > 0 and len_q > 0


    """Class for test tables on Normal mode"""
    def test_tables_sp_qi(self, board, prefixdig, ssh):
        """Test tables SP Q/I on normal mode"""
        qmn = (1, 15, 1)
        q_val = set_table(prefixdig, "SP", "Q", qmn)
        sleep(1)
        i_val = set_table(prefixdig, "SP", "I", qmn)
        sleep(1)

        mem_pos = int(read_reg(board, SP_REG, ssh), NELM)
        mem_values = read_mem(board, int(mem_pos/2), NELM*2, ssh)
        q_val_qmn = str(float_to_fixed(q_val, *qmn))
        i_val_qmn = str(float_to_fixed(i_val, *qmn))

        # get a random position to check
        pos = randint(1, 15)
        # q pos = pos*2
        # i pos = pos*2 + 1

        assert q_val_qmn == mem_values[pos*2+1]
        assert i_val_qmn == mem_values[pos*2]

        # check the readback pv
        pos = randint(1, 15)
        rbv_i = caget_assert(prefixdig + ":SPTbl-I-RB")
        rbv_q = caget_assert(prefixdig + ":SPTbl-Q-RB")
        assert fixed_to_float(float_to_fixed(i_val, *qmn),*qmn) == rbv_i[pos]
        assert fixed_to_float(float_to_fixed(q_val, *qmn),*qmn) == rbv_q[pos]

    def test_tables_ff_qi(self, board, prefixdig, ssh):
        """Test tables FF Q/I on normal mode"""
        qmn = (1, 15, 1)
        q_val = set_table(prefixdig, "FF", "Q", qmn)
        sleep(5)
        i_val = set_table(prefixdig, "FF", "I", qmn)

        mem_pos = int(read_reg(board, FF_REG, ssh), NELM)
        mem_values = read_mem(board, int(mem_pos/2), NELM*2, ssh)
        q_val_qmn = str(float_to_fixed(q_val, *qmn))
        i_val_qmn = str(float_to_fixed(i_val, *qmn))

        # get a random position to check
        pos = randint(1, 15)
        # q pos = pos*2
        # i pos = pos*2 + 1

        assert q_val_qmn == mem_values[pos*2+1]
        assert i_val_qmn == mem_values[pos*2]

        # check the readback pv
        pos = randint(1, 15)
        rbv_i = caget_assert(prefixdig + ":FFTbl-I-RB")
        rbv_q = caget_assert(prefixdig + ":FFTbl-Q-RB")
        assert fixed_to_float(float_to_fixed(i_val, *qmn),*qmn) == rbv_i[pos]
        assert fixed_to_float(float_to_fixed(q_val, *qmn),*qmn) == rbv_q[pos]


    """Class for test tables on Normal mode"""
    @pytest.mark.parametrize("ctrl_type", ["SP"])
    def test_tables_sizes(self, board, prefixdig, ctrl_type, ssh):
        qmn = (1, 15, 1)

        big_len = 100
        # first write a table of big lenght
        i_val = set_table(prefixdig, ctrl_type, "I", qmn, big_len)
        sleep(1)
        q_val = set_table(prefixdig, ctrl_type, "Q", qmn, big_len)
        sleep(1)

        # check if the readback has the right size

        i_len = caget_assert(prefixdig + ":"+ ctrl_type +"Tbl-I-RB.NORD")
        q_len = caget_assert(prefixdig + ":"+ ctrl_type +"Tbl-Q-RB.NORD")

        assert i_len == big_len and q_len == big_len

        # then use a smaller table, but without writting it
        small_len = 50
        i_val = set_table(prefixdig, ctrl_type, "I", qmn, small_len)
        sleep(1)
        q_val = set_table(prefixdig, ctrl_type, "Q", qmn, small_len)
        sleep(1)
        # Reload I readback
        caput_assert(prefixdig + ":" + ctrl_type + "Tbl-I-RB.PROC", 1)

        i_len = caget_assert(prefixdig + ":" + ctrl_type + "Tbl-I-RB.NORD")
        q_len = caget_assert(prefixdig + ":" + ctrl_type + "Tbl-Q-RB.NORD")

        assert i_len == small_len and q_len == small_len



#class TestLargeTables:
#    @pytest.mark.parametrize("ctrl_type", ["SP", "FF"])
#    def test_large_tables(self, board, prefixdig, ctrl_type, ssh):
#        """Test tables SP Q/I on normal mode"""
#        qmn = (1, 15, 1)
#        max_size = int(caget_assert(prefixdig + ":" + ctrl_type + "Tbl-MaxSmpNm") / 2)
#        size = max_size
#
#        q_val = set_table(prefixdig, ctrl_type, "Q", qmn, size)
#        sleep(1)
#        i_val = set_table(prefixdig, ctrl_type, "I", qmn, size)
#        sleep(1)
#
#        mem_pos = int(read_reg(board, SP_REG, ssh), 16)
#        size_mem = (2*size + 0x1F) &~0x1F
#        mem_values = read_mem(board, int(mem_pos/2), size_mem, ssh)
#        q_val_qmn = str(float_to_fixed(q_val, *qmn))
#        i_val_qmn = str(float_to_fixed(i_val, *qmn))
#
#
#        # get a random position to check
#        pos = randint(1, size)
#        # q pos = pos*2
#        # i pos = pos*2 + 1
#        print("mem read size ", len(mem_values), " pos to read ", pos)
#
#        assert q_val_qmn == mem_values[pos*2]
#        assert i_val_qmn == mem_values[pos*2+1]
#
#        # check the readback pv
#        pos = randint(1, size)
#        rbv_i = caget_assert(prefixdig + ":SPTbl-I-RB")
#        rbv_q = caget_assert(prefixdig + ":SPTbl-Q-RB")
#        assert fixed_to_float(float_to_fixed(i_val, *qmn),*qmn) == rbv_i[pos]
#        assert fixed_to_float(float_to_fixed(q_val, *qmn),*qmn) == rbv_q[pos]

from time import sleep

from helper import caget_assert, change_state, read_reg_int, caput_assert, \
            check_readback

class TestLoopMode:
    def test_init(self, prefixdig, board, ssh):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        check_readback(prefixdig + ":OpenLoop", 1)

        reg1 = read_reg_int(board, "0x40A", ssh)
        reg2 = read_reg_int(board, "0x411", ssh)
        assert (reg1 & 60 == 8) and (reg2 & 60 == 8)

        check_readback(prefixdig + ":OpenLoop", 0)

        reg1 = read_reg_int(board, "0x40A", ssh)
        reg2 = read_reg_int(board, "0x411", ssh)
        assert (reg1 & 60 == 0) and (reg2 & 60 == 0)


    def test_after_on_reset(self, prefixdig, board, ssh):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        caput_assert(prefixdig + ":OpenLoop", 1)

        for state in ["ON", "RESET", "INIT"]:
            assert change_state(prefixdig, state)

        assert caget_assert(prefixdig + ":OpenLoop-RB") == 1

        reg1 = read_reg_int(board, "0x40A", ssh)
        reg2 = read_reg_int(board, "0x411", ssh)
        assert (reg1 & 60 == 8) and (reg2 & 60 == 8)

        caput_assert(prefixdig + ":OpenLoop", 0)

        for state in ["ON", "RESET", "INIT"]:
            assert change_state(prefixdig, state)

        assert caget_assert(prefixdig + ":OpenLoop-RB") == 0

        reg1 = read_reg_int(board, "0x40A", ssh)
        reg2 = read_reg_int(board, "0x411", ssh)
        assert (reg1 & 60 == 0) and (reg2 & 60 == 0)

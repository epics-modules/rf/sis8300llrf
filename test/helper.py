#!/usr/bin/env python3

import os
import re
from glob import glob
from time import sleep
from math import exp, cos, sin, pi, nan
import csv
from random import random, randint
from epics import caget, caput, ca
import numpy as np

BASE_BOARD = "/dev/sis8300"
ENV_PREFIX = "LLRF_IOC_NAME"
SIS8300DRV_RTM_ATT_VM = 8
TRIG_REG = "0x404"
MAXACQ = 0x200000
PREFIXDIG=":DIG01"
PREFIXRTM=":RFM01"
PREFIXIOC="LLRF:Ctrl-IOC-01"

REG_LP1="0x442"
REG_LP2="0x443"
REG_LP3="0x444" # enable/disable

REG_NOTCH1="0x43f"
REG_NOTCH2="0x440"
REG_NOTCH3="0x441" # enable/disable

FF_REG="0x41e"
SP_REG="0x41f"

REQMODFILE="/iocs/sis8300llrf/REQMODs.list"

def caget_assert(PV,param_timeout=1, param_as_string=False):
    if param_as_string:
        status = caget(PV, as_string=True, timeout=1)
        assert status is not None and isinstance(status,str)
    else:
        status = caget(PV,timeout = param_timeout)
        assert status is not None
    return status

def caput_assert(PV,value,param_timeout=1):
    status = caput(PV,value, timeout=param_timeout)
    assert status is not None
    return status

def read_reg(board, reg, ssh=""):
    if ssh != "":
        p = os.popen("ssh " + ssh + " sis8300drv_reg %s %s" % (board, reg))
    else:
        p = os.popen("sis8300drv_reg %s %s" % (board, reg))
    ret = p.read().split("\n")[0]
    p.close()

    return ret

def read_reg_int(board,reg, ssh=""):
    if ssh != "":
        p = os.popen("ssh " + ssh + " sis8300drv_reg %s %s" % (board, reg))
    else:
        p = os.popen("sis8300drv_reg %s %s" % (board, reg))
    ret = p.read().split("\n")[0]
    p.close()
    ret = int(ret,16)
    return ret


def write_reg(board, reg, val, ssh=""):
    if ssh != "":
        os.system("ssh " + ssh + " sis8300drv_reg %s %s -w %s" % (board, reg, val))
    else:
        os.system("sis8300drv_reg %s %s -w %s" % (board, reg, val))

def change_state(prefix, state):
    caput_assert(prefix + ":Msg", state)
    sleep(0.5)
    res = caget_assert(prefix + ":FSM", param_as_string=True)
    if state == "RESET":
        return res == "RESETTING"
    return res == state

def float_to_fixed(val, m, n, signed):
    pow_2_frac_bits = float(0x1 << n)
    pow_2_frac_bits_int_bits = float(0x1 << (m + n))

    val_int64 = int(val * pow_2_frac_bits)
    # Check if signed
    if (signed):
        if (val_int64 < 0):
            val_int64 += int(pow_2_frac_bits_int_bits)
        # check upper limit of signed int
        elif (val_int64 > (pow_2_frac_bits_int_bits / 2.0 - 1.0)):
            return nan

    if (val_int64 >> m + n):
        return nan

    return val_int64

def fixed_to_float(num, m, n, signed):
    value = float(num)
    if (signed and value >= 2**(m+n-1)):
        value -= float(2**(m+n))

    value = value / 2**n

    return value

def sim_bp_trig(board, ssh=""):
    write_reg(board, TRIG_REG, "0x10", ssh)
    write_reg(board, TRIG_REG, "0x20", ssh)
    write_reg(board, TRIG_REG, "0x40", ssh)
    write_reg(board, TRIG_REG, "0x80", ssh)
    sleep(0.1)

def calc_lp_consts(cutoff, fsamp, n):
    omg0 = 2*pi*cutoff
    h = 1/(fsamp/n)

    constA = round(-exp(-omg0*h), 12)
    constB = round(1 - exp(-omg0*h), 12)

    return [constA, constB]

# calculate and return constants from Notch
def calc_notch_consts(bd, freq, fsamp, n):
    h = 1/(fsamp/n);
    omg0 = 2*pi*freq;

    areal = round(exp(-(pi*bd*h))*cos(omg0*h), 12)
    aimag = round(exp(-(pi*bd*h))*sin(omg0*h), 12)
    breal = round((1-exp(-(pi*bd*h)))*cos(omg0*h), 12)
    bimag = round((1-exp(-(pi*bd*h)))*sin(omg0*h), 12)

    return([areal, aimag, breal, bimag])

def get_min_max_mn(m, n, signed):
    if signed:
        min = -2**(m-1)
        max = 2**(m-1) - 2**(-n)
    else:
        min = 0
        max = (2**m) - (2**(-n))

    return (min, max)

def read_mem(board, offset, size, ssh):
    if ssh != "":
        p = os.popen("ssh " + ssh + " sis8300drv_mem %s -o %d -n %d" % (board, offset, size))
    else:
        p = os.popen("sis8300drv_mem %s -o %d -n %d" % (board, offset, size))
    ret = (p.read().split("\n"))[:-1]
    p.close()

    return ret

def get_mods_vers(prefixioc):
    modpv = prefixioc + ":MODULES"
    verspv =  prefixioc + ":VERSIONS"
    mods = caget(modpv)
    if mods is None:
        "No module version available"
        return
    vers = caget_assert(verspv)

    r = "Modules versions: \n"

    for i in range(len(mods)):
        r += mods[i] + " - " + vers[i] + "\n"

    return r

def check_readback(pv, value):
    caput_assert(pv, value)
    sleep(0.1)
    res = caget_assert(pv + "-RB")
    if type(value) == float:
        res = round(res, PREC)
        value = round(value, PREC)
    return value == res

# csv helpers

PREC=12
PREC_SLOPE=8
PREC_OFFSET=3
MAXELEM=2000
FILENAME="/tmp/input.csv"

def generate_calib_table_linear(size):
    """
    Generate a linear calibration table with random numbers.
    size: number of elements of csv
    return the slope, offset, egu/raw
    """
    slope = round(random(), PREC_SLOPE)
    offset = round(random(), PREC_OFFSET)

    raw = []
    for i in range(size):
        raw.append(round(random(), PREC))

    raw = sorted(raw)

    egu = []
    for i in range(size):
        egu.append(round(raw[i]*slope + offset, PREC))

    return(slope,offset, egu, raw)

def generate_calib_table(size):
    """
    Generate a calibration table with a specific curve
    """
    order = randint(2,20)
    coef = []
    for i in range(order + 1):
        coef.append(round(random(), PREC))


    raw = []
    egu = []
    for i in range(size):
        raw.append(random())

    raw = sorted(raw)

    for i in range(size):
        egu.append(np.polyval(coef, raw[i]))

    return(order, coef, egu, raw)



# Generate a comletely random csv without know the slope and offset
def generate_csv_rand(file_name, size, ssh=""):
    input = []
    digitised = []
    for i in range(size):
        input.append(round(random(), PREC))
        digitised.append(round(random(), PREC))

    input = sorted(input)
    digitised = sorted(digitised)

    with open(file_name, 'w') as f:
        w = csv.writer(f)
        w.writerow(["# digitilised, input"])
        for i in range(size):
            w.writerow([digitised[i], input[i]])

    if ssh != "":
        p = os.popen("scp " + file_name + " " + ssh + ":" + file_name)
        sleep(1)

# generate an unordered CSV
def generate_csv_unordered(file_name, size, ssh=""):
    input = []
    digitised = []
    ordered = True

    for i in range(size):
        input.append(round(random(), PREC))
        digitised.append(round(random(), PREC))

    input = sorted(input, reverse = True)
    digitised = sorted(digitised, reverse = True)

    with open(file_name, 'w') as f:
        w = csv.writer(f)
        w.writerow(["# digitilised, input"])
        for i in range(size):
            w.writerow([digitised[i], input[i]])

    if ssh != "":
        p = os.popen("scp " + file_name + " " + ssh + ":" + file_name)
        sleep(1)

def check_readback_float(pv, m, n, sign):
        mini = caget_assert(pv + ".DRVL")
        maxi = caget_assert(pv + ".DRVH")
        val = round(random()*(maxi-mini)+mini, n)
        caput_assert(pv, val)
        sleep(0.1)
        res = caget_assert(pv + "-RB")

        return res == fixed_to_float(float_to_fixed(val, m, n, sign), m, n, sign)

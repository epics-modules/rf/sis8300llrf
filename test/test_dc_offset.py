#!/usr/bin/env python3
import pytest
from random import randint

from helper import read_reg, read_reg_int
from helper import caget_assert, caput_assert

""" DC Offset is configurable from a single 32-bit FPGA register @ address 0xf42
The settable range is -512 to + 512 is represented as 2's complement 10-bit signed integer.
10-bits are available for I component from least significant bits 0-9.
10-bits for the Q component from bits 16-25.
The remainder of register 0xf42 is reserved at time of writing.
This setting is exposed as EPICS PV $(P)$(R) for I and Q components.
I has $(R)=VM-DCOffset-I
Q has $(R)=VM-DCOffset-Q
"""

pv_property_I = ":VM-DCOffset-I"
pv_property_Q = ":VM-DCOffset-Q"
REG_DC_OFFSET_COUNTS = "0xf42"
MAX_COUNTS = 2047
MIN_COUNTS = -2048
HEX_MASK = int('0xfff',16)

class TestDCOffset:
    #I-Component
    def test_set_max_counts_I(self, prefixrtm, board, ssh):
        caput_assert(prefixrtm + pv_property_I, MAX_COUNTS)
        result = read_reg_int(board, REG_DC_OFFSET_COUNTS, ssh)
        assert hex(result & HEX_MASK) == "0x7ff"
    def test_set_minus_one_counts_I(self, prefixrtm, board, ssh):
        caput_assert(prefixrtm + pv_property_I, -1)
        result = read_reg_int(board, REG_DC_OFFSET_COUNTS, ssh)
        assert hex(result & HEX_MASK) == "0xfff"
    def test_set_min_counts_I(self, prefixrtm, board, ssh):
        caput_assert(prefixrtm + pv_property_I, MIN_COUNTS)
        result = read_reg_int(board, REG_DC_OFFSET_COUNTS, ssh)
        assert hex(result & HEX_MASK) == "0x800"
    def test_set_zero_counts_I(self, prefixrtm, board, ssh):
        caput_assert (prefixrtm + pv_property_I, 0)
        result = read_reg_int(board, REG_DC_OFFSET_COUNTS, ssh)
        assert hex(result & HEX_MASK) == "0x0"

    # Q-Component
    def test_set_max_counts_Q(self, prefixrtm, board, ssh):
        caput_assert (prefixrtm + pv_property_Q, MAX_COUNTS)
        result = read_reg_int(board, REG_DC_OFFSET_COUNTS, ssh) >> 16
        assert hex(result) == "0x7ff"
    def test_set_minus_one_count_Q(self, prefixrtm, board, ssh):
        caput_assert (prefixrtm + pv_property_Q, -1)
        result = read_reg_int(board, REG_DC_OFFSET_COUNTS, ssh) >> 16
        assert hex(result) == "0xfff"
    def test_set_min_counts_Q(self, prefixrtm, board, ssh):
        caput_assert (prefixrtm + pv_property_Q, MIN_COUNTS)
        result = read_reg_int(board, REG_DC_OFFSET_COUNTS, ssh) >> 16
        assert hex(result) == "0x800"
    def test_set_zero_count_Q(self, prefixrtm, board, ssh):
        caput_assert (prefixrtm + pv_property_Q, 0)
        result = read_reg_int(board, REG_DC_OFFSET_COUNTS, ssh) >> 16
        assert hex(result) == "0x0"

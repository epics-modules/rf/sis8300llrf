from random import random
from time import sleep

from helper import caput_assert
from helper import SIS8300DRV_RTM_ATT_VM, change_state, float_to_fixed, \
                    read_reg, caget_assert, fixed_to_float

from epics import PV, ca

class MonitorMissedPulses:
    def __init__(self, prefix):
            self.missed = 0
            self.PVMissedPulses = PV(prefix + ":PulseMissed", callback = self.check_missed)

    def check_missed(self, pvname=None, value=None, **kw):
        if value > 0:
            self.missed += value

    def return_missed(self):
        return self.missed
        

class TestAttenuation:
    def set_rand_att(self, board, prefixrtm, ch, ssh):
        m = 5
        n = 1

        # generate a random value
        max_value = 2**m - 2**-n
        att_val = random()*max_value

        # set value
        caput_assert(prefixrtm + ":AI" + str(ch) + "-Att", str(att_val))
        valqmn = float_to_fixed(att_val, m, n, 0)

        sleep(1)

        # check PV RBV
        rbv_val =  caget_assert(prefixrtm + ":AI" + str(ch) + "-Att-RB")
        if fixed_to_float(float_to_fixed(att_val, m, n, 0), m, n, 0) != rbv_val:
            return False


        # read from register
        reg = "0xF8" + str(2 + (ch//4))
        res = int(read_reg(board, reg, ssh), 16)

        # get the desired part
        rshift = ch % 4 * 8
        res = (res >> rshift) & 0x000000FF

        # check register
        return valqmn == res

    def set_rand_att_vmout(self, board, prefixrtm, ssh):
        m = 4
        n = 2
        # generate a random value
        max_value = 2**m - 2**-n
        att_val = random()*max_value

        # set value
        caput_assert(prefixrtm + ":RFOutAtt", str(att_val))
        valqmn = float_to_fixed(att_val, m, n, 0)

        sleep(0.5)

        # check PV RBV
        rbv_val =  caget_assert(prefixrtm + ":RFOutAtt-RB")
        if fixed_to_float(float_to_fixed(att_val, m, n, 0), m, n, 0) != rbv_val:
            return False

        # read from register
        reg = "0xF8" + str(2 + (8//4))
        res = int(read_reg(board, reg, ssh), 16)

        # get the desired part
        rshift = 8 % 4 * 8
        res = (res >> rshift) & 0x000000FF

        return valqmn == res


    def test_init(self, board, prefixdig, prefixrtm, ssh):
        if  caget_assert(prefixdig + ":FSM", param_as_string=True) != 'INIT':
            for state in ["RESET", "INIT"]:
                assert change_state(prefixdig, state)

        for ch in range(0,8):
            assert self.set_rand_att(board, prefixrtm, ch, ssh) == True

        assert self.set_rand_att_vmout(board, prefixrtm, ssh)  == True

    def test_on(self, board, prefixdig, prefixrtm, ssh):
        if  caget_assert(prefixdig + ":FSM", param_as_string=True) != 'ON':
            assert change_state(prefixdig, "ON")

        for ch in range(0,8):
            assert self.set_rand_att(board, prefixrtm, ch, ssh) == True

        assert self.set_rand_att_vmout(board, prefixrtm, ssh)  == True

    def test_after_reset(self, board, prefixdig, prefixrtm, ssh):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        for ch in range(0,8):
            assert self.set_rand_att(board, prefixrtm, ch, ssh) == True

        assert self.set_rand_att_vmout(board, prefixrtm, ssh)  == True

    """ 
    Test to check if pulses are missed when the attenuator is changed during the acquisition
    """
    def test_missed(self, board, ssh, prefixdig, prefixrtm, prefixisland):
        # this test should be run only if there is an EVR Island available
        if prefixisland == None:
            return

        # prepare EVR
        caput_assert(prefixisland + ":CycleFreq-SP", 14)
        caput_assert(prefixisland + ":Mode", "Free Running")
    
        # reset state machine
        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        missedpulses = MonitorMissedPulses(prefixdig)
        
        # change the attenuation
        for ch in range(0,8):
            assert self.set_rand_att(board, prefixrtm, ch, ssh) == True

        # wait to check for a missed pulse
        ca.poll(2)

        # Stop EVR 
        caput_assert(prefixisland + ":Mode", "On Demand")

        # check if some pulse where missed
        assert missedpulses.return_missed() == 0
      
        # check if some pulse where acquired
        assert caget_assert(prefixdig + ":PulseDoneCnt")  > 0

'''Test for PID parameters'''
from random import random

from helper import caget_assert, caput_assert

PV_KP="%s:RFCtrlKp"
PV_KI="%s:RFCtrlKi"
PV_KD="%s:RFCtrlKd"
PV_KP_USER_INP="%s:RFCtrlKp-UserInp"
PV_KI_USER_INP="%s:RFCtrlKi-UserInp"
PV_KD_USER_INP="%s:RFCtrlKd-UserInp"
PV_COMMIT_PID="%s:RFCtrl-CommitPIParams"

PV_TF="%s:RFCtrlTf"
PV_A2I="%s:RFCtrlDa2-I-RB"
PV_B0I="%s:RFCtrlDb0-I-RB"
PV_B1I="%s:RFCtrlDb1-I-RB"
PV_B2I="%s:RFCtrlDb2-I-RB"
PV_A2Q="%s:RFCtrlDa2-Q-RB"
PV_B0Q="%s:RFCtrlDb0-Q-RB"
PV_B1Q="%s:RFCtrlDb1-Q-RB"
PV_B2Q="%s:RFCtrlDb2-Q-RB"
PV_FREQ_SAMP="%s:FreqSampling"
PV_IQ_N="%s:IQSmpNearIQ-N-RB"

PID_PARAM_RB_DIFF = 0.01

def get_rb_error(val_set, val_rb):
    # Get approx error between value set and read
    return abs(val_set - val_rb) / val_set


class TestPIs:
    '''Test to check if the D coefficients are being calculated correctly'''
    def test_D(self, prefix, prefixdig):
        kd = random()
        tf = random()

        caput_assert(PV_KD  % prefixdig, kd)
        caput_assert(PV_TF  % prefixdig, tf)

        caput_assert(PV_A2I % prefixdig + ".PROC", 1)
        a2i_rb = caget_assert(PV_A2I % prefixdig)
        caput_assert(PV_B0I % prefixdig + ".PROC", 1)
        b0i_rb = caget_assert(PV_B0I % prefixdig)
        caput_assert(PV_B1I % prefixdig + ".PROC", 1)
        b1i_rb = caget_assert(PV_B1I % prefixdig)
        caput_assert(PV_B2I % prefixdig + ".PROC", 1)
        b2i_rb = caget_assert(PV_B2I % prefixdig)

        caput_assert(PV_A2Q % prefixdig + ".PROC", 1)
        a2q_rb = caget_assert(PV_A2Q % prefixdig)
        caput_assert(PV_B0Q % prefixdig + ".PROC", 1)
        b0q_rb = caget_assert(PV_B0Q % prefixdig)
        caput_assert(PV_B1Q % prefixdig + ".PROC", 1)
        b1q_rb = caget_assert(PV_B1Q % prefixdig)
        caput_assert(PV_B2Q % prefixdig + ".PROC", 1)
        b2q_rb = caget_assert(PV_B2Q % prefixdig)

        assert a2i_rb == a2q_rb
        assert b0i_rb == b0q_rb
        assert b1i_rb == b1q_rb
        assert b2i_rb == b2q_rb

        # calculate s and t, and the expected coefficients
        fs = caget_assert(PV_FREQ_SAMP % prefix)
        iqn = caget_assert(PV_IQ_N % prefixdig)
        ts = iqn/(fs*1000000)

        nd = 1/tf
        s = kd*nd
        t = 1 + ts*nd

        a2 = 1/(t**2)
        b0 = -s/t
        b1 = (s*(t-1))/(t**2)
        b2 = s/(t**2)

        assert abs(a2-a2i_rb) < 10**-10
        assert abs(b0-b0i_rb) < 10**-10
        assert abs(b1-b1i_rb) < 10**-10
        assert abs(b2-b2i_rb) < 10**-10


    def test_set_pid_params_at_once(self, prefix, prefixdig):
        kp_inp = random()
        ki_inp = random()
        kd_inp = random()

        caput_assert(PV_KP_USER_INP  % prefixdig, kp_inp)
        caput_assert(PV_KI_USER_INP  % prefixdig, ki_inp)
        caput_assert(PV_KD_USER_INP  % prefixdig, kd_inp)
        caput_assert(PV_COMMIT_PID  % prefixdig, 1)

        kp = caget_assert(PV_KP % prefixdig)
        ki = caget_assert(PV_KI % prefixdig)
        kd = caget_assert(PV_KD % prefixdig)

        kp_rb = caget_assert(PV_KP % prefixdig + "-RB")
        ki_rb = caget_assert(PV_KI % prefixdig + "-RB")
        kd_rb = caget_assert(PV_KD % prefixdig + "-RB")

        assert kp == kp_inp
        assert ki == ki_inp
        assert kd == kd_inp
 
        assert get_rb_error(kp, kp_rb) < PID_PARAM_RB_DIFF
        assert get_rb_error(ki, ki_rb) < PID_PARAM_RB_DIFF
        assert get_rb_error(kd, kd_rb) < PID_PARAM_RB_DIFF

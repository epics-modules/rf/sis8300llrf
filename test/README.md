# How to run

You should specify the board number and IOC prefixes. An example for a generic system:

`pytest --board=3 --prefix=LLRF-LLRF1: --prefixdig=LLRF-010:RFS-DIG-101 --prefixrtm=LLRF-010:RFS-RFM-101`

## Running tests remotely

It is possible to run tests on a remote system, for this is necessary that you 
have your keys configured on the remote system. An example running tests 
remotely on a machine called icslab


`pytest --board=3 --prefix=LLRF-LLRF1: --prefixdig=LLRF-010:RFS-DIG-101 --prefixrtm=LLRF-010:RFS-RFM-101 --ssh=icslab`

"""
Test Post mortem PVs

This test consider that the EVR is running at 14Hz
"""
#!/usr/bin/env python3
import pytest
from time import sleep
import numpy as np

from helper import change_state
from helper import caget_assert, caput_assert

ENPM="%s:%s-PMEn"
CMP0PM="%s:%s-PMCmp0"
CMP1PM="%s:%s-PMCmp1"
XAXISPM="%s:%s-PMXAxis"

CMP0="%s:%s-Cmp0"
CMP1="%s:%s-Cmp1"
XAXIS="%s:%s-XAxis"
XAXISINT="%s:%sXAxis"

EN="%s:%s-En"
ENTRANSF="%s:%s-EnTransf"
SIGMONILOCK="%s:%s-SMonIlckEn"
SIGMONSTREVT="%s:%s-SMonStartEvnt"
SIGMONSTPEVT="%s:%s-SMonStopEvnt"

ENINT="%s:%sEn"
ENTRANSFINT="%s:%sEnTransf"

class TestPM:
    @pytest.mark.parametrize("ch", list(range(0,8)))
    def test_values_dwn(self, prefixdig, ch):
        # Enable Channel transfer
        caput_assert(EN % (prefixdig, "Dwn" + str(ch)), 1)
        caput_assert(ENTRANSF % (prefixdig, "Dwn" + str(ch)), 1)
        # Disable Interlock for channel 0
        caput_assert(SIGMONILOCK % (prefixdig, "AI0"), 0)
        # Enable PM for channel
        caput_assert(ENPM % (prefixdig, "Dwn" + str(ch)), 1)
        # Start acquisition
        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)
        # Enable SigMon Interlock for channel 0
        caput_assert(SIGMONSTREVT % (prefixdig, "AI0"), 0)
        caput_assert(SIGMONSTPEVT % (prefixdig, "AI0"), 1)
        caput_assert(SIGMONILOCK % (prefixdig, "AI0"), 1)
        sleep(1)
        # Check values
        assert np.array_equal(caget_assert(CMP0PM % (prefixdig, "Dwn" + str(ch))), caget_assert(CMP0 % (prefixdig, "Dwn" + str(ch))))
        assert np.array_equal(caget_assert(CMP1PM % (prefixdig, "Dwn" + str(ch))), caget_assert(CMP1 % (prefixdig, "Dwn" + str(ch))))
        assert np.array_equal(caget_assert(XAXISPM % (prefixdig, "Dwn" + str(ch))), caget_assert(XAXIS % (prefixdig, "Dwn" + str(ch))))

        # Disable PM
        caput_assert(ENPM % (prefixdig, "Dwn" + str(ch)), 0)
        # Start acquisition
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)
        change_state(prefixdig, "ON")
        sleep(1)
        # Check values
        assert not np.array_equal(caget_assert(CMP0PM % (prefixdig, "Dwn" + str(ch))), caget_assert(CMP0 % (prefixdig, "Dwn" + str(ch))))
        assert not np.array_equal(caget_assert(CMP1PM % (prefixdig, "Dwn" + str(ch))), caget_assert(CMP1 % (prefixdig, "Dwn" + str(ch))))


    @pytest.mark.parametrize("ch", ["IntChRFCErr", "IntChCavRot", "IntChCavFil", "IntChRFCOut", "IntChFWOut", "IntChILCtrl"])
    def test_values_int(self, prefixdig, ch):
        # Enable Channel transfer
        caput_assert(ENINT % (prefixdig, ch), 1)
        caput_assert(ENTRANSFINT % (prefixdig, ch), 1)
        # Disable Interlock for channel 0
        caput_assert(SIGMONILOCK % (prefixdig, "AI0"), 0)
        # Enable PM for channel
        caput_assert(ENPM % (prefixdig, ch), 1)
        # Start acquisition
        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)
        # Enable SigMon Interlock for channel 0
        caput_assert(SIGMONSTREVT % (prefixdig, "AI0"), 0)
        caput_assert(SIGMONSTPEVT % (prefixdig, "AI0"), 1)
        caput_assert(SIGMONILOCK % (prefixdig, "AI0"), 1)
        sleep(1)
        # Check values
        cmp0 = caget_assert(CMP0 % (prefixdig, ch))
        cmp1 = caget_assert(CMP1 % (prefixdig, ch))
        xaxis = caget_assert(XAXISINT % (prefixdig, ch))
        assert np.array_equal(caget_assert(CMP0PM % (prefixdig, ch)), cmp0)
        assert np.array_equal(caget_assert(CMP1PM % (prefixdig, ch)), cmp1)
        assert np.array_equal(caget_assert(XAXISPM % (prefixdig, ch)), xaxis)


        # Disable PM
        caput_assert(ENPM % (prefixdig, ch), 0)
        # Start acquisition
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)
        change_state(prefixdig, "ON")
        sleep(1)
        # Check values
        if not np.array_equal(caget_assert(CMP0 % (prefixdig, ch)), cmp0):
            assert not np.array_equal(caget_assert(CMP0PM % (prefixdig, ch)), caget_assert(CMP0 % (prefixdig, ch)))
        if not np.array_equal(caget_assert(CMP1 % (prefixdig, ch)), cmp1):
            assert not np.array_equal(caget_assert(CMP1PM % (prefixdig, ch)), caget_assert(CMP1 % (prefixdig, ch)))


    # Test to stress the cbLow queue
    def test_stress(self, prefixdig):
        for ch in range(7):
            caput_assert(EN % (prefixdig, "Dwn" + str(ch)), 1)
            caput_assert(ENTRANSF % (prefixdig, "Dwn" + str(ch)), 1)
            caput_assert(ENPM % (prefixdig, "Dwn" + str(ch)), 1)

        for ch in ["IntChRFCErr", "IntChCavRot", "IntChCavFil", "IntChRFCOut", "IntChFWOut", "IntChILCtrl"]:
            caput_assert(ENINT % (prefixdig, ch), 1)
            caput_assert(ENTRANSFINT % (prefixdig, ch), 1)
            caput_assert(ENPM % (prefixdig, ch), 1)

        # Disable Interlock for channel 0
        caput_assert(SIGMONILOCK % (prefixdig, "AI0"), 0)

        # Start acquisition
        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)
        # Enable SigMon Interlock for channel 0
        caput_assert(SIGMONSTREVT % (prefixdig, "AI0"), 0)
        caput_assert(SIGMONSTPEVT % (prefixdig, "AI0"), 1)
        caput_assert(SIGMONILOCK % (prefixdig, "AI0"), 1)
        sleep(1)

        # reset (if there are some issue it won't be able to reset)
        assert change_state(prefixdig, "RESET")

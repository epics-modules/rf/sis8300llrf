from random import randint, random, uniform
from math import pi, sin, cos, atan2, sqrt
from time import sleep

import pytest
from epics import ca
from helper import check_readback, change_state, sim_bp_trig, caget_assert, \
                   caput_assert,fixed_to_float, float_to_fixed
import numpy as np


IQLIMITS = [-1, 0.999969] # for SP and FF
QMNIQ = (1, 15, 1)

MAGLIMITS = [0, 1]
ANGLIMITS = [-pi, pi] # for SP and FF

#TODO improve this precision
PREC = 4

PVMAG = "%s:%sTbl-Mag"
PVANG = "%s:%sTbl-Ang"
PVMAGRB = "%s:%sTbl-Mag-RB"
PVANGRB = "%s:%sTbl-Ang-RB"
PVI = "%s:%sTbl-I"
PVQ = "%s:%sTbl-Q"
PVIRB = "%s:%sTbl-I-RB"
PVQRB = "%s:%sTbl-Q-RB"
PVWRTTBL = "%s:%sTbl-TblToFW"
PVWRTOK = "%s:%sTbl-WrtOK"

def iq2magang(I, Q):
    mag = sqrt(I**2 + Q**2)
    ang = atan2(Q, I)

    return (mag, ang)

def magang2iq(mag, ang):
    I = mag*cos(ang)
    Q = mag*sin(ang)

    return (I, Q)

class TestPIMagAng:
    @pytest.mark.parametrize("pitype", ["SP", "FF"])
    def test_mag_ang_const(self, prefixdig, pitype):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        # disable calibration
        caput_assert(prefixdig+":RFCtrl" + pitype + "-CalEn", 0)
        mag = uniform(*MAGLIMITS)
        ang = uniform(*ANGLIMITS)

        caput_assert(prefixdig+":RFCtrlCnst" + pitype + "-Mag", mag)
        caput_assert(prefixdig+":RFCtrlCnst" + pitype + "-Ang", ang)

        sleep(0.1)

        I, Q = magang2iq(mag, ang)

        I_rbv = caget_assert(prefixdig+":RFCtrlCnst" + pitype + "-I-RB")
        Q_rbv = caget_assert(prefixdig+":RFCtrlCnst" + pitype + "-Q-RB")

        assert abs(I - I_rbv) < 10**(-1*(PREC))
        assert abs(Q - Q_rbv) < 10**(-1*(PREC))


    """
    This simple test check if is possible to retrieve Mag table before set
    any value on it
    """
    @pytest.mark.parametrize("pitype", ["SP", "FF"])
    def test_get_mag_ang_table(self, prefixdig, pitype):
        caput_assert(PVMAGRB % (prefixdig, pitype) + ".PROC", 1)
        caput_assert(PVANGRB % (prefixdig, pitype) + ".PROC", 1)

    """
    Generate random mag/ang tables with random sizes and check the readback
    tables (mag/ang and i/q) have the same size
    """
    @pytest.mark.parametrize("pitype", ["SP", "FF"])
    def test_table_size(self, prefixdig, pitype):
        # disable calibration
        caput_assert(prefixdig+":RFCtrl" + pitype + "-CalEn", 0)

        # test the same table size
        #max_elem = caget_assert(prefixdig + ":"+ pitype +"Tbl-MaxSmpNm")
        max_elem=20000
        size = randint(2, max_elem)
        mag = []
        ang = []

        for i in range(size):
            mag.append(uniform(*MAGLIMITS))
            ang.append(uniform(*ANGLIMITS))

        caput_assert(PVMAG % (prefixdig, pitype), mag, param_timeout=5)
        caput_assert(PVANG % (prefixdig, pitype), ang, param_timeout=5)

        # write tables
        caput_assert(PVWRTTBL % (prefixdig, pitype), 1, param_timeout=10)
        sleep(0.1)
        wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
        while wrt_ok != 0:
            wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
            sleep(0.1)
        # update readback from mag/ang and I/Q
        caput_assert(PVMAGRB % (prefixdig, pitype) + ".PROC", 1, param_timeout=5)
        caput_assert(PVANGRB % (prefixdig, pitype) + ".PROC", 1, param_timeout=5)
        caput_assert(PVIRB % (prefixdig, pitype) + ".PROC", 1, param_timeout=5)
        caput_assert(PVQRB % (prefixdig, pitype) + ".PROC", 1, param_timeout=5)

        mag_size = caget_assert(PVMAGRB % (prefixdig, pitype) + ".NORD", param_timeout=5)
        ang_size = caget_assert(PVANGRB % (prefixdig, pitype) + ".NORD", param_timeout=5)

        i_size = caget_assert(PVIRB % (prefixdig, pitype) + ".NORD", param_timeout=5)
        q_size = caget_assert(PVQRB % (prefixdig, pitype) + ".NORD", param_timeout=5)

        assert mag_size == size
        assert ang_size == size
        assert i_size == size
        assert q_size == size


        # test different table sizes
        #max_elem = caget_assert(prefixdig + ":"+ pitype +"Tbl-MaxSmpNm")
        max_elem=20000
        mag_size = randint(2, max_elem)
        ang_size= randint(2, max_elem)

        if mag_size > ang_size:
            size = ang_size
        else:
            size = mag_size

        mag = []
        ang = []

        for i in range(mag_size):
            mag.append(uniform(*MAGLIMITS))
        for i in range(ang_size):
            ang.append(uniform(*ANGLIMITS))

        caput_assert(PVMAG % (prefixdig, pitype), mag, param_timeout=5)
        caput_assert(PVANG % (prefixdig, pitype), ang, param_timeout=5)

        # write tables
        caput_assert(PVWRTTBL % (prefixdig, pitype), 1, param_timeout=5)
        sleep(0.1)
        wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
        while wrt_ok != 0:
            wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
            sleep(0.1)

        # update readback from mag/ang and I/Q
        caput_assert(PVMAGRB % (prefixdig, pitype) + ".PROC", 1, param_timeout=5)
        caput_assert(PVANGRB % (prefixdig, pitype) + ".PROC", 1, param_timeout=5)
        caput_assert(PVIRB % (prefixdig, pitype) + ".PROC", 1, param_timeout=5)
        caput_assert(PVQRB % (prefixdig, pitype) + ".PROC", 1, param_timeout=5)

        mag_size_rb = caget_assert(PVMAGRB % (prefixdig, pitype) + ".NORD", param_timeout=5)
        ang_size_rb = caget_assert(PVANGRB % (prefixdig, pitype) + ".NORD", param_timeout=5)

        i_size = caget_assert(PVIRB % (prefixdig, pitype) + ".NORD", param_timeout=5)
        q_size = caget_assert(PVQRB % (prefixdig, pitype) + ".NORD", param_timeout=5)

        assert mag_size_rb == size
        assert ang_size_rb == size
        assert i_size == size
        assert q_size == size


    """
    Check if the readback tables (I/Q and Mag/Ang) have the right values
    """
    @pytest.mark.parametrize("pitype", ["SP", "FF"])
    def test_table_readback(self, prefixdig, pitype):
        # disable calibration
        caput_assert(prefixdig+":RFCtrl" + pitype + "-CalEn", 0)

        # test the same table size
        #max_elem = caget_assert(prefixdig + ":"+ pitype +"Tbl-MaxSmpNm")
        max_elem=20000
        size = randint(2, max_elem)
        mag = []
        ang = []

        for i in range(size):
            mag.append(uniform(*MAGLIMITS))
            ang.append(uniform(*ANGLIMITS))

        caput_assert(PVMAG % (prefixdig, pitype), mag)
        caput_assert(PVANG % (prefixdig, pitype), ang)
        sleep(0.1)

        # write tables
        caput_assert(PVWRTTBL % (prefixdig, pitype), 1, param_timeout=5)
        sleep(0.1)
        wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
        while wrt_ok != 0:
            wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
            sleep(0.1)

        # update readback from mag/ang and I/Q
        caput_assert(PVMAGRB % (prefixdig, pitype) + ".PROC", 1)
        caput_assert(PVANGRB % (prefixdig, pitype) + ".PROC", 1)
        caput_assert(PVIRB % (prefixdig, pitype) + ".PROC", 1)
        caput_assert(PVQRB % (prefixdig, pitype) + ".PROC", 1)

        sleep(0.1)
        i_rbv = caget_assert(PVIRB % (prefixdig, pitype))
        q_rbv = caget_assert(PVQRB % (prefixdig, pitype))
        mag_rbv = caget_assert(PVMAGRB % (prefixdig, pitype))
        ang_rbv = caget_assert(PVANGRB % (prefixdig, pitype))

        for pos in range(size):
            i, q = magang2iq(mag[pos], ang[pos])
            i = fixed_to_float(float_to_fixed(i, *QMNIQ),*QMNIQ)
            q = fixed_to_float(float_to_fixed(q, *QMNIQ),*QMNIQ)
            mag_cur, ang_cur = iq2magang(i, q)

            assert i == i_rbv[pos]
            assert q == q_rbv[pos]
            assert mag_cur  == mag_rbv[pos]
            assert ang_cur  == ang_rbv[pos]


#    @pytest.mark.parametrize("pitype", ["SP", "FF"])
#    def test_linear_waveform(self, prefixdig, pitype):
#        for state in ["RESET", "INIT"]:
#            assert change_state(prefixdig, state)
#
#        # enable calibration
#        caput_assert(prefixdig+":RFCtrl" + pitype + "-CalEn", 1)
#        # set linear
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalLin", 1)
#
#        # generate RAW and EGU arrays
#        nelem = randint(20,1000)
#
#        while nelem == caget_assert(prefixdig+":RFCtrl" + pitype + "CalEGU.NORD"):
#            nelem = randint(20,1000)
#
#        slope = uniform(2, 100)
#        offset = uniform(2, 100)
#
#        print("Slope ", slope, "Offset ", offset)
#
#        raw = []
#        for i in range(nelem):
#            raw.append(random())
#
#        raw = sorted(raw)
#
#        egu = []
#        for i in range(nelem):
#            egu.append(raw[i]*slope + offset)
#
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEGU", egu)
#        ca.poll(0.1)
#        # Might be an error considering the size is different from the last size
#        assert caget_assert(prefixdig+":RFCtrl" + pitype + "CalStat") == 1
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalRaw", raw)
#        ca.poll(0.1)
#        # Check if the calibration didn't generate any error
#        assert caget_assert(prefixdig+":RFCtrl" + pitype + "CalStat") == 0
#
#        max_val = egu[-1]
#        min_val = egu[0]
#
#        mag = uniform(min_val, max_val)
#        ang = uniform(-pi, pi)
#        mag_raw = (mag - offset)/slope
#
#        caput_assert(prefixdig+":RFCtrlCnst" + pitype + "-Mag", mag)
#        caput_assert(prefixdig+":RFCtrlCnst" + pitype + "-Ang", ang)
#
#        I, Q = magang2iq(mag_raw, ang)
#
#        ca.poll(0.1)
#        I_rbv = caget_assert(prefixdig+":RFCtrlCnst" + pitype + "-I-RB")
#        Q_rbv = caget_assert(prefixdig+":RFCtrlCnst" + pitype + "-Q-RB")
#
#        assert abs(I - I_rbv) < 10**(-1*(PREC))
#        assert abs(Q - Q_rbv) < 10**(-1*(PREC))
#
#
#    @pytest.mark.parametrize("pitype", ["SP", "FF"])
#    def test_linear_waveform_invalid(self, prefixdig, pitype):
#        for state in ["RESET", "INIT"]:
#            assert change_state(prefixdig, state)
#
#        # enable calibration
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEn", 1)
#        # set linear
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalLin", 1)
#
#        # generate RAW and EGU arrays
#        nelem = randint(20,1000)
#
#        while nelem == caget_assert(prefixdig+":RFCtrl" + pitype + "CalEGU.NORD"):
#            nelem = randint(20,1000)
#
#        raw = []
#        egu = []
#        for i in range(nelem):
#            raw.append(random())
#            egu.append(random())
#
#        raw = sorted(raw)
#        # Invalid EGU
#        egu = sorted(egu, reverse = True)
#
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEGU", egu)
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalRaw", raw)
#        ca.poll(0.1)
#        # The calibration should have an error, becaues the calibration is
#        # invalid
#        assert caget_assert(prefixdig+":RFCtrl" + pitype + "CalStat") == 1
#
#        max_val = egu[-1]
#        min_val = egu[0]
#
#        mag = uniform(*MAGLIMITS)
#        ang = uniform(*ANGLIMITS)
#
#        caput_assert(prefixdig+":RFCtrlCnst" + pitype + "-Mag", mag)
#        caput_assert(prefixdig+":RFCtrlCnst" + pitype + "-Ang", ang)
#
#        I, Q = magang2iq(mag, ang)
#
#        ca.poll(0.1)
#        I_rbv = caget_assert(prefixdig+":RFCtrlCnst" + pitype + "-I-RB")
#        Q_rbv = caget_assert(prefixdig+":RFCtrlCnst" + pitype + "-Q-RB")
#
#        assert abs(I - I_rbv) < 10**(-1*(PREC))
#        assert abs(Q - Q_rbv) < 10**(-1*(PREC))
#
#
#    @pytest.mark.parametrize("pitype", ["SP", "FF"])
#    def test_non_linear_waveform(self, prefixdig, pitype):
#        for state in ["RESET", "INIT"]:
#            assert change_state(prefixdig, state)
#
#        # enable calibration
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEn", 1)
#        # set non-linear
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalLin", 0)
#
#        # generate RAW and EGU arrays
#        nelem = randint(20,1000)
#        while nelem == caget_assert(prefixdig+":RFCtrl" + pitype + "CalEGU.NORD"):
#            nelem = randint(20,1000)
#
#        raw = []
#        egu = []
#        for i in range(nelem):
#            raw.append(random())
#            egu.append(random())
#
#        raw = sorted(raw)
#        egu = sorted(egu)
#
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEGU", egu)
#        ca.poll(0.1)
#        # Might be an error considering the size is different from the last size
#        assert caget_assert(prefixdig+":RFCtrl" + pitype + "CalStat") == 1
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalRaw", raw)
#        ca.poll(0.1)
#        # Check if the calibration didn't generate any error
#        assert caget_assert(prefixdig+":RFCtrl" + pitype + "CalStat") == 0
#        ca.poll(0.1)
#
#        max_val = egu[-1]
#        min_val = egu[0]
#
#        mag = uniform(min_val, max_val)
#        ang = uniform(-pi, pi)
#
#        check_readback(prefixdig+":RFCtrlCnst" + pitype + "-Mag", mag)
#        check_readback(prefixdig+":RFCtrlCnst" + pitype + "-Ang", ang)
#
#    @pytest.mark.parametrize("pitype", ["SP", "FF"])
#    def test_non_linear_waveform_invalid(self, prefixdig, pitype):
#        for state in ["RESET", "INIT"]:
#            assert change_state(prefixdig, state)
#
#        # enable calibration
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEn", 1)
#        # set non-linear
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalLin", 0)
#
#        # generate RAW and EGU arrays
#        nelem = randint(20,1000)
#        while nelem == caget_assert(prefixdig+":RFCtrl" + pitype + "CalEGU.NORD"):
#            nelem = randint(20,1000)
#
#        raw = []
#        egu = []
#        for i in range(nelem):
#            raw.append(random())
#            egu.append(random())
#
#        raw = sorted(raw)
#        # Invalid EGU
#        egu = sorted(egu, reverse = True)
#
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEGU", egu)
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalRaw", raw)
#        ca.poll(0.1)
#        # Check if the calibration generate an error (it should)
#        assert caget_assert(prefixdig+":RFCtrl" + pitype + "CalStat") == 1
#        ca.poll(0.1)
#
#        mag = uniform(*MAGLIMITS)
#        ang = uniform(*ANGLIMITS)
#
#        caput_assert(prefixdig+":RFCtrlCnst" + pitype + "-Mag", mag)
#        caput_assert(prefixdig+":RFCtrlCnst" + pitype + "-Ang", ang)
#
#        I, Q = magang2iq(mag, ang)
#
#        ca.poll(0.1)
#        I_rbv = caget_assert(prefixdig+":RFCtrlCnst" + pitype + "-I-RB")
#        Q_rbv = caget_assert(prefixdig+":RFCtrlCnst" + pitype + "-Q-RB")
#
#        # With an invalid tables the values shouldn't be calibrated
#
#        assert abs(I - I_rbv) < 10**(-1*(PREC))
#        assert abs(Q - Q_rbv) < 10**(-1*(PREC))
#
#    """
#    Check if the readback tables (I/Q and Mag/Ang) have the right values
#    """
#    @pytest.mark.parametrize("pitype", ["SP", "FF"])
#    def test_table_readback_calibrated(self, prefixdig, pitype):
#        for state in ["RESET", "INIT"]:
#            assert change_state(prefixdig, state)
#
#        # enable calibration
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEn", 1)
#        # set linear
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalLin", 1)
#
#        # generate RAW and EGU arrays
#        nelem = randint(20,1000)
#
#        while nelem == caget_assert(prefixdig+":RFCtrl" + pitype + "CalEGU.NORD"):
#            nelem = randint(20,1000)
#
#        slope = uniform(2, 100)
#        offset = uniform(2, 100)
#
#        raw = []
#        for i in range(nelem):
#            raw.append(random())
#
#        raw = sorted(raw)
#
#        egu = []
#        for i in range(nelem):
#            egu.append(raw[i]*slope + offset)
#
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEGU", egu)
#        sleep(0.1)
#        # Should have an error because EGU size is different from Raw size
#        assert caget_assert(prefixdig+":RFCtrl" + pitype + "CalStat") == 1
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalRaw", raw)
#        sleep(0.1)
#        # Check if the calibration didn't generate any error
#        assert caget_assert(prefixdig+":RFCtrl" + pitype + "CalStat") == 0
#
#        # test the same table size
#        #max_elem = caget_assert(prefixdig + ":"+ pitype +"Tbl-MaxSmpNm")
#        max_elem=20000
#        size = randint(2, max_elem)
#        mag = []
#        ang = []
#
#        mag_limits = (MAGLIMITS[0] * slope + offset, MAGLIMITS[1] * slope + offset)
#        for i in range(size):
#            mag.append(uniform(*mag_limits))
#            ang.append(uniform(*ANGLIMITS))
#
#        caput_assert(PVMAG % (prefixdig, pitype), mag)
#        caput_assert(PVANG % (prefixdig, pitype), ang)
#        sleep(0.1)
#
#        # write tables
#        caput_assert(PVWRTTBL % (prefixdig, pitype), 1)
#        sleep(0.1)
#        wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
#        while wrt_ok != 0:
#            wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
#            sleep(0.1)
#
#        # update readback from mag/ang and I/Q
#        caput_assert(PVMAGRB % (prefixdig, pitype) + ".PROC", 1)
#        caput_assert(PVANGRB % (prefixdig, pitype) + ".PROC", 1)
#        caput_assert(PVIRB % (prefixdig, pitype) + ".PROC", 1)
#        caput_assert(PVQRB % (prefixdig, pitype) + ".PROC", 1)
#
#        sleep(0.1)
#        i_rbv = caget_assert(PVIRB % (prefixdig, pitype))
#        q_rbv = caget_assert(PVQRB % (prefixdig, pitype))
#        mag_rbv = caget_assert(PVMAGRB % (prefixdig, pitype))
#        ang_rbv = caget_assert(PVANGRB % (prefixdig, pitype))
#
#        for pos in range(size):
#            i, q = magang2iq((mag[pos] - offset)/slope, ang[pos])
#            i = fixed_to_float(float_to_fixed(i, *QMNIQ),*QMNIQ)
#            q = fixed_to_float(float_to_fixed(q, *QMNIQ),*QMNIQ)
#            mag_cur, ang_cur = iq2magang(i, q)
#            mag_cur = mag_cur*slope + offset
#
#            assert i == i_rbv[pos]
#            assert q == q_rbv[pos]
#            assert abs(mag_cur - mag_rbv[pos]) < 10**-12
#            assert ang_cur  == ang_rbv[pos]
#
#
#    """
#    Check if the table has the valid not changed if the calibration
#    is invalid
#    """
#    @pytest.mark.parametrize("pitype", ["SP", "FF"])
#    def test_table_readback_calibrated_invalid(self, prefixdig, pitype):
#        for state in ["RESET", "INIT"]:
#            assert change_state(prefixdig, state)
#
#        # enable calibration
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEn", 1)
#        # set linear
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalLin", 1)
#
#        # generate RAW and EGU arrays
#        nelem = randint(20,1000)
#
#        while nelem == caget_assert(prefixdig+":RFCtrl" + pitype + "CalEGU.NORD"):
#            nelem = randint(20,1000)
#
#        raw = []
#        egu = []
#        for i in range(nelem):
#            raw.append(random())
#            egu.append(random())
#
#        raw = sorted(raw)
#        # Invalid EGU
#        egu = sorted(egu, reverse = True)
#
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEGU", egu)
#        caput_assert(prefixdig+":RFCtrl" + pitype + "CalRaw", raw)
#        sleep(0.1)
#        # Should have an error status
#        assert caget_assert(prefixdig+":RFCtrl" + pitype + "CalStat") == 1
#
#        # test the same table size
#        #max_elem = caget_assert(prefixdig + ":"+ pitype +"Tbl-MaxSmpNm")
#        max_elem=20000
#        size = randint(2, max_elem)
#        mag = []
#        ang = []
#
#        for i in range(size):
#            mag.append(uniform(*MAGLIMITS))
#            ang.append(uniform(*ANGLIMITS))
#
#        caput_assert(PVMAG % (prefixdig, pitype), mag)
#        caput_assert(PVANG % (prefixdig, pitype), ang)
#        sleep(0.1)
#
#        # write tables
#        caput_assert(PVWRTTBL % (prefixdig, pitype), 1)
#        sleep(0.1)
#        wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
#        while wrt_ok != 0:
#            wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
#            sleep(0.1)
#
#
#        # update readback from mag/ang and I/Q
#        caput_assert(PVMAGRB % (prefixdig, pitype) + ".PROC", 1)
#        caput_assert(PVANGRB % (prefixdig, pitype) + ".PROC", 1)
#        caput_assert(PVIRB % (prefixdig, pitype) + ".PROC", 1)
#        caput_assert(PVQRB % (prefixdig, pitype) + ".PROC", 1)
#
#        sleep(0.1)
#        i_rbv = caget_assert(PVIRB % (prefixdig, pitype))
#        q_rbv = caget_assert(PVQRB % (prefixdig, pitype))
#        mag_rbv = caget_assert(PVMAGRB % (prefixdig, pitype))
#        ang_rbv = caget_assert(PVANGRB % (prefixdig, pitype))
#
#        for pos in range(size):
#            i, q = magang2iq(mag[pos], ang[pos])
#            i = fixed_to_float(float_to_fixed(i, *QMNIQ),*QMNIQ)
#            q = fixed_to_float(float_to_fixed(q, *QMNIQ),*QMNIQ)
#            mag_cur, ang_cur = iq2magang(i, q)
#
#            assert i == i_rbv[pos]
#            assert q == q_rbv[pos]
#            assert abs(mag_cur - mag_rbv[pos]) < 10**-12
#            assert ang_cur  == ang_rbv[pos]

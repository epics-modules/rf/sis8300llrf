from helper import caget_assert
from helper import change_state, check_readback
import pytest

class TestTableMode:
    @pytest.mark.parametrize("pitype", ["SP", "FF"])
    def test_table_mode(self, prefixdig, pitype):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        assert check_readback(prefixdig + ":" + pitype + "Tbl-Mode", 1)
        assert caget_assert(prefixdig + ":" + pitype + "Tbl-Mode.STAT") == 0
        assert caget_assert(prefixdig + ":" + pitype + "Tbl-Mode.SEVR") == 0
        assert check_readback(prefixdig + ":" + pitype + "Tbl-Mode", 0)
        assert caget_assert(prefixdig + ":" + pitype + "Tbl-Mode.STAT") == 0
        assert caget_assert(prefixdig + ":" + pitype + "Tbl-Mode.SEVR") == 0

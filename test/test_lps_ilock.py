from time import sleep

from helper import caget_assert, caput_assert
from helper import change_state, write_reg, sim_bp_trig, TRIG_REG, read_reg

class TestLPSIlock:
    def test_simulated(self, prefixdig, board, ssh):
        caput_assert(prefixdig + ":LPSIlockDeadTime", 0.1) # set a short dead time to get an interlock
        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        write_reg(board, "0xF05", "0x2", ssh)
        write_reg(board, "0xF06", "0x0", ssh)

        sim_bp_trig(board, ssh)

        write_reg(board, "0xF06", "0x2", ssh)
        sleep(1)
        assert caget_assert(prefixdig + ":FSMFirmware") == 14 # Interlock
        assert caget_assert(prefixdig + ":IlckCause") == 3

        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        sleep(1)
        assert caget_assert(prefixdig + ":IlckCause") == 0

    def test_fast_ilock(self, prefixdig, board, ssh):
        caput_assert(prefixdig + ":LPSIlockDeadTime", 0) # always fast ilock
        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)


        #start pulse
        write_reg(board, TRIG_REG, "0x10", ssh)
        sleep(0.001)
        write_reg(board, TRIG_REG, "0x20", ssh)
        sleep(0.001)

        f05_bkp = read_reg(board, "0xF05", ssh)
        write_reg(board, "0xF05", "0x2", ssh)
        write_reg(board, "0xF06", "0x0", ssh)
        write_reg(board, "0xF06", "0x2", ssh)
        write_reg(board, "0xF05", f05_bkp, ssh)

        assert caget_assert(prefixdig + ":TrigILock") == 1
        assert caget_assert(prefixdig + ":IlckCause") == 0 # no interlock

        write_reg(board, "0xF05", "0x2", ssh)
        write_reg(board, "0xF06", "0x2", ssh)

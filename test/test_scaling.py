from math import nan
from random import randint
from time import sleep

import pytest
import numpy as np
from helper import caget_assert, caput_assert
from helper import generate_calib_table_linear, generate_calib_table,\
                   PREC, PREC_SLOPE, PREC_OFFSET, change_state, sim_bp_trig

@pytest.mark.parametrize("ch", list(range(0,8)))
def test_linear_calib(prefixdig, ch, max_elem):
    # generate random calibration table
    slope, offset, egu, raw = generate_calib_table_linear(max_elem)
    caput_assert(prefixdig + (":AI%d-CalEGU") % ch, egu)
    caput_assert(prefixdig + (":AI%d-CalRaw") % ch, raw)
    caput_assert(prefixdig + (":AI%d-CalOrd") % ch, 1)

    coef = caget_assert(prefixdig + (":AI%d-CalCoef") % ch)

    assert abs(coef[0] - offset) < 10**-12
    assert abs(coef[1] - slope)  < 10**-12

## Test enable / disable
@pytest.mark.parametrize("ch", list(range(0,8)))
def test_en_dis(prefixdig, ch, max_elem):
    slope, offset, egu, raw = generate_calib_table_linear(max_elem)
    caput_assert(prefixdig + (":AI%d-CalEGU") % ch, egu)
    caput_assert(prefixdig + (":AI%d-CalRaw") % ch, raw)
    caput_assert(prefixdig + (":AI%d-CalOrd") % ch, 1)
    caput_assert(prefixdig + (":AI%d-CalEn") % ch, 1)

    assert caget_assert(prefixdig + (":AI%d-CalEn-RB") % ch) == 1

    caput_assert(prefixdig + (":AI%d-CalEGU") % ch, [0])

    assert caget_assert(prefixdig + (":AI%d-CalEn-RB") % ch) == 0

    caput_assert(prefixdig + (":AI%d-CalEGU") % ch, egu)
    caput_assert(prefixdig + (":AI%d-CalRaw") % ch, raw)
    caput_assert(prefixdig + (":AI%d-CalEn") % ch, 0)
    caput_assert(prefixdig + (":AI%d-CalEn") % ch, 1)
    sleep(0.1)
    assert caget_assert(prefixdig + (":AI%d-CalEn-RB") % ch) == 1

## Test a real curve
@pytest.mark.parametrize("ch", list(range(0,8)))
def test_curve(prefixdig, ch, max_elem):
    order, coef, egu, raw = generate_calib_table(max_elem)
    caput_assert(prefixdig + (":AI%d-CalEGU") % ch, egu)
    caput_assert(prefixdig + (":AI%d-CalRaw") % ch, raw)
    caput_assert(prefixdig + (":AI%d-CalOrd") % ch, order)
    caput_assert(prefixdig + (":AI%d-CalEn") % ch, 1)

    sleep(0.1)

    assert caget_assert(prefixdig + (":AI%d-CalEn-RB") % ch) == 1
    calc_coef = caget_assert(prefixdig + (":AI%d-CalCoef") % ch)
    for i in range(order):
        abs(coef[i] - calc_coef[i]) < 10**-9


## Test calibration using value output on VM
def test_calibration_vm(prefixdig, prefixrtm, max_elem, prefixisland, vm_ch = 7):
    # Prepare EVR Island
    caput_assert(prefixisland + ":OpMode", "Island")
    caput_assert(prefixisland + ":CycleFreq-SP", 14)
    caput_assert(prefixisland + ":Mode", "Free Running")
    # prepare IOC to output some value
    caput_assert(prefixrtm + ":RFOutEn", 1)
    caput_assert(prefixrtm + ":RFOutAtt", 0)
    caput_assert(prefixdig + ":OpenLoop", 1)
    caput_assert(prefixdig + ":RFCtrlCnstFF-I", 0.9)
    caput_assert(prefixdig + ":RFCtrlCnstFFEn", 1)
    caput_assert(prefixdig + ":Dwn%d-En" % vm_ch, 1)
    caput_assert(prefixdig + ":Dwn%d-EnTransf" % vm_ch, 1)

    #Disable calibration
    caput_assert(prefixdig + (":AI%d-CalEn") % vm_ch, 0)
    # put system at ON state
    for state in ["RESET", "INIT", "ON"]:
        assert change_state(prefixdig, state)

    maxi = np.max(caget_assert(prefixdig + ":Dwn%d-Cmp0" % vm_ch))
    #avg = np.average(caget_assert(prefixdig + ":Dwn%d-Cmp0" % vm_ch)[50:100])

    # apply calibration
    order, coef, egu, raw = generate_calib_table(max_elem)
    caput_assert(prefixdig + (":AI%d-CalEGU") % vm_ch, egu)
    caput_assert(prefixdig + (":AI%d-CalRaw") % vm_ch, raw)
    caput_assert(prefixdig + (":AI%d-CalOrd") % vm_ch, order)
    caput_assert(prefixdig + (":AI%d-CalEn") % vm_ch, 1)

    sleep(1)

    assert caget_assert(prefixdig + (":AI%d-CalEn-RB") % vm_ch) == 1

    max_calibrated = np.max(caget_assert(prefixdig + ":Dwn%d-Cmp0" % vm_ch))

    #avg_calibrated = np.average(caget_assert(prefixdig + ":Dwn%d-Cmp0" % vm_ch)[50:100])

    #print(avg)
    print(coef)

    #assert (np.polyval(coef,avg) - avg_calibrated) < 10**-3
    assert (np.polyval(coef,maxi) - max_calibrated) < 10**-3


    caput_assert(prefixisland + ":Mode", "On Demand")

# run an acquisition
#    sim_bp_trig(board, ssh)
#    sleep(0.1)


#@pytest.mark.parametrize("ch", list(range(0,10)))
#def test_fitted_line(prefixdig, ch, file_name, max_elem, ssh):
#    """
#    check if fitted line is right
#    """
#    generate_csv_rand(file_name, max_elem, ssh)
#
#    caput_assert(prefixdig + (":AI%d-CalCSV") % ch, file_name)
#    sleep(0.1)
#
#    # check if the number of elements is correct
#    n_elem = caget_assert(prefixdig + (":AI%d-CalRaw.NORD") % ch)
#    assert n_elem == max_elem
#
#    # check if the last element on fitted line is correct
#    slope = caget_assert(prefixdig + (":AI%d-LinCvF-RB") % ch)
#    assert slope != nan
#
#    offset = caget_assert(prefixdig + (":AI%d-LinCvO-RB") % ch)
#    assert offset != nan
#
#    elems = caget_assert(prefixdig + (":AI%d-CalFit") % ch)
#    assert len(elems) == max_elem
#
#    last_elem = elems[-1]
#
#    elems_dig = caget_assert(prefixdig + (":AI%d-CalRaw") % ch)
#    assert len(elems_dig) == max_elem
#
#    last_elem_dig = elems_dig[-1]
#
#    diff = abs(last_elem - (last_elem_dig*slope + offset))
#
#    assert diff < 10**-(PREC-1)
#
#
#@pytest.mark.parametrize("ch", list(range(0,10)))
#def test_one_line(prefixdig, ch, file_name, ssh):
#    generate_csv(file_name, 1, ssh)
#
#    caput_assert(prefixdig + (":AI%d-CalCSV") % ch, file_name)
#    sleep(0.1)
#
#    slope = caget_assert(prefixdig + (":AI%d-LinCvF-RB") % ch)
#    assert slope != nan
#
#    offset = caget_assert(prefixdig + (":AI%d-LinCvO-RB") % ch)
#    assert offset != nan
#
#    assert slope == 1 and offset == 0
#
#
#@pytest.mark.parametrize("ch", list(range(0,10)))
#def test_unorderd_csv(prefixdig, ch, file_name, max_elem, ssh):
#    generate_csv_unordered(file_name, max_elem, ssh)
#
#    caput_assert(prefixdig + (":AI%d-CalCSV") % ch, file_name)
#    sleep(0.1)
#
#    stat = caget_assert(prefixdig + (":AI%d-CalCSV.STAT") % ch)
#    sevr = caget_assert(prefixdig + (":AI%d-CalCSV.SEVR") % ch)
#
#    slope = caget_assert(prefixdig + (":AI%d-LinCvF-RB") % ch)
#    assert slope != nan
#
#    offset = caget_assert(prefixdig + (":AI%d-LinCvO-RB") % ch)
#    assert offset != nan
#
#    assert stat == 2 and sevr == 3 and slope == 1 and offset == 0
#
#
#@pytest.mark.parametrize("ch", list(range(0,10)))
#def test_last_lines_blank(prefixdig, ch, file_name, max_elem, ssh):
#    (slope, offset, _, _) = generate_csv(file_name, max_elem, ssh)
#
#    # add n blank lines at the end
#    with open(file_name, "a") as f:
#        f.seek(0, 2)
#        for i in range(randint(1, max_elem)):
#            f.write("\n")
#
#    caput_assert(prefixdig + (":AI%d-CalCSV") % ch, file_name)
#    sleep(0.1)
#
#    slope_IOC = caget_assert(prefixdig + (":AI%d-LinCvF-RB") % ch)
#    assert slope != nan
#
#    offset_IOC = caget_assert(prefixdig + (":AI%d-LinCvO-RB") % ch)
#    assert offset != nan
#
#    diff_slope = abs(slope - slope_IOC)
#    diff_offset = abs(offset - offset_IOC)
#    print("slope ioc ", slope_IOC)
#    print("offset ioc ", offset_IOC)
#
#    assert diff_slope < 10**-(PREC_SLOPE-1)
#    assert diff_offset < 10**-(PREC_OFFSET-1)

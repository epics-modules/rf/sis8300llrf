from random import randint, random, uniform
from math import pi, sin, cos, atan2, sqrt
from time import sleep

import pytest
from epics import ca
from helper import check_readback, change_state, sim_bp_trig, caget_assert, \
                   caput_assert,fixed_to_float, float_to_fixed
import numpy as np

MAXELEM = 50000
MAXCALELEM = 5000

IQLIMITS = [-1, 0.999969] # for SP and FF
QMNIQ = (1, 15, 1)

MAGLIMITS = [0, 1]
ANGLIMITS = [-pi, pi] # for SP and FF

#TODO improve this precision
PREC = 4

PVMAG = "%s:%sTbl-Mag"
PVANG = "%s:%sTbl-Ang"
PVMAGRB = "%s:%sTbl-Mag-RB"
PVANGRB = "%s:%sTbl-Ang-RB"
PVI = "%s:%sTbl-I"
PVQ = "%s:%sTbl-Q"
PVIRB = "%s:%sTbl-I-RB"
PVQRB = "%s:%sTbl-Q-RB"
PVWRTTBL = "%s:%sTbl-TblToFW"
PVWRTOK = "%s:%sTbl-WrtOK"

PVEGUCALIB= "%s:RFCtrl%sCalEGU"
PVRAWCALIB= "%s:RFCtrl%sCalRaw"

def iq2magang(I, Q):
    mag = sqrt(I**2 + Q**2)
    ang = atan2(Q, I)

    return (mag, ang)

def magang2iq(mag, ang):
    I = mag*cos(ang)
    Q = mag*sin(ang)

    return (I, Q)


def prepare_calibration(prefixdig, pitype, order):
    for state in ["RESET", "INIT"]:
        assert change_state(prefixdig, state)

    # Disable calibration
    caput_assert(prefixdig+":RFCtrl" + pitype + "-CalEn", 0)

    # generate RAW and EGU arrays
    nelem = randint(20, MAXCALELEM)
    slope = uniform(2, 100)
    offset = uniform(2, 100)

    raw = sorted([random() for i in range(nelem)])
    egu = sorted([raw[i]*slope + offset for i in range(nelem)])

    caput_assert(prefixdig+":RFCtrl" + pitype + "-CalEGU", egu)
    caput_assert(prefixdig+":RFCtrl" + pitype + "-CalRaw", raw)
    caput_assert(prefixdig+":RFCtrl" + pitype + "-CalOrdE2R", order)
    caput_assert(prefixdig+":RFCtrl" + pitype + "-CalOrdR2E", order)

    return (slope, offset, egu, raw)

class TestPICalibTables:
    """Test to check if some segmentation fault happen when
    small table size is used"""
    @pytest.mark.parametrize("pitype", ["SP", "FF"])
    def test_small_tables(self, prefixdig, pitype):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)
        # Disable calibration
        caput_assert(prefixdig+":RFCtrl" + pitype + "-CalEn", 0)
        sizes = [1, 10, 100, 1000]
        for size in sizes:
            # test the same table size
            mag = []
            ang = []

            for i in range(size):
                mag.append(uniform(*MAGLIMITS))
                ang.append(uniform(*ANGLIMITS))

            caput_assert(PVMAG % (prefixdig, pitype), mag)
            caput_assert(PVANG % (prefixdig, pitype), ang)
            sleep(1)
            caput_assert(PVWRTTBL % (prefixdig, pitype), 1)
            sleep(0.1)
            wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
            while wrt_ok != 0:
                wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
                sleep(0.1)
            caput_assert(PVIRB % (prefixdig, pitype) + ".PROC", 1)
            caput_assert(PVQRB % (prefixdig, pitype) + ".PROC", 1)
            sleep(1)
            caput_assert(PVMAGRB % (prefixdig, pitype) + ".PROC", 1)
            caput_assert(PVANGRB % (prefixdig, pitype) + ".PROC", 1)

    """
    """
    @pytest.mark.parametrize("pitype", ["SP", "FF"])
    def test_mag_ang_readback(self, prefixdig, pitype):
        # Disable calibration
        caput_assert(prefixdig+":RFCtrl" + pitype + "-CalEn", 0)

        for max_size in range(2, 32):
            # test the same table size
            size = randint(2, max_size)
            mag = []
            ang = []

            for i in range(size):
                mag.append(uniform(*MAGLIMITS))
                ang.append(uniform(*ANGLIMITS))

            caput_assert(PVMAG % (prefixdig, pitype), mag)
            caput_assert(PVANG % (prefixdig, pitype), ang)
            sleep(1)

            # write tables
            caput_assert(PVWRTTBL % (prefixdig, pitype), 1)
            sleep(0.1)
            wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
            while wrt_ok != 0:
                wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
                sleep(0.1)
            # update readback from mag/ang and I/Q
            caput_assert(PVMAGRB % (prefixdig, pitype) + ".PROC", 1)
            caput_assert(PVANGRB % (prefixdig, pitype) + ".PROC", 1)

            sleep(1)
            mag_rbv = caget_assert(PVMAGRB % (prefixdig, pitype))
            ang_rbv = caget_assert(PVANGRB % (prefixdig, pitype))

            for pos in range(size):
                i, q = magang2iq(mag[pos], ang[pos])
                i = fixed_to_float(float_to_fixed(i, *QMNIQ),*QMNIQ)
                q = fixed_to_float(float_to_fixed(q, *QMNIQ),*QMNIQ)
                mag_cur, ang_cur = iq2magang(i, q)
                assert mag_cur  == mag_rbv[pos]
                assert ang_cur  == ang_rbv[pos]


    """
    This simple test check if is possible to retrieve Mag table before set
    any value on it
    """
    @pytest.mark.parametrize("pitype", ["SP", "FF"])
    def test_get_mag_ang_table(self, prefixdig, pitype):
        caput_assert(PVMAGRB % (prefixdig, pitype) + ".PROC", 1)
        caput_assert(PVANGRB % (prefixdig, pitype) + ".PROC", 1)

    """
    Generate random mag/ang tables with random sizes and check the readback
    tables (mag/ang and i/q) have the same size
    """
    @pytest.mark.parametrize("pitype", ["SP", "FF"])
    def test_table_size(self, prefixdig, pitype):
        # Disable calibration
        caput_assert(prefixdig+":RFCtrl" + pitype + "-CalEn", 0)

        # test the same table size
        size = randint(2, MAXELEM)
        mag = []
        ang = []

        for i in range(size):
            mag.append(uniform(*MAGLIMITS))
            ang.append(uniform(*ANGLIMITS))

        caput_assert(PVMAG % (prefixdig, pitype), mag)
        caput_assert(PVANG % (prefixdig, pitype), ang)

        # write tables
        caput_assert(PVWRTTBL % (prefixdig, pitype), 1)
        sleep(0.1)
        wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
        while wrt_ok != 0:
            wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
            sleep(0.1)
        # update readback from mag/ang and I/Q
        caput_assert(PVMAGRB % (prefixdig, pitype) + ".PROC", 1)
        caput_assert(PVANGRB % (prefixdig, pitype) + ".PROC", 1)
        sleep(1)
        caput_assert(PVIRB % (prefixdig, pitype) + ".PROC", 1)
        caput_assert(PVQRB % (prefixdig, pitype) + ".PROC", 1)

        sleep(1)
        mag_size = caget_assert(PVMAGRB % (prefixdig, pitype) + ".NORD")
        ang_size = caget_assert(PVANGRB % (prefixdig, pitype) + ".NORD")

        i_size = caget_assert(PVIRB % (prefixdig, pitype) + ".NORD")
        q_size = caget_assert(PVQRB % (prefixdig, pitype) + ".NORD")

        assert mag_size == size
        assert ang_size == size
        assert i_size == size
        assert q_size == size


        # test different table sizes
        mag_size = randint(2, MAXELEM)
        ang_size= randint(2, MAXELEM)

        if mag_size > ang_size:
            size = ang_size
        else:
            size = mag_size

        mag = []
        ang = []

        for i in range(mag_size):
            mag.append(uniform(*MAGLIMITS))
        for i in range(ang_size):
            ang.append(uniform(*ANGLIMITS))

        caput_assert(PVMAG % (prefixdig, pitype), mag)
        caput_assert(PVANG % (prefixdig, pitype), ang)

        sleep(1)
        # write tables
        caput_assert(PVWRTTBL % (prefixdig, pitype), 1)
        sleep(0.1)
        wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
        while wrt_ok != 0:
            wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
            sleep(0.1)
        # update readback from mag/ang and I/Q
        caput_assert(PVMAGRB % (prefixdig, pitype) + ".PROC", 1)
        caput_assert(PVANGRB % (prefixdig, pitype) + ".PROC", 1)
        sleep(1)
        caput_assert(PVIRB % (prefixdig, pitype) + ".PROC", 1)
        caput_assert(PVQRB % (prefixdig, pitype) + ".PROC", 1)

        sleep(1)
        mag_size_rb = caget_assert(PVMAGRB % (prefixdig, pitype) + ".NORD")
        ang_size_rb = caget_assert(PVANGRB % (prefixdig, pitype) + ".NORD")

        i_size = caget_assert(PVIRB % (prefixdig, pitype) + ".NORD")
        q_size = caget_assert(PVQRB % (prefixdig, pitype) + ".NORD")

        assert mag_size_rb == size
        assert ang_size_rb == size
        assert i_size == size
        assert q_size == size


    """
    Check if the readback tables (I/Q and Mag/Ang) have the right values
    """
    @pytest.mark.parametrize("pitype", ["SP", "FF"])
    def test_readback(self, prefixdig, pitype):
        # Disable calibration
        caput_assert(prefixdig+":RFCtrl" + pitype + "-CalEn", 0)

        # test the same table size
        size = randint(2, MAXELEM)
        mag = []
        ang = []

        for i in range(size):
            mag.append(uniform(*MAGLIMITS))
            ang.append(uniform(*ANGLIMITS))

        caput_assert(PVMAG % (prefixdig, pitype), mag)
        caput_assert(PVANG % (prefixdig, pitype), ang)
        sleep(1)

        # write tables
        caput_assert(PVWRTTBL % (prefixdig, pitype), 1)
        sleep(0.1)
        wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
        while wrt_ok != 0:
            wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
            sleep(0.1)
        # update readback from mag/ang and I/Q
        sleep(1)
        caput_assert(PVMAGRB % (prefixdig, pitype) + ".PROC", 1)
        caput_assert(PVANGRB % (prefixdig, pitype) + ".PROC", 1)
        sleep(1)
        caput_assert(PVIRB % (prefixdig, pitype) + ".PROC", 1)
        caput_assert(PVQRB % (prefixdig, pitype) + ".PROC", 1)

        sleep(1)
        i_rbv = caget_assert(PVIRB % (prefixdig, pitype))
        q_rbv = caget_assert(PVQRB % (prefixdig, pitype))
        mag_rbv = caget_assert(PVMAGRB % (prefixdig, pitype))
        ang_rbv = caget_assert(PVANGRB % (prefixdig, pitype))

        for pos in range(size):
            i, q = magang2iq(mag[pos], ang[pos])
            i = fixed_to_float(float_to_fixed(i, *QMNIQ),*QMNIQ)
            q = fixed_to_float(float_to_fixed(q, *QMNIQ),*QMNIQ)
            mag_cur, ang_cur = iq2magang(i, q)

            assert i == i_rbv[pos]
            assert q == q_rbv[pos]
            assert mag_cur  == mag_rbv[pos]
            assert ang_cur  == ang_rbv[pos]



    """
    Check if the readback tables (I/Q and Mag/Ang) have the right values
    """
    @pytest.mark.parametrize("pitype", ["SP", "FF"])
    def test_readback_calibrated(self, prefixdig, pitype):
        slope, offset, _, _ = prepare_calibration(prefixdig, pitype, order=1)
        sleep(0.1)
        caput_assert(prefixdig+":RFCtrl" + pitype + "-CalEn", 1)
        sleep(0.1)
        assert caget_assert(prefixdig+":RFCtrl" + pitype + "-CalEn-RB") == 1

        # test the same table size
        size = randint(2, MAXELEM)
        mag = []
        ang = []

        mag_limits = (MAGLIMITS[0] * slope + offset, MAGLIMITS[1] * slope + offset)
        for i in range(size):
            mag.append(uniform(*mag_limits))
            ang.append(uniform(*ANGLIMITS))

        caput_assert(PVMAG % (prefixdig, pitype), mag)
        caput_assert(PVANG % (prefixdig, pitype), ang)
        sleep(1)

        # write tables
        caput_assert(PVWRTTBL % (prefixdig, pitype), 1)
        sleep(0.1)
        wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
        while wrt_ok != 0:
            wrt_ok = caget_assert(PVWRTOK % (prefixdig, pitype))
            sleep(0.1)
        # update readback from mag/ang and I/Q
        caput_assert(PVMAGRB % (prefixdig, pitype) + ".PROC", 1)
        caput_assert(PVANGRB % (prefixdig, pitype) + ".PROC", 1)
        sleep(1)
        caput_assert(PVIRB % (prefixdig, pitype) + ".PROC", 1)
        caput_assert(PVQRB % (prefixdig, pitype) + ".PROC", 1)

        sleep(1)
        i_rbv = caget_assert(PVIRB % (prefixdig, pitype))
        q_rbv = caget_assert(PVQRB % (prefixdig, pitype))
        mag_rbv = caget_assert(PVMAGRB % (prefixdig, pitype))
        ang_rbv = caget_assert(PVANGRB % (prefixdig, pitype))

        for pos in range(0, size, 10):
            i, q = magang2iq((mag[pos] - offset)/slope, ang[pos])
            i = fixed_to_float(float_to_fixed(i, *QMNIQ),*QMNIQ)
            q = fixed_to_float(float_to_fixed(q, *QMNIQ),*QMNIQ)
            mag_cur, ang_cur = iq2magang(i, q)
            mag_cur = mag_cur*slope + offset

            assert i == i_rbv[pos]
            assert q == q_rbv[pos]
            assert abs(mag_cur - mag_rbv[pos]) < 10**-9
            assert ang_cur  == ang_rbv[pos]

        # Disable calibration after tests
        caput_assert(prefixdig+":RFCtrl" + pitype + "-CalEn", 0)

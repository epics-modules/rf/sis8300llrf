import pytest
from random import randrange
from time import sleep

from helper import caget_assert
from helper import change_state, sim_bp_trig

class TestSimBPTrig:
    @pytest.mark.parametrize("pulses", [50, 49, randrange(1, 50)])
    def test_bp_trig(self, board, prefixdig, pulses, ssh):
        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        for i in range(pulses):
            sim_bp_trig(board, ssh)

        sleep(0.1)
        res = caget_assert(prefixdig + ":PulseDoneCnt")
        assert pulses == res

        sleep(0.1)
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

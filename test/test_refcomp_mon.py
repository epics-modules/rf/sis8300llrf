from random import randint

from helper import check_readback, change_state, sim_bp_trig
from helper import caget_assert

class TestRefLineMonitor:
    def test_basic(self, prefixdig, board, ssh):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        #TODO improve this values
        avgpos = randint(1,100)
        avgwidth = randint(100, 10000)

        check_readback(prefixdig + ":RefLineAvgPos", avgpos)
        check_readback(prefixdig + ":RefLineAvgWid", avgwidth)

        for state in ["RESET", "INIT", "ON"]:
            assert change_state(prefixdig, state)

        # run a pulse
        sim_bp_trig(board, ssh)

#        assert caget_assert(prefixdig + ":CtrlIn-I") != 0 or \
#            caget_assert(prefixdig + ":CtrlIn-Q") != 0 or \
#            caget_assert(prefixdig + ":RefLine-I") != 0 or \
#            caget_assert(prefixdig + ":RefLine-Q") != 0
#
#        assert caget_assert(prefixdig + ":CtrlInMag") != 0 or \
#            caget_assert(prefixdig + ":CtrlInAng") != 0 or \
#            caget_assert(prefixdig + ":RefLineMag") != 0 or \
#            caget_assert(prefixdig + ":RefLineAng") != 0

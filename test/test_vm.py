#!/usr/bin/env python3
import pytest
from time import sleep
from random import randint, random, uniform

from helper import change_state, check_readback, float_to_fixed, fixed_to_float,\
                   read_reg, check_readback_float
from helper import caget_assert, caput_assert

from test_pi_magang_tables import prepare_calibration


# Limits for set magnitude limit
RANGE_OUT_MAG_LIM_CALIB = (50, 200)
RANGE_OUT_MAG_LIM_NO_CALIB = (0, 0.9999)

# Percentage differences between readback and magnitude limit
OUT_MAG_LIM_CALIB_DIFF = 0.3
OUT_MAG_LIM_NO_CALIB_DIFF = 0.01


def get_rb_error(val_set, val_rb):
    # Get approx error between value set and read
    return abs(val_set - val_rb) / val_set


class TestVM:
    def test_bo_readbacks(self, board, prefixdig):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        assert check_readback(prefixdig + ":OutInvIEn", 1)
        assert check_readback(prefixdig + ":OutInvIEn", 0)

        assert check_readback(prefixdig + ":OutInvQEn", 1)
        assert check_readback(prefixdig + ":OutInvQEn", 0)

        assert check_readback(prefixdig + ":OutSwapIQEn", 1)
        assert check_readback(prefixdig + ":OutSwapIQEn", 0)

        assert check_readback(prefixdig + ":OutMagLimEn", 1)
        assert check_readback(prefixdig + ":OutMagLimEn", 0)

        assert check_readback(prefixdig + ":VMPredistEn", 1)
        assert check_readback(prefixdig + ":VMPredistEn", 0)

    def test_ao_readbacks(self, board, prefixdig):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)


        assert check_readback_float(prefixdig + ":OutMagLim", 16, 16, 0)
        assert check_readback_float(prefixdig + ":VMPredistDCOff-I", 1, 15, 1)
        assert check_readback_float(prefixdig + ":VMPredistDCOff-Q", 1, 15, 1)
        assert check_readback_float(prefixdig + ":VMPredistRC00", 2, 14, 1)
        assert check_readback_float(prefixdig + ":VMPredistRC01", 2, 14, 1)
        assert check_readback_float(prefixdig + ":VMPredistRC10", 2, 14, 1)
        assert check_readback_float(prefixdig + ":VMPredistRC11", 2, 14, 1)


    def test_output_en(self, board, prefixdig, prefixrtm, ssh):
        # Check current main/secondary status of board for later reversion if required
        latchSecondary = 0
        main = caget_assert(prefixdig + ":Main-RB")
        if main == 0:
            latchSecondary = 1
            caput_assert(prefixdig + ":Main.DISP", 0)
            caput_assert(prefixdig + ":Main", 1)
        res = caput_assert(prefixrtm + ":RFOutEn", 1)
        assert res == 1
        reg = read_reg(board, "0x12F", ssh)
        assert reg == "0x700"

        # Revert board to its original "main/secondary" status
        if latchSecondary:
             caput_assert(prefixdig + ":Main", 0)
             caput_assert(prefixdig + ":Main.DISP", 1)
             latchSecondary = 0

    def test_output_dis(self, board, prefixdig, prefixrtm, ssh):
        # Check current main/secondary status of board for later reversion if required
        latchSecondary = 0
        main = caget_assert(prefixdig + ":Main-RB")
        if main == 0:
            latchSecondary = 1
            caput_assert(prefixdig + ":Main.DISP", 0)
            caput_assert(prefixdig + ":Main", 1)
        res = caput_assert(prefixrtm + ":RFOutEn", 0)
        assert res == 1
        reg = read_reg(board, "0x12F", ssh)
        assert reg == "0x600"

        # Revert board to its original "main/secondary" status
        if latchSecondary:
             caput_assert(prefixdig + ":Main", 0)
             caput_assert(prefixdig + ":Main.DISP", 1)
             latchSecondary = 0

    @pytest.mark.parametrize("pitype", ["FF"])
    @pytest.mark.parametrize("order", [1, 2, 3])
    def test_out_mag_limit_calibrated(self, prefixdig, pitype, order):
        prepare_calibration(prefixdig, pitype, order=1)
        
        sleep(0.1)
        caput_assert(prefixdig+":RFCtrl" + pitype + "-CalEn", 1)
        sleep(0.1)
        
        out_mag_lim = randint(*RANGE_OUT_MAG_LIM_CALIB)
        caput_assert(prefixdig+":OutMagLim", out_mag_lim)
        sleep(0.1)
        out_mag_limit_rb = caget_assert(prefixdig + ":OutMagLim-RB")
        assert get_rb_error(out_mag_lim, out_mag_limit_rb) < OUT_MAG_LIM_CALIB_DIFF


    @pytest.mark.parametrize("pitype", ["FF"])
    def test_out_mag_limit_not_calibrated(self, prefixdig, pitype):
        caput_assert(prefixdig+":RFCtrl" + pitype + "-CalEn", 0)
        sleep(0.1)
        
        out_mag_lim = uniform(*RANGE_OUT_MAG_LIM_NO_CALIB)
        caput_assert(prefixdig+":OutMagLim", out_mag_lim)
        sleep(0.1)
        out_mag_limit_rb = caget_assert(prefixdig + ":OutMagLim-RB")
        print("RB VAR ", prefixdig + ":OutMagLim-RB")
        print("RB VAL ", out_mag_limit_rb)
        assert get_rb_error(out_mag_lim, out_mag_limit_rb) < OUT_MAG_LIM_NO_CALIB_DIFF

